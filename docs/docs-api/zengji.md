---
title: 曾记 v1.0.0
language_tabs:
  - shell: Shell
  - http: HTTP
  - javascript: JavaScript
  - ruby: Ruby
  - python: Python
  - php: PHP
  - java: Java
  - go: Go
toc_footers: []
includes: []
search: true
highlight_theme: darkula
headingLevel: 2

---

# 曾记

> v1.0.0

# 视频模板

## GET 首页-home-视频分类

GET /view/index/videoType

> 返回示例

> 成功

```json
"{\n    \"code\": 200,\n    \"msg\": \"视频分类ok\",\n    \"data\": [\n        {\n            \"id\": 4, // id\n            \"name\": \"推荐视频\"  // 标题\n        }\n    ]\n}"
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 视频详情

GET /video/index/newList

视频详情，需要传递视频id，有两种情况
一： 对应的为商品
二： 为活动

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|id|query|string|true|none|
|limit|query|string|false|数据2条|
|AUTHORIZATION|header|string|true|token值|

> 返回示例

> 成功

```json
"{\n    \"code\": 200,\n    \"msg\": \"视频详情ok\",\n    \"data\": {\n        \"id\": 3,  // 视频id\n        \"uid\": 1, // 用户id\n        \"type\": 2,  // 类型 1为商品 2 为活动\n        \"relation_id\": 1, // 关联的商品或者活动id\n        \"title\": \"买买买\", // 标题\n        \"cover\": \"http://admin.miaommei.com/uploads/video_img/20200713/a9800df982247ef0f3b71cb4162b394c.jpg\", // 视频图片\n        \"video\": \"http://admin.miaommei.com/uploads/video/20200713/0563f89331c15826d19523ef11e200e6.mp4\", // 视频\n        \"likes\": 10, //点赞数\n        \"shares\": 55, // 分享数\n        \"is_likes\": false, // 喜欢或者 不喜欢\n        \"is_follow\": false, // 是否关注视频发布者\n        \"comments\": 0, // 评论数\n        \"add_time\": 1594639173, // 添加视频时间\n        \"activity\": [  // 活动信息\n            {\n                \"id\": 1,  // 活动id\n                \"name\": \"测试活动\", // 活动名称\n                \"cover\": \"http://admin.miaommei.com/uploads/activity/20200618/7e2a2279927510bb24864d3f78f6bf86.jpg\", // 图片\n                \"hours\": 1594726271, // 过期时间\n                \"title\": \"测试活动分享\",\n                \"share\": \"http://admin.miaommei.com/uploads/activity/20200618/e6a21b98c62bc78e50965de2ad80a2a8.jpg\", // 分享图\n                \"num\": 3, \n                \"content\": [ // 开团赚  第一个人是3  ，第二个是6 ，第三个是9\n                    \"3\",\n                    \"6\",\n                    \"9\"\n                ]\n            }\n        ]\n    }\n}"
```

```json
"{\n    \"code\": 200,\n    \"msg\": \"视频详情ok\",\n    \"data\": {\n        \"id\": 3,\n        \"uid\": 1,\n        \"type\": 1,\n        \"relation_id\": 1,\n        \"title\": \"买买买\",\n        \"cover\": \"http://admin.miaommei.com/uploads/video_img/20200713/a9800df982247ef0f3b71cb4162b394c.jpg\",\n        \"video\": \"http://admin.miaommei.com/uploads/video/20200713/0563f89331c15826d19523ef11e200e6.mp4\",\n        \"likes\": 10,\n        \"shares\": 55,\n        \"comments\": 0,\n        \"add_time\": 1594639173,\n        \"goods\": [  // 商品详情 id\n            {\n                \"id\": 1,  // 商品id\n                \"title\": \"测试商品1\", // 商品标题\n                \"name\": \"测试商品1\", // 商品名称\n                \"cover\": [ // 轮播图\n                    \"http://admin.miaommei.com/uploads/goods/20200611/3bc32d061cdedb6eddc1c8ac7120b893.jpg\", \n                    \"http://admin.miaommei.com/uploads/goods/20200611/3817b9b6d21e27091025c725f0cf3d1e.jpg\"\n                ],\n                \"share\": \"http://admin.miaommei.com/uploads/goods_share/20200611/95d28b7ab959cb75a94449a4cf02f50d.jpg\", // 分享图\n                \"default_price\": \"0.00\", // 商品价格\n                \"default_discount_price\": \"0.00\" // 商品折扣价\n            }\n        ]\n    }\n}"
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 视频评论列表二级评论

POST /video/comment/getChildList

将一级评论的id传递过来fid=1

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|fid|query|string|true|一级评论的id|
|page|query|string|false|第几页 ，默认是第一页|
|limit|query|string|false|拿取几条记录，默认3条|
|qc|query|string|false|去重的id  必须使用英文逗号隔开如：4,7  或者  4|
|Authorization|header|string|false|none|

> 返回示例

> 成功

```json
"{\n    \"code\": 200,\n    \"msg\": \"二级评论ok\",\n    \"data\": {\n        \"total\": 3, // 总记录\n        \"per_page\": 10,\n        \"current_page\": 1,\n        \"last_page\": 1,\n        \"data\": [\n            {\n                \"id\": 8, // id\n                \"uid\": 58, \n                \"upid\": 58, \n                \"content\": \"哎哎哎\", // 评论信息\n                \"reply\": null, // 回复内容\n                \"add_time\": 1594265027, // 添加时间  \n                \"reply_time\": 0, // 回复时间 需要if\n                \"is_delete\": 0, \n                \"fid\": 1, // 一级评论id\n                \"user_nickname\": \"测试58\", // 评论人昵称\n                \"user_avatarurl\": \"http://admin.miaommei.com/uploads/user/20200708/ada9cec5ffd3a6c4e5856a37f4ad2bf8.jpg\", // 评论人头像地址\n                \"p_user_nickname\": \"测试58\", // @人 昵称  需要if\n                \"p_user_avatarurl\": \"http://admin.miaommei.com/uploads/user/20200708/ada9cec5ffd3a6c4e5856a37f4ad2bf8.jpg\" // @人 头像地址  需要if\n            }\n        ]\n    }\n}"
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 视频分享

GET /video/index/shareVideo

用户点击视频分享 -视频分享数加1

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|id|query|string|true|视频id|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 视频列表

POST /video/index/list

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|type|query|string|true|分类type 值number类型|
|title|query|string|false|搜索值|
|page|query|string|true|分页|
|limit|query|string|false|拿取的数据量|
|is_hot|query|string|false|1表示推荐视频，首页  2 表示 不是推荐视频|
|isRefresh|query|string|true|是否刷新-随机打乱  true 表示 刷新随机打乱|

> 返回示例

> 成功

```json
"{\n    \"code\": 200,\n    \"msg\": \"视频分类ok11\",\n    \"data\": {\n        \"total\": 2,\n        \"per_page\": 2,\n        \"current_page\": 1,\n        \"last_page\": 1,\n        \"data\": [\n            {\n                \"id\": 1, // id\n                \"title\": \"测试视频\", // 标题\n                \"cover\": \"https://admin.miaommei.com/uploads/video_img/20200620/aeebb38477c9e450ea557004b569ca25.jpg\", // 图片\n                \"likes\": 0, // 点赞数\n                \"shares\": 0, // 分享树\n                \"comments\": 0, // 评论数\n                \"add_time\": 1592642186, // 添加时间\n                \"user_nickname\": \"风再起时\", // 用户昵称\n                \"user_avatarurl\": \"https://admin.miaommei.com/uploads/user/20200620/fb4a6a52568e28316c95292a96b51556.jpg\" // 用户头像地址\n                \"position\": ''\n                \n            }\n        ]\n    }\n}\n"
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 视频详情-点击喜欢或者取消

GET /user/video/videoChecklike

视频详情点击喜欢或者取消

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|vid|query|string|true|视频id|
|like|query|string|true|1表示喜欢  2 表示  取消喜欢|
|vid22|query|string|false|none|
|AUTHORIZATION|header|string|true|用户的tokne|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 视频切换操作-上一条和下一条

POST /video/index/getSlide

视频上一条或者下一条切换操作

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|id|query|string|true|视频id值|
|order|query|string|true|大于或者 小于符号 >  <|
|AUTHORIZATION|header|string|true|none|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 视频评论列表

POST /video/comment/list

视频评论列表

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|relation_id|query|string|true|视频的id，必须传递|
|limit|query|string|false|拿取数量|
|page|query|string|false|页数|
|Authorization|header|string|false|none|

> 返回示例

> 成功

```json
"{\n    \"code\": 200,\n    \"msg\": \"视频评论 ok11\",\n    \"data\": {\n        \"total\": 3,\n        \"per_page\": 10,\n        \"current_page\": 1,\n        \"last_page\": 1,\n        \"data\": [\n            {\n                \"id\": 8, // 视频id\n                \"uid\": 58,  \n                \"content\": \"12123123da\", // 评论内容\n                \"add_time\": 1594210153, // 视频时间\n                \"child_count\": 1, // 下级评论数\n                \"user_nickname\": \"测试58\", // 评论用户\n                \"user_avatarurl\": \"http://admin.miaommei.com/uploads/user/20200708/ada9cec5ffd3a6c4e5856a37f4ad2bf8.jpg\", // 用户头像地址\n                \"child_comment\": { // 下级评论列表 需要if\n                    \"id\": 6,\n                    \"uid\": 58, \n                    \"upid\": 58,\n                    \"content\": \"123546546\", // 评论内容\n                    \"reply\": null,   // 回复内容\n                    \"add_time\": 1594210391, // 评论时间\n                    \"reply_time\": 0, // 回复时间 \n                    \"is_delete\": 0,\n                    \"fid\": 8,\n                    \"user_nickname\": \"测试58\", // 评论用户\n                    \"user_avatarurl\": \"http://admin.miaommei.com/uploads/user/20200708/ada9cec5ffd3a6c4e5856a37f4ad2bf8.jpg\", // 头像地址\n                    \"p_user_nickname\": \"测试58\", // @用户  可以自己@自己回复\n                    \"p_user_avatarurl\": \"http://admin.miaommei.com/uploads/user/20200708/ada9cec5ffd3a6c4e5856a37f4ad2bf8.jpg\" // @谁的头像地址\n                }\n            },\n            {\n                \"id\": 7,\n                \"uid\": 58,\n                \"content\": \"12123123da\",\n                \"add_time\": 1594210146,\n                \"child_count\": 0,\n                \"user_nickname\": \"测试58\",\n                \"user_avatarurl\": \"http://admin.miaommei.com/uploads/user/20200708/ada9cec5ffd3a6c4e5856a37f4ad2bf8.jpg\"\n            },\n            {\n                \"id\": 6,\n                \"uid\": 58,\n                \"content\": \"12123123da\",\n                \"add_time\": 1594210144,\n                \"child_count\": 0,\n                \"user_nickname\": \"测试58\",\n                \"user_avatarurl\": \"http://admin.miaommei.com/uploads/user/20200708/ada9cec5ffd3a6c4e5856a37f4ad2bf8.jpg\"\n            }\n        ]\n    }\n}"
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 视频详情切换列表3条数据

GET /video/index/newGet

视频详情切换列表3条数据操作

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|order|query|string|false|请求的方向  <    >  表示 向上或者 向下拿取数据|
|id|query|string|false|id下面的数据|
|Authorization|header|string|false|none|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

# 订单

## GET 订单-取消订单

GET /order/index/cancel

订单-取消订单

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|id|query|string|true|订单id，必写|
|AUTHORIZATION|header|string|true|token值失效|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 订单列表

GET /order/index/list

订单列表-用户需要登录-需要携带token

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|status|query|string|true|0-已取消,1-待付款，2-待发货，3-待收货，4-已收货，5-已完成|
|AUTHORIZATION|header|string|true|用户token|

> 返回示例

> 成功

```json
"{\n    \"code\": 200,\n    \"msg\": \"订单列表ok\",\n    \"data\": {\n        \"total\": 2,\n        \"per_page\": 10,\n        \"current_page\": 1,\n        \"last_page\": 1,\n        \"data\": [\n            {\n                \"id\": 198, // 订单id\n                \"order_sn\": \"20072118015646380510\", // 订单号\n                \"total\": \"28.00\", // 支付金额\n                \"type\": 1, \n                \"add_time\": 1595325716, // 订单时间\n                \"is_appraise\": 0, // 显示 待评价:  is_appraise == 0 && status >= 4 \n                //  is_appraise == 1 百分百显示已评价\n                \"good_attr\": {\n                    \"id\": 7, // 商品规格id\n                    \"cost_price\": \"15.00\", \n                    \"price\": \"20.00\", // 商品价格\n                    \"discount_price\": \"18.00\", \n                    \"stock\": 100,\n                    \"live_price\": \"16.00\",\n                    \"integral\": 0,\n                    \"sale\": 0,\n                    \"add_time\": 1593513186,\n                    \"is_delete\": 0,\n                    \"valjson\": [],\n                    \"img\": \"\" // 图片\n                }\n            },\n            {\n                \"id\": 5,\n                \"order_sn\": \"20200616092850\",\n                \"total\": \"370.00\",\n                \"type\": 1,\n                \"add_time\": 1592270920\n            }\n        ]\n    }\n}"
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 删除订单ok

GET /order/index/del

删除订单ok

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|id|query|string|true|订单id号码|
|AUTHORIZATION|header|string|true|删除订单ok|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 订单-付款

GET /order/index/payment

订单付款

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|id|query|string|true|订单号 |
|AUTHORIZATION|header|string|false|none|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 订单-申请售后

GET /service/index/apply

订单-申请售后处理

> Body 请求参数

```yaml
type: object
properties:
  ddddd:
    type: string
    example: 12123ads123asd
required:
  - ddddd

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|oid|query|string|true|订单id|
|type|query|string|true|1换货，2退货退款|
|cover|query|string|true|上传的图片，需要使用英文逗号切割|
|price|query|string|true|用户填写的申请退款金额|
|apply_reason|query|string|true|用户申请理由|
|order_goods_id|query|string|true|订单商品-id|
|good_id|query|string|true|商品-id|
|num|query|string|true|申请数量|
|AUTHORIZATION|header|string|true|none|
|body|body|object|false|none|
|» ddddd|body|string|true|none|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 订单详情

GET /order/index/get

用户需要传递token

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|id|query|string|true|订单id|
|AUTHORIZATION|header|string|true|none|

> 返回示例

> 成功

```json
"{\n    \"code\": 200,\n    \"msg\": \"查看订单详情\",\n    \"data\": {\n        \"order_sn\": \"20072120334132626870\", // 订单号\n        \"status\": 1,//订单状态，0-已取消,1-待付款，2-待发货，3-待收货，4-已收货，5-已完成'\n        \"add_time\": 1595334821, // 下单时间\n        \"total\": \"28.00\", // 支付金额\n        \"price\": \"20.00\", // 商品金额\n        \"purchase_money\": \"\",// 自购省金额\n        \"volume\": \"0.00\", // 购物券金额\n        \"coupon_money\": 0, // 优惠券金额\n        \"freight\": \"10.00\", // 运费\n        \"is_service\":0, // 0 表示可以 1 申请售后  0 不可以申请售后\n        \"purchase_money\": \"2.00\", // 自购省\n        \"remarks\": \"\",//用户下单时备注的信息\n        \"deliver\": \"快递配送\",// 配送方式  直接渲染\n        \"order_goods_id\": 1, // 订单商品id\n        \"address\": {\n            \"id\": 1,\n            \"uid\": 1,\n            \"address_str\": \"广东省 广州市 白云区 元下田美食广场\", // 地址详情\n            \"tel\": \"13800000001\" // 手机号码\n        },\n        \"good_list\":[ // 商品数组\n            {\n                \n            }\n        ],\n        //\"express_desc\": \"\", // 物流信息  默认 没有  需要if\n        \"express_desc\": [\n            \"upd_time\": '', // 更新时间\n            \"desc\": '' // 描述信息\n        ]\n    }\n}"
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 订单-确认收货

GET /order/index/receipt

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|id|query|string|true|订单确认收货|
|AUTHORIZATION|header|string|true|none|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

# 售后模块

## GET 售后详情

GET /service/index/get

售后详情操作

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|id|query|string|true|售后订单id|
|AUTHORIZATION|header|string|true|none|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "ok",
  "data": {
    "id": 2,
    "// 售后id \"service_sn\"": "86a64c6f2f09826e4df6aed04b29cd54-1595577133",
    "// 服务号 \"type\"": 1,
    "// 类型1  退款退货   2  换货 \"price\"": 20,
    "// 申请退款金额 \"refund\"": 0,
    "// 实际退款金额 \"status\"": 3,
    "// 状态 \"apply_reason\"": "效果不好/不喜欢",
    "// 申请理由 \"reply_reason\"": null,
    "// 回复理由 \"add_time\"": 1595577133,
    "// 添加时间 \"change_express_id\"": 3,
    "// 物流id  -  买家发货给商家的物流信息id \"merchant_express_id\"": 0,
    "// 物流id -  商家发货给买家的物流信息 \"num\"": 2,
    "// 数量 \"img\"": "",
    "// 图片 \"title\"": "快来购买",
    "// 标题 \"name\"": "测试222",
    "// 名称 \"goods_price\"": "20.00",
    "// 商品价格 \"oneChangeExpress\"": {
      "// 买家发货给商家的物流详情表 - 需要if \"id\"": 3,
      "company": 1,
      "// 需要调用物流公司接口  获取到全部的物流api \"express_sn\"": "121412ads12sd",
      "// 快递号-物流号 \"add_time\"": 1595603849,
      "// // 填写时间 \"desc\"": "null // 物流描述信息  需要if"
    },
    "oneMerchantExpress": null,
    "// // 商家发货给买家的物流详情表 - 需要if 和参数 oneChangeExpress 一样 \"address\"": [],
    "// 后台收货人信息": null
  }
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 售后列表

GET /service/index/list

售后列表

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|page|query|string|false|分页|
|limit|query|string|false|拿取的数据|
|AUTHORIZATION|header|string|true|必须|

> 返回示例

> 成功

```json
"{\n    \"code\": 200,\n    \"msg\": \"售后列表ok\",\n    \"data\": {\n        \"total\": 1, \n        \"per_page\": 3,\n        \"current_page\": 1,\n        \"last_page\": 1,\n        \"data\": [\n            {\n                \"id\": 2, // 售后id\n                \"order_id\": 176, // 订单id\n                \"order_orn\": \"\", // 订单号码\n                \"type\": 1, // 类型1换货      ，2 退货退款\n                \"price\": 20,// 退款金额\n                \"add_time\": 1595577133, // 添加时间\n                \"status\": 1, //  状态1-申请，2-(已审核)等待买家寄货，3-(买家已寄货)等待卖家(收或者发,退款表示收，换货表示发)货，4-(商家已收货)-（退款中或商家发货中），5-完成（退款ok），6-已拒绝\n                \"num\": 2, // 申请数量\n                \"img\": \"\", // 图片\n                \"title\": \"快来购买\", // 商品标题 -  长\n                \"name\": \"测试222\" // 商品名称 - 短\n            }\n        ]\n    }\n}"
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 售后-填写发货物流信息

GET /service/index/updateExchange

订单号get

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|id|query|string|true|售后id|
|company_id|query|string|true|物流公司-id|
|express_sn|query|string|true|快递单号|
|AUTHORIZATION|header|string|true|用户名token|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 物流公司名称

GET /service/index/logisticsList

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|AUTHORIZATION|query|string|false|121221|
|Authorization|header|string|false|token|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

# 用户视频-video

## POST 我的视频-评论

POST /user/video/myVideoComment

必传参数
Headers:Authorization
非必传
page：1 分页   默认1
limit ：15 每页展示数据数    默认15

> Body 请求参数

```yaml
type: object
properties:
  page:
    type: string
    description: 分页   默认1
    example: "1"
  limit:
    type: string
    description: 每页展示数据数    默认15
    example: "15"

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|
|» page|body|string|false|分页   默认1|
|» limit|body|string|false|每页展示数据数    默认15|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": {
    "total": 6,
    "per_page": 15,
    "current_page": 1,
    "last_page": 1,
    "data": [
      {
        "视频id\"vid\"": 1,
        "用户id\"id\"": 58,
        "视频图片\"cover\"": "/uploads/video_img/20200620/aeebb38477c9e450ea557004b569ca25.jpg",
        "视频地址\"video\"": "/uploads/video/20200620/a40f35dfd12b6a4d63e9c2bc68a9f854.mp4",
        "用户昵称\"nickname\"": "测试58",
        "用户头像\"avatarurl\"": "/uploads/user/20200708/ada9cec5ffd3a6c4e5856a37f4ad2bf8.jpg",
        "评论内容\"content\"": "测试测试",
        "时间\"add_time\"": "07-16"
      },
      {
        "vid": 1,
        "id": 58,
        "cover": "/uploads/video_img/20200620/aeebb38477c9e450ea557004b569ca25.jpg",
        "video": "/uploads/video/20200620/a40f35dfd12b6a4d63e9c2bc68a9f854.mp4",
        "nickname": "测试58",
        "avatarurl": "/uploads/user/20200708/ada9cec5ffd3a6c4e5856a37f4ad2bf8.jpg",
        "content": "嗷嗷嗷",
        "add_time": "07-16"
      },
      {
        "vid": 3,
        "id": 4,
        "cover": "/uploads/video_img/20200713/a9800df982247ef0f3b71cb4162b394c.jpg",
        "video": "/uploads/video/20200713/0563f89331c15826d19523ef11e200e6.mp4",
        "nickname": "测试4",
        "avatarurl": "https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLwFZscibqGJOjicqv3OINUEYY0CgVDfWPNicrtM5lW7BveTicK2iaj5OTFBML9IcibxJTgQb3rsI3h0KBA/132",
        "content": "12213baba4",
        "add_time": "07-13"
      },
      {
        "vid": 2,
        "id": 58,
        "cover": "/uploads/video_img/20200703/7383352b28fdc2d28f7bc6b5a545ae06.jpg",
        "video": "/uploads/video/20200714/b9fbf936e601434c16fa2d575f40999c.mp4",
        "nickname": "测试58",
        "avatarurl": "/uploads/user/20200708/ada9cec5ffd3a6c4e5856a37f4ad2bf8.jpg",
        "content": "12123babada3",
        "add_time": "07-08"
      },
      {
        "vid": 2,
        "id": 58,
        "cover": "/uploads/video_img/20200703/7383352b28fdc2d28f7bc6b5a545ae06.jpg",
        "video": "/uploads/video/20200714/b9fbf936e601434c16fa2d575f40999c.mp4",
        "nickname": "测试58",
        "avatarurl": "/uploads/user/20200708/ada9cec5ffd3a6c4e5856a37f4ad2bf8.jpg",
        "content": "12123babada2",
        "add_time": "07-08"
      },
      {
        "vid": 2,
        "id": 58,
        "cover": "/uploads/video_img/20200703/7383352b28fdc2d28f7bc6b5a545ae06.jpg",
        "video": "/uploads/video/20200714/b9fbf936e601434c16fa2d575f40999c.mp4",
        "nickname": "测试58",
        "avatarurl": "/uploads/user/20200708/ada9cec5ffd3a6c4e5856a37f4ad2bf8.jpg",
        "content": "12123babada1",
        "add_time": "07-08"
      }
    ]
  }
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 视频详情-添加二级评论

POST /user/video/addCommentChild%EF%BC%887/22%EF%BC%89

必传参数
Headers:Authorization
id： 视频的id
uid：被评论的用户id
content：视频的评论内容

> Body 请求参数

```yaml
type: object
properties:
  id:
    type: string
    description: 视频id
    example: "1"
  content:
    type: string
    description: 评论
    example: "1"
  uid:
    type: string
    description: 评论用户id，login接口里的主键id
    example: "55"

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|
|» id|body|string|false|视频id|
|» content|body|string|false|评论|
|» uid|body|string|false|评论用户id，login接口里的主键id|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 个人视频页

POST /user/video/videoHome

必传参数
Headers:Authorization
uid：用户id
非必传
page：1 分页   默认1
limit ：15 每页展示数据数    默认15

> Body 请求参数

```yaml
type: object
properties:
  page:
    type: string
    description: 分页   默认1
    example: "1"
  "limit ":
    type: string
    description: 每页展示数据数    默认15
    example: "15"
  uid:
    type: string
    description: 用户id
    example: "1"
required:
  - uid

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|
|» page|body|string|false|分页   默认1|
|» limit|body|string|false|每页展示数据数    默认15|
|» uid|body|string|true|用户id|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": {
    "user": {
      "id": 7,
      "用户昵称\"nickname\"": "风再起时",
      "用户头像\"avatarurl\"": ":///uploads/user/20200620/fb4a6a52568e28316c95292a96b51556.jpg"
    },
    "是否已关注\"is_follow\"": false,
    "粉丝数量\"fans_number\"": 0,
    "点赞数量\"likes_number\"": 0,
    "list": {
      "total": 1,
      "per_page": 15,
      "current_page": 1,
      "last_page": 1,
      "data": [
        {
          "id": 1,
          "视频标题\"title\"": "测试视频",
          "视频图片\"cover\"": ":///uploads/video_img/20200620/aeebb38477c9e450ea557004b569ca25.jpg",
          "视频地址\"video\"": ":///uploads/video/20200620/a40f35dfd12b6a4d63e9c2bc68a9f854.mp4",
          "视频点赞数\"likes\"": 0,
          "视频播放数\"views\"": 0,
          "时间\"add_time\"": "6月20日"
        }
      ]
    }
  }
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 我的视频-头部数据

POST /user/video/myVideo

必传参数
Headers:Authorization

> Body 请求参数

```yaml
type: object
properties: {}

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": {
    "我的作品\"my_video_number\"": 16,
    "点赞数\"my_like_number\"": "2354565",
    "我的喜欢\"video_follow_number\"": 1,
    "关注数\"follow_number\"": 19,
    "粉丝数\"fans_number\"": 2,
    "新评论\"new_comment\"": true
  }
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 视频-添加分享活动（7/23）

POST /user/video/getActivityLog

必传参数
Headers:Authorization
非必传
aid：活动id
title：搜索活动名

> Body 请求参数

```yaml
type: object
properties:
  aid:
    type: string
    description: 活动id
    example: "0"
  title:
    type: string
    description: 搜索活动名
    example: "0"

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|
|» aid|body|string|false|活动id|
|» title|body|string|false|搜索活动名|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": [
    {
      "活动id\"id\"": 1,
      "我的活动id\"log_id\"": 2,
      "活动名\"name\"": "测试活动",
      "活动图片\"cover\"": "/uploads/activity/20200618/7e2a2279927510bb24864d3f78f6bf86.jpg",
      "价格\"price\"": "60.00",
      "参与人\"participate_num\"": 0,
      "返-佣金\"share_commission\"": "6.00"
    },
    {
      "id": 1,
      "log_id": 1,
      "name": "测试活动",
      "cover": "/uploads/activity/20200618/7e2a2279927510bb24864d3f78f6bf86.jpg",
      "price": "60.00",
      "participate_num": 0,
      "share_commission": "6.00"
    }
  ]
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 新增视频观看记录（7/25）

POST /user/video/addWatchRecord

必传参数
Headers:Authorization
vid：视频id

> Body 请求参数

```yaml
type: object
properties:
  vid:
    type: string
    description: 视频id
    example: "1"

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|
|» vid|body|string|false|视频id|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": false
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 我的视频-作品

POST /user/video/myVideoList

必传参数
Headers:Authorization
非必传
page：1 分页   默认1
limit ：15 每页展示数据数    默认15

> Body 请求参数

```yaml
type: object
properties:
  page:
    type: string
    description: 分页   默认1
    example: "1"
  "limit ":
    type: string
    description: 每页展示数据数    默认15
    example: "15"

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|
|» page|body|string|false|分页   默认1|
|» limit|body|string|false|每页展示数据数    默认15|

> 返回示例

> 成功

```json
"{\n    \"code\": 200,\n    \"msg\": \"操作成功\",\n    \"data\": {\n        \"total\": 16,\n        \"per_page\": 15,\n        \"current_page\": 1,\n        \"last_page\": 2,\n        \"data\": [\n            {\n                \"id\": 2,\n                视频标题\"title\": \"测试\",\n                视频图片\"cover\": \":///uploads/video_img/20200703/7383352b28fdc2d28f7bc6b5a545ae06.jpg\",\n                视频地址\"video\": \":///uploads/video/20200714/b9fbf936e601434c16fa2d575f40999c.mp4\",\n                视频点赞数\"likes\": 111,\n                视频播放数\"views\": 0,\n                视频状态\"is_check\": \"已发布\",\n                时间\"add_time\": \"7月3日\"\n            },\n            /*是否通过审核，默认0审核中，1审核通过，2审核失败*/\n            {\n                \"id\": 3,\n                \"title\": \"买买买\",\n                \"cover\": \":///uploads/video_img/20200713/a9800df982247ef0f3b71cb4162b394c.jpg\",\n                \"video\": \":///uploads/video/20200713/0563f89331c15826d19523ef11e200e6.mp4\",\n                \"likes\": 10,\n                \"views\": 0,\n                \"is_check\": \"已发布\",\n                \"add_time\": \"7月13日\"\n            },\n            {\n                \"id\": 4,\n                \"title\": \"视频1\",\n                \"cover\": \":///uploads/video_img/20200714/70374364b511daeee7d5479c7e68b500.gif\",\n                \"video\": \":///uploads/video/20200716/eac7d308cb5157ace4297f9da36bdf07.mp4\",\n                \"likes\": 0,\n                \"views\": 0,\n                \"is_check\": \"已发布\",\n                \"add_time\": \"7月14日\"\n            },\n            {\n                \"id\": 5,\n                \"title\": \"视频2\",\n                \"cover\": \":///uploads/video_img/20200714/1b8f1684072242785bef5b641d8af1c2.gif\",\n                \"video\": \":///uploads/video/20200716/d5a41c99a7f86e96bf062be301c351fe.mp4\",\n                \"likes\": 0,\n                \"views\": 0,\n                \"is_check\": \"已发布\",\n                \"add_time\": \"7月14日\"\n            },\n            {\n                \"id\": 6,\n                \"title\": \"视频3\",\n                \"cover\": \":///uploads/video_img/20200714/2feda83a0b95380343924cb8a1e3d854.gif\",\n                \"video\": \":///uploads/video/20200716/cf22713a5dd038751624775b1546390a.mp4\",\n                \"likes\": 0,\n                \"views\": 0,\n                \"is_check\": \"已发布\",\n                \"add_time\": \"7月14日\"\n            },\n            {\n                \"id\": 7,\n                \"title\": \"视频4\",\n                \"cover\": \":///uploads/video_img/20200714/6df8fd13f16a1f96fd5f6f5d4d52d15d.gif\",\n                \"video\": \":///uploads/video/20200716/50e7a28b0493012cb9542a8f2a147421.mp4\",\n                \"likes\": 0,\n                \"views\": 0,\n                \"is_check\": \"已发布\",\n                \"add_time\": \"7月14日\"\n            },\n            {\n                \"id\": 8,\n                \"title\": \"视频5\",\n                \"cover\": \":///uploads/video_img/20200714/7de01c0c61267a06ce973a0c2e3a8743.gif\",\n                \"video\": \":///uploads/video/20200716/922c00e1a77a210271611b0cc7926880.mp4\",\n                \"likes\": 0,\n                \"views\": 0,\n                \"is_check\": \"已发布\",\n                \"add_time\": \"7月14日\"\n            },\n            {\n                \"id\": 9,\n                \"title\": \"视频6\",\n                \"cover\": \":///uploads/video_img/20200714/d63f8f2f1992fe4c3f3eafeef3d83d99.gif\",\n                \"video\": \":///uploads/video/20200716/59e179c1047360f54d68474af766d9e3.mp4\",\n                \"likes\": 0,\n                \"views\": 0,\n                \"is_check\": \"已发布\",\n                \"add_time\": \"7月14日\"\n            },\n            {\n                \"id\": 10,\n                \"title\": \"视频7\",\n                \"cover\": \":///uploads/video_img/20200714/a114d6df6d0828765dadc53fdea0ac5a.gif\",\n                \"video\": \":///uploads/video/20200716/b00b05935cea686737b109e46a4dc419.mp4\",\n                \"likes\": 0,\n                \"views\": 0,\n                \"is_check\": \"已发布\",\n                \"add_time\": \"7月14日\"\n            },\n            {\n                \"id\": 11,\n                \"title\": \"视频8\",\n                \"cover\": \":///uploads/video_img/20200714/49894081a45acfc3176004d4d0edd99a.gif\",\n                \"video\": \":///uploads/video/20200716/53e10c9680146d1a791bb27426f1304b.mp4\",\n                \"likes\": 0,\n                \"views\": 0,\n                \"is_check\": \"已发布\",\n                \"add_time\": \"7月14日\"\n            },\n            {\n                \"id\": 12,\n                \"title\": \"视频9\",\n                \"cover\": \":///uploads/video_img/20200714/83b141727260b96427577eb9c8b07d22.gif\",\n                \"video\": \":///uploads/video/20200716/a8399c1b98791f0663150201495431cf.mp4\",\n                \"likes\": 0,\n                \"views\": 0,\n                \"is_check\": \"已发布\",\n                \"add_time\": \"7月14日\"\n            },\n            {\n                \"id\": 13,\n                \"title\": \"视频10\",\n                \"cover\": \":///uploads/video_img/20200714/9ab320d4b442d18db04c5ed17c217d67.gif\",\n                \"video\": \":///uploads/video/20200716/46fe3087c97332588596f7d793821c45.mp4\",\n                \"likes\": 0,\n                \"views\": 0,\n                \"is_check\": \"已发布\",\n                \"add_time\": \"7月14日\"\n            },\n            {\n                \"id\": 14,\n                \"title\": \"测试\",\n                \"cover\": \":///uploads/video_img/20200714/c4d132f8e6eb795e41ecf2b799fcd350.jpg\",\n                \"video\": \":///uploads/video/20200714/c4d132f8e6eb795e41ecf2b799fcd350.mp4\",\n                \"likes\": 1123213,\n                \"views\": 0,\n                \"is_check\": \"已发布\",\n                \"add_time\": \"7月14日\"\n            },\n            {\n                \"id\": 15,\n                \"title\": \"1\",\n                \"cover\": \":///uploads/video_img/20200714/ca207ba6b3ba85b0152ff2aca98c9e4b.png\",\n                \"video\": \":///uploads/video/20200714/ca207ba6b3ba85b0152ff2aca98c9e4b.mp4\",\n                \"likes\": 0,\n                \"views\": 0,\n                \"is_check\": \"已发布\",\n                \"add_time\": \"7月14日\"\n            },\n            {\n                \"id\": 16,\n                \"title\": \"2\",\n                \"cover\": \":///uploads/video_img/20200714/4a5d2a32f0c1d6843e918bbed9ceac58.png\",\n                \"video\": \":///uploads/video/20200714/3aebe31cf93d5777553627ad1276fa76.mp4\",\n                \"likes\": 0,\n                \"views\": 0,\n                \"is_check\": \"已发布\",\n                \"add_time\": \"7月14日\"\n            }\n        ]\n    }\n}"
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 我的视频-删除

POST /user/video/delVideo

必传参数
Headers:Authorization
vid：视频id

> Body 请求参数

```yaml
type: object
properties:
  vid:
    type: string
    description: 视频id
    example: "17"
required:
  - vid

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|
|» vid|body|string|true|视频id|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": true
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 视频-上传视频文件（7/22）

POST /user/video/publishVideo

必传参数
Headers:Authorization
video：视频文件键名
type：video   上传文件类型

> Body 请求参数

```yaml
type: object
properties:
  type:
    type: string
    description: 上传文件类型
    example: video
required:
  - type

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|
|» type|body|string|true|上传文件类型|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 发布视频-视频分类（7/18）

POST /user/video/getType

必传参数
Headers:Authorization

> Body 请求参数

```yaml
type: object
properties: {}

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": [
    {
      "id": 2,
      "pid": 0,
      "分类名\"name\"": "美甲美睫",
      "二级分类列表\"list\"": []
    },
    {
      "id": 3,
      "pid": 0,
      "name": "美食时尚",
      "list": []
    },
    {
      "id": 4,
      "pid": 0,
      "name": "美食",
      "list": [
        {
          "id": 7,
          "pid": 2,
          "name": "美容美发"
        },
        {
          "id": 8,
          "pid": 2,
          "name": "瑜伽健身"
        }
      ]
    },
    {
      "id": 5,
      "pid": 0,
      "name": "附近",
      "list": []
    },
    {
      "id": 6,
      "pid": 0,
      "name": "医美知识",
      "list": []
    }
  ]
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 视频-添加分享商品（7/22）

POST /user/video/getGoods

必传参数
Headers:Authorization
非必传
gid：如果已选择商品重新选择时把已选择的商品id传过来
title：搜索商品标题
hot：1-精选商品，4个排序都不传则默认就是精选
sales：1-销量降序 2-销量升序
commission：1-佣金降序 2-佣金升序
price：1-价格降序 2-价格升序

> Body 请求参数

```yaml
type: object
properties:
  gid:
    type: string
    description: 如果已选择商品重新选择时把已选择的商品id传过来
    example: "0"
  title:
    type: string
    description: 搜索商品标题
  hot:
    type: string
    description: 1-精选商品，4个排序都不传则默认就是精选
    example: "0"
  sales:
    type: string
    description: 1-销量降序 2-销量升序
    example: "0"
  commission:
    type: string
    description: 1-佣金降序 2-佣金升序
    example: "0"
  price:
    type: string
    description: 1-价格降序 2-价格升序
    example: "0"
  page:
    type: string
    description: 分页
    example: "1"
  limit:
    type: string
    example: "15"

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|
|» gid|body|string|false|如果已选择商品重新选择时把已选择的商品id传过来|
|» title|body|string|false|搜索商品标题|
|» hot|body|string|false|1-精选商品，4个排序都不传则默认就是精选|
|» sales|body|string|false|1-销量降序 2-销量升序|
|» commission|body|string|false|1-佣金降序 2-佣金升序|
|» price|body|string|false|1-价格降序 2-价格升序|
|» page|body|string|false|分页|
|» limit|body|string|false|none|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": {
    "total": 21,
    "per_page": 15,
    "current_page": 1,
    "last_page": 2,
    "data": [
      {
        "id": 20,
        "分享标题\"title\"": "测试",
        "分享图片\"share\"": ":///uploads/goods_share/20200702/d83901219850a1d4b71d19a2481fc334.jpg",
        "默认价格\"default_price\"": "0.00",
        "划线价格\"default_discount_price\"": "0.00",
        "是否热门\"is_hot\"": 0,
        "销量\"sales_volume\"": 0,
        "返-佣金\"share_commission\"": "0.00",
        "是否选中这个商品\"select\"": 0
      },
      {
        "id": 23,
        "title": "面膜",
        "share": ":///uploads/goods_share/20200710/df798af92b54619efeefe250f18853be.jpg",
        "default_price": "0.00",
        "default_discount_price": "0.79",
        "is_hot": 0,
        "sales_volume": 1000,
        "share_commission": "0.00",
        "select": 0
      },
      {
        "id": 25,
        "title": "面膜",
        "share": ":///uploads/goods_share/20200710/df798af92b54619efeefe250f18853be.jpg",
        "default_price": "0.00",
        "default_discount_price": "0.79",
        "is_hot": 0,
        "sales_volume": 1000,
        "share_commission": "0.00",
        "select": 0
      },
      {
        "id": 29,
        "title": "面膜",
        "share": ":///uploads/goods_share/20200710/df798af92b54619efeefe250f18853be.jpg",
        "default_price": "0.00",
        "default_discount_price": "0.79",
        "is_hot": 0,
        "sales_volume": 1000,
        "share_commission": "0.00",
        "select": 0
      },
      {
        "id": 31,
        "title": "面膜",
        "share": ":///uploads/goods_share/20200710/df798af92b54619efeefe250f18853be.jpg",
        "default_price": "0.00",
        "default_discount_price": "0.79",
        "is_hot": 0,
        "sales_volume": 1000,
        "share_commission": "0.00",
        "select": 0
      },
      {
        "id": 32,
        "title": "面膜",
        "share": ":///uploads/goods_share/20200710/df798af92b54619efeefe250f18853be.jpg",
        "default_price": "0.00",
        "default_discount_price": "0.79",
        "is_hot": 0,
        "sales_volume": 1000,
        "share_commission": "0.00",
        "select": 0
      }
    ]
  }
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 视频详情-添加一级评论（7/22）

POST /user/video/addComment

必传参数
Headers:Authorization
id： 视频的id
content：视频的评论内容

> Body 请求参数

```yaml
type: object
properties:
  id:
    type: string
    description: 视频id
    example: "1"
  content:
    type: string
    description: 评论
    example: "1"

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|
|» id|body|string|false|视频id|
|» content|body|string|false|评论|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 我的视频-喜欢

POST /user/video/myLikeVideo

必传参数
Headers:Authorization
非必传
page：1 分页   默认1
limit ：15 每页展示数据数    默认15

> Body 请求参数

```yaml
type: object
properties:
  page:
    type: string
    description: 分页   默认1
    example: "1"
  limit:
    type: string
    description: 每页展示数据数    默认15
    example: "15"

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|
|» page|body|string|false|分页   默认1|
|» limit|body|string|false|每页展示数据数    默认15|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": {
    "total": 1,
    "per_page": 15,
    "current_page": 1,
    "last_page": 1,
    "data": [
      {
        "视频id\"vid\"": 1,
        "用户id\"uid\"": 7,
        "用户昵称\"nickname\"": "风再起时",
        "用户头像\"avatarurl\"": "/uploads/user/20200620/fb4a6a52568e28316c95292a96b51556.jpg",
        "视频标题\"title\"": "测试视频",
        "视频图片\"cover\"": "/uploads/video_img/20200620/aeebb38477c9e450ea557004b569ca25.jpg",
        "视频地址\"video\"": "/uploads/video/20200620/a40f35dfd12b6a4d63e9c2bc68a9f854.mp4",
        "视频点赞数\"likes\"": 0,
        "时间\"add_time\"": "06-20"
      }
    ]
  }
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 视频-上传图片文件（7/22）

POST /user/video/publishVideoImg

必传参数
Headers:Authorization
img：视频文件键名
id：视频记录id
type：image   上传文件类型

> Body 请求参数

```yaml
type: object
properties:
  type:
    type: string
    description: 上传文件类型
    example: image
  id:
    type: string
    description: 视频记录id
    example: "1"
required:
  - type

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|
|» type|body|string|true|上传文件类型|
|» id|body|string|false|视频记录id|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 删除视频文件（7/22）

POST /user/video/delVideoFile

必传参数
Headers:Authorization
vid：视频id

> Body 请求参数

```yaml
type: object
properties:
  vid:
    type: string
    description: 视频id
    example: "30"
required:
  - vid

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|
|» vid|body|string|true|视频id|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 发布视频-提交发布（7/23）

POST /user/video/publishVideoLog

必传参数
Headers:Authorization
id：上传视频返回的id
title：视频标题
type_id：选择视频分类的id
type：1-商品 2-视频
aid：活动id
log_id：活动log_id
gid：商品id

> Body 请求参数

```yaml
type: object
properties:
  id:
    type: string
    description: 上传视频返回的id
    example: "0"
  title:
    type: string
    description: 视频标题
    example: "0"
  type_id:
    type: string
    description: 选择视频分类的id
    example: "0"
  type:
    type: string
    description: 1-商品 2-视频
    example: "0"
  aid:
    type: string
    description: 活动id
    example: "0"
  log_id:
    type: string
    description: 活动log_id
    example: "0"
  gid:
    type: string
    description: 商品id
    example: "0"
  position:
    type: string
    description: 位置信息
    example: "0"

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|
|» id|body|string|false|上传视频返回的id|
|» title|body|string|false|视频标题|
|» type_id|body|string|false|选择视频分类的id|
|» type|body|string|false|1-商品 2-视频|
|» aid|body|string|false|活动id|
|» log_id|body|string|false|活动log_id|
|» gid|body|string|false|商品id|
|» position|body|string|false|位置信息|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

# 直播

## POST 上传图片

POST /live/index/uploadImg

必要参数：
头部 Authorization：token
图片上传成功后返回key，创建直播间时只需要带上key

> Body 请求参数

```yaml
type: object
properties:
  img:
    type: string
    description: 图片文件
    example: 图片文件
    format: binary
  type:
    type: string
    description: 文件类型
    example: image
  uptype:
    type: string
    description: 上传图片类别：fm-卡片分享图，bj-背景图
    example: fm
  key:
    type: string
    description: 首次上传不用填（首次上传成功后会返回key），fm，bj 分2次传，第2次带上key
    example: "0"

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|AUTHORIZATION|header|string|true|none|
|body|body|object|false|none|
|» img|body|string(binary)|false|图片文件|
|» type|body|string|false|文件类型|
|» uptype|body|string|false|上传图片类别：fm-卡片分享图，bj-背景图|
|» key|body|string|false|首次上传不用填（首次上传成功后会返回key），fm，bj 分2次传，第2次带上key|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": [
    {
      "img": "https://tapi.miaommei.com/uploads/live_goods_img/202009/20200902112707_4033.jpeg",
      "mid": "BjszWh4hM0FEWZzqiWYfCtIeXaEyiGCFJHbfb9bEJZi88glMHDqnmqgBtQH5ZLUd",
      "key": "1"
    }
  ]
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 直播列表

POST /live/index/getList

必要参数：
头部 Authorization：token

> Body 请求参数

```yaml
type: object
properties:
  title:
    type: string
    description: 直播标题/主播昵称（用户昵称）
    example: 测试
  type:
    type: string
    description: 直播分类
    example: "1"
  limit:
    type: string
    description: 分页条数
    example: "15"
  page:
    type: string
    description: 页码
    example: "1"

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|AUTHORIZATION|header|string|true|none|
|body|body|object|false|none|
|» title|body|string|false|直播标题/主播昵称（用户昵称）|
|» type|body|string|false|直播分类|
|» limit|body|string|false|分页条数|
|» page|body|string|false|页码|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": {
    "total": 1,
    "per_page": 15,
    "current_page": 1,
    "last_page": 1,
    "data": [
      {
        "id": 13,
        "uid": 4033,
        "微信直播间id\"room_id\"": 11,
        "直播标题\"title\"": "带货哈哈哈",
        "直播昵称\"nickname\"": "黄灿",
        "头像\"avatarurl\"": "https://thirdwx.qlogo.cn/mmopen/vi_32/JrRM5l97CxDXiaKWn0X6l7oZZkG5PFGSola2CicZiamFiaHysmhxfSgMhV97c8gyQbnb6HnptmhtHEDS7qPlhDJecQ/132",
        "分享图片\"share\"": "https://tapi.miaommei.com黄灿",
        "直播类型\"type\"": 1,
        "//1带货，2普通直播 直播分类id\"type_id\"": 1,
        "直播回放地址\"playback_url\"": "",
        "直播开始时间\"start_time\"": 1599035400,
        "直播结束时间\"end_time\"": 1599039000
      }
    ]
  }
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 直播商品列表

POST /live/index/getGoodsList

必要参数：
头部 Authorization：token

> Body 请求参数

```yaml
type: object
properties:
  start:
    type: string
    description: 直播时间 开始
    example: 10:00
  end:
    type: string
    description: 直播时间 结束
    example: 11:00
  name:
    type: string
    description: 搜索条件：选填
    example: 商品昵称
  orderby:
    type: string
    description: 排序：1热门，2佣金，3价格
    example: "1"
  desc:
    type: string
    description: 排序方式：desc降序，asc升序  默认desc
    example: desc

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|AUTHORIZATION|header|string|true|none|
|body|body|object|false|none|
|» start|body|string|false|直播时间 开始|
|» end|body|string|false|直播时间 结束|
|» name|body|string|false|搜索条件：选填|
|» orderby|body|string|false|排序：1热门，2佣金，3价格|
|» desc|body|string|false|排序方式：desc降序，asc升序  默认desc|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": {
    "total": 4,
    "per_page": 15,
    "current_page": 1,
    "last_page": 1,
    "data": [
      {
        "id": 2,
        "商品名称\"name\"": "😂😂正式商品2",
        "商品图片\"cover\"": "https://admin-miaomeimei-1301812909.cos.ap-guangzhou.myqcloud.com/newUploads/lives/goodsImg/2020_09_02/1599026054_654.jpg",
        "商品id\"goods_id\"": 234,
        "佣金\"commission\"": "0.010",
        "priceType": 3,
        "原价\"price\"": 69,
        "折扣价\"price2\"": 52,
        "状态\"status\"": "2 //1为可选，2为不可选（该时间段已被选）"
      },
      {
        "id": 3,
        "name": "商品21😊😊😊",
        "cover": "https://tapi.miaommei.com/newUploads/lives/goodsImg/20200901/e1b7e1310177e970d49e749c8f6ba252.jpg",
        "goods_id": 158,
        "commission": "0.200",
        "priceType": 3,
        "price": 90,
        "price2": 60,
        "status": 2
      },
      {
        "id": 4,
        "name": "😂😂笑死你",
        "cover": "https://tapi.miaommei.com/newUploads/lives/goodsImg/20200902/ca6f6b0b83f3e096bb44c9344b9bcae4.jpg",
        "goods_id": 161,
        "commission": "0.020",
        "priceType": 3,
        "price": 60,
        "price2": 50,
        "status": 1
      },
      {
        "id": 5,
        "name": "哈哈贼福😃😃😃",
        "cover": "https://admin-miaomeimei-1301812909.cos.ap-guangzhou.myqcloud.com/newUploads/lives/goodsImg/2020_09_02/1599025352_478.jpg",
        "goods_id": 200,
        "commission": "0.200",
        "priceType": 3,
        "price": 60,
        "price2": 50,
        "status": 1
      }
    ]
  }
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 选择商品

POST /live/index/selectGoods

必要参数：
头部 Authorization：token
选择成功后返回goods_key，创建直播时带上goods_key

> Body 请求参数

```yaml
type: object
properties:
  goods_id:
    type: string
    description: 商品id
    example: "158"
  start:
    type: string
    description: 直播时间 开始
    example: 10:00
  end:
    type: string
    description: 直播时间 结束
    example: 11:00

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|AUTHORIZATION|header|string|true|none|
|body|body|object|false|none|
|» goods_id|body|string|false|商品id|
|» start|body|string|false|直播时间 开始|
|» end|body|string|false|直播时间 结束|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": {
    "goods_key": "1599035400-1599039000"
  }
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 直播分类列表

POST /live/index/getTypeList

必要参数：
头部 Authorization：token

> Body 请求参数

```yaml
type: object
properties: {}

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|AUTHORIZATION|header|string|true|none|
|body|body|object|false|none|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": [
    {
      "id": 1,
      "name": "直播带货"
    },
    {
      "id": 3,
      "name": "美妆达人"
    }
  ]
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 直播预告

POST /live/index/getNoticeList

必要参数：
头部 Authorization：token

> Body 请求参数

```yaml
type: object
properties:
  title:
    type: string
    description: 直播标题/主播昵称（用户昵称）
    example: 测试
  type:
    type: string
    description: 直播分类
    example: "1"
  limit:
    type: string
    description: 分页条数
    example: "15"
  page:
    type: string
    description: 页码
    example: "1"

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|AUTHORIZATION|header|string|true|none|
|body|body|object|false|none|
|» title|body|string|false|直播标题/主播昵称（用户昵称）|
|» type|body|string|false|直播分类|
|» limit|body|string|false|分页条数|
|» page|body|string|false|页码|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 删除直播间

POST /live/index/delLive

必要参数：
头部 Authorization：token

> Body 请求参数

```yaml
type: object
properties:
  id:
    type: string
    description: 直播间id
    example: "1"

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|AUTHORIZATION|header|string|true|none|
|body|body|object|false|none|
|» id|body|string|false|直播间id|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 直播中心

POST /live/index/getMyList

必要参数：
头部 Authorization：token

> Body 请求参数

```yaml
type: object
properties:
  title:
    type: string
    description: 直播标题/主播昵称（用户昵称）
    example: 测试
  type:
    type: string
    description: 直播分类
    example: "1"
  page:
    type: string
    description: 页码
    example: "1"
  limit:
    type: string
    description: 分页条数
    example: "15"

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|AUTHORIZATION|header|string|true|none|
|body|body|object|false|none|
|» title|body|string|false|直播标题/主播昵称（用户昵称）|
|» type|body|string|false|直播分类|
|» page|body|string|false|页码|
|» limit|body|string|false|分页条数|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": {
    "total": 1,
    "per_page": 15,
    "current_page": 1,
    "last_page": 1,
    "data": [
      {
        "id": 23,
        "uid": 4033,
        "直播间id\"room_id\"": 19,
        "直播间标题\"title\"": "带货哈哈哈",
        "用户名\"nickname\"": "黄灿",
        "用户头像\"avatarurl\"": "https://thirdwx.qlogo.cn/mmopen/vi_32/JrRM5l97CxDXiaKWn0X6l7oZZkG5PFGSola2CicZiamFiaHysmhxfSgMhV97c8gyQbnb6HnptmhtHEDS7qPlhDJecQ/132",
        "房间图片\"share\"": "https://tapi.miaommei.com/uploads/live_goods_img/202009/20200903100307_4033.jpeg",
        "直播类型\"type\"": 1,
        "//1带货直播，2普通直播 直播分类\"type_id\"": 1,
        "//分类id 直播回放地址\"playback_url\"": "",
        "直播状态\"status\"": "预热中",
        "发布状态（显示/隐藏）\"is_display\"": "已发布",
        "直播开始时间\"start_time\"": 1599185860,
        "直播结束时间\"end_time\"": 1599207460
      }
    ]
  }
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 创建直播

POST /live/index/createLive

必要参数：
头部 Authorization：token

> Body 请求参数

```yaml
type: object
properties:
  title:
    type: string
    description: 请填写直播间名称
    example: 测试1
  wechat:
    type: string
    description: 请填写主播微信号
    example: 微信号
  coverImg:
    type: string
    description: 请先选择直播背景图
    example: 图片上传返回的key
  shareImg:
    type: string
    description: 请先选择直播分享图
    example: 图片上传返回的key
  start:
    type: string
    description: 直播开始时间
    example: 14：30
  end:
    type: string
    description: 直播结束时间
    example: 15：30
  type:
    type: string
    description: 请选择直播类型  1带货 2普通直播
    example: "1"
  goods:
    type: string
    description: 请选择加入直播间的商品  直播商品id
    example: 158,234
  type_id:
    type: string
    description: 直播分类id
    example: "1"
  screen:
    type: string
    description: 请先选择横屏/竖屏直播
    example: "0"
  goods_key:
    type: string
    description: 选择商品时返回的goods_key
    example: 1599035400-1599039000

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|none|
|body|body|object|false|none|
|» title|body|string|false|请填写直播间名称|
|» wechat|body|string|false|请填写主播微信号|
|» coverImg|body|string|false|请先选择直播背景图|
|» shareImg|body|string|false|请先选择直播分享图|
|» start|body|string|false|直播开始时间|
|» end|body|string|false|直播结束时间|
|» type|body|string|false|请选择直播类型  1带货 2普通直播|
|» goods|body|string|false|请选择加入直播间的商品  直播商品id|
|» type_id|body|string|false|直播分类id|
|» screen|body|string|false|请先选择横屏/竖屏直播|
|» goods_key|body|string|false|选择商品时返回的goods_key|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 移除选中的商品

POST /live/index/delGoods

必要参数：
头部 Authorization：token

> Body 请求参数

```yaml
type: object
properties:
  goods_id:
    type: string
    description: 商品id
    example: "158"
  goods_key:
    type: string
    description: 选择商品后返回的goods_key
    example: 1599035400-1599039000

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|AUTHORIZATION|header|string|true|none|
|body|body|object|false|none|
|» goods_id|body|string|false|商品id|
|» goods_key|body|string|false|选择商品后返回的goods_key|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": true
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

# 直播佣金

## GET 直播佣金-直播列表数据

GET /live/money/getLiveListMiao

需要表头携带token进行请求

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|limit|query|string|false|拿取数据量|
|page|query|string|false|分页|
|Authorization|header|string|true|用户的token|

> 返回示例

> 成功

```json
"{\n    \"code\": 200,\n    \"msg\": \"直播数据列表返回ok\",\n    \"data\": {\n        \"total\": 8,\n        \"per_page\": 2,\n        \"current_page\": 3,\n        \"last_page\": 4,\n        \"data\": [\n            {\n                \"id\": 5, \n                \"order_num\": 0, // 成交订单数\n                \"miao\": 0, // 佣金\n                \"money\": 0, // 成交订单金额\n                \"title\": \"啦啦啦啦啦\", // 标题\n                \"share\": \"https://tapi.miaommei.com/uploads/live_goods_img/202009/20200904144411_4068.jpeg\", // 封面图片\n                \"add_time\": \"2020-09-04 14:45:03\" // 日期\n            },\n            {\n                \"id\": 6,\n                \"order_num\": 0,\n                \"miao\": 0,\n                \"money\": 0,\n                \"title\": \"啦啦啦啦啦\",\n                \"share\": \"https://tapi.miaommei.com/uploads/live_goods_img/202009/20200904144411_4068.jpeg\",\n                \"add_time\": \"2020-09-04 14:45:05\"\n            }\n        ]\n    }\n}"
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 总数据展示-喵币展示等

GET /live/money/getliveMiao

需要表头携带token进行请求

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|用户的token|

> 返回示例

> 成功

```json
"{\n    \"code\": 200,\n    \"msg\": \"拿取佣金数据ok\",\n    \"data\": {\n        // 直播数据-总数据\n        \"total\": {\n            \"order\": 1, // 订单数\n            \"miao\": 3.19, // 佣金\n            \"money\": 31.9 // 成交金额\n        },\n        //直播数据- 今日\n        \"today\": {\n            \"order\": 1, // 订单数\n            \"miao\": 3.19, // 佣金\n            \"money\": 31.9 // 成交金额\n        },\n        {\n        \"pre_miao\": 0, // 预估喵币\n        \"await_miao\": 0, // 结算喵币\n        \"fail_miao\": 0, // 无效喵币\n        \"credit_miao\": 0, // 累计喵币\n    }\n}"
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

# 购物券

## POST 查看购物券详情

POST /volume/index/getInfo

必要参数：
头部 Authorization：token

> Body 请求参数

```yaml
type: object
properties:
  type:
    type: string
    description: 活动类型 1-399，2-1999
    example: "1"

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|AUTHORIZATION|header|string|true|none|
|body|body|object|false|none|
|» type|body|string|false|活动类型 1-399，2-1999|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 购物券活动列表

POST /volume/index/getList

必要参数：
头部 Authorization：token

> Body 请求参数

```yaml
type: object
properties: {}

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|AUTHORIZATION|header|string|true|none|
|body|body|object|false|none|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 获取用户分享权限

POST /volume/index/checkShare

必要参数：
头部 Authorization：token

> Body 请求参数

```yaml
type: object
properties:
  id:
    type: string
    description: 购物券活动列表id
    example: "1"

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|AUTHORIZATION|header|string|true|none|
|body|body|object|false|none|
|» id|body|string|false|购物券活动列表id|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

# 店铺

## GET 店铺商品-上架下架操作

GET /shop/shopGoods/editStatus

店铺商品-上架下架操作

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|status|query|string|true|1表示上架    2 下架|
|id|query|string|true|商品id|
|Authorization|header|string|true|用户token|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 店铺申请/重新提交审核

POST /shop/index/createShop

必要参数：
头部 Authorization：token
type:1-线上门店，2-线下门店
code:入驻邀请码，初次申请店铺时可以填写

> Body 请求参数

```yaml
type: object
properties:
  name:
    type: string
    description: 店铺名
    example: 店铺a
  real_name:
    type: string
    description: 商家真实姓名
    example: 姓名
  phone:
    type: string
    description: 手机号
    example: "13500135000"
  identity:
    type: string
    description: 身份证
    example: "440823000000"
  logo:
    type: string
    description: 店铺logo图片地址
    example: https://xxx.xxx.com
  card_img_front:
    type: string
    description: 身份证正面
    example: https://xxx.xxx.com
  card_img_back:
    type: string
    description: 身份证反面
    example: https://xxx.xxx.com
  certificate_photo:
    type: string
    description: 证件照：三证合一（营业执照，机构证书，社会信用代码证书）
    example: https://xxx.xxx.com
  desc:
    type: string
    description: 店铺描述
    example: 好牌子
  id:
    type: string
    description: 店铺id ，重新提交审核的时候必传
    example: "0"
  type:
    type: string
    description: 店铺类型：1-线上门店，2-线下门店  默认为1
    example: "1"
  shop_nav:
    type: string
    description: 经营类目id ，线下门店必填 默认0
    example: "0"
  store_address:
    type: string
    description: 店铺详细地址，线下门店必填 默认空
    example: 广州天河
  longitude:
    type: string
    description: 经度，线下门店必填 默认空
    example: "113.0212"
  latitude:
    type: string
    description: 纬度，线下门店必填 默认空
    example: "23.03121"
  code:
    type: string
    description: 入驻邀请码，初次申请店铺时可以填写
    example: NKAUN1Y

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|AUTHORIZATION|header|string|true|none|
|body|body|object|false|none|
|» name|body|string|false|店铺名|
|» real_name|body|string|false|商家真实姓名|
|» phone|body|string|false|手机号|
|» identity|body|string|false|身份证|
|» logo|body|string|false|店铺logo图片地址|
|» card_img_front|body|string|false|身份证正面|
|» card_img_back|body|string|false|身份证反面|
|» certificate_photo|body|string|false|证件照：三证合一（营业执照，机构证书，社会信用代码证书）|
|» desc|body|string|false|店铺描述|
|» id|body|string|false|店铺id ，重新提交审核的时候必传|
|» type|body|string|false|店铺类型：1-线上门店，2-线下门店  默认为1|
|» shop_nav|body|string|false|经营类目id ，线下门店必填 默认0|
|» store_address|body|string|false|店铺详细地址，线下门店必填 默认空|
|» longitude|body|string|false|经度，线下门店必填 默认空|
|» latitude|body|string|false|纬度，线下门店必填 默认空|
|» code|body|string|false|入驻邀请码，初次申请店铺时可以填写|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 商家-售后-拒绝审核

GET /shop/service/noReview

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|id|query|string|true|售后序号|
|desc|query|string|true|拒绝理由|
|Authorization|header|string|true|token|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 售后详情

GET /shop/service/getInfo

售后列表

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|id|query|string|true|售后列表序号|
|Authorization|header|string|true|token值|

> 返回示例

> 成功

```json
"{\r\n    \"code\": 200,\r\n    \"msg\": \"售后详情\",\r\n    \"data\": {\r\n        \"id\": 68,\r\n        \"type\": 2, //类型 2 退款退货   1  换货\r\n        \"coverImgs\" => [], // 照片信息\r\n        \"nickname\": \"繁～\", // 用户昵称\r\n        \"order_orn\": \"20100921061426626414\", // 订单号\r\n        \"pay_time\": 1602248774, // 支付时间\r\n        \"price\": \"0.00\", // 退款金额\r\n        \"refund\": 0, // 最终退款金额\r\n        \"status\": 5, // 状态\r\n        \"status_str\": \"完结\", // 状态文字描述\r\n        \"apply_reason\": \"效果不好/不喜欢\", // 申请理由\r\n        \"reply_reason\": null, // 拒绝理由\r\n        \"add_time\": 1602248812, // 添加时间\r\n        \"sum_price\": 1.36, // 商品总额\r\n        \"freight\": 0, // 运费\r\n        \"total\": 0, // 实收款\r\n        \"address\": {\r\n            \"name\": \"哈哈农夫\",\r\n            \"tel\": \"123451023456\",\r\n            \"address\": \"阿贾克斯登记卡电视机卡都是借口就爱大开杀戒爱德克斯杰卡斯登记卡就开始\"\r\n        }, // 商家收货人 信息 \r\n        \"address_show\": false,//是否显示 商家收货人\r\n        \"changeExpress\": {  // 买家物流信息\r\n            \"company\": \"圆通快递\",\r\n            \"express_sn\": \"456456\"\r\n        },\r\n        \"changeExpress_show\": true, // 是否显示买家物流信息\r\n        \"merchantExpress\": {}, // 商家物流信息\r\n        \"merchantExpress_show\": false, // 是否显示商家物流信息\r\n        \"img\": \"https://admin-miaomeimei-1301812909.cos.ap-guangzhou.myqcloud.com/newUploads/product/goods/attrImg/2020_08_27/1598517059_881.5\", // 商品图片\r\n        \"title\": \"米雅诗防脱育发液小红瓶精华液营养滋润发丝发根植物护发精油\", // 商品标题\r\n        \"num\": 2 // 申请数量\r\n    }\r\n\r\n    /*\r\n    // 对应状态  下标取\r\n        $status = [\r\n            '待审核',\r\n            '审核通过',\r\n            '待买家发货',\r\n            '商家待收货',\r\n            '商家处理中',\r\n            '完结',\r\n            '拒绝'\r\n        ];\r\n    */\r\n}"
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 店铺商品

GET /shop/shopGoods/goodsList

店铺商品

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|type|query|string|true|商品分类|
|name|query|string|true|搜索名称|
|status|query|string|false|是否上架  1  上架    2  下架|
|page|query|string|true|分页数量|
|limit|query|string|true|每页显示的数量|
|Authorization|header|string|true|用户token|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "店铺商品列表",
  "data": {
    "total": 8,
    "per_page": 10,
    "current_page": 1,
    "last_page": 1,
    "data": [
      {
        "id": 293,
        "// 商品序号 \"name\"": "日本资生堂洗面奶 洗颜专科珊珂绵润泡沫洁面乳120g深层清洁男女",
        "// 名称 \"share\"": "https://admin-miaomeimei-1301812909.cos.ap-guangzhou.myqcloud.com/newUploads/product/goods/share/2020_08_18/1597739233_723.jpg",
        "// 分享图 \"status\"": 1,
        "// 状态  1  上架   2  下架 \"stock\"": 976,
        "// 库存 \"sale\"": 406,
        "// 销量 \"default_discount_price\"": "109.00",
        "// 零拼价 \"min_money\"": 109,
        "// 最小金额 \"default_live_price\"": 0,
        "// 默认直播价 \"live_status\"": "1 // 状态  1  直播商品  0 取消直播， 前端展示按钮为：  1 取消直播  0加直播 // 默认为2 表示 没有该直播商品 则不能进行 申请直播和取消直播 隐藏直播按钮操作"
      }
    ]
  }
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 获取店铺订单详情

POST /shop/order/getOrderInfo

必要参数：
头部 Authorization：token

> Body 请求参数

```yaml
type: object
properties:
  id:
    type: string
    description: 订单号
    example: "1313"

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|AUTHORIZATION|header|string|true|none|
|body|body|object|false|none|
|» id|body|string|false|订单号|

> 返回示例

> 成功

```json
"{\n    \"code\": 200,\n    \"msg\": \"操作成功\",\n    \"data\": {\n        \"order_sn\": \"20092315253231729423\",//订单编号\n        \"total\": \"0.00\",//实际付款金额\n        \"volume\": \"79.00\",//使用购物券金额\n        \"freight\": \"0.00\",//运费\n        \"coupon_money\": 0,//优惠券金额\n        \"status\": 2,//1-待发货，2-已发货\n        \"service\": 0,//没有售后服务0，1已申请等待审核， 2处理售后服务中，3 售后处理完毕，4被商家拒绝售后\n        \"express_id\": 0,\n        \"deliver\": 1,//配送方式，1-快递配送，2-到店自提，3-同城配送\n        \"remarks\": \"\",//订单备注\n        \"add_time\": \"2020-09-23 15:25:32\",//下单时间\n        \"logistics\": [  //未发货-可以选择发货的快递公司列表\n            {\n                \"id\": 1,\n                \"company\": \"申通快递\"\n            },\n            {\n                \"id\": 2,\n                \"company\": \"圆通快递\"\n            },\n            {\n                \"id\": 3,\n                \"company\": \"顺丰快递\"\n            },\n            {\n                \"id\": 4,\n                \"company\": \"韵达快递\"\n            },\n            {\n                \"id\": 5,\n                \"company\": \"天天快递\"\n            },\n            {\n                \"id\": 6,\n                \"company\": \"德邦快递\"\n            },\n            {\n                \"id\": 7,\n                \"company\": \"中通快递\"\n            },\n            {\n                \"id\": 8,\n                \"company\": \"EMS\"\n            },\n            {\n                \"id\": 10,\n                \"company\": \"百世汇通\"\n            }\n        ],\n        \"express\": { //已发货\n            \"express_sn\": “1002052552”, //快递单号\n            \"company\": \"百世汇通\"  //快递公司\n        },\n        \"goods\": [\n            {\n                \"price\": \"79.00\", //商品价格\n                \"num\": 1, //购买数量\n                \"volume\": \"79.00\", //使用购物券金额\n                \"total\": 0, //实付金额\n                \"purchase_money\": \"0.00\", //自购省金额\n                \"is_appraise\": 0, //是否评论 0-未评论，1-已评论\n                商品图片\"img\": \"https://admin-miaomeimei-1301812909.cos.ap-guangzhou.myqcloud.com/newUploads/product/goods/attrImg/2020_08_27/1598514035_364.1\",\n                商品标题\"title\": \"玛奇诺育发液防脱首乌生发增发剂增长剂密头皮护理按摩男女士通用\",\n                商品规格\"good_attr\": [\n                    \"净含量:100ml\"\n                ],\n                \"money\": {\n                    \"total\": 79,//商品总额\n                    \"volume\": 79,//购物券抵扣\n                    \"purchase\": 0//自购省\n                }\n            }\n        ],\n        \"address\": {\n            \"name\": \"测试\", //收货人\n            \"phone\": \"13800138000\",//收货电话\n            \"addr\": \"浙江杭州淳安 监控没事没事\"//收货地址\n        }\n    }\n}"
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 获取线下门店经营类目

POST /shop/index/getShopNavList

必要参数：
头部 Authorization：token

> Body 请求参数

```yaml
type: object
properties: {}

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|AUTHORIZATION|header|string|true|none|
|body|body|object|false|none|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": [
    {
      "id": 29,
      "//类目id \"type_key\"": "shop_nav_list",
      "title": "医美整形",
      "//类目名称 \"img_url\"": "https://admin-miaomeimei-1301812909.cos.ap-guangzhou.myqcloud.com/newUploads/homeDisplay/2020_09_11/1599819058_182.png",
      "show": 1,
      "expires_in": 0,
      "add_time": 1600249977,
      "is_skip": 0,
      "is_offline": 1,
      "url": "",
      "app_url": "",
      "sort": 15,
      "is_delete": 0
    },
    {
      "id": 30,
      "type_key": "shop_nav_list",
      "title": "养生馆",
      "img_url": "https://admin-miaomeimei-1301812909.cos.ap-guangzhou.myqcloud.com/newUploads/homeDisplay/ysg.png",
      "show": 1,
      "expires_in": 0,
      "add_time": 1600249977,
      "is_skip": 0,
      "is_offline": 1,
      "url": "",
      "app_url": "",
      "sort": 16,
      "is_delete": 0
    },
    {
      "id": 31,
      "type_key": "shop_nav_list",
      "title": "美容院",
      "img_url": "https://admin-miaomeimei-1301812909.cos.ap-guangzhou.myqcloud.com/newUploads/homeDisplay/mry.png",
      "show": 1,
      "expires_in": 0,
      "add_time": 1600249977,
      "is_skip": 0,
      "is_offline": 1,
      "url": "",
      "app_url": "",
      "sort": 17,
      "is_delete": 0
    },
    {
      "id": 32,
      "type_key": "shop_nav_list",
      "title": "美甲店",
      "img_url": "https://admin-miaomeimei-1301812909.cos.ap-guangzhou.myqcloud.com/newUploads/homeDisplay/mj.png",
      "show": 1,
      "expires_in": 0,
      "add_time": 1600249977,
      "is_skip": 0,
      "is_offline": 1,
      "url": "",
      "app_url": "",
      "sort": 18,
      "is_delete": 0
    },
    {
      "id": 33,
      "type_key": "shop_nav_list",
      "title": "美发店",
      "img_url": "https://admin-miaomeimei-1301812909.cos.ap-guangzhou.myqcloud.com/newUploads/homeDisplay/mf.png",
      "show": 1,
      "expires_in": 0,
      "add_time": 1600249977,
      "is_skip": 0,
      "is_offline": 1,
      "url": "",
      "app_url": "",
      "sort": 19,
      "is_delete": 0
    }
  ]
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 用户领取优惠券

GET /user/coupon/applyCoupon

用户领取优惠券

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|coupon_id|query|string|true|优惠券id|
|Authorization|header|string|false|token|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 店铺营收数据

GET /shop/index/getRevenue

营收数据

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|time|query|string|false|日期  年份-月份 格式：  xxxx-xx|
|Authorization|header|string|true|token值|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "拿取营收数据成功",
  "data": {
    "money": 432,
    "// 可提现余额 \"monthOrderNum\"": 3,
    "// 本月订单 \"orderNum\"": 22,
    "// 订单总数 \"monthServiceNum\"": 0,
    "// 本月售后 \"monthDealOrderNum\"": 3,
    "// 本月成交 \"sumMoney\"": 27567.96,
    "// 总成交金额 \"preOrderNum\"": "20 // 待确认收款订单数"
  }
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 店铺商品-更改直播状态

GET /shop/shopGoods/editLives

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|live_status|query|string|true|直播商品状态1   直播   0  取消直播|
|id|query|string|true|商品id|
|Authorization|header|string|true|token值|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 商家-售后-通过审核

GET /shop/service/yesReview

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|id|query|string|true|售后序号|
|Authorization|header|string|true|token|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 查看单个商品可领取优惠券列表

GET /mall/index/coupon

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|id|query|string|true|商品id，必须填写|
|Authorization|header|string|false|token|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "拿取优惠券列表成功",
  "data": [
    {
      "id": 2,
      "// 序号 \"status\"": 1,
      "//  类型：1-全店铺使用，2-指定商品使用 \"total_amount\"": "100.00",
      "// 满减抵扣必填，无条件抵扣时不用填写 0 \"amount\"": "5.00",
      "// 抵扣金额 \"total\"": 100,
      "// 发放数量 \"start_time\"": 1600473600,
      "// 开始时间 \"end_time\"": 1603065600,
      "// 结束时间 \"apply_status\"": "1 // 1 显示 已领取   0 显示领取"
    }
  ]
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 判断店铺状态

POST /shop/index/checkShop

必要参数：
头部 Authorization：token

> Body 请求参数

```yaml
type: object
properties: {}

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|AUTHORIZATION|header|string|true|none|
|body|body|object|false|none|

> 返回示例

> 成功

```json
"{\n    \"code\": 200,\n    \"msg\": \"操作成功\",\n    \"data\": {\n        \"online\": {//线上\n            \"apply_status\": 2, //0-未申请，1-申请中，2-已申请，3-申请拒绝\n            \"describe\": \"\" //拒绝说明\n        },\n        \"offline\": {//线下\n            \"apply_status\": 0,\n            \"describe\": \"\"\n        }\n    }\n}"
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 订单发货

POST /shop/order/deliverGoods

必要参数：
头部 Authorization：token

> Body 请求参数

```yaml
type: object
properties:
  id:
    type: string
    description: 订单id
    example: "1313"
  company_id:
    type: string
    description: 物流公司id
    example: "3"
  express_sn:
    type: string
    description: 物流单号
    example: S03213205

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|AUTHORIZATION|header|string|true|none|
|body|body|object|false|none|
|» id|body|string|false|订单id|
|» company_id|body|string|false|物流公司id|
|» express_sn|body|string|false|物流单号|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": true
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 我的店铺-店铺中心

GET /shop/index/info

店铺中心

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token值拿取|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "拿取店铺中心数据Ok",
  "data": {
    "name": "韩后官方旗舰店",
    "// 店铺名称 \"logo\"": "https://admin-miaomeimei-1301812909.cos.ap-guangzhou.myqcloud.com/newUploads/product/goods/branner/2020_08_18/1597739245_292.jpg",
    "// logo图片 \"security_deposit\"": 60.5,
    "// 保证金 金额 \"toDayMoney\"": 209.35,
    "// 今日成交额 \"toDayLimit\"": 3,
    "// 今天成交笔 \"monthLimit\"": 3,
    "// 本月订单笔 \"shipLimit\"": 3,
    "// 待发货 \"afterSaleLimit\"": "0 // 待售后"
  }
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 店铺订单列表

POST /shop/order/myOrderList

必要参数：
头部 Authorization：token

> Body 请求参数

```yaml
type: object
properties:
  order_sn:
    type: string
    description: 订单号
    example: "0123456"
  status:
    type: string
    description: 订单状态 0-全部，-1 -取消，1-待付款，2-待发货，3-待收货，4-已收货，5-已完成
    example: "0"
  page:
    type: string
    description: 分页 页码
    example: "1"
  limit:
    type: string
    description: 每页数据条数
    example: "15"

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|AUTHORIZATION|header|string|true|none|
|body|body|object|false|none|
|» order_sn|body|string|false|订单号|
|» status|body|string|false|订单状态 0-全部，-1 -取消，1-待付款，2-待发货，3-待收货，4-已收货，5-已完成|
|» page|body|string|false|分页 页码|
|» limit|body|string|false|每页数据条数|

> 返回示例

> 成功

```json
"{\n    \"code\": 200,\n    \"msg\": \"操作成功\",\n    \"data\": {\n        \"total\": 16,\n        \"per_page\": 15,\n        \"current_page\": 1,\n        \"last_page\": 2,\n        \"data\": [\n            \n            {\n                \"id\": 1313, //订单id\n                \"order_sn\": \"20092315253231729423\", //订单编号\n                \"total\": 79, //付款总额\n                \"volume\": \"79.00\", //使用购物券金额\n                \"status\": 2, //订单状态，0-已取消,1-待付款，2-待发货，3-待收货，4-已收货，5-已完成\n                \"service\": 0, //没有售后服务0，1已申请等待审核， 2处理售后服务中，3 售后处理完毕，4被商家拒绝售后\n                \"add_time\": \"2020-09-23 15:25\", //订单创建时间\n                \"nickname\": \"繁～\", //购买用户昵称\n                \"avatarurl\": \"https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLVgBQA8jyxnRnYZOxVPricIicwJja5LMVusLGwpcLAzibvv2321ibWy4ynhUIaetG1O5KBic6CeU4cpkA/132\",\n                \"goods\": [\n                    {\n                        \"order_id\": 1313,\n                        \"goods_id\": 344, //购买商品id\n                        \"title\": \"玛奇诺育发液防脱首乌生发增发剂增长剂密头皮护理按摩男女士通用\", //商品名称\n                        \"price\": \"79.00\", //商品价格\n                        \"attr_str\": [ //规格\n                            \"净含量:100ml\"\n                        ],\n                        商品图片\"img\": \"https://admin-miaomeimei-1301812909.cos.ap-guangzhou.myqcloud.com/newUploads/product/goods/attrImg/2020_08_27/1598514035_364.1\",\n                        \"num\": 1, //购买数量\n                        \"live_name\": \"哈哈哈1\" //忽略\n                    }\n                ],\n                \"live\": {\n                    \"name\": \"哈哈哈1\", //直播用户昵称\n                    \"price\": \"79.00\" //直播价格\n                }\n            },\n            {\n                \"id\": 1315,\n                \"order_sn\": \"20092315290485905198\",\n                \"total\": 79,\n                \"volume\": \"79.00\",\n                \"status\": 2,\n                \"service\": 0,\n                \"add_time\": \"2020-09-23 15:29\",\n                \"nickname\": \"繁～\",\n                \"avatarurl\": \"https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLVgBQA8jyxnRnYZOxVPricIicwJja5LMVusLGwpcLAzibvv2321ibWy4ynhUIaetG1O5KBic6CeU4cpkA/132\",\n                \"goods\": [\n                    {\n                        \"order_id\": 1315,\n                        \"goods_id\": 344,\n                        \"title\": \"玛奇诺育发液防脱首乌生发增发剂增长剂密头皮护理按摩男女士通用\",\n                        \"price\": \"79.00\",\n                        \"attr_str\": [\n                            \"净含量:100ml\"\n                        ],\n                        \"img\": \"https://admin-miaomeimei-1301812909.cos.ap-guangzhou.myqcloud.com/newUploads/product/goods/attrImg/2020_08_27/1598514035_364.1\",\n                        \"num\": 1,\n                        \"live_name\": \"0\"\n                    }\n                ],\n                \"live\": []\n            },\n            {\n                \"id\": 1316,\n                \"order_sn\": \"20092315295373858919\",\n                \"total\": 51.35,\n                \"volume\": \"0.00\",\n                \"status\": 1,\n                \"service\": 0,\n                \"add_time\": \"2020-09-23 15:29\",\n                \"nickname\": \"繁～\",\n                \"avatarurl\": \"https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLVgBQA8jyxnRnYZOxVPricIicwJja5LMVusLGwpcLAzibvv2321ibWy4ynhUIaetG1O5KBic6CeU4cpkA/132\",\n                \"goods\": [\n                    {\n                        \"order_id\": 1316,\n                        \"goods_id\": 344,\n                        \"title\": \"玛奇诺育发液防脱首乌生发增发剂增长剂密头皮护理按摩男女士通用\",\n                        \"price\": \"79.00\",\n                        \"attr_str\": [\n                            \"净含量:100ml\"\n                        ],\n                        \"img\": \"https://admin-miaomeimei-1301812909.cos.ap-guangzhou.myqcloud.com/newUploads/product/goods/attrImg/2020_08_27/1598514035_364.1\",\n                        \"num\": 1,\n                        \"live_name\": \"0\"\n                    }\n                ],\n                \"live\": []\n            }\n        ]\n    }\n}"
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 获取重新申请店铺的数据

POST /shop/index/getReapplyInfo

必要参数：
头部 Authorization：token

> Body 请求参数

```yaml
type: object
properties: {}

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|AUTHORIZATION|header|string|true|none|
|body|body|object|false|none|

> 返回示例

> 成功

```json
"{\n    \"code\": 200,\n    \"msg\": \"操作成功\",\n    \"data\": {\n        \"id\": 1,\n        \"name\": \"韩后官方旗舰店\",//店铺名\n        \"real_name\": \"韩要贺\",//真实姓名\n        \"phone\": \"13800138000\",//手机号\n        \"uid\": 4033,//小程序端uid\n        \"logo\": \"https://admin-miaomeimei-1301812909.cos.ap-guangzhou.myqcloud.com/newUploads/product/goods/branner/2020_08_18/1597739245_292.jpg\",\n        \"apply_status\": 1,//申请已通过   0申请中，1审核通过，2审核拒绝\n        \"is_open\": 1,//是否开启 0-关闭 ，1 开启\n        \"warm_number\": 123,//预热数\n        \"add_time\": \"2020-09-21 10:59:20\",\n        \"is_delete\": 0,\n        \"upd_time\": 0,\n        \"shop_id\": 1, //店铺id\n        \"card_img\": [ //身份证正反照\n            \"https://admin-miaomeimei-1301812909.cos.ap-guangzhou.myqcloud.com/newUploads/shop/user/card/2020_09_22/1600745860_179.jpg\",\n            \"https://admin-miaomeimei-1301812909.cos.ap-guangzhou.myqcloud.com/newUploads/shop/user/card/2020_09_22/1600745865_391.jpg\"\n        ],\n        //证据照\n        \"certificate_photo\": \"https://admin-miaomeimei-1301812909.cos.ap-guangzhou.myqcloud.com/newUploads/shop/user/certificate/2020_09_22/1600745870_696.jpg\",\n        \"identity\": \"440927199955245555\",//身份证号\n        //描述\n        \"desc\": \"北国风光，千里冰封，万里雪飘。\\r\\n望长城内外，惟余莽莽；大河上下，顿失滔滔。\\r\\n山舞银蛇，原驰蜡象，欲与天公试比高。\\r\\n须晴日，看红装素裹，分外妖娆。\\r\\n江山如此多娇，引无数英雄竞折腰。\\r\\n惜秦皇汉武，略输文采；唐宗宋祖，稍逊风骚。\\r\\n一代天骄，成吉思汗，只识弯弓射大雕。\\r\\n俱往矣，数风流人物，还看今朝。\",\n        //富文本介绍\n        \"rule\": \"&lt;p class=&quot;poem-detail-main-text&quot; style=&quot;font-size: 15px; color: rgb(51, 51, 51); line-height: 25px; font-family: Arial, sans-serif; white-space: normal; background-color: rgb(255, 255, 255);&quot;&gt;&lt;span class=&quot;body-zhushi-span&quot; style=&quot;border-bottom: 1px solid rgb(38, 115, 219); cursor: pointer;&quot;&gt;北国&lt;/span&gt;风光，千里冰封，万里&lt;span class=&quot;body-zhushi-span&quot; style=&quot;border-bottom: 1px solid rgb(38, 115, 219); cursor: pointer;&quot;&gt;雪&lt;/span&gt;飘。&lt;/p&gt;&lt;p class=&quot;poem-detail-main-text&quot; style=&quot;font-size: 15px; color: rgb(51, 51, 51); line-height: 25px; font-family: Arial, sans-serif; white-space: normal; background-color: rgb(255, 255, 255);&quot;&gt;望长城内外，&lt;span class=&quot;body-zhushi-span&quot; style=&quot;border-bottom: 1px solid rgb(38, 115, 219); cursor: pointer;&quot;&gt;惟&lt;/span&gt;余&lt;span class=&quot;body-zhushi-span&quot; style=&quot;border-bottom: 1px solid rgb(38, 115, 219); cursor: pointer;&quot;&gt;莽莽&lt;/span&gt;；&lt;span class=&quot;body-zhushi-span&quot; style=&quot;border-bottom: 1px solid rgb(38, 115, 219); cursor: pointer;&quot;&gt;大河上下&lt;/span&gt;，&lt;span class=&quot;body-zhushi-span&quot; style=&quot;border-bottom: 1px solid rgb(38, 115, 219); cursor: pointer;&quot;&gt;顿失滔滔&lt;/span&gt;。&lt;/p&gt;&lt;p class=&quot;poem-detail-main-text&quot; style=&quot;font-size: 15px; color: rgb(51, 51, 51); line-height: 25px; font-family: Arial, sans-serif; white-space: normal; background-color: rgb(255, 255, 255);&quot;&gt;山舞银蛇，原驰蜡象，欲与&lt;span class=&quot;body-zhushi-span&quot; style=&quot;border-bottom: 1px solid rgb(38, 115, 219); cursor: pointer;&quot;&gt;天公&lt;/span&gt;试比高。&lt;/p&gt;&lt;p class=&quot;poem-detail-main-text&quot; style=&quot;font-size: 15px; color: rgb(51, 51, 51); line-height: 25px; font-family: Arial, sans-serif; white-space: normal; background-color: rgb(255, 255, 255);&quot;&gt;&lt;span class=&quot;body-zhushi-span&quot; style=&quot;border-bottom: 1px solid rgb(38, 115, 219); cursor: pointer;&quot;&gt;须&lt;/span&gt;晴日，看&lt;span class=&quot;body-zhushi-span&quot; style=&quot;border-bottom: 1px solid rgb(38, 115, 219); cursor: pointer;&quot;&gt;红装素裹&lt;/span&gt;，&lt;span class=&quot;body-zhushi-span&quot; style=&quot;border-bottom: 1px solid rgb(38, 115, 219); cursor: pointer;&quot;&gt;分外妖娆&lt;/span&gt;。&lt;/p&gt;&lt;p class=&quot;poem-detail-main-text&quot; style=&quot;font-size: 15px; color: rgb(51, 51, 51); line-height: 25px; font-family: Arial, sans-serif; white-space: normal; background-color: rgb(255, 255, 255);&quot;&gt;江山如此多娇，引无数英雄&lt;span class=&quot;body-zhushi-span&quot; style=&quot;border-bottom: 1px solid rgb(38, 115, 219); cursor: pointer;&quot;&gt;竞折腰&lt;/span&gt;。&lt;/p&gt;&lt;p class=&quot;poem-detail-main-text&quot; style=&quot;font-size: 15px; color: rgb(51, 51, 51); line-height: 25px; font-family: Arial, sans-serif; white-space: normal; background-color: rgb(255, 255, 255);&quot;&gt;惜&lt;span class=&quot;body-zhushi-span&quot; style=&quot;border-bottom: 1px solid rgb(38, 115, 219); cursor: pointer;&quot;&gt;秦皇&lt;/span&gt;&lt;span class=&quot;body-zhushi-span&quot; style=&quot;border-bottom: 1px solid rgb(38, 115, 219); cursor: pointer;&quot;&gt;汉武&lt;/span&gt;，&lt;span class=&quot;body-zhushi-span&quot; style=&quot;border-bottom: 1px solid rgb(38, 115, 219); cursor: pointer;&quot;&gt;略输文采&lt;/span&gt;；&lt;span class=&quot;body-zhushi-span&quot; style=&quot;border-bottom: 1px solid rgb(38, 115, 219); cursor: pointer;&quot;&gt;唐宗&lt;/span&gt;&lt;span class=&quot;body-zhushi-span&quot; style=&quot;border-bottom: 1px solid rgb(38, 115, 219); cursor: pointer;&quot;&gt;宋祖&lt;/span&gt;，&lt;span class=&quot;body-zhushi-span&quot; style=&quot;border-bottom: 1px solid rgb(38, 115, 219); cursor: pointer;&quot;&gt;稍逊风骚&lt;/span&gt;。&lt;/p&gt;&lt;p class=&quot;poem-detail-main-text&quot; style=&quot;font-size: 15px; color: rgb(51, 51, 51); line-height: 25px; font-family: Arial, sans-serif; white-space: normal; background-color: rgb(255, 255, 255);&quot;&gt;&lt;span class=&quot;body-zhushi-span&quot; style=&quot;border-bottom: 1px solid rgb(38, 115, 219); cursor: pointer;&quot;&gt;一代天骄&lt;/span&gt;，&lt;span class=&quot;body-zhushi-span&quot; style=&quot;border-bottom: 1px solid rgb(38, 115, 219); cursor: pointer;&quot;&gt;成吉思汗&lt;/span&gt;，&lt;span class=&quot;body-zhushi-span&quot; style=&quot;border-bottom: 1px solid rgb(38, 115, 219); cursor: pointer;&quot;&gt;只识弯弓射大雕&lt;/span&gt;。&lt;/p&gt;&lt;p class=&quot;poem-detail-main-text&quot; style=&quot;font-size: 15px; color: rgb(51, 51, 51); line-height: 25px; font-family: Arial, sans-serif; white-space: normal; background-color: rgb(255, 255, 255);&quot;&gt;&lt;span class=&quot;body-zhushi-span&quot; style=&quot;border-bottom: 1px solid rgb(38, 115, 219); cursor: pointer;&quot;&gt;俱往矣&lt;/span&gt;，&lt;span class=&quot;body-zhushi-span&quot; style=&quot;border-bottom: 1px solid rgb(38, 115, 219); cursor: pointer;&quot;&gt;数风流人物&lt;/span&gt;，还看今朝。&lt;/p&gt;&lt;p&gt;&lt;img src=&quot;https://tadmin.miaommei.com/uploads/ueditor/img/20200922/1600745931668236.png&quot; title=&quot;1600745931668236.png&quot; alt=&quot;test.png&quot;/&gt;&lt;/p&gt;\",\n        \"refuse_str\": null,//拒绝理由\n        \"code\": \"0\"//邀请码\n    }\n}"
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 店铺-创建优惠券

GET /shop/coupon/createCoupon

店铺创建优惠券

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|status|query|string|true|类型：1-全店铺使用，2-指定商品使用|
|related_id|query|string|false|如果是指定 如果类型为2  则传递 商品iD，使用逗号分隔|
|total_amount|query|string|false|满减抵扣必填，无条件抵扣时不用填写 为0 表示无限制 超过0 表示满减|
|amount|query|string|true|抵扣金额|
|total|query|string|true|发放数量|
|start_time|query|string|true|优惠券开始时间，时间戳|
|end_time|query|string|true|优惠券 结束时间，  时间戳|
|name|query|string|false|优惠券名称 可写可不写 方便 商家在后台展示数据|
|Authorization|header|string|false|token值|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 店铺商品删除

GET /shop/shopGoods/goodsDel

店铺商品删除

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|id|query|string|true|商品id|
|Authorization|header|string|true|token|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 店铺售后列表

GET /shop/service/list

店铺售后-列表

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|page|query|string|false|分页  - 第几页|
|limit|query|string|false|拿取的数据量 -  默认10条|
|Authorization|header|string|false|token值 |

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "售后列表",
  "data": {
    "total": 16,
    "per_page": 10,
    "current_page": 1,
    "last_page": 2,
    "data": [
      {
        "id": 66,
        "order_goods_id": 1490,
        "type": 2,
        "// 类型，1-换货，2-退货退款 \"apply_price\"": "1899.91",
        "// 申请退款金额 \"price\"": "1899.91",
        "// 商品价格金额 \"add_time\"": 1602242284,
        "// 添加时间 \"status\"": "待买家发货",
        "// 此处直接显示后台返回的文字即可 \"num\"": 10,
        "// 数量 \"img\"": "https://admin-miaomeimei-1301812909.cos.ap-guangzhou.myqcloud.com/newUploads/product/goods/attrImg/2020_10_09/1602231453_647.jpg",
        "// 图片 \"title\"": "测试33340",
        "// 标题 \"attr_str\"": "量:456 ",
        "// 规格 \"total\"": "189991 // 当前商品支付价格"
      }
    ]
  }
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 单个商品-可用优惠券ok

GET /mall/index/apply_coupon

可用优惠券列表ok

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|id|query|string|true|商品id|
|Authorization|header|string|true|用户token值|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "拿取可用优惠券ok",
  "data": [
    {
      "id": 65,
      "// 序号 \"type\"": 1,
      "// 类型  1-商家券，2-平台券 \"use_status\"": 1,
      "// 类型：1-全店铺使用，2-指定商品使用 \"total_amount\"": "100.00",
      "// 满减抵扣必填，等于 0 表示无条件抵扣 \"amount\"": "5.00",
      "// 抵扣金额 \"add_time\"": 1600777629,
      "// 领取时间 \"start_time\"": 1600473600,
      "// 开始时间 \"end_time\"": "1603065600 // 结束时间"
    }
  ]
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 优惠券-方法记录

GET /shop/coupon/couponList

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|page|query|string|false|分页数量|
|limit|query|string|false|拿取的数据|
|type|query|string|false|类型：0-全部，1-领取中，2-已结束|
|Authorization|header|string|true|totken|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "优惠券发放记录列表ok",
  "data": {
    "total": 7,
    "per_page": 10,
    "current_page": 1,
    "last_page": 1,
    "data": [
      {
        "status": 1,
        "//类型：1-全店铺使用，2-指定商品使用 \"total_amount\"": "100.00",
        "// 满减抵扣必填，无条件抵扣时不用填写 如果为0 表示 无限制 \"amount\"": "5.00",
        "// 抵扣金额 \"total\"": 100,
        "// 发放数量 \"day\"": 1,
        "// 天数 \"add_time\"": 1600744783,
        "// 添加时间 \"start_time\"": 1600743313,
        "// 开始时间 \"end_time\"": 1600829713,
        "// 结束时间 \"apply_num\"": 0,
        "// 领取数量 \"use_num\"": 0,
        "// 使用数量 \"expired_true\"": false,
        "// 是否过期  默认false   如果为true 则显示 是 \"name\"": "韩后官方旗舰店",
        "// 名称 \"goods_num\"": 0,
        "// 类型为2 指定商品可用 表示商品数量 ，默认为0": null
      }
    ]
  }
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

# app-专属

## GET 手机短信验证码登录

GET /user/login/phoneLoginCode

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|phone|query|string|true|手机短信验证码登录|
|code|query|string|true|验证码，必须位六位纯数字|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "登录成功",
  "data": {
    "id": 4034,
    "token": "9a1b46fe74219cdfcdcabeb1964cb320",
    "session_key": "xD5T8ROZo8iadILCgqc/Ow==",
    "unionid": "oDFSSv12KPr3DMgTTd8LAF8CNzFM",
    "openid": "o8lmR4uJaZJLasqdZXI7z36XJt4w",
    "phone": "13048271212",
    "name": "0",
    "nickname": "ㅤ",
    "avatarurl": "https://thirdwx.qlogo.cn/mmopen/vi_32/wbOgeAfsJ45VLibpuhlTOibAicx3HmrofGwNJocCoSgVojJVlMvnPMMrITU2vRQAUbpEDX3iaLvaRLByaenuFSbQKQ/132",
    "invite_id": 4171,
    "invite_code": "UHJ60T0",
    "is_proceeds": 1,
    "pass": "0",
    "remarks": "0",
    "binding_time": 1600269677,
    "is_fictitious": 1,
    "add_time": 1598603450,
    "upd_time": 0,
    "level": 3,
    "last_time": 1600659267,
    "is_delete": 0
  }
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 手机号码发送短信验证码

GET /user/login/phoneCode

手机号码发送短信验证码

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|phone|query|string|true|手机号码|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "发送验证码成功",
  "data": ""
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

# 用户优惠券

## POST 我的优惠券列表

POST /user/coupon/myCouponList

优惠券列表

> Body 请求参数

```yaml
type: object
properties:
  page:
    type: string
    description: 分页 页码
    example: "1"
  limit:
    type: string
    description: 每页数据条数
    example: "15"
  status:
    type: string
    description: 优惠券状态，1-未使用，2-已使用，3-已过期
    example: "0"
  is_expire:
    type: string
    description: 是否筛选即将过期的：0-否，1-是
    example: "0"

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|AUTHORIZATION|header|string|true|none|
|body|body|object|false|none|
|» page|body|string|false|分页 页码|
|» limit|body|string|false|每页数据条数|
|» status|body|string|false|优惠券状态，1-未使用，2-已使用，3-已过期|
|» is_expire|body|string|false|是否筛选即将过期的：0-否，1-是|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": {
    "about_expire": 3,
    "data": [
      {
        "id": 23,
        "title": "杂货店2",
        "logo": "https://tapi.miaommei.com/uploads/linshi/linshi_1601108886.jpg.png",
        "add": 1601347424,
        "list": [
          {
            "优惠券id\"id\"": 125,
            "领取时间\"add_time\"": "2020-09-29",
            "优惠券开始时间\"start_time\"": "2020-09-29",
            "优惠券结束时间\"end_time\"": "2020-10-02",
            "状态\"status\"": 0,
            "//0-未使用，1-已使用 类型\"use_status\"": 2,
            "//1-全店铺使用，2-指定商品使用 优惠券面额\"amount\"": "11.00",
            "优惠券使用限制金额\"total_amount\"": "0.00",
            "//这个值是满减时必须满足的最大金额 店铺id\"shop_id\"": 23,
            "优惠券名称\"name\"": "11",
            "店铺名\"title\"": "杂货店2",
            "店铺logo\"logo\"": "https://tapi.miaommei.com/uploads/linshi/linshi_1601108886.jpg.png",
            "优惠券类别\"type\":1": null,
            "//1-无限制，2-满减": null
          }
        ]
      },
      {
        "id": 18,
        "title": "韩后官方旗舰店",
        "logo": "https://admin-miaomeimei-1301812909.cos.ap-guangzhou.myqcloud.com/newUploads/shop/user/logo/2020_09_25/1601028139_967.png",
        "add": 1601347324,
        "list": [
          {
            "id": 116,
            "add_time": "2020-09-29",
            "start_time": "2020-09-29",
            "end_time": "2020-10-02",
            "status": 0,
            "use_status": 1,
            "amount": "10.00",
            "total_amount": "0.00",
            "shop_id": 18,
            "name": "26号可用",
            "title": "韩后官方旗舰店",
            "logo": "https://admin-miaomeimei-1301812909.cos.ap-guangzhou.myqcloud.com/newUploads/shop/user/logo/2020_09_25/1601028139_967.png",
            "type": 1
          }
        ]
      },
      {
        "id": 17,
        "title": "杂货店2",
        "logo": "https://tapi.miaommei.com/uploads/linshi/linshi_1601108886.jpg.png",
        "add": 1601347224,
        "list": [
          {
            "id": 124,
            "add_time": "2020-09-29",
            "start_time": "2020-09-30",
            "end_time": "2020-10-08",
            "status": 0,
            "use_status": 2,
            "amount": "1.00",
            "total_amount": "1.00",
            "shop_id": 17,
            "name": "1",
            "title": "杂货店2",
            "logo": "https://tapi.miaommei.com/uploads/linshi/linshi_1601108886.jpg.png",
            "type": 2
          }
        ]
      },
      {
        "id": 21,
        "title": "杂货店2",
        "logo": "https://tapi.miaommei.com/uploads/linshi/linshi_1601108886.jpg.png",
        "add": 1601347224,
        "list": [
          {
            "id": 119,
            "add_time": "2020-09-29",
            "start_time": "2020-09-30",
            "end_time": "2020-10-08",
            "status": 0,
            "use_status": 2,
            "amount": "3.00",
            "total_amount": "3.00",
            "shop_id": 21,
            "name": "3",
            "title": "杂货店2",
            "logo": "https://tapi.miaommei.com/uploads/linshi/linshi_1601108886.jpg.png",
            "type": 2
          },
          {
            "id": 123,
            "add_time": "2020-09-29",
            "start_time": "2020-09-29",
            "end_time": "2020-10-02",
            "status": 0,
            "use_status": 2,
            "amount": "2.00",
            "total_amount": "2.00",
            "shop_id": 21,
            "name": "2",
            "title": "杂货店2",
            "logo": "https://tapi.miaommei.com/uploads/linshi/linshi_1601108886.jpg.png",
            "type": 2
          }
        ]
      }
    ]
  }
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

# 公共

## POST 图片上传

POST /api/index/uploadImg

必要参数：
头部 Authorization：token

> Body 请求参数

```yaml
type: object
properties:
  type:
    type: string
    description: 上传文件类型：图片为image
    example: image
  name:
    type: string
    description: 上传文件键名：如果是img则不用传
    example: img
  prefix:
    type: string
    description: 前缀名：非必传
    example: images
required:
  - type

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|AUTHORIZATION|header|string|true|none|
|body|body|object|false|none|
|» type|body|string|true|上传文件类型：图片为image|
|» name|body|string|false|上传文件键名：如果是img则不用传|
|» prefix|body|string|false|前缀名：非必传|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

# 分享

## GET 分享-列表

GET /share/index/list

分享-列表

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|page|query|string|true|分页第几页|
|limit|query|string|true|拿取的数据|
|status|query|string|true|订单状态，0-全部,1-待付款，2-待发货，3-待收货，4-已收货，5-已完成,6-已退款|
|AUTHORIZATION|header|string|true|none|

> 返回示例

> 成功

```json
"{\n    \"code\": 200,\n    \"msg\": \"分享订单ok\",\n    \"data\": {\n        \"total\": 20,\n        \"per_page\": 5,\n        \"current_page\": 1,\n        \"last_page\": 4,\n        \"data\": [\n            {\n                \"id\": 6, // id\n                \"add_time\": 1595668428, //下单 时间\n                \"status\": 2, // 状态\n                \"miao\": 0 // 喵币 ,必须要付款后才有\n                \"user_nickname\": \"黄灿\", // 昵称\n                \"user_avatarurl\": \"https://wx.qlogo.cn/mmopen/vi_32/hYwAzL7NVSVqian1RQmTKJA84kyNzMibjga9G9A6FyicrVcKhPeibJqibjS9qmicMf35ibzM9v81c7azwjeNTAyRsN8Bg/132\" // 头像\n            },\n            {\n                \"id\": 7,\n                \"add_time\": 1595668428,\n                \"status\": 2,\n                \"miao\": 0\n            },\n            {\n                \"id\": 8,\n                \"add_time\": 1595668428,\n                \"status\": 2,\n                \"miao\": 0\n            },\n            {\n                \"id\": 9,\n                \"add_time\": 1595668428,\n                \"status\": 2,\n                \"miao\": 0\n            },\n            {\n                \"id\": 10,\n                \"add_time\": 1595668428,\n                \"status\": 2,\n                \"miao\": 0\n            }\n        ]\n    }\n}"
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

# 商城模板

## GET 根据视频id和活动id获取到该视频发布者对应的拼团列表id，支付页面需要

GET /mall/index/getActivityLogId

-----

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|activity_id|query|string|true|活动列表id，视频详情可以获取|
|video_id|query|string|true|视频id，视频详情可以获取|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "拿取视频关联的活动-拼团列表id-ok",
  "data": {
    "activity_log_id": "1 // 返回拼团列表id"
  }
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 商城首页图标

POST /view/index/homeDisplay

> Body 请求参数

```yaml
type: object
properties:
  type:
    type: string
    description: 1小图标，2大图标
    example: "1"

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|body|body|object|false|none|
|» type|body|string|false|1小图标，2大图标|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": [
    {
      "name": "测试3",
      "img": "https://cn.bing.com/images/search?q=github+%e5%9b%be%e6%a0%87&id=2ED8DD855B4A9D0FB422A7F767845DCEA36B778F&FORM=IQFRBA",
      "page": "0"
    },
    {
      "name": "测试4",
      "img": "https://cn.bing.com/images/search?q=github+%e5%9b%be%e6%a0%87&id=2ED8DD855B4A9D0FB422A7F767845DCEA36B778F&FORM=IQFRBA",
      "page": "0"
    }
  ]
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 拼团活动详情

GET /mall/activity/get

拼团活动详情操作

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|id|query|string|true|获取的拼团活动id|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "拼团活动列表ok",
  "data": {
    "activity": {
      "id": 2,
      "// id \"name\"": "来来",
      "// 名称 \"cover\"": "https://admin.miaommei.com/uploads/activity/20200711/de1431435ab1661a41cbf0386a76cf84.jpg",
      "// 图片 \"rule\"": "&lt;p&gt;阿三顶顶顶顶顶顶顶顶顶顶&lt;/p&gt;",
      "// 详情 \"sort\"": 66,
      "// 排序 \"hours\"": 1594635989,
      "// 活动结束时间 \"title\"": "测试ok",
      "share": "https://admin.miaommei.com/uploads/activity/20200711/dbdcea16fea276855a2266bbf70bd1f2.jpg",
      "// 分享图片 \"num\"": 3,
      "// 当前开团人数 \"type\"": 2,
      "// 类型  1 表示金额  2 表示百分比 需要判断 \"content\"": [
        "// 第几个人拿取的 佣金  第一个  3  第二个 6  （需要迭代） \"3\"",
        "6",
        "9"
      ]
    },
    "goods": [
      {
        "id": 1,
        "// 商品信息 \"name\"": "测试商品1",
        "// 名称 \"share\"": "https://admin.miaommei.com/uploads/goods_share/20200611/95d28b7ab959cb75a94449a4cf02f50d.jpg",
        "// 分享图 \"title\"": "测试商品1",
        "// 标题 \"default_price\"": "0.00",
        "// 默认价格 \"default_discount_price\"": "0.00",
        "// 折扣价 \"sort\"": 20,
        "is_activity": 1
      },
      {
        "id": 2,
        "name": "测试商品2",
        "share": "https://admin.miaommei.com/uploads/goods_share/20200611/b73a84fb71b57241fbfdb280dcca0db0.jpg",
        "title": "测试商品2",
        "default_price": "0.00",
        "default_discount_price": "0.00",
        "sort": 50,
        "is_activity": 1
      }
    ]
  }
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 商城获取详情

POST /mall/index/getMalls

商品获取详情

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|id|query|string|true|商品id值 必须大于0|
|activity_id|query|string|false|如果是拼团活动 需要传递此id|
|video_id|query|string|false|视频详情跳转过来的|
|lives|query|string|false|直播商品|
|Authorization|header|string|false|用户token值|

> 返回示例

> 成功

```json
"{\n    \"code\": 200,\n    \"msg\": \"商品详情ok\",\n    \"data\": {\n        // 商品信息和用户信息\n        \"commodity\": {\n            \"id\": 23,\n            \"name\": \"面膜\", // 名称\n            \"title\": \"面膜\", // 标题\n            \"cover\": [  // 轮播图\n                \"https://admin.miaommei.com/uploads/goods/20200710/38c2d7a3fd45ead7ac7f90d3c00b4180.jpg\"\n            ],\n            \"share\": \"https://admin.miaommei.com/uploads/goods_share/20200710/df798af92b54619efeefe250f18853be.jpg\", // 分享图片\n            \"stock\":100, //总库存\n            \"default_price\": 0, // 商品价\n            \"default_discount_price\": 79, // 折扣价\n            \"sales_volume\": 200, // 销量\n            \"is_activity\": 1, // 是否参加活动 1 是  0 否\n            \"activity_id\":1, // 需要if  点击商品 中 查看活动 需要携带的活动id\n            \"products_json\": { // 产品参数  键值对\n                \"电脑\": \"联想\",\n                \"品牌\": \"暗黑\"\n            },\n            \"is_collect\":true, // true表示关注  否则  false\n            \"desc\": \"\", // 商品详情 html需要解析\n            \"user_share\": 10, // 如果有身份则会显示分享比例  需要if\n            \"default_discount_price_share\": 7.9, // 当前用户等级的分享提成 需要if\n             \"lives_bool\": false, // 是否从直播跳转过来的  且 当前是直播中的商品\n            \"coupon_true\": true // 当前商品是否可以使用优惠券\n        },\n        // 品牌信息\n        \"brand\": {\n                \"id\": 1, //  品牌id\n                \"name\": \"GoodsBrand\", // 品牌名称\n                \"cover\": \"http://admin.miaommei.com/uploads/goods_brand/20200611/9ea626f9bf515287c108cce24925c937.jpg\" // 品牌图片\n            },\n        \"attrs\": [  // 商品规格    价格表 \n            { \n                \"price\": 7000, // 商品价\n                \"discount_price\": 6000, // 划线价\n                \"stock\": 100, //库存\n                \"live_price\": 6500, // 直播价\n                \"integral\": 15,  // 积分\n                \"sale\": 0, //销量\n                \"valjson\": {  // 匹配的规格数据\n                    \"码数\": \"大\",\n                    \"颜色\": \"黄\"\n                }\n            }\n        ],\n        \"attrOjb\": {  // 当前商品的规格数据\n            \"key\": [\n                \"码数\",\n                \"颜色\"\n            ],\n            \"data\": {\n                \"码数\": [\n                    \"大\",\n                    \"中\",\n                    \"小\"\n                ],\n                \"颜色\": [\n                    \"黄\",\n                    \"红\"\n                ]\n            }\n        }\n    }\n}"
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 上传的文件api

POST /api/index/uploads

上传图片的api 需要表单提交数据 

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|prefix|query|string|false|前缀目录表示-商品goods-视频video|
|name|query|string|false|默认就是图片上传，如果是video则是视频|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 发起支付

GET /mall/index/pay

发起支付

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|id|query|string|true|当前商品id|
|num|query|string|true|购买的数量|
|pay_type|query|string|true|支付方式  1 微信支付  ，  2 余额支付|
|attr_id|query|string|true|当前商品的规格参数组 id  必须填写|
|wallet|query|string|false|用户是否使用购物券 0  不适用优惠券  1 使用|
|phone|query|string|true|地址中的手机号码|
|desc|query|string|false|用户所输入的订单描述文字说明|
|video_id|query|string|false|视频id用户中查看 视频 点击了立即购买跳转 需要 将参数一直携带过来|
|activity_id|query|string|false|活动id|
|puid|query|string|false|分享者的puid|
|activity_log_id|query|string|false|拼团活动加入的活动id|
|lives|query|string|false|如果有此字段则表示从直播中跳转过来的|
|address|query|string|false|收货地址详情|
|name|query|string|false|收货地址-用户名|
|coupon|query|string|false|是否使用优惠券1 表示使用|
|AUTHORIZATION|header|string|true|用户的token值|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 商品评论-添加

GET /mall/comment/add

需要用户token

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|id|query|string|true|商品id|
|level|query|string|true|评分星级|
|discuss|query|string|true|评论内容|
|cover|query|string|false|图片地址|
|oid|query|string|true|订单id不得为空|
|AUTHORIZATION|header|string|true|用户token|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 品牌介绍

GET /mall/index/getBrand

品牌介绍描述

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|id|query|string|true|品牌介绍id，  商品中的bid|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "品牌描述ok",
  "data": {
    "id": 1,
    "// id \"name\"": "测试品牌",
    "// 名称 \"cover\"": "https://admin.miaommei.com/uploads/goods_brand/20200611/9ea626f9bf515287c108cce24925c937.jpg",
    "// 轮播图 \"desc\"": "测试专用",
    "// 描述信息 \"sort\"": "0 // 排序"
  }
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 调用支付api统一下单

GET /mall/pay

调用支付api统一下单,此处是微信测试

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|amount|query|string|true|支付金额|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 商品规格

GET /mall/index/getGoodsAttr

商品规格列表

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|id|query|string|true|序号 -商品序号|
|Authorization|header|string|false|token值验证|

> 返回示例

> 成功

```json
"{\r\n    \"code\": 200,\r\n    \"msg\": \"规格数据返回\", // 消息\r\n    \"data\": [\r\n        {\r\n            \"id\": 266, // 序号 规格序号\r\n            \"attr_str\": \"颜色:黑金色\", // 规格参数\r\n            \"img\": \"https://admin-miaomeimei-1301812909.cos.ap-guangzhou.myqcloud.com/newUploads/product/goods/attrImg/2020_08_08/de46e72fc05d1b65561b727f8786d804.jpg\", // 图片\r\n            \"goods_id\": 105, // 商品序号\r\n            \"price\": \"159.00\" // 价格\r\n        },\r\n        {\r\n            \"id\": 267,\r\n            \"attr_str\": \"颜色:蓝白色\",\r\n            \"img\": \"https://admin-miaomeimei-1301812909.cos.ap-guangzhou.myqcloud.com/newUploads/product/goods/attrImg/2020_08_08/e32edeb44c27738b3463d14e647020bd.jpg\",\r\n            \"goods_id\": 105, \r\n            \"price\": \"159.00\"\r\n        }\r\n    ]\r\n}"
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 商品评论列表

GET /mall/comment/list

商品评论列表，和商城列表传递一样的数据

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|goods_id|query|string|false|商品id|
|limit|query|string|false|拿取的分页数据|
|page|query|string|false|默认第一页   |

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 支付页面详情

GET /mall/index/payData

需要携带用户的token进行传递

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|id|query|string|true|商品的id|
|num|query|string|true|当前商品购买的数量|
|attr_id|query|string|true|当前用户所选的规格组id|
|activity_id|query|string|false|当前用户所选的拼团活动id|
|video_id|query|string|false|当前用户看视频后跳转过来的视频id|
|AUTHORIZATION|header|string|true|用户的 token 必须要携带|

> 返回示例

> 成功

```json
"{\n    \"code\": 200,\n    \"msg\": \"支付详情返回成功\",\n    \"data\": {\n        \"id\": 1, \n        \"name\": \"测试商品1\",\n        \"title\": \"测试商品1\",\n        \"add_time\": 1591857882,\n        \"cover\": [\n            \"https://admin.miaommei.com/uploads/goods/20200611/3bc32d061cdedb6eddc1c8ac7120b893.jpg\",\n            \"https://admin.miaommei.com/uploads/goods/20200611/3817b9b6d21e27091025c725f0cf3d1e.jpg\"\n        ],\n        \"share\": \"https://admin.miaommei.com/uploads/goods_share/20200611/95d28b7ab959cb75a94449a4cf02f50d.jpg\",\n        \"ticket\": 30, // 购物券可以抵消的商品比例 结合attr中的discount_price值\n        \"freight\": 10.00, // 运费\n        \"attr\": { // 当前用户所选择的规格\n            \"id\": 2,\n            \"cost_price\": \"70.00\",\n            \"price\": \"90.00\",\n            \"discount_price\": \"85.00\",\n            \"stock\": 100,\n            \"live_price\": \"80.00\",\n            \"integral\": 0,\n            \"sale\": 0,\n            \"add_time\": 1591857882,\n            \"is_delete\": 0,\n            \"valjson\": [], // 规格值\n            \"img\": \"\" // 图片\n        },\n        \"adders\": { // 用户的默认地址  if\n            \"id\": 1,\n            \"uid\": 1,\n            \"province\": \"广东\",\n            \"city\": \"广州\",\n            \"area\": \"白云\",\n            \"address\": \"元下田美食广场\",\n            \"tel\": \"13800000001\",\n            \"name\": \"david\",\n            \"is_default\": 1,\n            \"add_time\": 1592221049,\n            \"is_delete\": 0,\n            \"sort\": 0\n        },\n        \"wallet\": 20, // 购物券余额\n        \"wallet_money\": 20 // 购物券抵扣的金额\n    }\n}"
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 商城-商品列表

POST /mall/index/malls

商品列表- 后面携带一个参数type  表示分类

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|type|query|string|false|0表示 全部商品 1  表示具体哪类|
|limit|query|string|false|5表示查询的数据|
|page|query|string|false|默认1 表示 分页|
|name|query|string|false|搜索的名称|
|orderKey|query|string|false|排序值 add_time表示时间，sales_volume销量,default_discount_price价格|
|order|query|string|false|排序的升序或者降序  desc  升  ，asc降|
|is_hot|query|string|false|1表示精选，0表示不是精选|
|bid|query|string|false|品牌id需要大于0|
|is_explosion|query|string|false|1 表示 爆款商品|
|is_welfare|query|string|false|1 表示福利商品|
|is_new|query|string|false|1 表示新品|
|AUTHORIZATION|header|string|false|用户token携带进来会返回分享赚的商品信息|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "商品ok",
  "data": {
    "total": 4,
    "per_page": 15,
    "current_page": 1,
    "last_page": 1,
    "data": [
      {
        "id": 3,
        "// id \"name\"": "测试222",
        "// 商品名称 \"share\"": "http://admin.miaommei.com/uploads/goods_share/20200630/5bc9770e45fff29d59056a525e2fd63b.jpg",
        "// 图片 \"title\"": "快来购买",
        "// 标题 描述 \"default_price\"": 150,
        "// 原价 \"default_discount_price\"": 120,
        "// 折扣价 \"sales_volume\"": 1999,
        "// 销量 \"sort\"": 9999,
        "// 排序 ，已经拍好序号了 \"share_money\"": "12 // 此处前端需要判断是否有分享价 只有登录 并且 会员等级有才会显示分享价"
      }
    ]
  }
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 计算价格

GET /mall/index/sumTotal

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|id|query|string|true|商品id|
|attr_id|query|string|true|商品规格id|
|num|query|string|true|数量|
|wallet|query|string|false|是否使用购物券   1  使用  2 不使用|
|lives|query|string|false|表示 直播跳转商品|
|coupon|query|string|false|优惠券id|
|Authorization|header|string|true|用户token值|

> 返回示例

> 成功

```json
"{\r\n    \"code\": 200,\r\n    \"msg\": \"计算ok\",\r\n    \"data\": {\r\n        \"sum_money\": \"128.00\", // 总支付金额\r\n        \"money\": \"19.20\",  // 实际支付金额\r\n        \"wallet\": \"64.00\", // 购物券抵扣金额\r\n        \"wallet_money\": \"100.00\",  // 购物券余额\r\n        \"purchase\": \"44.80\" // 自购省抵扣金额\r\n    }\r\n}"
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 商城轮播图

GET /view/index/ProductBanner

商城轮播图

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "商城轮播图ok",
  "data": [
    {
      "id": 4,
      "// id 4 \"img_url\"": "https://admin.miaommei.com/newUploads//bannerLive/20200709/39dbf2b13dfb3ca7eea7ec41d8e06bfa.jpg",
      "// 图片地址 \"title\"": "测试",
      "// 图片标题 \"is_skip\"": 1,
      "// 是否跳转 ，如果跳转为 \"goods_id\"": "0 // 商品的id 跳转"
    }
  ]
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 商城首页-爆款、福利、新品

GET /mall/index/shopHomeList

商城首页-爆款、福利、新品

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|false|none|

> 返回示例

> 成功

```json
"{\r\n    \"code\": 200,\r\n    \"msg\": \"爆款、福利、新品\",\r\n    \"data\": {\r\n        \"explosionList\": [ // 爆款商品\r\n            {\r\n                \"id\": 353, // 序号\r\n                \"name\": \"freeplus芙丽芳丝洗面奶氨基酸系净润洗面霜男女温和清洁敏感肌\", // 名称\r\n                \"share\": \"https://admin-miaomeimei-1301812909.cos.ap-guangzhou.myqcloud.com/newUploads/product/goods/share/2020_08_31/1598853842_836.1\", // 图片\r\n                \"title\": \"freeplus芙丽芳丝洗面奶氨基酸系净润洗面霜男女温和清洁敏感肌\", // 标题\r\n                \"default_price\": \"155.00\", // 划线价\r\n                \"default_discount_price\": \"109.00\", // 商品价\r\n                \"min_money\": 109, // 最小价格\r\n                \"max_money\": 109, //  最大价格\r\n                \"sort\": 99, // 排序\r\n                \"is_activity\": 0, // 是否拼团活动\r\n                \"sales_volume\": 952, // 销量\r\n                \"share_money\": \"7.63\" // 需要if 分享金额\r\n            },\r\n            {\r\n                \"id\": 126,\r\n                \"name\": \"Za隔离霜防晒遮瑕三合一妆前打底乳姬芮官网正品李佳奇推荐\",\r\n                \"share\": \"https://admin-miaomeimei-1301812909.cos.ap-guangzhou.myqcloud.com/newUploads/product/goods/share/2020_09_04/1599206990_378.jpg\",\r\n                \"title\": \"Za隔离霜防晒遮瑕三合一妆前打底乳姬芮官网正品李佳奇推荐\",\r\n                \"default_price\": \"88.00\",\r\n                \"default_discount_price\": \"58.00\",\r\n                \"min_money\": 58,\r\n                \"max_money\": 58,\r\n                \"sort\": 98,\r\n                \"is_activity\": 0,\r\n                \"sales_volume\": 708,\r\n                \"share_money\": \"4.06\"\r\n            },\r\n            {\r\n                \"id\": 254,\r\n                \"name\": \"御泥坊绿豆泥浆面膜深层清洁收缩毛孔去黑头补水控油涂抹泥膜正品\",\r\n                \"share\": \"https://admin-miaomeimei-1301812909.cos.ap-guangzhou.myqcloud.com/newUploads/product/goods/share/2020_08_31/1598870624_122.jpg\",\r\n                \"title\": \"御泥坊绿豆泥浆面膜深层清洁收缩毛孔去黑头补水控油涂抹泥膜正品\",\r\n                \"default_price\": \"69.90\",\r\n                \"default_discount_price\": \"0.70\",\r\n                \"min_money\": 70,\r\n                \"max_money\": 70,\r\n                \"sort\": 96,\r\n                \"is_activity\": 0,\r\n                \"sales_volume\": 1258,\r\n                \"share_money\": \"0.04\"\r\n            }\r\n        ],\r\n        \"welfareList\": [ // 福利商品\r\n            {\r\n                \"id\": 273,\r\n                \"name\": \"凡士林经典修护润唇膏玫瑰花蕾味滋润唇部嘴唇男女护唇膏保湿正品\",\r\n                \"share\": \"https://admin-miaomeimei-1301812909.cos.ap-guangzhou.myqcloud.com/newUploads/product/goods/share/2020_08_15/1597458058_710.jpg\",\r\n                \"title\": \"凡士林经典修护润唇膏玫瑰花蕾味滋润唇部嘴唇男女护唇膏保湿正品\",\r\n                \"default_price\": \"39.00\",\r\n                \"default_discount_price\": \"19.00\",\r\n                \"min_money\": 19,\r\n                \"max_money\": 19,\r\n                \"sort\": 91,\r\n                \"is_activity\": 0,\r\n                \"sales_volume\": 512,\r\n                \"share_money\": \"1.33\"\r\n            },\r\n            {\r\n                \"id\": 241,\r\n                \"name\": \"自然堂水润保湿柔肤水爽肤水补水保湿控油滋润清透锁水护肤品 \",\r\n                \"share\": \"https://admin-miaomeimei-1301812909.cos.ap-guangzhou.myqcloud.com/newUploads/product/goods/share/2020_08_08/d1d01c1ef6511cb60e132f25ee80e136.jpg\",\r\n                \"title\": \"自然堂水润保湿柔肤水爽肤水补水保湿控油滋润清透锁水护肤品\",\r\n                \"default_price\": \"99.00\",\r\n                \"default_discount_price\": \"79.00\",\r\n                \"min_money\": 79,\r\n                \"max_money\": 79,\r\n                \"sort\": 90,\r\n                \"is_activity\": 0,\r\n                \"sales_volume\": 1023,\r\n                \"share_money\": \"5.53\"\r\n            }\r\n        ],\r\n        \"newList\": [ // 新品商品\r\n            {\r\n                \"id\": 343,\r\n                \"name\": \"coco zeusee洗发水护发素沐浴露3件套香水洗护持久留香\",\r\n                \"share\": \"https://admin-miaomeimei-1301812909.cos.ap-guangzhou.myqcloud.com/newUploads/product/goods/share/2020_09_14/1600054573_227.jpg\",\r\n                \"title\": \"coco zeusee洗发水护发素沐浴露3件套香水洗护持久留香\",\r\n                \"default_price\": \"139.00\",\r\n                \"default_discount_price\": \"87.00\",\r\n                \"min_money\": 87,\r\n                \"max_money\": 87,\r\n                \"sort\": 93,\r\n                \"is_activity\": 0,\r\n                \"sales_volume\": 223,\r\n                \"share_money\": \"6.09\"\r\n            },\r\n            {\r\n                \"id\": 239,\r\n                \"name\": \"雅漾舒泉调理喷雾300ml 大喷舒缓敏感定妆喷雾爽肤水保湿补水\",\r\n                \"share\": \"https://admin-miaomeimei-1301812909.cos.ap-guangzhou.myqcloud.com/newUploads/product/goods/share/2020_08_08/c219367eb54bab9b5adab494e2534fd6.jpg\",\r\n                \"title\": \"雅漾舒泉调理喷雾300ml 大喷舒缓敏感定妆喷雾爽肤水保湿补水\",\r\n                \"default_price\": \"168.00\",\r\n                \"default_discount_price\": \"116.00\",\r\n                \"min_money\": 116,\r\n                \"max_money\": 116,\r\n                \"sort\": 93,\r\n                \"is_activity\": 0,\r\n                \"sales_volume\": 2597,\r\n                \"share_money\": \"8.12\"\r\n            }\r\n        ]\r\n    }\r\n}"
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 用户支付成功后的回调操作

GET /mall/notify

用户支付成功后的回调操作

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 商城-商品分类

POST /mall/mallType

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|type|query|string|false|默认0 获取一级分类 具体 将一级分类的下面的id传递过来即可获取到下级|
|Authorization|header|string|false|none|

> 返回示例

> 成功

```json
"{\n    \"code\": 200,\n    \"msg\": \"商城分类ok\",\n    \"data\": [\n        {\n            \"id\": 1, // id\n            \"name\": \"品牌热卖\", // 分类名称\n            \"cover\": \"https://admin.miaommei.com/uploads/goods_type/20200630/004aa16e73d40fc4ef8fc94d6b1f6785.jpg\" // 分类图\n        }\n    ]\n}"
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 拼团活动列表

GET /mall/activity/list

拼团活动列表  和 商品列表一样  有分页 和多少条记录  limit  和  page

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "拼团活动列表ok",
  "data": {
    "total": 1,
    "// 总记录 \"per_page\"": 10,
    "// 当前拿取数据数量 \"current_page\"": 1,
    "// 当前页 \"last_page\"": 1,
    "data": [
      {
        "id": 2,
        "name": "来来",
        "cover": "https://admin.miaommei.com/uploads/activity/20200711/de1431435ab1661a41cbf0386a76cf84.jpg",
        "// 图片 \"rule\"": "&lt;p&gt;阿三顶顶顶顶顶顶顶顶顶顶&lt;/p&gt;",
        "// 详情 \"sort\"": 66,
        "// 排序 \"hours\"": 1594635989,
        "// 活动结束时间 \"title\"": "测试ok",
        "share": "https://admin.miaommei.com/uploads/activity/20200711/dbdcea16fea276855a2266bbf70bd1f2.jpg",
        "// 分享图片 \"num\"": 3,
        "// 当前开团人数 \"type\"": 2,
        "// 类型  1 表示金额  2 表示百分比 需要判断 \"content\"": [
          "// 第几个人拿取的 佣金  第一个  3  第二个 6  （需要迭代） \"3\"",
          "6",
          "9"
        ],
        "end_time": 1594457565,
        "// 活动结束时间  ， 活动列表都必须有的 ， 如果大于当前时间 那么显示  已结束， 如果 小于 则显示倒计时 \"add_time\"": 1594457565
      }
    ]
  }
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 活动规则

GET /mall/activity/getRule

获取活动规则  需要传入 活动id

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|id|query|string|true|活动id必须填写|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "规则ok",
  "data": {
    "id": 15,
    "rule": "<p style=\"text-indent: 0em;\">用户购买，分享链接给好友，就能得到奖励啦。</p><p style=\"text-indent: 0em;\">分享给一个好友，可以拿到商品总价15%的奖励；</p><p style=\"text-indent: 0em;\">分享给两个好友，可以拿到商品总价30%的奖励；</p><p style=\"text-indent: 0em;\">分享给三个好友，可以拿到商品总价55%的奖励；</p><p style=\"text-indent: 0em;\">也就是免单啦！</p><p style=\"text-indent: 0em;\">小伙伴们抓紧时间分享起来吧~</p><p style=\"text-indent: 0em;\">（分享好友需要用户先下单才能拿到奖励哦；用户没有购买，也可以分享，只不过没有奖励，所以记得自己也要购买哦~）</p>"
  }
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

# 用户模板-user

## POST 我的页面-取消关注

POST /user/video/delFollow

必传参数
Headers:Authorization
uid：关注的用户的id

> Body 请求参数

```yaml
type: object
properties:
  uid:
    type: string
    description: 关注的用户的id
    example: "2"

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|
|» uid|body|string|false|关注的用户的id|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": true
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 钱包页面-团队奖励

POST /user/index/teamReward

必传参数
Headers:Authorization

> Body 请求参数

```yaml
type: object
properties: {}

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": {
    "今日\"day\"": "0.00",
    "昨天\"yesterday\"": "0.00",
    "本周\"week\"": "0.00",
    "本月\"month\"": "0.00",
    "累计喵币\"total\"": "0.00",
    "我的客户\"invite_num\"": 2
  }
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 添加收货地址（7/22）

POST /user/branch/addAddress

必传参数
Headers:Authorization
2种情况：
有id则非必传，无id则为必传
id：收货地址id，编辑时必传
tel：手机号
name：收货人姓名
address：详细收货地址
province_sn：省号
province：省名
city_sn：市号
city：市名
area_sn：区号
area：区名

> Body 请求参数

```yaml
type: object
properties:
  province_sn:
    type: string
    description: "省号 "
    example: "110000"
  province:
    type: string
    description: 省名
    example: 北京市
  city_sn:
    type: string
    description: 市号
    example: "110100"
  city:
    type: string
    description: 市名
    example: 北京市
  area_sn:
    type: string
    description: 区号
    example: "110105"
  area:
    type: string
    description: 区名
    example: 朝阳区
  address:
    type: string
    description: 详细地址
    example: 朝天椒
  tel:
    type: string
    description: 手机号
    example: "13500135001"
  name:
    type: string
    description: 收货人姓名
    example: 渣渣辉
  id:
    type: string
    description: 收货地址id
    example: "8"
  is_default:
    type: string
    description: 是否默认 0-不默认 1默认
    example: "0"

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|
|» province_sn|body|string|false|省号|
|» province|body|string|false|省名|
|» city_sn|body|string|false|市号|
|» city|body|string|false|市名|
|» area_sn|body|string|false|区号|
|» area|body|string|false|区名|
|» address|body|string|false|详细地址|
|» tel|body|string|false|手机号|
|» name|body|string|false|收货人姓名|
|» id|body|string|false|收货地址id|
|» is_default|body|string|false|是否默认 0-不默认 1默认|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": true
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 喵币页面-兑换记录

POST /user/index/getMiaoList

必传参数
Headers:Authorization
非必传参数
time：2020-6  按月份筛选 #默认当前月份

> Body 请求参数

```yaml
type: object
properties:
  time:
    type: string
    description: "按月份筛选 #默认当前月份"
    example: 2020-7

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|
|» time|body|string|false|按月份筛选 #默认当前月份|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": {
    "total": 1,
    "per_page": 10,
    "current_page": 1,
    "last_page": 1,
    "data": [
      {
        "balance": "1.00",
        "add_time": "2020-07-10 20:23",
        "type": "喵币兑换"
      }
    ]
  }
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 钱包页面-视频收益（7/18）

POST /user/index/videoReward

必传参数
Headers:Authorization

> Body 请求参数

```yaml
type: object
properties: {}

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 绑定手机号

POST /user/index/bindingPhone

必传参数
Headers:Authorization
encryptedData：微信返回
iv：微信返回

> Body 请求参数

```yaml
type: object
properties:
  encryptedData:
    type: string
    example: 12PyqMpNcGjKBm67uc2x6D+DAKn4LESwkqe2C3SAiLa/KaSJt9R07XSni2f+K3ozYrrc2Kj/Yhy+rFPgKHHbzpwIqKdEJ+2tkwiO73UtrAwlSAUOYUeQzY12pUfvUKf4O55PggXnz6gqUqe12WVNFjeD4qWlwelLDhULjlTXQgpcalub3f9C5hzEwghGYguIejx71crv+LKq7veu7O8Mpw==
  iv:
    type: string
    example: Umx8qDDBFrZ2ttIDceUruQ==

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|
|» encryptedData|body|string|false|none|
|» iv|body|string|false|none|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 添加银行卡-校验

POST /user/branch/checkCard

必传参数
Headers:Authorization
card： 银行卡号

> Body 请求参数

```yaml
type: object
properties:
  card:
    type: string
    description: 银行卡号
    example: "6214832050000000"

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|
|» card|body|string|false|银行卡号|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": {
    "bank_name": "招商银行",
    "type": "储蓄卡",
    "card": "6214832050000000"
  }
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 添加银行卡-orc校验

POST /user/branch/orcBank

必传参数
Headers:Authorization
img：图片
type：文件类型

> Body 请求参数

```yaml
type: object
properties:
  img:
    type: string
    description: 图片
  type:
    type: string
    description: 文件类型
    example: image
required:
  - img
  - type

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|
|» img|body|string|true|图片|
|» type|body|string|true|文件类型|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": {
    "bank_name": "中国工商银行",
    "type": "储蓄卡",
    "card": "6212263602058243254"
  }
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 喵币兑换（7/22）

POST /user/branch/exchangeMiao

必传参数
Headers:Authorization
miao：喵币必须100的整数倍

> Body 请求参数

```yaml
type: object
properties:
  miao:
    type: string
    description: 喵币必须100的整数倍
    example: "101"

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|
|» miao|body|string|false|喵币必须100的整数倍|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": true
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 我的页面-钱包

POST /user/index/myWallet

必传参数
Headers:Authorization

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": {
    "balance": "1.11",
    "miao": "0.00"
  }
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 获取用户特权（7/24）

POST /user/index/getLevelPower

必传参数
Headers:Authorization

> Body 请求参数

```yaml
type: object
properties: {}

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": {
    "升级所需经验\"level\"": 300000,
    "等级名称\"name\"": "资深掌柜",
    "等级说明\"desc\"": "新晋掌柜折扣商品,新晋掌柜折扣自购/分享奖励,新晋掌柜折扣商品11111,新晋掌柜折扣商品22222",
    "个人消费\"consume\"": 2000,
    "团队消费\"teams\"": 352000
  }
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 所有等级规则说明

GET /view/index/ruleSpecifications

获取到所有的等级规则等
video	视频收益-累计喵币
group	累计喵币-拼团奖励
share	累计喵币-分享奖励
team	累计喵币-团队奖励
volume	购物券-购物券余额
personal	掌柜特权-个人消费
team_consumption	掌柜特权-团队消费
hierarchical_rules	掌柜特权-等级规则
withdraw	提现-提现说明
meow_coin_exchange	喵币兑换-兑换说明
invitation_code	如何获取邀请码
voucher_privilege  购物券特权规则
shop_agreement    用户协议-店铺

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|key|query|string|true|说明匹配文字|

> 返回示例

> 成功

```json
"{\n    \"code\": 200,\n    \"msg\": \"拿取说明ok\",\n    \"data\": {\n        \"id\": 1, // id\n        \"desc\": \"视频收益-累计喵币\", // 描述\n        \"val\": \"<p>sdaaaaaaaaaaaaaaaaaaaa<img src=\\\"/uploads/ueditor/img/20200728/1595929547850928.jpg\\\" title=\\\"1595929547850928.jpg\\\" alt=\\\"u=2534506313,1688529724&fm=26&gp=0.jpg\\\"/></p><p>啊实打实大苏打实打实大苏打实打实大苏打</p><p><br/></p><p>敖德萨所多撒多</p><p>sdasad</p><p>dd</p><p>dd</p><p><br/></p>\", // 值\n        \"type\": 1, // 类型 1 表示 富文本   0 表示纯文本   需要if\n        \"title\": \"视频收益\" // 标题\n    }\n    \n    /*\n    * 键值 说明如下：\n    video\t视频收益-累计喵币\ngroup\t累计喵币-拼团奖励\nshare\t累计喵币-分享奖励\nteam\t累计喵币-团队奖励\nvolume\t购物券-购物券余额\npersonal\t掌柜特权-个人消费\nteam_consumption\t掌柜特权-团队消费\nhierarchical_rules\t掌柜特权-等级规则\nwithdraw\t提现-提现说明\nmeow_coin_exchange\t喵币兑换-兑换说明\n        invitation_code\t如何获取邀请码\n    */\n}"
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 钱包页面-分享奖励/拼团奖励

POST /user/index/sharingRewards

必传参数
Headers:Authorization
type：4    #2-拼团奖励  4-分享奖励
#还差分享订单

> Body 请求参数

```yaml
type: object
properties:
  type:
    type: string
    description: "type：4    #2-拼团奖励  4-分享奖励"
    example: "4"
required:
  - type

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|
|» type|body|string|true|type：4    #2-拼团奖励  4-分享奖励|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": {
    "累计喵币\"total\"": "1.00",
    "预估喵币\"estimate\"": "0.10",
    "待结算喵币\"settlement\"": "0.00",
    "无效喵币\"invalid\"": "0.00"
  }
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 获取活动浏览记录

POST /user/branch/getActivityList

必传参数
Headers:Authorization
aid：活动id

> Body 请求参数

```yaml
type: object
properties:
  aid:
    type: string
    example: "15"
required:
  - aid

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|
|» aid|body|string|true|none|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 钱包页面-余额

POST /user/index/getBalances

必传参数
Headers:Authorization
非必传参数
type 1-收入 2-支出  不传则返回收入数据
page：1 分页   默认1
limit ：15 每页展示数据数    默认15

> Body 请求参数

```yaml
type: object
properties:
  type:
    type: string
    description: 类型 1为收入 2为支出，默认返回收入
    example: "1"
  page:
    type: string
    description: 分页   默认1
    example: "1"
  limit:
    type: string
    description: 每页展示数据数    默认15
    example: "15"

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|
|» type|body|string|false|类型 1为收入 2为支出，默认返回收入|
|» page|body|string|false|分页   默认1|
|» limit|body|string|false|每页展示数据数    默认15|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": {
    "wallet": {
      "balance": "0.00",
      "miao": "0.00",
      "give_balance": "0.00"
    },
    "balanceList": {
      "total": 2,
      "per_page": 15,
      "current_page": 1,
      "last_page": 1,
      "data": [
        {
          "uid": 1,
          "type": "喵币兑换1",
          "balance": "1.00",
          "add_time": "2020-07-10 20:23"
        },
        {
          "uid": 1,
          "type": "退款 (订单号:20200618135700)",
          "balance": "0.10",
          "add_time": "2020-07-06 11:37"
        }
      ]
    }
  }
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 我的页面-我的活动（7/18）

POST /user/branch/myActivity

必传参数
Headers:Authorization

> Body 请求参数

```yaml
type: object
properties:
  astatus:
    type: string
    description: "//  -1全部   0 进行中 ，1 已完成  2  已结束（超时）  "
    example: "-1"

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|astatus|query|string|true|//  -1全部   0 进行中 ，1 已完成  2  已结束（超时）  |
|Authorization|header|string|true|token|
|body|body|object|false|none|
|» astatus|body|string|false|//  -1全部   0 进行中 ，1 已完成  2  已结束（超时）|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": {
    "total": 2,
    "per_page": 15,
    "current_page": 1,
    "last_page": 1,
    "data": [
      {
        "活动id\"id\"": 1,
        "我的活动id\"log_id\"": 2,
        "订单id\"oid\"": 7,
        "num": 3,
        "nums": 0,
        "活动名\"name\"": "测试活动",
        "活动标题\"title\"": "测试活动分享",
        "活动图片\"cover\"": "/uploads/activity/20200618/7e2a2279927510bb24864d3f78f6bf86.jpg",
        "分享图片\"share\"": "/uploads/activity/20200618/e6a21b98c62bc78e50965de2ad80a2a8.jpg",
        "价格\"price\"": 6000,
        "状态\"astatus\"": 2,
        "//0-进行中 1-已完成 2-已结束 \"add_time\"": 1592460102,
        "结束时间戳\"end_time\"": 1592467302
      },
      {
        "id": 1,
        "log_id": 1,
        "oid": 6,
        "num": 3,
        "nums": 2,
        "name": "测试活动",
        "title": "测试活动分享",
        "cover": "/uploads/activity/20200618/7e2a2279927510bb24864d3f78f6bf86.jpg",
        "share": "/uploads/activity/20200618/e6a21b98c62bc78e50965de2ad80a2a8.jpg",
        "price": 6000,
        "astatus": 2,
        "// 0 进行中 ，1 已完成  2  已结束（超时） \"add_time\"": 1592459939,
        "end_time": 1592467139
      }
    ]
  }
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 提现-申请提现

POST /user/branch/addWithdrawal

必传参数
Headers:Authorization
amount：金额
bank_id：银行卡id

> Body 请求参数

```yaml
type: object
properties:
  amount:
    type: string
    description: 金额
    example: "100"
  bank_id:
    type: string
    description: 银行卡id
    example: "1"

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|
|» amount|body|string|false|金额|
|» bank_id|body|string|false|银行卡id|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": true
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 提现-绑定银行卡列表

POST /user/branch/getBankList

必传参数
Headers:Authorization

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 我的页面-确认绑定邀请人

POST /user/index/bindInviteCode

必传参数
Headers:Authorization
code：XI1W0EE  邀请码

> Body 请求参数

```yaml
type: object
properties:
  code:
    type: string
    description: 邀请码
    example: XI1W0EE
required:
  - code

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|
|» code|body|string|true|邀请码|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": []
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 个人卡片分享海报

POST /api/index/myCardShare

必传参数
Headers:Authorization

> Body 请求参数

```yaml
type: object
properties: {}

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": {
    "title": "邀请你加入美妆说分享赚",
    "share_img": "https://admin.miaommei.com/newUploads//view/poster/20200811/95a3af250b0d00639ac074c681d07cdc.jpeg"
  }
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 我的收货地址（7/22）

POST /user/branch/myAddrList

必传参数
Headers:Authorization
非必传
id：传id则获取地址详情

> Body 请求参数

```yaml
type: object
properties:
  id:
    type: string
    example: "1"

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|
|» id|body|string|false|none|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": [
    {
      "详细地址\"address\"": "元下田美食广场",
      "收货人手机\"tel\"": "13800000001",
      "收货人姓名\"name\"": "david",
      "是否默认\"is_default\"": 1,
      "add_time": 1592221049,
      "省\"province\"": "广东",
      "市\"city\"": "广州",
      "区\"area\"": "白云"
    }
  ]
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 喵币页面-历史概况

POST /user/index/historyList

必传参数
Headers:Authorization
非必传参数
type：1-视频任务 2-拼团奖励 3-团队奖励 4-分享奖励  默认获取全部类型 （1-4）
date：2020 年份  默认为今年

> Body 请求参数

```yaml
type: object
properties:
  type:
    type: string
    description: type：1-视频任务 2-拼团奖励 3-团队奖励 4-分享奖励  默认获取全部类型
    example: "1"
  date:
    type: string
    description: date：2020 年份  默认为今年
    example: "2020"

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|
|» type|body|string|false|type：1-视频任务 2-拼团奖励 3-团队奖励 4-分享奖励  默认获取全部类型|
|» date|body|string|false|date：2020 年份  默认为今年|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": {
    "One": "0.00",
    "Two": "0.00",
    "Three": "0.00",
    "Four": "0.00",
    "Five": "0.00",
    "Six": "0.10",
    "Seven": "1.00",
    "Eight": "0.00",
    "Nine": "0.00",
    "Ten": "0.00",
    "Eleven": "0.00",
    "Twelve": "10.00"
  }
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 我的页面-我的收藏（7/18）

POST /user/index/myCollect

必传参数
Headers:Authorization

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": {
    "total": 2,
    "per_page": 15,
    "current_page": 1,
    "last_page": 1,
    "data": [
      {
        "id": 3,
        "商品名\"name\"": "测试222",
        "分享标题\"title\"": "快来购买",
        "分享图片\"share\"": "/uploads/goods_share/20200630/5bc9770e45fff29d59056a525e2fd63b.jpg",
        "商品状态\"status\"": 1,
        "//商品状态，1-上架，2-下架，3-缺货，4-预售，5-禁售 折扣价\"default_discount_price\"": 120,
        "原价\"default_price\"": 150,
        "add_time": 1594884484
      },
      {
        "id": 20,
        "商品名\"name\"": "护肤",
        "分享标题\"title\"": "测试",
        "分享图片\"share\"": "/uploads/goods_share/20200702/d83901219850a1d4b71d19a2481fc334.jpg",
        "商品状态\"status\"": 1,
        "折扣价\"default_discount_price\"": 0,
        "原价\"default_price\"": 0,
        "add_time": 1594884484
      }
    ]
  }
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 我的页面-添加关注

POST /user/video/addFollow

必传参数
Headers:Authorization
uid： 关注的用户的id

> Body 请求参数

```yaml
type: object
properties:
  uid:
    type: string
    description: 用户id
    example: "2"

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|
|» uid|body|string|false|用户id|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": true
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 我的页面-我的客户

POST /user/index/myCustomer

必传参数
Headers:Authorization
非必传
nickname： 按昵称查询
level：0   会员等级1-体验掌柜 2-新晋掌柜 3-资深掌柜 4-精英掌柜 5-超级掌柜   默认0为全部等级
page：1 分页   默认1
limit ：15 每页展示数据数    默认15

> Body 请求参数

```yaml
type: object
properties:
  page:
    type: string
    description: 分页   默认1
    example: "1"
  limit:
    type: string
    description: 每页展示数据数    默认15
    example: "15"
  level:
    type: string
    description: 会员等级1-体验掌柜 2-新晋掌柜 3-资深掌柜 4-精英掌柜 5-超级掌柜   默认0为全部等级
    example: "0"
  nickname:
    type: string
    description: 按昵称查询
    example: 测试

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|
|» page|body|string|false|分页   默认1|
|» limit|body|string|false|每页展示数据数    默认15|
|» level|body|string|false|会员等级1-体验掌柜 2-新晋掌柜 3-资深掌柜 4-精英掌柜 5-超级掌柜   默认0为全部等级|
|» nickname|body|string|false|按昵称查询|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": {
    "invite_number": 2,
    "//我的客户 \"order_number\"": 5,
    "//订单总数 \"order_total\"": "1100.00",
    "//订单总额 \"invite_order_num\"": 1,
    "//下单客户 \"list\"": {
      "total": 2,
      "per_page": 15,
      "current_page": 1,
      "last_page": 1,
      "data": [
        {
          "id": 2,
          "头像\"avatarurl\"": "https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLwFZscibqGJOjicqv3OINUEYY0CgVDfWPNicrtM5lW7BveTicK2iaj5OTFBML9IcibxJTgQb3rsI3h0KBA/132",
          "昵称\"nickname\"": "测试2",
          "等级\"level\"": "新晋掌柜",
          "时间\"binding_time\"": "2020-07-10 18:15",
          "自购订单数\"my_order_num\"": 2,
          "客户数量\"invite_number\"": 0,
          "客户订单数量\"invite_order_num\"": 1,
          "自购订单金额\"my_order_total\"": "520.00",
          "客户订单金额\"invite_order_total\"": "420.00",
          "订单总数\"order_num\"": 3,
          "订单总额\"order_total\"": "940.00"
        },
        {
          "id": 54,
          "avatarurl": "https://wx.qlogo.cn/mmopen/vi_32/HZ9saP9Z5cBaVwJNicaXacpacXXuXlcGgANWCExicjQB8ppwL32G5ffASP46Hp4WichZ5dROiayBcKBCgyiahwgNRicA/132",
          "nickname": "红尘痴子",
          "level": "游客",
          "binding_time": "2020-07-10 18:27",
          "invite_number": 0,
          "my_order_num": 0,
          "invite_order_num": 0,
          "my_order_total": "0.00",
          "invite_order_total": "0.00",
          "order_num": 0,
          "order_total": "0.00"
        },
        {
          "id": 54,
          "avatarurl": "https://wx.qlogo.cn/mmopen/vi_32/HZ9saP9Z5cBaVwJNicaXacpacXXuXlcGgANWCExicjQB8ppwL32G5ffASP46Hp4WichZ5dROiayBcKBCgyiahwgNRicA/132",
          "nickname": "红尘痴子",
          "level": "游客",
          "binding_time": "2020-07-10 18:27",
          "my_order_num": 0,
          "invite_order_num": 0,
          "my_order_total": "0.00",
          "invite_order_total": "0.00",
          "order_num": 0,
          "order_total": "0.00"
        }
      ]
    }
  }
}
```

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": {
    "invite_number": 2,
    "//我的客户 \"order_number\"": 5,
    "//订单总数 \"order_total\"": "1100.00",
    "//订单总额 \"invite_order_num\"": 1,
    "//下单客户 \"list\"": {
      "total": 2,
      "per_page": 15,
      "current_page": 1,
      "last_page": 1,
      "data": [
        {
          "id": 54,
          "头像\"avatarurl\"": "https://wx.qlogo.cn/mmopen/vi_32/HZ9saP9Z5cBaVwJNicaXacpacXXuXlcGgANWCExicjQB8ppwL32G5ffASP46Hp4WichZ5dROiayBcKBCgyiahwgNRicA/132",
          "昵称\"nickname\"": "红尘痴子",
          "等级\"level\"": "游客",
          "时间\"binding_time\"": "2020-07-10 18:27",
          "客户数量\"invite_number\"": 0,
          "自购订单数\"my_order_num\"": 0,
          "客户订单数\"invite_order_num\"": 0,
          "自购订单金额\"my_order_total\"": "0.00",
          "客户订单金额\"invite_order_total\"": "0.00",
          "订单总数\"order_num\"": 0,
          "订单总额\"order_total\"": "0.00"
        },
        {
          "id": 2,
          "avatarurl": "https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLwFZscibqGJOjicqv3OINUEYY0CgVDfWPNicrtM5lW7BveTicK2iaj5OTFBML9IcibxJTgQb3rsI3h0KBA/132",
          "nickname": "测试2",
          "level": "新晋掌柜",
          "binding_time": "2020-07-10 18:15",
          "invite_number": 2,
          "my_order_num": 2,
          "invite_order_num": 1,
          "my_order_total": "520.00",
          "invite_order_total": "420.00",
          "order_num": 3,
          "order_total": "940.00"
        }
      ]
    }
  }
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 我的页面-购物券

POST /user/branch/getVolume

必传参数
Headers:Authorization

> Body 请求参数

```yaml
type: object
properties: {}

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|

> 返回示例

> 成功

```json
{
  "code": 200,
  "//已购买购物券 \"msg\"": "操作成功",
  "data": {
    "volume_balance": "50.00",
    "//购物券余额 \"list\"": {
      "total": 2,
      "per_page": 15,
      "current_page": 1,
      "last_page": 1,
      "data": [
        {
          "order_sn": "20315131032135",
          "balance": "100.00",
          "add_time": "2020-06-16 09:27",
          "type": "1 //充值  balance为正数值"
        },
        {
          "order_sn": "20200615200000",
          "balance": "50.00",
          "add_time": "2020-06-15 19:50",
          "type": "2 //消费  balance为负数值"
        }
      ]
    }
  }
}
```

```json
{
  "code": 200,
  "//未购买购物券 \"msg\"": "操作成功",
  "data": {
    "volume": "1000",
    "//购物券数值 \"miao\"": 44,
    "//分享得喵币 \"is_binding\"": "1 //是否绑定邀请码，1则可以购买购物券，0则跳转到绑定邀请码页面"
  }
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 我的活动-活动详情（7/21）

POST /user/branch/myActivityInfo

必传参数
Headers:Authorization

> Body 请求参数

```yaml
type: object
properties:
  log_id:
    type: string
    example: "1"

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|
|» log_id|body|string|false|none|

> 返回示例

> 成功

```json
"{\n    \"code\": 200,\n    \"msg\": \"操作成功\",\n    \"data\": {\n        \"num\": 3, //总共可邀请人数\n        \"nums\": 2, //已邀请人数\n        \"name\": \"测试222\", //商品名称\n        \"title\": \"快来购买\", //活动标题\n        \"share\": \"/uploads/goods_share/20200630/5bc9770e45fff29d59056a525e2fd63b.jpg\", //商品分享图片\n        \"json\": {\n            \"good_attr\": { //商品规格\n                \"id\": 7,\n                \"img\": \"\",\n                \"sale\": 0,\n                \"price\": \"20.00\",\n                \"stock\": 100,\n                \"valjson\": [],\n                \"add_time\": 1593513186,\n                \"integral\": 0,\n                \"is_delete\": 0,\n                \"cost_price\": \"15.00\",\n                \"live_price\": \"16.00\",\n                \"discount_price\": \"18.00\"\n            }\n        },\n        \"end_time\": 1592467139, //结束时间\n        \"status\": 2, //活动状态 0-进行中 1-已完成 2-已结束\n        \"users\": [  //用户信息\n            {\n                \"id\": 1,\n                \"nickname\": \"测试1\",\n                \"avatarurl\": \"https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLwFZscibqGJOjicqv3OINUEYY0CgVDfWPNicrtM5lW7BveTicK2iaj5OTFBML9IcibxJTgQb3rsI3h0KBA/132\",\n                \"is_inviter\": 1 //是否团长 1-团长 0-团员\n            },\n            {\n                \"id\": 2,\n                \"nickname\": \"测试2\",\n                \"avatarurl\": \"https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLwFZscibqGJOjicqv3OINUEYY0CgVDfWPNicrtM5lW7BveTicK2iaj5OTFBML9IcibxJTgQb3rsI3h0KBA/132\",\n                \"is_inviter\": 0\n            },\n            {\n                \"id\": 3,\n                \"nickname\": \"测试3\",\n                \"avatarurl\": \"https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLwFZscibqGJOjicqv3OINUEYY0CgVDfWPNicrtM5lW7BveTicK2iaj5OTFBML9IcibxJTgQb3rsI3h0KBA/132\",\n                \"is_inviter\": 0\n            }\n        ]\n    }\n}"
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 我的页面-邀请码提交

POST /user/index/checkInviteCode

必传参数
Headers:Authorization
code：XI1W0EE  邀请码

> Body 请求参数

```yaml
type: object
properties:
  code:
    type: string
    description: 邀请码
    example: XI1W0EE
required:
  - code

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|
|» code|body|string|true|邀请码|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": {
    "name": "风再起时"
  }
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 我的页面-粉丝

POST /user/video/myFans

必传参数
Headers:Authorization

> Body 请求参数

```yaml
type: object
properties: {}

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": {
    "关注数\"follow_number\"": 2,
    "粉丝数\"fans_number\"": 3,
    "list": {
      "total": 3,
      "per_page": 15,
      "current_page": 1,
      "last_page": 1,
      "data": [
        {
          "粉丝用户id\"fans_id\"": 2,
          "粉丝昵称\"nickname\"": "测试2",
          "粉丝头像\"avatarurl\"": "https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLwFZscibqGJOjicqv3OINUEYY0CgVDfWPNicrtM5lW7BveTicK2iaj5OTFBML9IcibxJTgQb3rsI3h0KBA/132",
          "关注状态\"status\"": 0,
          "//0-单方关注，1-互相关注 时间\"add_time\"": 1594884484
        },
        {
          "fans_id": 2,
          "nickname": "测试2",
          "avatarurl": "https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLwFZscibqGJOjicqv3OINUEYY0CgVDfWPNicrtM5lW7BveTicK2iaj5OTFBML9IcibxJTgQb3rsI3h0KBA/132",
          "status": 0,
          "add_time": 1594884484
        },
        {
          "fans_id": 58,
          "nickname": "测试58",
          "avatarurl": "/uploads/user/20200708/ada9cec5ffd3a6c4e5856a37f4ad2bf8.jpg",
          "status": 1,
          "add_time": 1594884484
        }
      ]
    }
  }
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 导师微信

POST /user/branch/tutorWechat

必传参数
Headers:Authorization

> Body 请求参数

```yaml
type: object
properties: {}

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": {
    "wechat": "xiaochengxu",
    "name": "喵导师",
    "avatarurl": "https://admin.miaommei.com/uploads/user/20200620/fb4a6a52568e28316c95292a96b51556.jpg"
  }
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 提现-添加银行卡

POST /user/branch/addCard

必传参数
Headers:Authorization
identity： 身份证号
card： 银行卡号
name： 持卡人姓名
phone：预留手机号

> Body 请求参数

```yaml
type: object
properties:
  name:
    type: string
    description: 持卡人姓名
    example: 喵XX
  card:
    type: string
    description: 银行卡号
    example: "45613153132351351321"
  identity:
    type: string
    description: 身份证号
    example: "440823123456789911"
  phone:
    type: string
    description: 预留手机号
    example: "12345678911"

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|
|» name|body|string|false|持卡人姓名|
|» card|body|string|false|银行卡号|
|» identity|body|string|false|身份证号|
|» phone|body|string|false|预留手机号|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": {
    "id": "10",
    "bank_name": "中国工商银行",
    "card": "尾号 (3254) 储蓄卡",
    "is_default": true
  }
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 我的收藏-取消收藏（7/18）

POST /user/index/delCollect

必传参数
Headers:Authorization
gid：商品id

> Body 请求参数

```yaml
type: object
properties:
  gid:
    type: string
    description: 商品id
    example: "2"

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|
|» gid|body|string|false|商品id|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 提现-我的银行卡

POST /user/branch/getMyCard

必传参数
Headers:Authorization
非必传参数
card： 银行卡号

> Body 请求参数

```yaml
type: object
properties:
  card:
    type: string
    description: 银行卡号
    example: "45613153132351351321"

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|
|» card|body|string|false|银行卡号|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": {
    "balance": "1.10",
    "//可提现金额 \"rate\"": "10",
    "//费率（%） \"list\"": [
      {
        "id": 3,
        "bank_name": "招商银行",
        "card": "尾号 (1331) 储蓄卡",
        "is_default": false
      },
      {
        "id": 2,
        "bank_name": "中国银行",
        "card": "尾号 (1231) 储蓄卡",
        "is_default": false
      },
      {
        "id": 1,
        "bank_name": "中国银行",
        "card": "尾号 (1321) 储蓄卡",
        "is_default": false
      }
    ]
  }
}
```

```json
"{\n    \"code\": 200,\n    \"msg\": \"操作成功\",\n    \"data\": [  //搜索卡号返回结果\n        {\n            \"id\": 1,\n            \"bank_name\": \"中国银行\",\n            \"card\": \"尾号 (1321) 储蓄卡\",\n            \"is_default\": false\n        }\n    ]\n}"
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 钱包页面-喵币

POST /user/index/getMiao

必传参数
Headers:Authorization
非必传参数
type：1-收入 2-支出 默认为1
page：1 分页   默认1
limit ：15 每页展示数据数    默认15

> Body 请求参数

```yaml
type: object
properties:
  type:
    type: string
    description: type：1-收入 2-支出 默认为1
    example: "1"
  page:
    type: string
    description: 分页   默认1
    example: "1"
  limit:
    type: string
    description: 每页展示数据数    默认15
    example: "15"

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|
|» type|body|string|false|type：1-收入 2-支出 默认为1|
|» page|body|string|false|分页   默认1|
|» limit|body|string|false|每页展示数据数    默认15|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": {
    "miao": "1.10",
    "list": [
      {
        "type": "分享奖励(直播)",
        "uid": 1,
        "balance": "1.00",
        "add_time": "2020-07-31 00:00"
      },
      {
        "type": "拼团活动",
        "uid": 1,
        "balance": "0.10",
        "add_time": "2020-06-11 14:29"
      }
    ]
  }
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 二维码海报

POST /api/index/wxaCode

必传参数
Headers:Authorization
id：活动/商品/视频 id
type：分享类型 1-商品 2-活动 3-视频 4-个人
page：跳转路径

> Body 请求参数

```yaml
type: object
properties:
  type:
    type: string
    description: 分享类型 1-商品 2-活动 3-视频 4-个人
    example: "3"
  page:
    type: string
    description: 跳转路径
    example: /pages/lstOfGrpDetails/lstOfGrpDetails
  id:
    type: string
    description: 活动/商品/视频 id
    example: "1"
required:
  - type
  - page
  - id

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|
|» type|body|string|true|分享类型 1-商品 2-活动 3-视频 4-个人|
|» page|body|string|true|跳转路径|
|» id|body|string|true|活动/商品/视频 id|

> 返回示例

> 成功

```json
"{\n    \"code\": 200,\n    \"msg\": \"操作成功\",\n    \"data\": {\n        \"nickname\": \"黄灿\",\n        \"avatarurl\": \"https://wx.qlogo.cn/mmopen/vi_32/hYwAzL7NVSVqian1RQmTKJA84kyNzMibjga9G9A6FyicrVcKhPeibJqibjS9qmicMf35ibzM9v81c7azwjeNTAyRsN8Bg/132\",\n        \"code\": \"K9MBCL7\",\n        \"title\": \"邀请你参加拼团活动\",\n        \"qr_code\": \"https://abc.miaommei.com/uploads/qrcode/activity_K9MBCL7.png\",\n        \"goods\": [], //商品\n        \"group\": { //活动\n            \"name\": \"测试活动\",\n            \"title\": \"测试活动分享\",\n            \"share\": \"https://admin.miaommei.com/uploads/activity/20200618/e6a21b98c62bc78e50965de2ad80a2a8.jpg\"\n        },\n        \"video\": [] //视频\n    }\n}"
```

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": {
    "nickname": "黄灿",
    "avatarurl": "https://wx.qlogo.cn/mmopen/vi_32/hYwAzL7NVSVqian1RQmTKJA84kyNzMibjga9G9A6FyicrVcKhPeibJqibjS9qmicMf35ibzM9v81c7azwjeNTAyRsN8Bg/132",
    "code": "K9MBCL7",
    "title": "邀请你加入美妆说分享赚",
    "qr_code": "https://abc.miaommei.com/uploads/qrcode/self_K9MBCL7.png",
    "goods": [],
    "group": [],
    "video": []
  }
}
```

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": {
    "nickname": "黄灿",
    "avatarurl": "https://wx.qlogo.cn/mmopen/vi_32/hYwAzL7NVSVqian1RQmTKJA84kyNzMibjga9G9A6FyicrVcKhPeibJqibjS9qmicMf35ibzM9v81c7azwjeNTAyRsN8Bg/132",
    "code": "K9MBCL7",
    "title": "为你推荐一件商品",
    "qr_code": "https://abc.miaommei.com/uploads/qrcode/commodity_K9MBCL7.png",
    "goods": {
      "amount": "0.00",
      "title": "测试商品1",
      "cover": "https://admin.miaommei.com/uploads/goods_share/20200611/95d28b7ab959cb75a94449a4cf02f50d.jpg"
    },
    "group": [],
    "video": []
  }
}
```

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": {
    "nickname": "测试",
    "avatarurl": "https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLwFZscibqGJOjicqv3OINUEYY0CgVDfWPNicrtM5lW7BveTicK2iaj5OTFBML9IcibxJTgQb3rsI3h0KBA/132",
    "code": "XI1W0EE",
    "title": "测试视频",
    "qr_code": "https://abc.miaommei.com/uploads/qrcode/video_K9MBCL7.png",
    "goods": [],
    "group": {
      "cover": "https://admin.miaommei.com/uploads/video_img/20200620/aeebb38477c9e450ea557004b569ca25.jpg",
      "title": "测试视频"
    },
    "video": []
  }
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 我的页面-提现记录

POST /user/index/withdrawalList

必传参数
Headers:Authorization
非必填参数
time：2020-6  按月份筛选 #默认当前月份
type：3   类型 1-申请中 2-待打款 3-已打款 5-已驳回   默认为全部返回
page：1 分页   默认1
limit ：15 每页展示数据数    默认15

> Body 请求参数

```yaml
type: object
properties:
  time:
    type: string
    description: "按月份筛选 #默认当前月份"
    example: 2020-07
  type:
    type: string
    description: 类型 1-申请中 2-待打款 3-已打款 5-已驳回
    example: "1"
  page:
    type: string
    description: 分页   默认1
    example: "1"
  limit:
    type: string
    description: " 每页展示数据数    默认15"
    example: "15"

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|
|» time|body|string|false|按月份筛选 #默认当前月份|
|» type|body|string|false|类型 1-申请中 2-待打款 3-已打款 5-已驳回|
|» page|body|string|false|分页   默认1|
|» limit|body|string|false|每页展示数据数    默认15|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": {
    "total": 5,
    "per_page": 10,
    "current_page": 1,
    "last_page": 1,
    "data": [
      {
        "balance": "1.00",
        "status": "申请中",
        "remarks": null,
        "add_time": "2020-07-10 20:23",
        "type": "余额提现"
      },
      {
        "balance": "1.00",
        "status": "待打款",
        "remarks": "",
        "add_time": "2020-07-10 20:23",
        "type": "余额提现"
      },
      {
        "balance": "1.00",
        "status": "已打款",
        "remarks": "",
        "add_time": "2020-07-10 20:23",
        "type": "余额提现"
      },
      {
        "balance": "1.00",
        "status": "打款失败",
        "remarks": "jbk",
        "//备注 失败说明 \"add_time\"": "2020-07-10 20:23",
        "type": "余额提现"
      },
      {
        "balance": "1.00",
        "status": "已驳回",
        "remarks": "adk",
        "//备注 驳回说明 \"add_time\"": "2020-07-10 20:23",
        "type": "余额提现"
      }
    ]
  }
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 我的页面-购买购物券

POST /user/branch/rechargeVolume

必传参数
Headers:Authorization

> Body 请求参数

```yaml
type: object
properties: {}

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 我的页面-用户信息

POST /user/index/getInfo

必传参数
Headers:Authorization

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": [
    {
      "id": 1,
      "头像\"avatarurl\"": "https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLwFZscibqGJOjicqv3OINUEYY0CgVDfWPNicrtM5lW7BveTicK2iaj5OTFBML9IcibxJTgQb3rsI3h0KBA/132",
      "昵称\"nickname\"": "测试1",
      "手机\"phone\"": "13500135000",
      "等级\"level\"": "新晋掌柜",
      "推荐码\"invite_code\"": "XI1W0EE",
      "上级id\"invite_id\"": 1,
      "是否开通视频收益\"is_proceeds\"": 0,
      "//is_proceeds为0则显示开通视频，为1则显示购物券 我的客户\"invite_num\"": 3,
      "我的钱包\"balance\"": "6000",
      "购物券\"volume_num\"": 5000,
      "是否游客\"is_visitor\"": 1,
      "//is_visitor为1则显示等级，为0则显示游客 是否绑定上级\"is_binding\"": "1 //is_binding 0-未绑定上级，1-已绑定"
    },
    {
      "待支付\"pay_num\"": 0,
      "待发货\"send_num\"": 2,
      "待收货\"receive_num\"": 0,
      "待评论\"appraise_num\"": 0,
      "售后\"service_num\"": 0
    }
  ]
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 获取用户信息-微信

POST /user/index/updateUserInfo

提交微信授权信息

> Body 请求参数

```yaml
type: object
properties: {}

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": {}
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 我的页面-关注

POST /user/video/myFollow

必传参数
Headers:Authorization
非必填
nickname： 按昵称查询用户

> Body 请求参数

```yaml
type: object
properties:
  nickname:
    type: string
    description: 按昵称查询用户
    example: 测试2

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|
|» nickname|body|string|false|按昵称查询用户|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": {
    "关注数\"follow_number\"": 2,
    "粉丝数\"fans_number\"": 3,
    "list": {
      "total": 2,
      "per_page": 15,
      "current_page": 1,
      "last_page": 1,
      "data": [
        {
          "关注用户id\"uid\"": 2,
          "昵称\"nickname\"": "测试2",
          "头像\"avatarurl\"": "https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLwFZscibqGJOjicqv3OINUEYY0CgVDfWPNicrtM5lW7BveTicK2iaj5OTFBML9IcibxJTgQb3rsI3h0KBA/132",
          "状态\"status\"": 0,
          "时间\"add_time\"": 1594884484
        },
        {
          "uid": 58,
          "nickname": "测试58",
          "avatarurl": "/uploads/user/20200708/ada9cec5ffd3a6c4e5856a37f4ad2bf8.jpg",
          "status": 1,
          "add_time": 1594884484
        }
      ]
    }
  }
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 增加活动浏览记录

POST /user/branch/lookActivity

必传参数
Headers:Authorization
aid：活动id
uid：分享用户id

> Body 请求参数

```yaml
type: object
properties:
  aid:
    type: string
    example: "15"
  uid:
    type: string
    example: "1070"
required:
  - aid
  - uid

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|
|» aid|body|string|true|none|
|» uid|body|string|true|none|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 我的收藏-添加收藏（7/18）

POST /user/index/addCollect

必传参数
Headers:Authorization
gid：商品id

> Body 请求参数

```yaml
type: object
properties:
  gid:
    type: string
    description: 商品id
    example: "2"

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|
|» gid|body|string|false|商品id|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 我的收货地址-删除（7/22）

POST /user/branch/delAddress

必传参数
Headers:Authorization
id：收货地址id

> Body 请求参数

```yaml
type: object
properties:
  id:
    type: string
    example: "8"

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|
|» id|body|string|false|none|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "操作成功",
  "data": true
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

# 首页模板-等

## GET 掌柜说明-轮播图

GET /view/index/userViewLevel

用户模板-掌柜说明

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "用户等级权益轮播图ok",
  "data": [
    {
      "id": 2,
      "// id \"img\"": "https://admin.miaommei.com/levelBanner/20200708/13f091c85b8fc19c14c5a04e202fc6cc.jpg",
      "// 图片地址 \"desc\"": [
        "// 描述文件  是个数组 \"新晋掌柜\"",
        "囡囡鱼鱼"
      ]
    }
  ]
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 首页记录条

POST /view/index/textLogB

首页home-首页记录条

> Body 请求参数

```yaml
type: object
properties:
  timestamp:
    type: string
    example: "1598342554"
  sign:
    type: string
    example: 1a3bb99900f5aa80cd3ee1c6723593da

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|false|none|
|body|body|object|false|none|
|» timestamp|body|string|false|none|
|» sign|body|string|false|none|

> 返回示例

> 成功

```json
"{\n    \"code\": 200,\n    \"msg\": \"首页记录条ok\",\n    \"data\": [\n        {\n            \"sort\": 2, // 排序 已随机打乱，前端拼接显示即可\n            \"text\": \"恭喜获取10奖金\", // 文本信息，后台可以设置\n            \"username\": \"测试4\" // 用户名\n        }\n    ]\n}"
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 首页视频-是否

GET /view/index/viewHomeVideoHide

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|AUTHORIZATION|query|string|false|none|
|AUTHORIZATION|header|string|true|none|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "首页视频ok",
  "data": {
    "switch": true,
    "// 为 true  表示 需要展示 图片，隐藏视频 \"img\"": "https://admin.miaommei.com/newUploads//bannerHome/20200731/0ccb772812c60f191a563090f61f8aab.jpg"
  }
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 直播栏目广告图

GET /view/index/liveBroadcastBanner

直播栏目广告图

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 首页-轮播图

GET /view/index/Banner

不需要传递如何参数

> 返回示例

> 成功

```json
"{\n    \"code\": 200,\n    \"msg\": \"轮播图ok\",\n    \"data\": [\n            \"type_id\": 1, /*关联的类型id*/\n            \"type\": 1,   // 类型  1 为商品  2为用户  3为活动  ，目前不用判断，后期为给api\n            \"name\": \"测试\", // 名称\n            \"cover\": \"https://admin.miaommei.com/uploads/ad/20200630/3b24d8c52d594b2f56d07fd46fa5e6e9.jpg\", // 图片地址\n            \"is_skip\": 1 // 跳转 1跳转  2不跳转\n        }\n    ]\n}"
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

# 登录-验证模块-login_token

## POST 登录接口

POST /user/login/wxLogin

前端需要传递code过来,后端拿到openid和key

> Body 请求参数

```yaml
type: object
properties:
  code:
    type: string

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|AUTHORIZATION|header|string|false|将token值存储在本地 不需要每次都获取 可以直接获取到用户信息|
|body|body|object|false|none|
|» code|body|string|false|none|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

# 测试

## POST 测试111 Copy

POST /user/order/test2

> Body 请求参数

```yaml
type: object
properties:
  tid:
    type: string
    example: FddT6-1bVu2lfiggUR7FVmS0mwrvTlGIgksy6xh94XE
  order_sn:
    type: string
    example: O20200712100000
  goods_name:
    type: string
    example: 书法家
  total_amount:
    type: string
    example: ￥100.00
  pay_amount:
    type: string
    example: ￥100.00
  date:
    type: string
    example: 2015年01月05日
  timestamp:
    type: string
    example: "1598434478"
  sign:
    type: string
    example: 672734d474b44ed54acc2807f58edae1

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|none|
|body|body|object|false|none|
|» tid|body|string|false|none|
|» order_sn|body|string|false|none|
|» goods_name|body|string|false|none|
|» total_amount|body|string|false|none|
|» pay_amount|body|string|false|none|
|» date|body|string|false|none|
|» timestamp|body|string|false|none|
|» sign|body|string|false|none|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 测试111

POST /api/index/aliPay

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|none|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 模拟验证码登录

POST /user/login/infoLogin

模拟验证码登录-主要用于app开发时数据展示

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|phone|query|string|true|手机号码|
|code|query|string|true|验证码 |

> 返回示例

> 成功

```json
{
  "code": 200,
  "// 状态码 \"msg\"": "用户登录ok",
  "// 描述信息 \"data\"": {
    "// 数据结构 \"token\"": "",
    "// token值 \"phone\"": "13533580420",
    "// 手机号码 \"nickname\"": "黄灿",
    "// 用户昵称 \"avatarurl\"": "https://thirdwx.qlogo.cn/mmopen/vi_32/ JrRM5l97CxDXiaKWn0X6l7oZZkG5PFGSola2CicZiamFiaHysmhxfSgMhV97c8gyQbnb6HnptmhtHEDS7qPlhDJecQ/132",
    "// 用户头像 \"invite_code\"": null,
    "// 邀请码 \"is_proceeds\"": 1,
    "// 是否拥有购物券权限，  视频点赞 上传视频等操作 \"is_fictitious\"": 1,
    "// 是否为虚拟用户 \"add_time\"": 1598603025,
    "// 添加时间 \"level\"": "5 // 层级"
  }
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 删除用户数据(测试)

POST /user/order/test

必传参数
Headers:Authorization

> Body 请求参数

```yaml
type: object
properties:
  uid:
    type: string
    example: "1"

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|token|
|body|body|object|false|none|
|» uid|body|string|false|none|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 测试222

POST /live/index/index

> Body 请求参数

```yaml
type: object
properties:
  tid:
    type: string
    example: FddT6-1bVu2lfiggUR7FVmS0mwrvTlGIgksy6xh94XE
  goods_id:
    type: string
    example: "1"
  start:
    type: string
    example: 16:45
  end:
    type: string
    example: 17:00
  pay_amount:
    type: string
    example: ￥100.00
  date:
    type: string
    example: 2015年01月05日
  timestamp:
    type: string
    example: "1598434478"
  sign:
    type: string
    example: 672734d474b44ed54acc2807f58edae1

```

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|none|
|body|body|object|false|none|
|» tid|body|string|false|none|
|» goods_id|body|string|false|none|
|» start|body|string|false|none|
|» end|body|string|false|none|
|» pay_amount|body|string|false|none|
|» date|body|string|false|none|
|» timestamp|body|string|false|none|
|» sign|body|string|false|none|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

# 签到打卡

## GET 测试接口

GET /sign/index/sign_in

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|false|none|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 用户打卡记录

GET /sign/index/user_sign_detail

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|false|none|

> 返回示例

> 成功

```json
"{\r\n    \"code\": 200,\r\n    \"msg\": \"用户打卡详情\",\r\n    \"data\": {\r\n        \"id\": 4,\r\n        \"uid\": 4033, //用户ID\r\n        \"days\": 9,  //已连续多少天打卡\r\n        \"is_sign\": 2,   //今天是否打卡  1为今日没有打卡  2为今日已打卡\r\n        \"add_time\": 1602474573,\r\n        \"upd_time\": 1602474573,\r\n        \"is_delete\": 1,\r\n        \"sign_reward_rule\": [],\r\n        \"rules\": [\r\n            \"{\\\"status\\\":1,\\\"fate\\\":8,\\\"reward\\\":\\\"0.7\\\",\\\"is_reward_double\\\":1}\",\r\n            \"{\\\"status\\\":2,\\\"fate\\\":9,\\\"reward\\\":\\\"0.7\\\",\\\"is_reward_double\\\":1}\",\r\n            \"{\\\"status\\\":2,\\\"fate\\\":10,\\\"reward\\\":\\\"0.7\\\",\\\"is_reward_double\\\":1}\",\r\n            \"{\\\"status\\\":2,\\\"fate\\\":11,\\\"reward\\\":\\\"0.7\\\",\\\"is_reward_double\\\":1}\",\r\n            \"{\\\"status\\\":2,\\\"fate\\\":12,\\\"reward\\\":\\\"0.7\\\",\\\"is_reward_double\\\":1}\",\r\n            \"{\\\"status\\\":2,\\\"fate\\\":13,\\\"reward\\\":\\\"0.7\\\",\\\"is_reward_double\\\":1}\",\r\n            \"{\\\"status\\\":2,\\\"fate\\\":14,\\\"reward\\\":\\\"0.7\\\",\\\"is_reward_double\\\":1}\",\r\n            \"{\\\"status\\\":2,\\\"fate\\\":15,\\\"reward\\\":\\\"0.7\\\",\\\"is_reward_double\\\":1}\"\r\n            //status 为1已打卡  2为还没打卡\r\n            //fate 连续天数\r\n            //reward 为打卡奖励多少\r\n            //is_reward_double 1为不双倍  2为双倍\r\n        ],\r\n        \"miao\": 390604 //喵币\r\n    }\r\n}"
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 测试定时任务

GET /sign/index/test

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|false|none|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

# 购物车

## GET 购物车列表

GET /shopping/index/shoppingList

购物车列表

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|false|用户token值|

> 返回示例

> 成功

```json
"{\r\n    \"code\": 200,\r\n    \"msg\": \"购物车商品数据\",\r\n    \"data\": {\r\n        // 按照店铺对应\r\n        \"shoppingList\": [\r\n            {\r\n                \"name\": \"平台自营\", // 店铺名称\r\n                \"id\": 0, // 店铺序号\r\n                \"goods\": [ // 商品 - 数组\r\n                    {\r\n                        \"id\": 98, // 序号  删除时传此值\r\n                        \"goods_id\": 98, // 商品序号  用来点击时跳转到对应的商品详情\r\n                        \"goods_attr_id\": 286, // 商品规格序号 \r\n                        \"soldOut\": false, // 为true  表示没有货 不能选中\r\n                        \"re_show\": false, // 是否需要重新选中规格 ，表示 商品规格已不存在\r\n                        \"num\": 1, // 购买数量\r\n                        \"attr_str\": \"规格:25gx10片\", // 规格字符串 渲染\r\n                        \"status\": false, // false 表示 没有选中 ，  true 表示选中\r\n                        \"title\": \"男士面膜除皱纹抗皱祛痘印痘疤淡化痘印男人去皱纹增白男土抗衰\", // 标题\r\n                        \"img\": \"https://admin-miaomeimei-1301812909.cos.ap-guangzhou.myqcloud.com/newUploads/product/goods/attrImg/2020_08_08/8cd0a7e634f3fb721b9f0eb31a3483df.jpg\", // 商品图片\r\n                        \"price\": \"45.00\" // 价格\r\n                    },\r\n                    {\r\n                        \"id\": 96,\r\n                        \"soldOut\": false,\r\n                        \"re_show\": false,\r\n                        \"num\": 1,\r\n                        \"attr_str\": \"规格:【2111蓝色】 50ML\",\r\n                        \"status\": 0,\r\n                        \"title\": \"LIANG ZI总裁香靓姿黑骑士香水男士持久淡香男士香水海洋香调\",\r\n                        \"img\": \"https://admin-miaomeimei-1301812909.cos.ap-guangzhou.myqcloud.com/newUploads/product/goods/attrImg/2020_08_08/c6ca16101e9f4d0d5c126d0f42209ea9.jpg\",\r\n                        \"price\": \"59.00\"\r\n                    }\r\n                ]\r\n            },\r\n            {\r\n                \"name\": \"林2222\",\r\n                \"id\": 23,\r\n                \"goods\": [\r\n                    {\r\n                        \"id\": 86,\r\n                        \"soldOut\": false,\r\n                        \"re_show\": false,\r\n                        \"num\": 1,\r\n                        \"attr_str\": \"\",\r\n                        \"status\": 0,\r\n                        \"title\": \"梵贞男士护肤三件套礼盒装正品脸部补水保湿控油祛痘美白收缩毛孔\",\r\n                        \"img\": \"https://admin-miaomeimei-1301812909.cos.ap-guangzhou.myqcloud.com/newUploads/product/goods/attrImg/2020_08_08/0aef2fc4d9066366ddcdad8810c3e78e.jpg\",\r\n                        \"price\": \"139.00\"\r\n                    },\r\n                    {\r\n                        \"id\": 86,\r\n                        \"soldOut\": false,\r\n                        \"re_show\": true,\r\n                        \"num\": 1,\r\n                        \"attr_str\": null,\r\n                        \"status\": 0,\r\n                        \"title\": \"梵贞男士护肤三件套礼盒装正品脸部补水保湿控油祛痘美白收缩毛孔\",\r\n                        \"img\": \"https://admin-miaomeimei-1301812909.cos.ap-guangzhou.myqcloud.com/newUploads/product/goods/branner/2020_08_08/ca5d3f8736230d7223c268cf89178676.jpg\",\r\n                        \"price\": \"139.00\"\r\n                    }\r\n                ]\r\n            },\r\n            {\r\n                \"name\": \"韩后官方旗舰店\",\r\n                \"id\": 1,\r\n                \"goods\": [\r\n                    {\r\n                        \"id\": 80,\r\n                        \"soldOut\": false,\r\n                        \"re_show\": false,\r\n                        \"num\": 2,\r\n                        \"attr_str\": \"\",\r\n                        \"status\": 0,\r\n                        \"title\": \"碧素堂矿物泥控油袪痘洁面乳100g清爽洗面奶\",\r\n                        \"img\": \"https://admin-miaomeimei-1301812909.cos.ap-guangzhou.myqcloud.com/newUploads/product/goods/attrImg/2020_08_08/36f0099ca4456278447a3717680e497a.jpg\",\r\n                        \"price\": \"29.80\"\r\n                    },\r\n                    {\r\n                        \"id\": 79,\r\n                        \"soldOut\": true,\r\n                        \"re_show\": false,\r\n                        \"num\": 1,\r\n                        \"attr_str\": \"规格:星黑\",\r\n                        \"status\": 0,\r\n                        \"title\": \"男士香水持久淡香清新自然社交艾诗轩黛蔚蓝男古龙香水情趣\",\r\n                        \"img\": \"https://admin-miaomeimei-1301812909.cos.ap-guangzhou.myqcloud.com/newUploads/product/goods/attrImg/2020_08_08/e05343a9880561f62dc985d5a46a3072.jpg\",\r\n                        \"price\": \"79.00\"\r\n                    },\r\n                    {\r\n                        \"id\": 79,\r\n                        \"soldOut\": false,\r\n                        \"re_show\": false,\r\n                        \"num\": 1,\r\n                        \"attr_str\": \"规格:蔚蓝\",\r\n                        \"status\": 0,\r\n                        \"title\": \"男士香水持久淡香清新自然社交艾诗轩黛蔚蓝男古龙香水情趣\",\r\n                        \"img\": \"https://admin-miaomeimei-1301812909.cos.ap-guangzhou.myqcloud.com/newUploads/product/goods/attrImg/2020_08_08/50fef10c08de991aa8f06f9590b9b502.jpg\",\r\n                        \"price\": \"79.00\"\r\n                    },\r\n                    {\r\n                        \"id\": 81,\r\n                        \"soldOut\": false,\r\n                        \"re_show\": false,\r\n                        \"num\": 1,\r\n                        \"attr_str\": \"\",\r\n                        \"status\": 0,\r\n                        \"title\": \"冰菊男士专用面膜控油去黑头淡化痘印补水收缩毛孔紧致送美白面膜\",\r\n                        \"img\": \"https://admin-miaomeimei-1301812909.cos.ap-guangzhou.myqcloud.com/newUploads/product/goods/attrImg/2020_08_08/77beb4428655536d394afb861ee3c1ff.jpg\",\r\n                        \"price\": \"59.90\"\r\n                    }\r\n                ]\r\n            }\r\n        ],\r\n        // 失效商品  不可点击 不可跳转 不可选中  只能删除\r\n        \"shixiaoList\": [\r\n            {\r\n                \"id\": 101, // 序号  删除时传此值\r\n                \"goods_id\": 98, // 商品序号  用来点击时跳转到对应的商品详情\r\n                \"goods_attr_id\": 286, // 商品规格序号 \r\n                \"soldOut\": false,\r\n                \"re_show\": true,\r\n                \"num\": 1,\r\n                \"attr_str\": \"颜色:黑色\",\r\n                \"status\": 0,\r\n                \"img\": \"https://admin-miaomeimei-1301812909.cos.ap-guangzhou.myqcloud.com/newUploads/product/goods/branner/2020_08_08/8f08bb0727fae4518e691b6dd5b990ef.jpg\",\r\n                \"price\": \"56.00\"\r\n            }\r\n        ]\r\n    }\r\n}"
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 店铺优惠券-列表

GET /shopping/pay/couponList

店铺优惠券列表

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|shop_id|query|string|true|店铺序号|
|Authorization|header|string|false|token值说明|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "购物车-店铺-优惠券列表",
  "data": [
    {
      "id": 102,
      "// 优惠券序号 \"status\"": 1,
      "// 状态 1  全店铺 通用  指定商品可用 \"total_amount\"": "50.00",
      "// 满多少抵扣 \"amount\"": "10.00",
      "// 抵扣金额 \"start_time\"": 1602049240,
      "// 开始时间 \"end_time\"": 1606369240,
      "// 结束时间 \"is_use\"": "false // false 表示可以领取  true  表示该优惠券 此用户已经领取过了"
    }
  ]
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 购物车-重新选择商品规格

GET /shopping/index/reAttr

购物车列表-购物车重新选择规格

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|id|query|string|true|序号 - 购物车列表序号|
|goods_id|query|string|true|商品序号|
|goods_attr_id|query|string|true|商品规格序号|
|Authorization|header|string|true|tokne 用户值|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 购物车-结算页面-支付列表

GET /shopping/pay/payList

购物车-结算页面-支付列表

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|wallet|query|string|false|1表示使用购物券|
|Authorization|header|string|true|token值|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "购物车结算页面",
  "data": {
    "shoppingList": [
      {
        "name": "平台自营",
        "id": 0,
        "goods": [
          {
            "id": 10,
            "goods_id": 96,
            "goods_attr_id": 262,
            "num": 5,
            "attr_str": "规格:【2111蓝色】 50ML",
            "is_team": 1,
            "title": "LIANG ZI总裁香靓姿黑骑士香水男士持久淡香男士香水海洋香调",
            "img": "https://admin-miaomeimei-1301812909.cos.ap-guangzhou.myqcloud.com/newUploads/product/goods/attrImg/2020_08_08/c6ca16101e9f4d0d5c126d0f42209ea9.jpg",
            "price": 5900,
            "sumMoney": "29500.00",
            "money": "28025.00",
            "purchase_money": 1475,
            "coupon_money": 0,
            "coupon_id": 0
          },
          {
            "id": 11,
            "goods_id": 98,
            "goods_attr_id": 252,
            "num": 3,
            "attr_str": "规格:25gx10片",
            "is_team": 1,
            "title": "男士面膜除皱纹抗皱祛痘印痘疤淡化痘印男人去皱纹增白男土抗衰",
            "img": "https://admin-miaomeimei-1301812909.cos.ap-guangzhou.myqcloud.com/newUploads/product/goods/attrImg/2020_08_08/8cd0a7e634f3fb721b9f0eb31a3483df.jpg",
            "price": 4500,
            "sumMoney": "13500.00",
            "money": "12825.00",
            "purchase_money": 675,
            "coupon_money": 0,
            "coupon_id": 0
          }
        ],
        "sumMoney": 43000,
        "totalMoney": 40850,
        "purchase_money": 2150,
        "coupon_money": 0,
        "coupon_id": 0
      },
      {
        "name": "林2222",
        "id": 23,
        "goods": [
          {
            "id": 9,
            "goods_id": 86,
            "goods_attr_id": 209,
            "num": 1,
            "attr_str": "",
            "is_team": 1,
            "title": "梵贞男士护肤三件套礼盒装正品脸部补水保湿控油祛痘美白收缩毛孔",
            "img": "https://admin-miaomeimei-1301812909.cos.ap-guangzhou.myqcloud.com/newUploads/product/goods/attrImg/2020_08_08/0aef2fc4d9066366ddcdad8810c3e78e.jpg",
            "price": 13900,
            "sumMoney": "13900.00",
            "money": "13900.00",
            "purchase_money": 0,
            "coupon_money": 0,
            "coupon_id": 0
          }
        ],
        "sumMoney": 13900,
        "totalMoney": 0,
        "purchase_money": 0,
        "coupon_money": 0,
        "coupon_id": 0
      }
    ],
    "meony_list": {
      "sumMoney": 569,
      "totalMoney": 408.5,
      "coupon_money": 0,
      "purchase_money": 21.5,
      "ids": [
        11,
        10,
        9
      ]
    }
  }
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 购物车-更改选中状态

GET /shopping/index/check

购物车 - 更改选中状态

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|ids|query|string|true|需要更新状态的序号  传入英文逗号切割 如果为0 表示全部|
|status|query|string|true|状态值  1  为选中  0 为不选中|
|Authorization|header|string|true|用户token|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 购物车列表-数量更新

GET /shopping/index/editNum

购物车列表-数量更新

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|id|query|string|true|购物车列表-数量|
|num|query|string|true|数量 |
|Authorization|header|string|false|token  用户识别码|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 购物车删除

GET /shopping/index/del

购物车删除

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|ids|query|string|true|删除序号，用英文逗号隔开即可   是否全删 如果全删  传值 0|
|Authorization|header|string|true|用户token值|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 发起支付

GET /shopping/pay/pay

发起支付

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|wallet|query|string|true|0表示不适用购物券  1使用购物券|
|name|query|string|true|用户姓名|
|phone|query|string|true|手机号码|
|address|query|string|true|用户地址|
|pay_type|query|string|true|默认微信支付1微信支付,   2  余额支付|
|desc|query|string|false|订单备注 店铺序号@描述信息   英文逗号切割 |
|Authorization|header|string|true|用户token|

> 返回示例

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 购物车收藏商品

GET /shopping/index/favorites

购物车收藏商品

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|goods_id|query|string|true|商品序号|
|goods_attr_id|query|string|true|商品规格记录|
|num|query|string|true|购买数量|
|Authorization|header|string|true|token|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "添加成功",
  "data": []
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 购物车-列表计算金额

GET /shopping/index/shoppingListSumMoney

购物车列表计算金额

### 请求参数

|名称|位置|类型|必选|说明|
|---|---|---|---|---|
|Authorization|header|string|true|用户token|

> 返回示例

> 成功

```json
{
  "code": 200,
  "msg": "购物车列表-金额计算",
  "data": {
    "sumMoney": 56900,
    "// 总额 \"totalMoney\"": 40850,
    "// 实际支付金额 \"coupon_money\"": 0,
    "// 优惠券金额 \"purchase_money\"": 2150,
    "// 自购省金额 \"ids\"": [
      "// 序号组 11",
      10,
      9
    ]
  }
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

# 数据模型

