<?php
// +----------------------------------------------------------------------
// | 应用设置
// +----------------------------------------------------------------------

return [
    'TMPL_CACHE_ON'=>false,      // 默认开启模板缓存
    // 应用地址
    'app_host'         => env('app.host', ''),
    // 开启多应用
    'auto_multi_app'   => true,
    // 应用的命名空间
    'app_namespace'    => '',
    // 是否启用路由
    'with_route'       => true,
    // 默认返回的类型
    'default_return_type'    => 'json',
    // 默认应用
    'default_app'      => 'index',
    // 默认时区
    'default_timezone' => 'Asia/Shanghai',

    // 应用映射（自动多应用模式有效）
    'app_map'          => [],
    // 域名绑定（自动多应用模式有效）
    'domain_bind'      => [],
    // 禁止URL访问的应用列表（自动多应用模式有效）
    'deny_app_list'    => [],

    // 异常页面的模板文件
    'exception_tmpl'   => app()->getThinkPath() . 'tpl/think_exception.tpl',

    // 错误显示信息,非调试模式有效
    'error_message'    => '页面错误！请稍后再试～',
    // 显示错误信息
    'show_error_msg'   => env('app.show_error_msg',false),
    // Trace信息 方便用于调式
    'SHOW_PAGE_TRACE' => env('app.show_page_trace',false), // 显示页面Trace信息
    // jwt验证密钥
    'jwt_key' => '123456789',
    'tpl_cache' => false,
    'template'               => [
        'tpl_cache'    => false,
        'tpl_time' => 1,
        'timeout' => 1,
        'tpl_timeout' => 1,
        'expire'     => 1
    ]
];
