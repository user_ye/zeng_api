<?php

// +----------------------------------------------------------------------
// | 缓存设置
// +----------------------------------------------------------------------

return [
    // 默认缓存驱动
//    'default' => env('cache.driver', 'file'),
    'default' => env('cache.driver', 'redis'),
    // 缓存连接方式配置
    'stores'  => [
        'file' => [
            // 驱动方式
            'type'       => 'File',
            // 缓存保存目录
            'path'       => '',
            // 缓存前缀
            'prefix'     => '',
            // 缓存有效期 0表示永久缓存
            'expire'     => 1,
            // 缓存标签前缀
            'tag_prefix' => 'tag:',
            // 序列化机制 例如 ['serialize', 'unserialize']
            'serialize'  => [],
        ],
        // 本机缓存
        'redis' => [
            'type'=>'redis',
            //服务器地址
            'host'=> env('redis.host','127.0.0.1'),
            'port' => env('redis.port',6379),
            'password' => env('redis.password',''),
            // 缓存前缀
            'prefix'     => env('redis.prefix','mmei_'),
            // 默认库
            'select' => env('redis.select',0)
        ],
        // 开发缓存 也就是线上缓存
        'redis_debug'=>[
            //驱动方式
            'type'=>'redis',
            //服务器地址
            'host'=>'42.194.183.140',
            'port' => 6379,
            'timeout' => 3600,
            'password' => 'zeng123456',
            // 缓存前缀
            'prefix' => env('redis.prefix','mmei_'),
            'select' => env('redis.select',0)
        ],
    ],
];
