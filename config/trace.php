<?php
// +----------------------------------------------------------------------
// | Trace设置 开启调试模式后有效
// +----------------------------------------------------------------------
return [
    // 内置Html和Console两种方式 支持扩展
    'type'    => 'Html',
    // 读取的日志通道名
    'channel' => '',
//    定义标签
/*
    'tabs' => [
        'base'  => '基本',
        'file'  => '文件',
        'info'  => '流程',
        'error' => '错误',
        'sql'   => 'SQL',
        'debug' => '调试',
        'user'  => '用户',
    ],
*/
//    'file'    =>    'app\trace\page_trace.html',
// 需要将 配置文件复制过来  vendor/topthink/think-trace/src/tpl/page_trace.tpl
    // 也可以合并几个操作
    'tabs' => [
        'base'                 => '基本',
        'sql'                  => 'SQL',
        'file'                 => '文件',
        'error|notice|warning' => '错误',
        'debug|info'           => '调试',
    ]
];
