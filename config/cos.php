<?php 
return [
        'open' => env('cos.open', 0), // 是否开启 cos 默认是 false
        'secretId' => env('cos.secret_id', ''), //"云 API 密钥 SecretId";
        'secretKey' => env('cos.secret_key', ''),//"云 API 密钥 SecretKey";
        'region' => env('cos.region', "ap-guangzhou"), //设置一个默认的存储桶地域
        'bucket' => env('cos.bucket', ""), //存储桶名称 格式：BucketName-APPID
        'prefix_url' => env('cos.prefix_url', ""), // 拼接的路径
];