<?php

return [
    // 免token 方法  不需要获取用户信息的接口，既游客可以访问的
    'is_login' => [
        // 手机登录
        '/user/login/phoneLoginCode',
        // 发送验证码
        'user/login/phoneCode',
        // 登录
        'user/login/wxLogin',
        // 首页轮播图
        'view/index/banner',
        'view/index/Banner',
        // 首页广告信息
        'view/index/textLogB',
        // 首页是否隐藏
        'view/index/viewHomeVideoHide',
        // 视频分类
        'view/index/videoType',
        // 视频列表
        'video/index/list',
        // 商品轮播图
        'view/index/ProductBanner',
        //商城首页图标
        'view/index/homeDisplay',
        // 商品列表
        'mall/malls',
        'mall/index/malls',
        // 商品分类
        'mall/mallType',
        'mall/index/mallType',
        // 商品详情
        'mall/index/getMalls',
        // 品牌列表
        'mall/index/getBrand',
        // 评论列表
        'mall/comment/list',
        // 活动列表
        'mall/activity/list',
        'user/order/test3',
        'live/index/index',
        // 上传素材
        'api/index/uploadImgWx',
        // 支付回调
        'mall/pay/notify',
        //直播列表/预告
        'live/index/getList',
        'live/index/getNoticeList',
        //购物券-分享
        'volume/index/getInfo',
        // 商城首页- 商品展示
        'mall/index/shopHomeList',
        'api/index/aliCallback',//支付宝回调
//        'user/order/test2',
    ],
    // 免校验签名的sign方法  静态页面或者不需要做接口校验 允许用户随意请求的接口
    'is_check' => [
//        'api/index/login',
//        'user/order/test2',
        'live/index/index',
        //商城首页图标
        'view/index/homeDisplay',
        // 上传素材
        'api/index/uploadImgWx',
        // 首页隐藏
        'view/index/viewHomeVideoHide',
        // 支付回调
        'mall/pay/notify',
        'api/index/aliCallback',//支付宝回调
    ],
    // 验证的值 用来加密的key
    "apiSerect" => env('api_Verify.api_serect', 'kajsbkd123b55kwbdkj128y87'),
    // 是否开启接口验证参数 默认是 开启1  关闭0 sign
    "open" => env('api_Verify.open', true),
];