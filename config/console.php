<?php
// +----------------------------------------------------------------------
// | 控制台配置
// +----------------------------------------------------------------------
return [
    // 指令定义
    'commands' => [
        'test' => 'app\command\Test',
        'dele' => 'app\command\Dele',
        'orderend' => 'app\command\OrderEnd',
        'pub' => 'app\command\Pub',
        'sub' => 'app\command\Sub',
        'videosavejson' => 'app\command\VideoSaveJson',
        'videosort'  => 'app\command\VideoSort',
        'livestatus' => 'app\command\LiveStatus',
        'sign'  => 'app\command\Sign'
    ],
];
