<?php
/**
 * Created by PhpStorm.
 * User: snuz
 * Date: 2020/7/19
 * Time: 15:58
 */
return [
    'appid' => env('wechat.appid', ''), // 小程序appid
    'appsecret' => env('wechat.appsecret', ''), // 小程序 appsecret
    'mchid' => env('wechat.mchid', ''), // 商户号
    'key' => env('wechat.key', ''), // 商户支付秘钥
];