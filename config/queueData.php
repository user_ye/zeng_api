<?php
/**
 * Created by PhpStorm.
 * User: snuz
 * Date: 2020/7/24
 * Time: 11:38
 */
return [
    // 团队结算队列
    'TeamSettlementJob' => [
        'class' => \app\common\job\TeamSettlementJob::class,
        'name' => 'TeamSettlementJob'
    ],
    // 测试队列开始
    'TestJob' => [
        'class' => \app\common\job\TestJob::class,
        'name' => 'TestJob'
    ],
    // 订单支付-超时处理
    'OrderPayDelay' => [
        'class' => \app\common\job\OrderPayDelay::class,
        'name' => 'OrderPayDelay'
    ],
    // 删除未提交视频记录和文件
    'VideoDelayJob' => [
        'class' => \app\common\job\VideoDelayJob::class,
        'name' => 'VideoDelayJob'
    ],
    // 上传文件处理
    'FileUploadsJob' => [
        'class' => \app\common\job\FileUploadsJob::class,
        'name' => 'FileUploadsJob'
    ],
    // 删除直播商品缓存
    'LiveBroadcastGoodsJob' => [
        'class' => \app\common\job\LiveBroadcastGoodsJob::class,
        'name' => 'LiveBroadcastGoodsJob'
    ]
];