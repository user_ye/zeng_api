
<?php
/**
 * Created by PhpStorm.
 * User: snuz
 * Date: 2020/7/15
 * Time: 11:07
 */
return
[
    'path' => __DIR__.'\..\public\static',
    'path_img' => __DIR__.'\..\public\static\img',
    'path_video' => __DIR__.'\..\public\static\video',
    'public_img' => __DIR__.'\..\public\uploads',
    'read_img' => '/uploads',
    'read_bank' => '/bank/',
    'static_host' => env('static.host', ''), // 是否开启 cos 默认是 false
];