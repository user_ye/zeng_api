## 开源：Apache2开源协议发布，并提供免费使用。（免费白嫖，欢迎欢迎 ）

## 教程视频：[首页B站](https://space.bilibili.com/258238619) `https://space.bilibili.com/258238619`
  + [01-搭建项目视频教程](https://www.bilibili.com/video/BV18R4y1M7DJ)  `https://www.bilibili.com/video/BV18R4y1M7DJ`
  + [02-搭建小程序教程](https://www.bilibili.com/video/BV1V8411s7S2) `https://www.bilibili.com/video/BV1V8411s7S2`
  + [03-商品sku 批量商品评论等操作](https://www.bilibili.com/video/BV1bG41177FH) `https://www.bilibili.com/video/BV1bG41177FH`
  + [04-视频操作 批量视频评论等操作](https://www.bilibili.com/video/BV1QW4y1J7t7)  `https://www.bilibili.com/video/BV1QW4y1J7t7`
  + [05-此项目已不再维护 期待我的下一个项目吧 开发神器 前后端 产品 测试 都可使用]

## 一条龙服务项目 定位：社交+短视频+商城+sass系统 (来白嫖) 
  + [管理系统](https://gitee.com/user_ye/zeng_admin)：`https://gitee.com/user_ye/zeng_admin`
    + 演示站点：`https://madmin.zengye.top/admin/login` 账号：`admin` 密码：`123456`
  + [门店商户管理系统](): 待补充 预计这两天
  + [api接口-app小程序专用](): **就是这个项目**
  + [小程序代码](https://gitee.com/user_ye/Mall-video): `https://gitee.com/user_ye/Mall-video`
  + 因为后台管理系统 `https://madmin.zengye.top` 走了cdn并且设置了防盗链 所以需要自己复制地址（游览器输入）进行访问 ，如果是从码云或其他地方跳转的会访问不了

## 项目演示图:
  + ![演示图如下](./docs/demo_img/1.gif)

## 欢迎白嫖 欢迎交流 联系方式如下：（主微信，qq很少上）
  + 微信：`zeng1569838235` 
  + qq: `1459584248`

### 接口文档使用的是 apifox 已将所有的到出在 或 根据自己喜好的进行导入 （postman, openapi等） 
  + [html类型](./docs/docs-api/zengji.html) 路径：`docs/docs-api/zengji.html`
  + [openapi.json类型](./docs/docs-api/openapi.json) 路径：`docs/docs-api/openapi.json`
  + [markdown类型](./docs/docs-api/zengji.md) 路径：`docs/docs-api/openapi.json`
  + [在线观看api接口文档html](https://madmin.zengye.top/static/docs/zengji.html) `https://madmin.zengye.top/static/docs/zengji.html`
  + 如果你都不想那么麻烦 那你可以加我微信 我把你拉到接口项目 可以直接进行调式

### 项目搭建：
  + 环境
    + php: 7.2 推荐 不能低于5.6
    + mysql : 5.6 - 5.7
    + nginx
    + redis
    + node
  + 进行配置nginx 并且配置伪静态
```nginx
location / {
	if (!-e $request_filename){
		rewrite  ^(.*)$  /index.php?s=$1  last;   break;
	}
}
``` 
  + 数据库需要从后台管理项目处拉取
  + 需要将 `env.ini.example` 复制一份到 `.env` 文件
    + 自行修改mysql 和 redis cos 等
  + 里面有做签名验证等 如果不需要将 .env的配置进行关闭即可 里面有很详细的信息

### 项目说明：
  + nodeCos 目录 处理cos上传 好像没用上 
  + nodeWatch 用来监听文件改动后 重启队列 (redis，tp)
  + SubCommission.php 用来计算用户佣金的关系

ThinkPHP 6.0
===============

> 运行环境要求PHP7.1+。

[官方应用服务市场](https://www.thinkphp.cn/service) | [`ThinkPHP`开发者扶持计划](https://sites.thinkphp.cn/1782366)

ThinkPHPV6.0版本由[亿速云](https://www.yisu.com/)独家赞助发布。

### 安装和搭建本项目
  + php版本需要大于7.2.5 需要安装php-redis扩展
  + 使用composer install 
  + 数据库配置需要将配置文件.example.env复制一份到.env上
  + 本地: `php think run`

## 主要新特性

* 采用`PHP7`强类型（严格模式）
* 支持更多的`PSR`规范
* 原生多应用支持
* 更强大和易用的查询
* 全新的事件系统
* 模型事件和数据库事件统一纳入事件系统
* 模板引擎分离出核心
* 内部功能中间件化
* SESSION/Cookie机制改进
* 对Swoole以及协程支持改进
* 对IDE更加友好
* 统一和精简大量用法

## 安装

~~~
composer create-project topthink/think tp 6.0.*
~~~

如果需要更新框架使用
~~~
composer update topthink/framework
~~~

## 文档

[完全开发手册](https://www.kancloud.cn/manual/thinkphp6_0/content)

## 参与开发

请参阅 [ThinkPHP 核心框架包](https://github.com/top-think/framework)。

## 版权信息

ThinkPHP遵循Apache2开源协议发布，并提供免费使用。

本项目包含的第三方源码和二进制文件之版权信息另行标注。

版权所有Copyright © 2006-2020 by ThinkPHP (http://thinkphp.cn)

All rights reserved。

ThinkPHP® 商标和著作权所有者为上海顶想信息科技有限公司。

更多细节参阅 [LICENSE.txt](LICENSE.txt)
