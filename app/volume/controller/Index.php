<?php
declare (strict_types = 1);

namespace app\volume\controller;
use app\common\controller\BaseController;
use app\volume\model\Volume;
use think\Request;
class Index extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        return '您好！这是一个[volume]示例应用';
    }

    /*
     * 购物券活动列表
     */
    public function getList()
    {
        $model = new Volume();
        $data = $model->getList($this->user);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    /*
     * 购物券活动列表
     */
    public function checkShare()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new Volume();
        $data = $model->checkShare($this->user);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    /**
     * 获取活动信息
     */
    public function getInfo()
    {
        $model = new Volume();
        $data = $model->getOne();
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }
}
