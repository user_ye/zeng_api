<?php
declare (strict_types = 1);
namespace app\volume\model;
use app\common\model\BaseModel;
use app\user\model\User;
use think\Db;
use think\Request;

Class Volume extends BaseModel
{
    public $error = '';
    public $map = [
        1=>'尊享6大权益',
        2=>'尊享9大权益',
        3=>'尊享9大权益',
        4=>'尊享9大权益',
        5=>'尊享9大权益',
    ];

    /*
     * 获取列表
     */
    public function getList($user){
        try{
            $data = input('post.');
            $where = [['status','=',1],['type','=',1],['is_delete','=',0]];
            if(!empty($data['code'])){
                $pUser = (new User())->field('level')->where(['invite_code'=>$data['code']])->find();
                if(isset($pUser['level'])){
                    if($pUser['level'] <= 1) return ['list'=>[],'page'=>'/pages/tabBar/index/index'];
                    if($pUser['level'] == 2) return ['list'=>[],'page'=>'/pages/rights-and-interests/privilege-details/privilege-details?type=1'];
                    $where[] = ['level','<=',$pUser['level']];
                }
            }
            if(!empty($data['type'])) $where[] = ['type','=',$data['type']];
            $field = 'id,title,level,type,initial,price,share,share_grade,end_time';
            $item = $this->where($where)->field($field)->order('id asc')->select();
            $list = empty($item) ? array():$item->toArray();
            if(!empty($list)){
                foreach($list as $k=>$v){
                    $list[$k]['volume'] = (int)getformat($v['initial']);
                    if($v['share_grade'] > 1) $list[$k]['is_share'] = $user->level >= $v['share_grade'] ? 1 : 0;
                    //elseif($v['share_grade'] == 1) $list[$k]['is_share'] = $user->is_proceeds == 1 ? 1 : 0;
                    else $list[$k]['is_share'] = 1;
                    $list[$k]['msg'] = $this->map[$v['id']]??'尊享6大权益';
                }
            }
            $ret['list'] = $list;
            $ret['page'] = '';
            return $ret;
        }catch (\Exception $e){
            /*if(stristr($e->getMessage(),'SQLSTATE')){
                $this->error = '数据走丢了，请稍后再试！';
            }else*/ $this->error = $e->getMessage();
            return false;
        }
    }

    /*
     * 获取数据
     */
    public function getOne($ret=[]){
        try{
            $data = Input('post.');
            if(!isset($data['type'])) $data['type'] = $ret['id']??0;
            if(empty($data['type'])||!in_array($data['type'],[1,2,3,4,5])) exception('活动类型错误!');
            $info = $this->field('id,title,initial,price,is_sale,sale_val,share,details_img,desc,start_time,end_time')->where(['id'=>$data['type'],'status'=>1,'is_delete'=>0])->find();
            if(!$info) exception('活动已过期!');
            $info['desc'] = htmlspecialchars_decode($info['desc']);
            $miao = 0;
            if($info['is_sale'] > 0){
                if($info['is_sale']==1){
                    $miao = getformat($info['price']*$info['sale_val']);
                }else $miao = $info['sale_val'];
            }
            $info['volume'] = (int)getformat($info['initial']);
            $info['miao'] = $miao;
            $info['msg'] = $this->map[$info['id']]??'尊享6大权益';
            return $info;
        }catch (\Exception $e){
            if(stristr($e->getMessage(),'SQLSTATE')){
                $this->error = '数据走丢了，请稍后再试！';
            }else $this->error = $e->getMessage();
            return false;
        }
    }

    /*
     * 判断是否可以分享
     */
    public function checkShare($user){
        try{
            $data = Input('post.');
            if(empty($data['id'])) exception('活动类型错误!');
            $info = $this->field('id,share_grade')->where(['id'=>$data['id'],'status'=>1,'is_delete'=>0])->find();
            if(!$info) exception('活动已过期!');
            if($info['share_grade'] > 1) $check = $user->level >= $info['share_grade'] ? 1 : 0;
            //elseif($info['share_grade'] == 1) $check = $user->is_proceeds == 1 ? 1 : 0;
            else $check = 1;
            return $check;
        }catch (\Exception $e){
            if(stristr($e->getMessage(),'SQLSTATE')){
                $this->error = '数据走丢了，请稍后再试！';
            }else $this->error = $e->getMessage();
            return false;
        }
    }
}