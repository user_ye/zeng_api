<?php
declare (strict_types = 1);

namespace app\service\model;

use app\common\model\BaseModel;
use app\order\model\Order;
use app\Request;
use think\Model;

/**
 * @mixin \think\Model
 */
class Express extends BaseModel
{
    public $sr_key = '_express_allList';

    // 获取到所有的数据
//    public function getAllList()
//    {
//        return $this->getListCache();
//    }
}
