<?php
declare (strict_types = 1);

namespace app\service\model;

use app\common\model\BaseModel;
use app\order\model\Order;
use app\Request;
use think\Model;

/**
 * @mixin \think\Model
 */
class Logistics extends BaseModel
{
    public $sr_key = '_logistics_allList';

    // 获取到所有的数据
    public function getAllList()
    {
        return $this->getListCache();
    }

    // 获取到数据
    public function getLogistics(){
        return $this::where('is_delete',0)->column('id,company');
    }

    // 进行返回对应的物流公司信息
    public function getNames($list,$key){
        foreach($list as $item)if($item['id'] == $key)return $item['company'];
        return '待定';
    }
}
