<?php
declare (strict_types = 1);

namespace app\service\validate;

use think\Validate;

class ServiceValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */
    public  $rule =   [
        'oid|订单'  => 'require|number|>:0',
        'type|类型'   => 'number|in:1,2',
//        'cover|图片' => 'require|min:4',
//        'price|退款金额' => 'require|>:0',
        'apply_reason|申请理由' => 'require|min:2',
        'order_goods_id|订单商品' => 'require|>:0',
        'good_id|商品' => 'require|>:0'
    ];
}