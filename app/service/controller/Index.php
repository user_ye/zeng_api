<?php
declare (strict_types = 1);

namespace app\service\controller;

use app\common\controller\BaseController;
use app\service\model\Express;
use app\service\model\Logistics;
use app\service\model\ServiceGoods;
use app\service\validate\ServiceValidate;
use think\Request;
use app\service\model\Service as ServiceModel;


class index extends BaseController
{
    /**
     * 订单申请售后
     */
    public function apply()
    {
        if(!$this->checkValidate(ServiceValidate::class,[]))
            return retu_json(400,'参数错误');
        $bool =  (new ServiceModel())->applyPlay($this);
        return retu_json(200,$bool ? '申请售后成功' : '失败');
    }

    public function list()
    {
        $model = new ServiceModel();
        $model->list($this);
        return retu_json(200,'售后列表ok',$model->list);
    }

    //  获取详情
    public function get()
    {
        $this->ruleValidate([
            'id|售后id' => 'require|integer|>:0',
        ]);
        $model = new ServiceModel();
        $list = $model->getOne($this);
        return retu_json(200,'ok',$list);
    }

    // 更新换货地址信息
    public function updateExchange()
    {
        $this->ruleValidate([
            'id|售后id' => 'require|integer|>:0',
            'company_id|物流公司'=>'require|integer|>:0',
            'express_sn|快递单号'=>'require|min:6|max:40|alphaDash'
        ]);
        $model = new ServiceModel();
        $model->returnExpress($this);
        return retu_json(200,'售后订单换货地址ok');
    }

    // 物流公司列表
    public function logisticsList()
    {
        return retu_json(200,'物流公司名称ok',(new Logistics())->getAllList());
    }
}
