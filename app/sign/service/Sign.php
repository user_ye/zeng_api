<?php
namespace app\sign\service;

use app\common\model\Config;
use app\sign\model\Sign as SignModel;
use app\user\model\MiaoLog;
use app\user\model\User;
use app\user\model\UserWallet;
use app\user\model\WalletOperationLog;
use think\facade\Db;
use think\facade\Log;


/*打卡签到事务处理文件*/
class Sign{

    /*用户打卡签到奖励增加*/
    public function sign_in_reward($user)
    {
        /*用户等级为*/
        $userData = User::where('id',$user->id)->find();
        if($userData['level'] <= 1){
            return retu_json(400,'会员等级不够不能打卡签到');
        }

        /*如果用户已打卡过，查询打卡多少天对上打卡天数的*/
        /*如果用户第一次打卡,查询打卡第一天打卡的奖励*/
        $signData = SignModel::where('uid' , $user->id)->find();
        if($signData){
            if($signData['is_sign'] == 2) return retu_json(400,'今日已打卡');
        }

        //拿到打卡奖励
        $configData = Config::where(['type' => 'sing_in_reward'])->find();
        $content = \GuzzleHttp\json_decode($configData['content'],true);
        $days = $signData ? $signData['days'] : 0 ;
        //$days + 1 是为了匹配上奖励的数组下标
        $days = $days >= 7 ? 7 : $days + 1 ;
        $price = ($content[$days]) * 100;

        /*增加天数的累计*/
        Db::startTrans();
        try {
            if ($signData) {
                /*有打卡记录 则增加打卡天数*/
                $userSignData = SignModel::find($signData['id']);
                $userSignData->days = $userSignData->days + 1;
                $userSignData->is_sign = 2;
                $userSignData->upd_time = strtotime(date("Y-m-d H:i:s"));
                $userSignData->save();
            } else {
                /*第一次打卡记录天数*/
                $signModel = new SignModel();
                $signModel->uid = $user->id;
                $signModel->days = 1;
                $signModel->is_sign = 2;
                $signModel->add_time = strtotime(date("Y-m-d H:i:s"));
                $signModel->upd_time = strtotime(date("Y-m-d H:i:s"));
                $signModel->is_delete = 1;
                $signModel->save();
            }

            /*奖励增加user_wallet表*/
            $userWalletData = UserWallet::where(['uid' => $user->id])->find();
            $userWalletData->miao = $userWalletData->miao + $price;
            $userWalletData->miaos = $userWalletData->miaos + $price;
            $userWalletData->save();

            /*记录打卡的验证记录miao_log表*/
            $miaoLogModel = new MiaoLog();
            $miaoLogModel->type = 8;
            $miaoLogModel->order_type = 3;
            $miaoLogModel->uid = $user->id;
            $miaoLogModel->reward = $price;
            $miaoLogModel->status = 3;
            $miaoLogModel->add_time = strtotime(date("Y-m-d H:i:s"));
            $miaoLogModel->end_time = strtotime(date("Y-m-d H:i:s"));
            $miaoLogModel->money = $price;
            $miaoLogModel->proportion = $content[$days];
            $miaoLogModel->save();

            /*记录打卡的流水记录wallet_operation_log表*/
            $woLogModel = new WalletOperationLog();
            $woLogModel->uid = $user->id;
            $woLogModel->reward = $price;
            $woLogModel->status = 1;
            $woLogModel->type = 2;
            $woLogModel->extend = 'sign';
            $woLogModel->extend_id = $user->id;
            $woLogModel->describe = '用户签到打卡奖励';
            $woLogModel->add_time = strtotime(date("Y-m-d H:i:s"));
            $woLogModel->save();

            Db::commit();
        }catch(\Exception $e){
            Db::rollback();
        }

        return $user->id;
    }

    /**
     *用户打卡详情
     */
    public function user_sign_detail($user)
    {
        $signData = SignModel::where('uid',$user->id)->find();
        $signData = $signData ? $signData : [];

        //取出每日打卡奖励多少加在数组上面
        $configData = Config::where(['type' => 'sing_in_reward'])->find();
        $contents = json_decode($configData['content'],true);

        $days = $signData ? $signData['days'] : 0;
        $signData['days'] = $signData ? $signData['days'] : 0;
        $content = $contents;

        $curls = [];
        if($days > 7)
        {
            //连续打卡超过八天的数据整理
            $weeks = intval($days / 8);
            $sum = $days % 8;

            //今天已经打卡 则显示日期  没有打卡日期变为 今
            //status状态为已打卡过的天数
            //is_reward_double 为判断第七天是否双倍 大于八天就全部为1 不用给双倍标识
            for($i=0;$i<8;$i++)
            {
                $data = [
                    'status' => $sum >= $i ? 1 : 2,
                    'fate' => $sum + 1 == $i && $signData['is_sign'] == 1 ? '今' : $i + ($weeks * 8) ,
                    'reward' => $content[7],
                    'is_reward_double' => 1
                ];
                array_push($curls,json_encode($data));
                unset($data);
            }
        }else{
            //打卡不超过八天的数据整理
            foreach($content as $key => $val)
            {
                if($days == 0) $fate = $days+1 == $key  ? '今' : $key;
                else $fate = $days == $key && (isset($signData['is_sign']) == 1) ? '今' : $key;
                $data = [
                    'status' => $days >= $key ? 1 : 2,
                    'fate' => $fate,
                    'reward' => $val,
                    'is_reward_double' => $key == 7 ? 1 : 2
                ];

                array_push($curls,json_encode($data));
                unset($data);
            }
        }

        $signData['rules'] = $curls;
        //取出总喵币
        $userWalletData = UserWallet::where('uid',$user->id)->find();
        $signData['miao'] = $userWalletData['miao'] / 100;

        //返回数据
        return $signData;
    }


    /*第二套方案七天循环规则*/
    public function update_reward_days()
    {
        //把天数为七天的和已打卡的改为 天数为0天和不打卡状态
        $where = [
            'is_sign' => 2,
            'days' => 7
        ];
        $data = [
            'is_sign' => 1,
            'days' => 0
        ];
        SignModel::update($data,$where);
    }


    /**
     *定时任务改变打卡状态和改变今日不打卡的连续打卡天数
     */
    public function update_reward_rules()
    {
        SignModel::update(['days' => 0],['is_sign'=>1,'days'=>['>',0]]);
        SignModel::update(['is_sign' => 1],['is_sign' => 2]);
    }



}