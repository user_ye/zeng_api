<?php
declare (strict_types = 1);

namespace app\sign\controller;

use app\common\controller\BaseController;
use app\sign\service\Sign;
use think\Request;

//extends BaseController;

class Index extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        if(empty($this->user)&&!in_array(Request::action(true),['user_sign_detail'])){
            if($this->checkeUser() == false)
                return retu_json(401, '游客无此权限，请先登录授权！', []);
            else
                return retu_json(402, '请先登录！', []);
        }
    }

    public function index()
    {
        return '您好！这是一个[sign]示例应用';
    }

    /*用户签到打卡接口*/
    public function sign_in()
    {
        $signService = new Sign();
        $uid = $signService->sign_in_reward($this->user);
        return retu_json(200,'签到打卡成功',$uid);
    }

    /*用户打卡详情*/
    public function user_sign_detail()
    {
        $signService = new Sign();
        $userSignData = $signService->user_sign_detail($this->user);
        //$userSignData = $signService->user_sign_detail($this->user);
        return retu_json(200,'用户打卡详情',$userSignData);
    }

    /*定时任务测试方法*/
    public function update_reward()
    {
        $signService = new Sign();
        $signService->update_reward_rules();
    }

    /*下面为第二套的代码  接口调用都一样  定时任务改变逻辑即好*/
    public function clock_in()
    {
        $signService = new Sign();
        $signService->update_reward_days();
    }

}


