<?php
declare (strict_types = 1);
namespace app\view\model;
use app\common\model\BaseModel;
//use think\Model;

Class UserViewLevel extends BaseModel
{
    // 缓存键值
    public $sr_key = 'UserViewLevel_list';
    public $field = 'id,img,sort,desc';

    //    预处理数据处理
    // public function getImgAttr($value)
    // {
    //     #return env('app.host_domin').'://'.$_SERVER['HTTP_HOST'].$value;
    //     return env('app.host_domin').'://'.env('app.static_host') .'/newUploads/'.$value;
    // }

    //  预处理数据
    public function getDescAttr($value)
    {
        return explode(',',$value);
    }

    // 获取数据
    public function getList()
    {
        $this->setSrConfig([
            'field' => $this->field,
            'where' => ['show' => 1]
        ]);
        return $this->getListCache(false);
    }
}