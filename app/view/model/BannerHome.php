<?php
declare (strict_types = 1);
namespace app\view\model;
use app\common\model\BaseModel;
use think\Model;

Class BannerHome extends BaseModel
{
    // 缓存键值
    public $sr_key = 'BannerHome_list';
    public $field = 'id,img_url,url';

    //    预处理数据处理
    /*
    public function getImgUrlAttr($value)
    {
        #return env('app.host_domin').'://'.$_SERVER['HTTP_HOST'].$value;
        return env('app.host_domin').'://'.env('app.static_host').$value;
    }
    */

    // 获取数据
    public function getList()
    {
    //    $list = $this->where(['show'=>1])->order('sort desc')->field('id,img_url,url')->select()->toArray();
    //    return $list;
        $this->setSrConfig([
            'field' => $this->field,
            'where' => ['show' => 1]
        ]);
        return $this->getListCache(false);
    }
}