<?php
declare (strict_types = 1);
namespace app\view\model;
use app\common\helper\helper;
use think\Model;

Class Ad extends Model
{
    public $field = 'id,title,img_url,show,is_skip,url,app_url';
    // 获取广告栏目
    public function getList($type,$field = null)
    {
        $list = [];
        if(!$type)return $list;
        if(AdType::where(['key'=>$type,'status'=>1,'is_delete'=>0])->count())$list = $this->where(['type_key'=>$type,'is_delete'=>0,'show'=>1])->order('sort desc')->field($field ?? $this->field)->select();
        foreach( $list as &$item) {
            if($item['img_url']) $item['img_url'] = helper::getImgUrls($item['img_url']);
        }
        return $list;
    }


    //根据条件获取列表
    public function getListByWhere($where = ['type_key'=>'shop_nav_list','is_offline'=>1,'is_delete'=>0])
    {
        $list = [];
        if(!$where)return $list;
        $list = $this->where($where)->select()->toArray();
        return $list;
    }

}