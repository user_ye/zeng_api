<?php
declare (strict_types = 1);
namespace app\view\model;
use app\common\model\BaseModel;
use app\user\model\User;

Class HomeDisplay extends BaseModel
{
    // 缓存键值
    public $sr_key = 'HomeDisplay_list';
    public $error;

    // 获取数据
    public function getList()
    {
        try{
            $data = input('post.');
            $where = ['status'=>1,'is_delete'=>0];
            $item = $this->field('type,name,img,page')->where($where)->select();
            $list = empty($item) ? array():$item->toArray();
            $ret = [];
            if(!empty($list)){
                foreach($list as $k=>$v){
                    if($v['type'] == 1){
                        $ret['small'][] = $v;
                    }else{
                        $ret['big'][] = $v;
                    }
                }
            }
            return $ret;
        }catch (\Exception $e){
            if(stristr($e->getMessage(),'SQLSTATE')){
                $this->error = '数据走丢了，请稍后再试！';
            }else $this->error = $e->getMessage();
            return false;
        }
    }

}