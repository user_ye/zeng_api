<?php
declare (strict_types = 1);
namespace app\view\model;
//use think\Model;

use app\common\model\BaseModel;


Class RuleDescription extends BaseModel
{
    // 缓存键值
    public $sr_key = 'RuleDescription_list';

    // 获取数据
    public function getList()
    {
        $key = Request()->post('key',-1);
        if(!$key || $key < 0)
            return retu_json(400,'键值不得为空');
        $this->setSrConfig(['order'=> 'id desc','obj_key'=>'key']);
        $newList = $this->getListKeyCache(false);
        if(!isset($newList[$key]))
            return retu_json(400,'键值不存在');
        $item = $newList[$key];
        unset($item['key']);
        return $item;
    }
}