<?php
declare (strict_types = 1);
namespace app\view\model;
//use think\Model;

use app\common\model\BaseModel;


Class ViewProductBanner extends BaseModel
{
    // 缓存键值
    public $sr_key = 'ViewProductBanner_list';
    public $field = 'id,img_url,title,is_skip,goods_id,url,wxchat_url';


    //    预处理数据处理
    // public function getImgUrlAttr($value)
    // {
    //     #return env('app.host_domin').'://'.$_SERVER['HTTP_HOST'].$value;
    //     return env('app.host_domin').'://'.env('app.static_host').$value;
    // }

    // 获取数据
    public function getList()
    {
//        $list = $this->where('show',1)->field('id,img_url,title,is_skip,goods_id')->order('sort desc')->select()->toArray();
//        $list = $this->
        $this->setSrConfig([
            'field' => $this->field,
            'where' => ['show' => 1]
        ]);
        return $this->getListCache(false);
    }
}