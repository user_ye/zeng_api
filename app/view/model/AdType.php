<?php
declare (strict_types = 1);
namespace app\view\model;
use think\Model;

Class AdType extends Model
{
    // 根据类型拿取值
    public function getTypes($key){
        if(!$key) return false;
        return $this::where(['is_delete' => 0,'key' => $key,'status'=>1])->find();
    }

    // 获取到所有的列表操作
    public function getAdList(){
        $this::hasMoney(Ad::class,'type_key','key');
    }
}