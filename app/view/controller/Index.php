<?php
declare (strict_types = 1);

namespace app\view\controller;

use app\common\controller\BaseController;
use app\common\model\Config;
use app\common\self\SelfRedis;
use app\video\model\VideoType as VideoTypeModel;
use app\video\model\Video as VideoModel;
use app\view\model\Ad;
use app\view\model\BannerHome;
use app\view\model\BannerLive;
use app\view\model\HomeDisplay;
use app\view\model\RuleDescription;
use app\view\model\UserViewLevel;
use app\view\model\ViewProductBanner;
use app\view\model\ViewText;
use think\Request;



class Index
{
//    首页-轮播图
    public function banner()
    {
        // $adModel = new Ad();
        // $list = $adModel->getList(1,'cover,type,type_id,name,is_skip');
        
        // return retu_json(200,'轮播图ok',$list);
        $list = (new Ad())->getList('home_banner');
        return retu_json(200,'首页-轮播图ok',$list);
        /*
        $model = new BannerHome();
        $list = $model->getList();
        return retu_json(200,'首页-轮播图ok'.($model->sr_status ? '缓存' : '最新'),$list);
        */
    }

    // 测试数据
    public function home()
    {
        $model = new BannerHome();
        $list = $model->getList();
        return retu_json(200,'首页数据ok',[
            'banner_list' => $list,
            'video_type' => VideoTypeModel::where(['status' => 1,'pid' => 0])->select(),
            'video_list' => VideoModel::where(['status' => 1, 'is_check' => 1,'is_delete' => 0])->limit(0,10)->field('id,type_id,cover,add_time')->select()
        ]);
    }


// 商城-轮播图
    public function ProductBanner()
    {
        // $model = new ViewProductBanner();
        // $list = $model->getList();
        // return retu_json(200,'商城轮播图ok'.($model->sr_status ? '缓存' : '最新'),$list);

        $list = (new Ad())->getList('shop_banner');
        return retu_json(200,'商城-轮播图ok',$list);
    }

    // 用户等级权益说明
    public function userViewLevel()
    {
        $model = new UserViewLevel();
        $list = $model->getList();
        return retu_json(200,'用户等级权益轮播图ok'.($model->sr_status ? '缓存' : '最新'),$list);
    }

// 首页-直播栏目-广告栏目
    public function liveBroadcastBanner()
    {
        /*
        $model = new BannerLive();
        $list = $model->getList();
        return retu_json(200,'直播栏目广告ok'.($model->sr_status ? '缓存' : '最新'),$list);
        */
        $list = (new Ad())->getList('home_live_banner');
        return retu_json(200,'直播栏目广告ok',$list);
    }


//    首页记录条
    public function textLog()
    {
        $model = new ViewText();
        $list = $model->getList();
        return retu_json(200,'首页记录条ok--'.($model->sr_status ? '缓存' : '最新'),$list);
    }


//    首页记录条
    public function textLogB()
    {
        $model = new ViewText();
        $list = $model->getListB();
        return retu_json(200,'首页记录条ok--'.($model->sr_status ? '缓存' : '最新'),$list);
    }

    //  视频分类
    public function videoType()
    {
        $model = new VideoTypeModel();
        return retu_json(200,'视频分类ok',$model->getPid());
    }

    /**
     * 规格说明等
     */
    public function ruleSpecifications()
    {
        $model = new RuleDescription();
        return retu_json(200,'拿取说明ok',$model->getList());
    }

    /**
     * 首页视频是否隐藏
     */
    public function viewHomeVideoHide()
    {
        $base = new BaseController(false);
        $base->getToken();
        // 如果有用户 且 用户 等于虚拟用户
        if(isset($base->user) && $base->user->is_fictitious == 1){
            return retu_json(200,'虚拟用户返回---',[
                'switch' => false,
                'img' => ''
            ]);
        }
        $list = (new Ad())->getList('home_view');
        $data = [
            'switch' => false,
            'img' => null
        ];
        if($list && count($list) > 0){
            $data['switch'] = true;
            $data['img'] = $list[0]['img_url'];
        }
        return retu_json(200,'首页展示',$data);
    }

    // 商城功能数据展示
    public function homeDisplay()
    {
        $adModel =  new Ad();
        $list = [
            'big' => $adModel->getList('shop_ai_ar_nav','img_url as img, title as name,url as page'),
            'small' => $adModel->getList('shop_nav_list','img_url as img, title as name,url as page')
        ];
        return retu_json(200,'拿取商城首页功能列表',$list);
    }

    // 视频具体某类
    /*
    public function videos(Request $request)
    {
        $get = $request->get();
        $model = new VideoModel();
        return retu_json(200,'视频分类ok',$model->getList($get));
    }
    */

    //    测试数据
    public function data(){
//        $test = Cache::store('redis_host')->get('test');
//        $a = Cache::store('redis_build')->get('test');

//        dump($test);
//        $redis = new Redis();
        #$a = $redis->connect('47.194.183.140','6379');
//        dump($redis->set('t',111111111111));
        //var_dump($redis->set('test',4568));
    }

    // 测试后台评论数据 
    public function setComment(Request $request){
        
        $sr = new SelfRedis();
        $data = $request->param();
        if(!$data || count($data) == 0 || !is_array($data)) return retu_json(400,'无数据传输');
        return retu_json(200,'ok',$sr->redis->set('admin_comment_list_'.time(),$data));
    }

    // 获取线下门店类型列表
    public function getOfflineList(){
        $model = new Ad();
        $data = $model->getListByWhere();
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }
}
