<?php
declare (strict_types = 1);

namespace app\demo\validate;

use think\Validate;

class BlogCreate1 extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */	
//	protected $rule = [];
    public $rule =   [
        'name|名'  => 'require|max:25',
        'age|年龄'   => 'number|between:1,120',
        'email|邮箱' => 'email',
    ];
}
