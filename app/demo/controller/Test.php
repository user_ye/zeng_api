<?php
declare (strict_types = 1);

namespace app\demo\controller;

use Exception;

class Test
{
    public function index()
    {
        return '您好！这是一个[demo]示例应用';
    }

    public function hello()
    {
        return '我是demo测试Test';
    }

    public function list()
    {
        $srcPath = "D:/code/new_api/tp/public/pay_log/PayOrderEnd.log";//本地文件绝对路径
        $file = file_get_contents($srcPath);
        $cosConfig = config('cos');
        $secretId = $cosConfig['secretId']; //"云 API 密钥 SecretId";
        $secretKey = $cosConfig['secretKey']; //"云 API 密钥 SecretKey";
        $region = $cosConfig['region']; //设置一个默认的存储桶地域
        require app()->getRootPath().'\vendor\autoload.php';
        $cosClient = new \Qcloud\Cos\Client([
            'region' => $region,
            #'schema' => 'https', //协议头部，默认为http
            'credentials'=> ['secretId'  => $secretId ,'secretKey' => $secretKey]]
        );

        ### 上传文件流
        try {    
            $bucket = $cosConfig['bucket']; //存储桶名称 格式：BucketName-APPID
            $key = "exampleobject222";
            var_dump($file);
            if ($file) {
                $result = $cosClient->putObject($bucket = $bucket,$key = $key,$body = $file);
                var_dump($result);
            }
        } catch (Exception $e) {
            var_dump("$e\n");
        }
    }
}
