<?php
/**
 * Created by PhpStorm.
 * User: snuz
 * Date: 2020/7/6
 * Time: 17:22
 */
declare (strict_types = 1);

namespace app\demo\controller;

// 引用
use app\common\controller\BaseController;
use app\common\model\Config;
use app\common\self\SelfRedis;
use app\demo\validate\BlogCreate;
use app\demo\validate\BlogCreate1;
use app\order\model\Order;
use app\Request;
use app\user\model\MiaoLog;
use app\user\model\User;
use think\App;
use think\Db;
use think\facade\Log;
use think\exception\ValidateException;
use think\facade\View;



class Blog extends BaseController{
    public function __construct()
    {
        parent::__construct(false);
    }

    // 验证器使用1
    public function v()
    {
        $this->checkValidate(BlogCreate::class);
    }

    // 验证器  使用2
    public function v1()
    {
        $this->checkValidate(BlogCreate1::class);
    }

    public function redisDemo()
    {
        // 实例化对象 默认 本地 如果需要线上 则传入  'redis_debug' 即可  $redis1 = new SelfRedis('redis_debug');
        $sr = new SelfRedis();
        // 设置索引库
        $sr->redis->select(1);
        // 拿取数据
        $sr->redis->get('t');
        // 设置
        $sr->redis->set('t',666666666666);
        // 获取当前配置
        // $sr->config;
        // 另外一个库
        $sr2 = new SelfRedis('redis_debug');
        $sr2->redis->set('t',[33333333333333,222222222222,333333333333]);
        dump($sr->redis->get('t'),$sr->config,$sr2->redis->get('t'),$sr2->config);
    }


    public function index(){

        return View::fetch('');

//        View::create('')
//        $view->fetch('/demo/blog/blog.html');
//        $v = new View($app);
//        $v->fetch('/demo/blog/blog.html');
//        return 111;
        // 获取到支付的配置
        $payConfig = Config::where('type','business')->find();
        if(!$payConfig)
            return false;
        $payConfigList =  json_decode($payConfig->content,true);
        // 'receipt', // 待收货  结束 天数
        // 'service' // 售后结束 天数

        // 拿取状态为 待收货3   已收货4  已完成5
        $time = time();
        $orderModel =  new Order();
        $list = $orderModel->whereIn('status',[3,4,5])->select();
        $orderModel->startTrans();
        $miaoLogModel = new MiaoLog();
        foreach($list as $item){
             // 待收货 如果大于 后台设置的自动收货时间
            if($item['status'] == 3 && $item->deliver_time &&  $time - $item->deliver_time > $payConfigList['receipt'] * 3600 * 24){
                Log::write([
                    'title' => '-待收货的处理--脚本处理逻辑---',
                    'status' => $item->status,
                    'id' => $item->id
                ],'orderend');
                $item['receive_time'] = $time;$item->status = 4;$item->save();
            // 已收货4  已收货 7天后 完结订单 10 且 将喵呗中的记录更新待结算
            }else if(in_array($item['status'],[4,5])  && $item['receive_time'] &&  $time - $item['receive_time'] > $payConfigList['service'] * 3600 * 24){
                Log::write([
                    'title' => '-已收货的处理--脚本处理逻辑---',
                    'status' => $item->status,
                    'id' => $item->id
                ],'orderend');
                $item['complete_time'] = $time;$item->status = 10;$item->save();
                $miaoLogNum = $miaoLogModel->where('oid',$item->id)->update(['status' => 2]);
                Log::write('喵呗-状态已更新，更新记录如下：'.$miaoLogNum,'orderend');
            }
        }
        $orderModel->commit();
    }


    // 请求数据
    public function read($id = 1)
    {
        return retu_json(200,'拿去成功'.$id);
    }

    // 编辑数据
    public function update($id)
    {
        return retu_json(200,'111','更新成功'.$id);
    }

    // 删除操作
    public function del($id)
    {
        return retu_json(200,'111','删除成功'.$id);
    }
}