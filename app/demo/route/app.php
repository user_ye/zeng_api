<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\facade\Route;

//Route::get('think', function () {
//    return 'hello,ThinkPHP6!';
//});

//Route::get('hello/:name', 'index/hello');

#Route::get('aaa/:name', 'test/hello');

//  一个模板 对应一个 路由组 当个参看上面的
Route::group('blog', function () {
    // 请求， 更新，删除
    Route::get(':id', 'read');
    Route::post(':id', 'update');
    #Route::delete(':id', 'delete');
    Route::post(':id', 'delete');
#})->completeMatch()->prefix('blog/')->ext('html')->pattern(['id' => '\d+']);
})->prefix('blog/')->pattern(['id' => '\d+']);