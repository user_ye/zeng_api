<?php
declare (strict_types = 1);

namespace app\command;

use app\common\self\MyJob;
use app\mall\model\GoodsComment;
use app\order\model\Order;
use app\order\model\OrderGoods;
use app\order\model\OrderInfo;
use app\user\model\User;
use app\video\model\Video;
use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use think\facade\Log;
use think\facade\Queue;

use app\common\self\SelfRedis;

//use think\Queue;


class Pub extends Command
{
    public $data;

    protected function configure()
    {
        // 指令配置
        $this->setName('pub')
            ->setDescription('我是订阅者');        
    }

    protected function execute(Input $input, Output $output)
    {
        $output->writeln('2222222222---------');
        $sr = new SelfRedis();
        $sr->redis->config('notify-keyspace-events','Ex');
        $sr->redis->set('dsds',111111);
        $s = $sr->redis->publish('test','hello,world');
        var_dump($s);
    	$output->writeln('--------订阅者推送ok---------------');
    }
}
