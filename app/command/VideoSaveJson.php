<?php
declare (strict_types = 1);

namespace app\command;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;

class VideoSaveJson extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('videosavejson')
            ->setDescription('视频截取保存');        
    }

    protected function execute(Input $input, Output $output)
    {
    	// 指令输出
        $output->writeln('videosavejson');
        $this->test1();
    }


    /*
     * test
     */
    public function test1()
    {
        $dir = '/www/wwwroot/api/tp/public/video_list/';//文件地址
        $path = '/www/wwwroot/api/tp/public/copy_video/';//存放地址
        $pathImg = '/copy_video/';//存放地址
        $host = 'https://admin-miaomeimei-1301812909.cos.ap-guangzhou.myqcloud.com/video/';//域名
        //$type = ['lxjk'=>23,'cnzk'=>22,'mrmf'=>21,'jfss'=>20,'mzzc'=>19,'hfys'=>18];//分类名  key为文件目录，value为视频分类表id  'lxjk'=>23,'cnzk'=>22,'mrmf'=>21,'jfss'=>20,'mzzc'=>19,'hfys'=>18
        // 视频类型
        $type = ['chuangtai' => 25];
        // 用户id信息
        $uids = ["656","654","648",629,644,606,271,644,542,275];
        $uidsLen = count($uids) - 1;
        $array_allFile_name = array();
        $array_name = array();
        $array_img = array();
        $i = 0;
        foreach($type as $k=>$v){
            if(is_dir($dir.$k)){
                $handle = opendir($dir. $k. "/.");
                while (false !== ($file = readdir($handle))) {
                    if ($file != "." && $file != "..") {
                        // 如果找不到.mp4那么跳过 并提示
                        if(strpos($file,'.mp4') == false){
                            var_dump('当前文件名找不到.mp4，已跳过');
                            fileLog("当前文件名找不到.mp4，已跳过，----当前文件路劲".$file,'视频截取ok.log');
                            continue;
                        }
                        $array_allFile_name[] = $file;
                        $key = rand(0,$uidsLen);
                        $_name = explode('.mp4',$file);
                        if(!is_array($_name)) $_name = [$_name];
                        $filename = date('YmdHis').'_'.$v."_$i.mp4";//生成文件名
                        //rename($dir.$v.'/'.$file,$dir.$v.'/'.$filename);
                        $cc = copy($dir.$k.'/'.$file, $path.$k.'/'.$filename);//拷贝
                        if($cc){
                            //获取视频第一帧画面生成图片并保存到数据库  参数1-文件地址，2-生成图片名，3-存储文件夹，4-第几秒画面，5-宽高
                            $img_name  = date('YmdHis').'_'.$v."_$i.jpeg";
                            $array_img[] = getVideoCover($path.$k.'/'.$filename, $img_name,$pathImg.$k.'_img/', 3, 0);
                            fileLog("当前文件名:".json_encode($_name).'图片信息：'.$img_name,'视频截取ok.log');
                            $array_name[] = ['uid'=>(int)$uids[$key],'type'=>0,'type_id'=>$v,'relation_id'=>0,'title'=>$_name[0],'cover'=>$host.$k.'/'.$img_name,'video'=>$host.$k.'/'.$filename,'status'=>1,'is_check'=>1,'likes'=>rand(500,5000),'shares'=>rand(100,599)];
                            $i++;
                        }else{
                            var_dump('复制目录失败，已跳过');
                            fileLog("拷贝失败：当前文件路劲".$file,'视频截取ok.log');
                        }
                    }
                }
                closedir($handle);
            }
        }
        var_dump('执行完毕-----');
        $sql = $this->multArrayInsert('m3_video',['uid','type','type_id','relation_id','title','cover','video','status','is_check','likes','shares'],$array_name);//生成sql
        file_put_contents('/保存视频.sql',$sql);//保存sql
        file_put_contents('/保存视频-name.json',json_encode($array_name));//保存json
        var_dump($sql,$array_name,$array_img);
    }


    public function multArrayInsert($table,$arr_key, $arr, $split = '`') {
        $arrValues = array();
        if (empty($table) || !is_array($arr_key) || !is_array($arr)) return false;
        $sql = "INSERT INTO %s( %s ) values %s ";
        foreach ($arr as $k => $v) {
            $arrValues[$k] = "'".implode("','",array_values($v))."'";
        }
        $sql = sprintf($sql, $table, "{$split}" . implode("{$split} ,{$split}", $arr_key) . "{$split}", "(" . implode(") , (", array_values($arrValues)) . ")");
        return $sql;
    }
}
