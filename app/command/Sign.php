<?php
declare (strict_types = 1);

namespace app\command;

use app\sign\model\Sign as SignModel;
use think\console\Command;
use think\console\Input;
use think\console\Output;

class Sign extends Command
{
    public $num = 0;

    protected function configure()
    {
        // 指令配置
        $this->setName('sign')
            ->setDescription('获取打卡状态');
    }

    protected function execute(Input $input, Output $output)
    {
        $startTime = time();
        $output->writeln('获取打卡状态开始----sign----时间：'.date('Y-m-d H:i:s'));
        $this->updateStatus($output);
        $endTime = time();
        // 指令输出
        $output->writeln('获取打卡状态结束----sign----总共影响了'."--所用时间".($startTime-$endTime).'秒'."，时间:".date('Y-m-d H:i:s'));
    }

    /**
     * 更新直播状态
     */
    public function updateStatus($output)
    {
        //fileLog('start-----'.time(),date('Ymd').'_sign.log');
        SignModel::update(['days' => 0],['is_sign'=>1]);
        SignModel::update(['is_sign' => 1],['is_sign' => 2]);
        //fileLog(time().'-----end',date('Ymd').'_sign.log');
    }
}
