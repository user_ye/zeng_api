<?php
declare (strict_types = 1);

namespace app\command;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use app\video\model\Video;
use app\video\model\VideoType;
use think\console\Output;

class VideoSort extends Command
{
    public $num = 0;

    protected function configure()
    {
        // 指令配置
        $this->setName('videosort')
            ->setDescription('视频随机打乱排序');        
    }

    protected function execute(Input $input, Output $output)
    {
        $startTime = time();
        $output->writeln('随机打乱视频开始----videosort----时间：'.date('Y-m-d H:i:s'));
        $this->sortVideoType($output);
        $endTime = time();
    	// 指令输出
    	$output->writeln('随机打乱视频结束----videosort----总共影响了'.$this->num."--所用时间".($startTime-$endTime).'秒'."，时间:".date('Y-m-d H:i:s'));
    }

    /**
     * 视频随机打乱排序开始
     */
    public function sortVideoType($output){
        // 获取到视频 分类
        $typeVideos = new VideoType();
        $list = $typeVideos->where(['status'=>1,'is_delete'=>0])->field('id,name')->select();
        $video = new Video();
        if($list && count($list) > 0){
            $list = $list->toArray();
            foreach($list as $item){
                list($len,$updateNum) = $video->sortType($item['id']);
                $output->writeln("\t\t----分类id".$item['id'].',名称：'.$item['name'].',改的数据:'.$updateNum.',总的数据:'.$len."\r\n");
                $this->num += $updateNum;
            }
        }
    }
}
