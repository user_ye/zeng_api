<?php
declare (strict_types = 1);

namespace app\command;

use app\api\model\WxApi;
use app\live\model\LiveBroadcast;
use think\console\Command;
use think\console\Input;
use think\console\Output;
use think\facade\Db;

class LiveStatus extends Command
{
    public $num = 0;

    protected function configure()
    {
        // 指令配置
        $this->setName('livestatus')
            ->setDescription('获取直播状态');
    }

    protected function execute(Input $input, Output $output)
    {
        $startTime = time();
        $output->writeln('获取直播状态开始----livestatus----时间：'.date('Y-m-d H:i:s'));
        $this->updateStatus($output);
        $endTime = time();
        // 指令输出
        $output->writeln('获取直播状态结束----livestatus----总共影响了'.$this->num."--所用时间".($startTime-$endTime).'秒'."，时间:".date('Y-m-d H:i:s'));
    }

    /**
     * 更新直播状态
     */
    public function updateStatus($output){
        fileLog('start-----'.time(),date('Ymd').'_ts.log');
        // 获取到视频 分类
        $model = new WxApi();
        $live = new LiveBroadcast();
        $total = 0;
        $room_info = [];
        $resData = [];
        $data = $model->getLiveList(['start'=>0,'limit'=>100]);
        if(!empty($data)){
            $total_wx = $data['total'];
            if(isset($data['room_info'])){
                while($total < $total_wx){
                    $total += count($data['room_info']);
                    $room_info = array_merge($room_info,$data['room_info']);
                    if($total_wx-$total > 100) $num = 100;
                    else $num = $total_wx-$total;
                    $data = $model->getLiveList(['start'=>$total,'limit'=>$num]);
                    if(empty($data['room_info'])) break;
                }
                if(!empty($room_info)){
                    foreach($room_info as $k=>$v){
                        $resData[$v['roomid']]['roomid'] = $v['roomid'];
                        $resData[$v['roomid']]['live_status'] = $v['live_status'];
                    }
                    $roomid = array_column($resData,'roomid');
                    //101=>'直播中', 102=>'未开始', 103=>'已结束', 104=>'禁播', 105=>'暂停', 106=>'异常', 107=>'已过期'
                    //0-待审核，1-预热中，2-直播中，3-回放，4-已结束，5-已过期
                    $statusMap = [101=>2,102=>1,103=>3,104=>4,105=>2,106=>2,107=>5];
                    $updateDate = [];
                    $list = $live->field('id,room_id')->where('room_id','in',$roomid)->select();
                    $list = !empty($list) ? $list->toArray() : [];
                    if(!empty($list)){
                        foreach($list as $k=>$v){
                            if(isset($resData[$v['room_id']])){
                                if(isset($statusMap[$resData[$v['room_id']]['live_status']])){
                                    $updateDate[$v['id']] = $statusMap[$resData[$v['room_id']]['live_status']];
                                }
                            }
                        }
                        if(!empty($updateDate))
                            $ids = implode(',', array_keys($updateDate));
                        $sql = "UPDATE m3_live_broadcast SET `status` = CASE id ";
                        foreach ($updateDate as $id => $ordinal) {
                            $sql .= sprintf("WHEN %d THEN %d ", $id, $ordinal);
                        }
                        $sql .= "END WHERE id IN ($ids)";
                        fileLog($sql.'=================================',date('Ymd').'_ts.log');
                        fileLog(json_encode([$list,$updateDate]),date('Ymd').'_ts.log');
                        Db::execute($sql);
                    }
                }

            }
        }
        fileLog(time().'-----end',date('Ymd').'_ts.log');
    }
}
