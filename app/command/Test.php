<?php
declare (strict_types = 1);

namespace app\command;

use app\api\model\WxApi;
use app\api\model\WxApiLikeGoods;
use app\common\self\MyJob;
use app\common\self\SelfRedis;
use app\live\model\LiveBroadcastInfo;
use app\live\model\LiveMiaoLog;
use app\mall\model\GoodsComment;
use app\mall\service\CommentServer;
use app\order\model\Order;
use app\order\model\OrderGoods;
use app\order\model\OrderInfo;
use app\order\service\order as orderService;
use app\user\model\User;
use app\user\model\UserWallet;
use app\video\model\Video;
use app\video\model\VideoType;
use think\console\Command;
use think\console\command\VendorPublish;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use think\Db;
use think\facade\Log;
use think\facade\Queue;
use think\view\driver\Think;

//use think\Queue;


class Test extends Command
{
    public $data;

    protected function configure()
    {
        // 指令配置
        $this->setName('test')
            ->setDescription('the test command');        
    }

    protected function execute(Input $input, Output $output)
    {
        $orderService = new orderService();
        $model = new Order();
        $orderService->delOrderInfos();
        // $orderService->editOrderGoodsS(709);
        // exit;
        // $list = $model->field('id')->select();
        // foreach($list as $item){
        //     var_dump($orderService->editOrderGoodsS($item['id']));
        // }
        exit;
        $commentServer = new CommentServer();
        $list = $commentServer->deleteComment(371);
        var_dump($list);
//        var_dump($delete_objKeys);
        exit;

        $serviceOrder =  new \app\order\service\order();
//        $limit = 0;
        $limit =  $serviceOrder->editAddres();
//        $limit = $serviceOrder->del(825  );
        var_dump($limit);
        exit;

		/*
        echo 222;
        $wxapi = new WxApi();
        $status = $wxapi->uploadImg(['img'=>'https://admin-miaomeimei-1301812909.cos.ap-guangzhou.myqcloud.com/admin/userNickName/2020_08_25/2020_08_25_1598341711_1263.jpg','type'=>'image']);
        var_dump($status);
        exit;
        $is = LiveMiaoLog::insertGetId([
            'uid' => 4070,
            'oid' => 0,
            'live_id' => 23,
            'reward' => 50,
            'add_time' => time()
        ]);
        var_dump($is);
        exit;
        var_dump(date('Ymd'));
        exit;
//        $wx_goods = new WxApiLikeGoods();
//        $list = $wx_goods->addGoods([
//                'coverImgUrl' => '-YZpT671hIWoiUctQnTROWf-IQxfKyXKoKfkm_DpFR-2HRUyk_iMQKKvGhmMiI4m',
//                'name' => '测试商品',
//                'priceType' => 3,
//                'price' => 100,
//                'price2' => 50,
//                'url' => 'pages/productDetails/productDetails?id=116'
//        ]);
//        $list = $wx_goods->returnGoods(694416480,4);
//        var_dump($list);

//        $list = $wx_goods->getGoodsStatus([4]);
//        var_dump($list);
//        //$list = $wx_goods->delGoods(3);
//        exit;
        $wx = new WxApi();
        $data = [
            'type' => 'image',
            'img' => 'D:/code/LIVETEST/api/public/uploads/ttt.jpg'
        ];
        var_dump($data);
        var_dump('2222');
        $res = $wx->uploadImg($data);
        var_dump($res);
        exit;
		*/
        // $video = new  Video();
        // $list = $video->where('title','杀杀杀')->find();
        // var_dump($list->getData());
        $data = file_get_contents('D:\code\test_api\tp\app\command\data1.json');
        $data = json_decode($data,true);
        $user = new User();
        $userWallet = new UserWallet();
        // var_dump($user->count());
        // $userLimit = $user->where('id','>=',1395)->select();
        $k = 0;
        foreach($data as $item){
            $id = $user->insertGetId($item);
            $userWallet->insertGetId(['uid' => $id]);
            var_dump($id);
            $k++;
            // $wallets[] = [
            //     'uid' => $item->id
            // ];
        }
        // var_dump($userLimit);
        //$count = $userWallet->insertAll($wallets);
        var_dump($k);
        exit;
        // $count = $user->insertAll($data);
        // var_dump($count);
        exit;
        $b = MyJob::pushQueue('FileUploadsJob',[
            'file' => 'D:/code/new_api/tp/public/pay_log/PayOrderEnd.log', // 文件路劲
            'cos_path' => 'cccccccccc'.time().'.log', // 存储库路劲
            'id' => 498,
            'table' =>  serialize(new Video()),
            'data' => [
                'title' => '我是测试啊'
            ]
        ]);

        exit;
        $b = MyJob::pushQueue('FileUploadsJob',[
            'file' => 'D:/code/new_api/tp/public/pay_log/PayOrderEnd.log', // 文件路劲
            'cos_path' => 'cccccccccc'.time().'.log', // 存储库路劲
            'id' => 500,
            'table' => 'video',
            'data' => [
                'title' => '我是测试啊'
            ]
        ]);
        exit;
        require 'vendor/autoload.php';
         //引用COS sdk
         #VendorPublish('sdk.vendor.autoload');
         #\think\Loader::import('sdk.vendor.autoload'); 
         $cosConfig = config('cos');
        $secretId = $cosConfig['secretId']; //"云 API 密钥 SecretId";
        $secretKey = $cosConfig['secretKey']; //"云 API 密钥 SecretKey";
        $region = $cosConfig['region']; //设置一个默认的存储桶地域
        # 创建存储对象
        $cosClient = new \Qcloud\Cos\Client([
                'region' => $region,
                #'schema' => 'https', //协议头部，默认为http
                'credentials'=> ['secretId'  => $secretId ,'secretKey' => $secretKey]]
        );
        $srcPath = "D:/code/new_api/tp/public/pay_log/PayOrderEnd.log";//本地文件绝对路径
    $file = file_get_contents($srcPath);
    var_dump($file);
### 上传文件流
try {    
    $bucket = $cosConfig['bucket']; //存储桶名称 格式：BucketName-APPID
    $key = "exampleobject2222";
    if ($file) {
        $result = $cosClient->putObject([
            "Bucket"=>$bucket,
            "Key"=>$key,
            "Body"=>$file
        ]);
        var_dump($result);
    }
} catch (\Exception $e) {
    var_dump("$e\n");
}
        exit;


        $output->writeln('3333333333---------');
        $b = MyJob::pushQueue('TestJob',['name'=> '十大撒旦所大'],2);
        exit;

        $sr = new SelfRedis();
        $sr->redis->select(9);
        $sr->redis->setNx('mmei_miaolog1111111111:team',10,2);

        $output->writeln('11111---------');

        $output->write(date("[Y-m-d H:i:s]").": 脚本结束".PHP_EOL);
        Log::write('当前时间:'.date('Y-m-d H:i:s'),'type');


        $orderModel = Order::find(182);
        $orderGoodsModelArrs = OrderGoods::where('order_id',$orderModel->id)->select();
        $teamOrder = [];
        foreach($orderGoodsModelArrs as $k=>$v)
            if($v['is_team'] == 1 ) $teamOrder[] = $v;
        $data = ['uid' => 65, 'order' => $teamOrder];
        $b = MyJob::pushQueue('TeamSettlementJob',$data,2);
        var_dump('------结束------------'.$b);
        $output->write($b);
        // 指令输出
        $output->write(date("[Y-m-d H:i:s]").": 脚本结束".PHP_EOL);
        exit;

    	// 指令输出
        $data = [
            'openid' => 'o68MO5MvmaEPlV6pASKNXliedBXY',
            'out_trade_no' => '20072020215546747212',
        ];

        $orn = substr($data['out_trade_no'],0,20);


        $orderModel = new Order();
        $userModel = new User();
        $orderInfoModel = new OrderInfo();

        // 拿取模型数据
        $orderOrnModel = $orderModel->where('order_sn',$orn)->find();
        $orderInfoIdModel = $orderInfoModel->where('oid',$orderOrnModel->id)->find();
        $userModel = $userModel->where('openid',$data['openid'])->find();



        $output->writeln('---模型调用开始-------------------');

        // 调用统一处理逻辑  需要走下面的处理
        $list = $orderModel->completeOrder([
            'orderOrnModel' => $orderOrnModel,
            'orderInfoModel' => $orderInfoIdModel,
            'userModel' => $userModel
        ]);
        var_dump($list);
    	$output->writeln('--------模型调用结束---------------');
    }

    /**
     * 视频随机打乱排序开始
     */
    public function sortVideoType(){
        // 获取到视频 分类
        $typeVideos = new VideoType();
        $list = $typeVideos->field('id')->select()->toArray();
        $video = new Video();
        if($list && count($list) > 0){
            $list = $list->toArray();
            foreach($list as $item){
                $video->sortType($item['id']);
            }
        }
    }

    /**
     * 拿取备用虚拟用户
     */
    public function xuNiUserData(){
        $files = 'D:\code\new_api\tp\public\5555';
        $model = new User();
        $list = [];
        //1、首先先读取文件夹
        $temp=scandir($files);
        $ids = [];
        foreach($temp as $v){            
            $a1 = $files.'/'.$v;
            var_dump($v);
            $s = $model->where('avatarurl','https://admin-miaomeimei-1301812909.cos.ap-guangzhou.myqcloud.com/admin/userNickName/'.$v)->find();
            if($s && $data = $s->toArray()){
                $list[] = $data;
            }
        }
        // https://admin-miaomeimei-1301812909.cos.ap-guangzhou.myqcloud.com/admin/userNickName/2020_08_03_007f86178e00d5946b11cc55998d1f18.jpg
        // https://miaommeis-1301812909.cos.ap-guangzhou.myqcloud.com/icon/%E7%BC%96%E7%BB%84%205%402x.png
        file_put_contents('D:\code\new_api\tp\public\userSql.json',json_encode($list));
    }

    /**
     * 插入虚拟用户数据中
     */
    public function insertUser(){
        $obj = file_get_contents('D:\code\new_api\tp\public\userSql.json');
        $obj = json_decode($obj,true);
        $model = new User();
        $uid = [];
        $wallerModel = new UserWallet();
        foreach($obj as $item){
            if($item['name']){
                $whereEre = [ 
                    'avatarurl'=>$item['avatarurl'],
                    'nickname' =>$item['nickname'] 
                ];
                if( $model->where($whereEre)->count() > 0 ){
                    continue;
                }
                $item['name'] = emojiEncode($item['name']);
                $item['nickname'] = emojiEncode($item['nickname']);
                $id = $model->insertGetId($item);
                $uid[] = $id;
                if($id){
                    $wallerModel->insert([
                        'uid' => $id,
                    ]);
                }
            }
        }
        file_put_contents('D:\code\new_api\tp\public\uid.json',json_encode($uid));
        #var_dump($model->saveAll($obj));
        exit;
        $db =new Db();
        $o = Db::table('m3_user')->saveAll($obj);
        #$o = $model->savaAll($obj);
        var_dump($o);
    }


    /**
     * 订单支付 -  回调 测试  模型开始
     */
    public function PayOrderEnd()
    {

        var_dump('开始订单支付-结束回调中——---');

        // 指令输出
        $oid = 300;

        $orderModel = new Order();
        $userModel = new User();
        $orderInfoModel = new OrderInfo();

        // 拿取模型数据
        $orderOrnModel = $orderModel->find($oid);
        $orderInfoIdModel = $orderInfoModel->where('oid',$orderOrnModel->id)->find();
        $userModel = $userModel->find($orderOrnModel->uid);



        // $output->writeln('---模型调用开始-------------------');

        // 调用统一处理逻辑  需要走下面的处理
        $list = $orderModel->completeOrder([
            'orderOrnModel' => $orderOrnModel,
            'orderInfoModel' => $orderInfoIdModel,
            'userModel' => $userModel
        ]);
        var_dump($list);
        var_dump('订单支付-结束回调中——---处理完毕-----');
    }
}
