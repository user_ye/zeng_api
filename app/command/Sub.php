<?php
declare (strict_types = 1);

namespace app\command;

use app\common\self\MyJob;
use app\mall\model\GoodsComment;
use app\order\model\Order;
use app\order\model\OrderGoods;
use app\order\model\OrderInfo;
use app\user\model\User;
use app\video\model\Video;
use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use think\facade\Log;
use think\facade\Queue;

use app\common\self\SelfRedis;
use think\queue\connector\Redis;
class Sub extends Command
{
    public $data;

    protected function configure()
    {
        // 指令配置
        $this->setName('sub')
            ->setDescription('我是发布者-通知者');       
    }


    protected function execute(Input $input, Output $output)
    {
        $output->writeln('11111---------');
        global $sr;
        $sr = new SelfRedis();
        # 防止没有配置 最好是重新配置一下
        $sr->redis->config('notify-keyspace-events','Ex');
        # 此处 不能使用 Redis 类型 $instance 会报错
        $sr->redis->subscribe(array('test'),function($instance, $channel, $message) use($sr){
                echo 'recieve message from '.$channel.':'.$message.'\n';
                var_dump(3333333);
        });
    }
}


