<?php
namespace app\order\service;

use app\live\model\LiveMiaoLog;
use app\order\model\Order as OrderModel;
use app\order\model\OrderGoods;
use app\order\model\OrderInfo;
use app\mall\model\ActivityLog;
use app\myself\Bc;
use app\service\model\Service;
use app\service\model\ServiceGoods;
use app\share\model\UserShareOrder;
use app\user\model\MiaoLog;
use app\user\model\User;

// 订单模型  -  提供服务
class order{

    // 将订单号所关联的信息进行删除
    public function del($id){
        if($id < 0) return 0;
        $orderOne = OrderModel::find($id);
        if(!$orderOne) return 0;
        $num = 0;
        $num += OrderInfo::where('oid',$id)->delete();
        $num += OrderGoods::where('order_id',$id)->delete();
        $num += ActivityLog::where('oid',$id)->delete();
        $num += UserShareOrder::where('oid',$id)->delete();
        $num += MiaoLog::where('oid',$id)->delete();
        $num += LiveMiaoLog::where('oid',$id)->delete();
        $num += Service::where('order_id',$id)->delete();
        $num += ServiceGoods::where('oid',$id)->delete();
        return $num;
    }

    // 提供一个用户id，将所有的订单关联信息删除
    public function getUserDel($userId = 0){
        if($userId <= 0 ) return 0;
        $total = 0;
        $userOne = User::where('id',$userId)->find();
        if(!$userOne) return 0;
        if($userOne->is_fictitious == 0) return 0;
        $orderData = OrderModel::where('uid',$userId)->field('id')->select();
        if(!$orderData) return 0;
        foreach($orderData as $item) $total += $this->del($item['id']);
        return $total;
    }

    /**
     * 将json字段中的收货人 写入为对应的收货人信息
     */
    public function editAdressJson($id){
        if(!$id || $id <= 0)return false;
        $model = new OrderModel();
        if(!$one = $model::field('id,address_json,receipt_name')->find($id))return false;
        if(!$one->address_json)return false;
        if($one->receipt_name || strlen($one->receipt_name) >= 2)return false;
        if(!$json = json_decode($one->address_json,true))return false;
        return $model::where('id',$one->id)->update([
            'receipt_name' => $json['name'],
            'receipt_phone' => $json['phone'],
            'receipt_address' => $json['address']
        ]);
    }

    /**
     * 补充填充 订单商品数据 - 此处只存在于一对一关系
     */
    public function editOrderGoodsS($id){
        if(!$id || $id <= 0)return false;
        $model = new OrderModel();
        // 拿取订单
        if(!$one = $model->find($id))return false;
        // 拿取订单详情数据
        $orderInfoOne = OrderInfo::where('oid',$one->id)->find();
        // var_dump($orderInfoOne['json']['good_attr']['discount_price']);
        // var_dump($one['volume']);
        // var_dump($orderInfoOne['json']['goods']['ticket']);
        $OrderGoodsOne = OrderGoods::where('order_id',$one->id)->find();
        $purchase_ratio = 0;
        $bc = new Bc();
        if($OrderGoodsOne->purchase_money && $OrderGoodsOne->purchase_money > 0)$purchase_ratio = $bc->calcDiv($OrderGoodsOne->purchase_money / $OrderGoodsOne->num,$OrderGoodsOne->price,3);
        $coupon_money = 0;
        if(isset($one->coupon_money) && $one->coupon_money > 0)$coupon_money = $one->coupon_money;
        $str = '';
        if($orderInfoOne['json']['good_attr']['valjson'] && is_array($orderInfoOne['json']['good_attr']['valjson']))foreach($orderInfoOne['json']['good_attr']['valjson'] as $k => $v)$str .= "$k:$v ";
        // 更新订单商品数据
        return OrderGoods::where('order_id',$one->id)->update([
            'attr_str' => $str,
            'discount_price' =>  $orderInfoOne['json']['good_attr']['discount_price'] * 100 , // 划线价
            'volume' => $one['volume'] * 100,// 购物券抵扣金额
            'volume_ratio' => $orderInfoOne['json']['goods']['ticket'] / 100, // 购物券可抵扣金额
            'purchase_ratio' => $purchase_ratio - 0, // 自购省比例
            'coupon_money' => $coupon_money, // 优惠券金额
            'title' => $orderInfoOne['json']['goods']['name'],// 商品名称
            'img' => $orderInfoOne['json']['good_attr']['img']
        ]);
    }
}
?>