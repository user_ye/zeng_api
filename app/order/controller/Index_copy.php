<?php
declare (strict_types = 1);

namespace app\order\controller;

use app\common\controller\BaseController;
use app\order\model\Order;
use think\Request;

class Index extends BaseController
{
    public function index()
    {
        return '您好！这是一个[order]示例应用';
    }

    /**
     * 订单列表
     */
    public function list()
    {
        $model = new Order();
        $model->getList(Request()->param(),$this->user);
        return retu_json(200,'订单列表ok',$model->list);
    }

    /**
     * 订单详情
     */
    public function get()
    {
        $model = new Order();
        $list = $model->getOne(Request()->param(),$this->user);
        return retu_json(200,'查看订单详情',$list);
    }

    /**
     * 取消
     */
    public function cancel()
    {
        (new Order())->checkCancel($this->user);
    }

    /**
     * 付款
     */
    public function payment()
    {
        $list = (new Order())->pendingPayment($this->user);
        return retu_json(200,'调起支付ok',$list);
    }

    // 确认收货
    public function receipt()
    {
        (new Order())->checkReceipt($this->user);
        return retu_json(200,'ok');
    }

    /**
     * 删除
     */
    public function del()
    {
        (new Order())->del($this->user);
        return retu_json(200,'删除ok');
    }

}
