<?php
declare (strict_types = 1);

namespace app\order\model;

use think\Model;

/**
 * @mixin \think\Model
 */
class OrderGoods extends Model
{
    public $page = '';//分页数据
    public $count = '';//数据总数
    public $error = '';//报错

    //数据预处理-  购物券金额
    public function getVolumeAttr($value)
    {
        return number_format($value/100,2,'.','');
    }

    //数据预处理-  自购省金额
    public function getPurchaseMoneyAttr($value)
    {
        return number_format($value/100,2,'.','');
    }

    //数据预处理-  json
    public function getJsonAttr($value)
    {
        return json_decode($value,true);
    }

    //数据预处理-  商品金额
    public function getPriceAttr($value)
    {
        return number_format($value/100,2,'.','');
    }

//    public function getJsonAttr($value)
//    {
////        return number_format($value/100,2,'.','');
//        return json_decode();
//    }


    /*
     * 获取店铺订单详情
     */
    public function getOrderGoodsInfo($oid){
        try{
            if(empty($oid)) exception('订单编号错误!');
            $where = [['order_id', '=', $oid]];
            $field = 'price,num,volume,total,purchase_money,is_appraise,attr_str,img,title';
            $item = $this->field($field)->where($where)->select();
            $goods['data'] = empty($item) ? array():$item->toArray();
            $total = $volume = $purchase = 0;
            if(!empty($goods['data'])){
                foreach($goods['data'] as $k=>$v){
                    $total += $v['price']*$v['num'];
                    $volume += $v['volume']*$v['num'];
                    $purchase += $v['purchase_money']*$v['num'];
                    if(!empty($v['attr_str'])){
                        $goods['data'][$k]['good_attr'] = array_filter(explode(' ',$v['attr_str']));
                    }
                    $goods['data'][$k]['total'] = getformat($v['total']);
                    unset($goods['data'][$k]['attr_str']);
                }
                $goods['money'] = ['total'=>$total,'volume'=>$volume,'purchase'=>$purchase];
            }
            return $goods;
        }catch (\Exception $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
}
