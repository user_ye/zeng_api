<?php
declare (strict_types = 1);

namespace app\order\model;

use think\Model;

/**
 * @mixin \think\Model
 */
class OrderInfo extends Model
{
    public $page = '';//分页数据
    public $count = '';//数据总数
    public $error = '';//报错

    //数据预处理-  购物券金额
    public function getVolumeAttr($value)
    {
        return number_format($value/100,2,'.','');
    }

    //数据预处理-  自购省金额
    public function getPurchaseMoneyAttr($value)
    {
        return number_format($value/100,2,'.','');
    }

    //数据预处理-  json
    public function getJsonAttr($value)
    {
        return json_decode($value,true);
    }


//    public function getJsonAttr($value)
//    {
////        return number_format($value/100,2,'.','');
//        return json_decode();
//    }
}
