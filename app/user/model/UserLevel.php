<?php
declare (strict_types = 1);

namespace app\user\model;

use think\Model;
use app\common\model\BaseModel;

/**
 * @mixin \think\Model
 */
class UserLevel extends BaseModel
{
    public $page = '';//分页数据
    public $count = '';//数据总数
    public $error = '';//报错
    public $sr_key = 'user_level_list'; // 缓存值

    /*
     * 获取等级列表
     */
    public function getList(){
        try{
            $where = ['is_delete'=>0];
            $field = 'id,name,purchase,share,team,manage,desc';
            $item = $this->where($where)->field($field)->order('sort desc')->select();
            $data = empty($item) ? array():$item->toArray();
            return $data;
        }catch (\Exception $e){
            if(stristr($e->getMessage(),'SQLSTATE')){
                $this->error = '数据走丢了，请稍后再试！';
            }else $this->error = $e->getMessage();
            return false;
        }
    }

    /**
     * @param $id 拿取所有数据 默认是缓存的   ，如果要读取数据库 需要传入false
     * @return array|bool
     */
    public function allGetList($key = 'id',$bool = true)
    {
        $list =  $this->getListCache($bool);
        $allList = [];
        foreach($list as $item){
            $allList[$item[$key]] = $item;
        }
        return $allList;
    }

    /*
     * 获取对应的等级信息
     */
    public function getLevelForId($id){
        try{
            if(empty($id)) exception('数据走丢了，请稍后再试!');
            $where = ['id' => $id,'is_delete'=>0];
            $field = 'id,name,purchase,share,team,manage,desc';
            $item = $this->where($where)->field($field)->find();
            $data = empty($item) ? array():$item->toArray();
            return $data;
        }catch (\Exception $e){
            if(stristr($e->getMessage(),'SQLSTATE')){
                $this->error = '数据走丢了，请稍后再试！';
            }else $this->error = $e->getMessage();
            return false;
        }
    }

    /*
     * 获取用户等级特权
     */
    public function getLevelPower($user){
        try{
            if(empty($user)) exception('找不到该用户!');
            $levelPow = $this->field('name,desc')->where(['id'=>$user->level,'is_delete'=>0])->find();
            $levelPow =  (!empty($levelPow))? $levelPow->toArray() : [];
            $levelPow['desc'] = !empty($levelPow['desc']) ? explode(',', $levelPow['desc']) : [];
            $levels = $this->field('level')->where(['id'=>$user->level+1,'is_delete'=>0])->find();
            $levelPow['level'] = (!empty($levels)) ? $levels->toArray()['level'] : 0;
            $levelPow['level'] = !empty($levelPow['level']) ? getformat($levelPow['level']) : 0;
            //$levelPow['level'] = !empty($levelPow['level']) ? number_format($levelPow['level'] / 100, 2, '.', '') : 0;
            $levelConsume = (new UserWallet())->field('consume,teams')->where(['uid'=>$user->id,'is_delete'=>0])->find();
            $levelConsume = (!empty($levelConsume)) ? $levelConsume->toArray() : [];
            $levelConsume['consume'] = (!empty($levelConsume)) ? getformat($levelConsume['consume']) : 0;
            $levelConsume['teams'] = (!empty($levelConsume)) ? getformat($levelConsume['teams']) : 0;
            $levelConsume['grade'] = $user->level??0;
            $level = array_merge($levelPow,$levelConsume);

            if(!$level) exception('数据走丢了，请稍后再试!');
            return $level;
        }catch (\Exception $e){
            if(stristr($e->getMessage(),'SQLSTATE')){
                $this->error = '数据走丢了，请稍后再试！';
            }else $this->error = $e->getMessage();
            return false;
        }
    }

    /*
     * 获取有效等级信息
     */
    public function getLevelInfo($id){
        $level = $id+1;
        $info = $this->field('id')->where(['id'=>$level,'is_delete'=>0])->find();
        if(!$info)
            $info = $this->getLevelInfo($level);
        if($info)
            return $info;
    }
}
