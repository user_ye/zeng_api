<?php
declare (strict_types = 1);

namespace app\user\model;

use app\common\model\Config;
use think\facade\Db;
use think\Model;

/**
 * @mixin \think\Model
 */
class UserVolumeLog extends Model
{
    public $page = '';//分页数据
    public $count = '';//数据总数
    public $error = '';//报错
}
