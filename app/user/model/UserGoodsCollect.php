<?php
declare (strict_types = 1);

namespace app\user\model;

use app\mall\model\Goods;
use think\Model;

/**
 * @mixin \think\Model
 */
class UserGoodsCollect extends Model
{
    public $page = '';//分页数据
    public $count = '';//数据总数
    public $error = '';//报错

    /*
     * 获取我的收藏
     */
    public function getMyCollect($id){
        try{
            if(empty($id)) exception('找不到该用户!');
            $data = Input('post.');
            $where = ['c.uid'=>$id];
            $limit = isset($data['limit'])&&!empty($data['limit']) ? $data['limit'] : 15;//每页显示数据
            $query = ['page' => (isset($data['page']) ? $data['page'] : 1)];//分页参数
            $field = 'g.id,g.name,g.title,g.share,g.status,g.default_discount_price,g.default_price,c.add_time';
            $item = $this->alias('c')->join('goods g','g.id=c.goods_id','left')->where($where)->field($field)->order('c.add_time desc')->paginate($limit, false, array('query'=>$query));
            $data = empty($item) ? array():$item->toArray();
            // 转换数据
            if($data && is_array($data['data'])){
                foreach($data['data'] as $k=>$item){
                    $data['data'][$k]['default_discount_price'] = getformat($item['default_discount_price']);
                    $data['data'][$k]['default_price'] = getformat($item['default_price']);
                    $data['data'][$k]['share'] = getHostDominUrl($item['share']);
                }
            }
            return $data;
        }catch (\Exception $e){
            if(stristr($e->getMessage(),'SQLSTATE')){
                $this->error = '数据走丢了，请稍后再试！';
            }else $this->error = $e->getMessage();
            return false;
        }
    }

    /*
     * 添加收藏
     */
    public function addCollect($id){
        try{
            if(empty($id)) exception('找不到该用户!');
            $data = Input('post.');
            if(empty($data['gid'])) exception('商品不存在!');
            if($this->where(['goods_id'=>$data['gid'],'uid'=>$id])->field('id')->find()) exception('已收藏!');
            if(!(new Goods())->getGoodsStatus($data['gid'])) exception('该商品无法收藏!');
            $this->save(['uid'=>$id,'goods_id'=>$data['gid'],'add_time'=>time()]);
            return true;
        }catch (\Exception $e){
            if(stristr($e->getMessage(),'SQLSTATE')){
                $this->error = '数据走丢了，请稍后再试！';
            }else $this->error = $e->getMessage();
            return false;
        }
    }

    /*
     * 取消收藏
     */
    public function delCollect($id){
        try{
            if(empty($id)) exception('找不到该用户!');
            $data = Input('post.');
            if(empty($data['gid'])) exception('商品不存在!');
            $where = ['uid'=>$id,'goods_id'=>$data['gid']];
            $info = $this->where($where)->field('id')->find();
            if(empty($info)) exception('商品不存在!');
            $this->where(['id'=>$info['id']])->delete();
            return true;
        }catch (\Exception $e){
            if(stristr($e->getMessage(),'SQLSTATE')){
                $this->error = '数据走丢了，请稍后再试！';
            }else $this->error = $e->getMessage();
            return false;
        }
    }
}
