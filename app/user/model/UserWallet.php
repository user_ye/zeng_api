<?php
declare (strict_types = 1);

namespace app\user\model;

use think\Model;

/**
 * @mixin \think\Model
 */
class UserWallet extends Model
{
    public $page = '';//分页数据
    public $count = '';//数据总数
    public $error = '';//报错

    /*
    //数据预处理 - 用户余额
    public function getBalanceAttr($value)
    {
        return number_format($value/100,2,'.','');
    }

    //数据预处理 - 赠送金额
    public function getGiveBalanceAttr($value)
    {
        return number_format($value/100,2,'.','');
    }

    //数据预处理 - 购物券余额
    public function getVolumeBalanceAttr($value)
    {
        return number_format($value/100,2,'.','');
    }

    //数据预处理 - 累计充值金额
    public function getVolumeTotalAttr($value)
    {
        return number_format($value/100,2,'.','');
    }

    //数据预处理 - 喵呗  分
    public function getMiaoAttr($value)
    {
        return number_format($value/100,2,'.','');
    }

    //数据预处理 - 累计喵呗  分
    public function getMiaosAttr($value)
    {
        return number_format($value/100,2,'.','');
    }

    // 预估喵呗
    public function getPreMiaoAttr($value)
    {
        return number_format($value/100,2,'.','');
    }
    */

    /*
     * 获取用户钱包数据
     */
    public function getWallet($id){
        try{
            if(empty($id)) exception('找不到该用户!');
            $where = ['uid' => $id,'is_delete'=>0];
            $field = 'balance,miao,give_balance';
            $item = $this->where($where)->field($field)->find();
            $wallet = empty($item) ? array():$item->toArray();
            if(!empty($wallet)){
                $wallet['balance'] = getformat($wallet['balance']);
                $wallet['miao'] = getformat($wallet['miao']);
                $wallet['give_balance'] = getformat($wallet['give_balance']);
            }else
                $wallet = ['balance'=>0.00,'miao'=>0.00,'give_balance'=>0.00];

            return $wallet;
        }catch (\Exception $e){
            if(stristr($e->getMessage(),'SQLSTATE')){
                $this->error = '数据走丢了，请稍后再试！';
            }else $this->error = $e->getMessage();
            return false;
        }
    }

    /*
     * 检查是否购买购物券
     */
    public function checkVolume($id){
        try{
            if(empty($id)) exception('找不到该用户!');
            $where = ['uid' => $id,'is_delete'=>0];
            $field = 'volume_total,volume_balance';
            $info = $this->where($where)->field($field)->find();
            if(!empty($info))
                $info['volume_balance'] = number_format($info['volume_balance']/100,2,'.','');
            else $info = ['volume_balance'=>0,'volume_total'=>0];
            return $info;
        }catch (\Exception $e){
            if(stristr($e->getMessage(),'SQLSTATE')){
                $this->error = '数据走丢了，请稍后再试！';
            }else $this->error = $e->getMessage();
            return false;
        }
    }
}
