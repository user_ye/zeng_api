<?php
declare (strict_types = 1);
namespace app\user\model;
use app\common\model\BaseModel;
use app\common\self\SelfRedis;
use app\shop\model\ShopCoupon;
use Exception;
use think\Model;

use think\Db;

Class UserCoupon extends BaseModel
{
    public $page = '';//分页数据
    public $count = '';//数据总数
    public $error = '';//报错


    /*
     * 获取我的优惠券列表
     */
    public function getMyCoupon($id){
        try{
            if(empty($id)) exception('找不到该用户!');
            $data = Input('post.');
            $about_expire = time()+(3600*24*3);
            $where = [['c.uid','=',$id],['c.is_delete','=',0]];
            if(empty($data['status'])||!in_array($data['status'],[1,2,3])) exception('请选择正确的优惠券状态!');
            else{
                if($data['status'] == 2) $where[] = ['c.status','=',$data['status']-1];
                elseif($data['status'] == 1){
                    if(!empty($data['is_expire'])){
                        $where[] = ['c.end_time','between',[time(),$about_expire]];
                    }else $where[] = ['c.end_time','>',time()];
                    $where[] = ['c.status','=',$data['status']-1];
                }else $where[] = ['c.end_time','<',time()];
            }
//            $limit = isset($data['limit'])&&!empty($data['limit']) ? $data['limit'] : 15;//每页显示数据
//            $query = ['page' => (isset($data['page']) ? $data['page'] : 1)];//分页参数
            $field = 'c.id,c.add_time,c.start_time,c.end_time,c.status,c.use_status,c.amount,c.total_amount,c.shop_id,sc.name,su.name as title,su.logo';
            $item = $this->alias('c')->join('shop_coupon sc','sc.id=c.coupon_id','left')->join('shop_users su','su.id=sc.shop_id','left')->field($field)->where($where)
                ->order('c.add_time desc')->select();//->paginate($limit, false, array('query'=>$query));
            $data = empty($item) ? array():$item->toArray();
            // 转换数据
            //$list = $data;
            $list['about_expire'] = $this->where([['end_time','between',[time(),$about_expire]],['status','=',0],['is_delete','=',0]])->count();
            $list['data'] = [];
            if(!empty($data) && is_array($data)){
                foreach($data as $k=>$item){
                    $keys = isset($list['data'][$item['shop_id']]['list']) ? count($list['data'][$item['shop_id']]['list']) : 0;
                    $list['data'][$item['shop_id']]['id'] = $item['shop_id'];
                    $list['data'][$item['shop_id']]['title'] = $item['title'];
                    $list['data'][$item['shop_id']]['logo'] = $item['logo'];
                    if(empty($list['data'][$item['shop_id']]['add'])) $list['data'][$item['shop_id']]['add'] = $item['add_time'];
                    elseif($item['add_time'] > $list['data'][$item['shop_id']]['add']) $list['data'][$item['shop_id']]['add'] = $item['add_time'];
                    $list['data'][$item['shop_id']]['list'][$keys] = $item;
                    $list['data'][$item['shop_id']]['list'][$keys]['add_time'] = date('Y-m-d',$item['add_time']);
                    $list['data'][$item['shop_id']]['list'][$keys]['start_time'] = date('Y-m-d',$item['start_time']);
                    $list['data'][$item['shop_id']]['list'][$keys]['end_time'] = date('Y-m-d',$item['end_time']);
                    $list['data'][$item['shop_id']]['list'][$keys]['type'] = ($item['total_amount'] == 0) ? 1 : 2;
                }
            }
            $list['data'] = array_values($list['data']);
            $last_names = array_column($list['data'],'add');
            array_multisort($last_names,SORT_DESC,$list['data']);
            return $list;
        }catch (\Exception $e){
            /*if(stristr($e->getMessage(),'SQLSTATE')){
                $this->error = '数据走丢了，请稍后再试！';
            }else*/ $this->error = $e->getMessage();
            return false;
        }
    }

    // 领取优惠券
    public function userApplyCoupon($user,$param){
        $coupon_id = isset($param['coupon_id']) ? $param['coupon_id'] : 0;
        if(!$coupon_id || $coupon_id < 0) return retu_json(400,'优惠券id不得为空');
        $where = [];
        $where[] = ['id','=',$coupon_id];
        $where[] = ['apply_status','=',1];
        $where[] = ['is_delete','=',0];
        $where[] = ['end_time','>',time()];
        if(!$couponOne = ShopCoupon::where($where)->find()) return retu_json(400,'该优惠券记录不存在');
        // 优惠券状态
        if($couponOne->apply_status == 2) return retu_json('优惠券已派发完毕');
        // 优惠券
        if($couponOne->apply_status == 3) return retu_json('该优惠券商家已回收');
        // 判断是否领取过
        if($this::where([
            'coupon_id' => $couponOne->id,
            'uid' => $user->id
            ])->count()) return retu_json(200,'该优惠券已领取');
        // 防止并发
        $sr = new SelfRedis();
        if($sr->redis->get('apply_coupon_'.$user->id)) return retu_json('3秒后重试');
        $sr->redis->setnx('apply_coupon_'.$user->id,1,3);
        // 拿取 键值
        $redis_key = 'apply_coupon_nums_'.$couponOne->id;
        $apply_nums = $sr->redis->get($redis_key);
        if($apply_nums && $apply_nums >= $couponOne->total) return retu_json('抱歉，此优惠券已经领取完毕');
        $bool = false;
        // 开始领取流程
        try{
            $this::startTrans();
            $data = [
                'uid' => $user->id,
                'shop_id' => $couponOne->shop_id,
                'coupon_id' => $couponOne->id,
                'type' => 1,
                'use_status' => $couponOne->status,
                'status' => 0,
                'total_amount' => $couponOne->total_amount,
                'amount' => $couponOne->amount,
                'add_time' => time(),
                'start_time' => $couponOne->start_time,
                'end_time' => $couponOne->end_time
            ];
            $coupon_id = $this::insertGetId($data);
            if(!$coupon_id) return retu_json(400,'网络异常，麻烦重新尝试1');
            $apply_limit = $sr->redis->inc($redis_key);
            $bool = true;
            $updateData = ['apply_num' => $apply_limit];
            // 如果相等 表示 已领取完毕  改变状态
            if($apply_limit >= $couponOne->total)$updateData['apply_status'] = 2;
            $flag = ShopCoupon::where('id',$couponOne->id)->update($updateData);
            if(!$flag)return exception('网络异常，麻烦重新尝试2');
            $this::commit();
            retu_json(200,'用户领取成功');
        }catch(Exception $e){
            $this::rollback();
            if($bool) $sr->redis->dec($redis_key);
            return retu_json(400,'网络错误，麻烦重新尝试'.$e->getMessage());
        }
    }

    // 获取用户 - 商品id对应优惠券列表 未使用
    public function getCouponGoods($goodsOne,$user){
        $bool = false;
        $list = [];
        if($goodsOne->shop_id > 0){
            $time = time();
            $item = $this::where([
                ['shop_id','=',$goodsOne->shop_id],
                ['uid','=',$user->id],
                ['status','=',0],
                ['start_time','<',$time],
                ['end_time','>',$time]
            ])->select();
            foreach($item as $i){
                $bool = false;
                if($i->use_status == 1)$bool= true;
                else if($i->use_status == 2 && $i->getShopCouponOne && $i->getShopCouponOne->related_id){
                    $arrs =  explode(',',$i->getShopCouponOne->related_id);
                    if(in_array($goodsOne->id,$arrs))$bool= true;
                }
                if($bool){
                    $list[] = [
                        'id' => $i['id'],
                        'type' => $i['type'],
                        'channel' => $i['channel'],
                        'status' => $i['status'],
                        'total_amount' => $i['total_amount'],
                        'amount' => $i['amount'],
                        'add_time' => $i['add_time'],
                        'start_time' => $i['start_time'],
                        'end_time' => $i['end_time']
                    ];
                }
            }
        }
        return $list;
    }

    /**
     * 获取到对应的商家优惠券
     */
    public function getShopCouponOne(){
        return $this::hasOne(ShopCoupon::class,'id','coupon_id');
    }
}