<?php
declare (strict_types = 1);

namespace app\user\model;

use think\Model;

/**
 * @mixin \think\Model
 */
class UserFollow extends Model
{
    public $page = '';//分页数据
    public $count = '';//数据总数
    public $error = '';//报错

    /*
     * 获取我的粉丝
     */
    public function getMyFans($id){
        try{
            if(empty($id)) exception('找不到该用户!');
            $data = Input('post.');
            $where = ['f.uid'=>$id];
            $limit = isset($data['limit'])&&!empty($data['limit']) ? $data['limit'] : 15;//每页显示数据
            $query = ['page' => (isset($data['page']) ? $data['page'] : 1)];//分页参数
            $field = 'f.fans_id,u.nickname,u.avatarurl,f.status,f.add_time';
            $item = $this->alias('f')->join('user u','u.id=f.fans_id','left')->where($where)->field($field)->order('f.add_time desc')->paginate($limit, false, array('query'=>$query));
            $data = empty($item) ? array():$item->toArray();
            return $data;
        }catch (\Exception $e){
            if(stristr($e->getMessage(),'SQLSTATE')){
                $this->error = '数据走丢了，请稍后再试！';
            }else $this->error = $e->getMessage();
            return false;
        }
    }

    /*
     * 获取我的关注
     */
    public function getMyFollow($id){
        try{
            if(empty($id)) exception('找不到该用户!');
            $data = Input('post.');
            $where[] = ['f.fans_id','=',$id];
            if(!empty($data['nickname']))
                $where[] = ['u.nickname','like',trim($data['nickname']).'%'];
            $limit = isset($data['limit'])&&!empty($data['limit']) ? $data['limit'] : 15;//每页显示数据
            $query = ['page' => (isset($data['page']) ? $data['page'] : 1)];//分页参数
            $field = 'f.uid,u.nickname,u.avatarurl,f.status,f.add_time';
            $item = $this->alias('f')->join('user u','u.id=f.uid','left')->where($where)->field($field)->order('f.add_time desc')->paginate($limit, false, array('query'=>$query));
            $data = empty($item) ? array():$item->toArray();
            return $data;
        }catch (\Exception $e){
            if(stristr($e->getMessage(),'SQLSTATE')){
                $this->error = '数据走丢了，请稍后再试！';
            }else $this->error = $e->getMessage();
            return false;
        }
    }

    /*
     * 获取我的粉丝数
     */
    public function getMyFansNum($id){
        try{
            if(empty($id)) exception('找不到该用户!');
            $where = ['uid'=>$id];
            $field = 'count(fans_id) as fans_number';
            $data = $this->where($where)->field($field)->find();
            return !empty($data['fans_number'])?$data['fans_number']:0;
        }catch (\Exception $e){
            if(stristr($e->getMessage(),'SQLSTATE')){
                $this->error = '数据走丢了，请稍后再试！';
            }else $this->error = $e->getMessage();
            return false;
        }
    }

    /*
     * 获取我的关注数
     */
    public function getMyFollowNum($id){
        try{
            if(empty($id)) exception('找不到该用户!');
            $where = ['fans_id'=>$id];
            $field = 'count(uid) as follow_number';
            $data = $this->where($where)->field($field)->find();
            return !empty($data['follow_number'])?$data['follow_number']:0;
        }catch (\Exception $e){
            if(stristr($e->getMessage(),'SQLSTATE')){
                $this->error = '数据走丢了，请稍后再试！';
            }else $this->error = $e->getMessage();
            return false;
        }
    }

    /*
     * 查询是否关注用户
     */
    public function checkMyFollow($id,$user_id){
        try{
            if(empty($id)||empty($user_id)) exception('找不到该用户!');
            $where = ['uid'=>$user_id,'fans_id'=>$id];
            $data = $this->where($where)->field('id,status')->find();
            return (!empty($data['id']))?true:false;
        }catch (\Exception $e){
            if(stristr($e->getMessage(),'SQLSTATE')){
                $this->error = '数据走丢了，请稍后再试！';
            }else $this->error = $e->getMessage();
            return false;
        }
    }

    /*
     * 添加关注用户
     */
    public function addFollow($id){
        try{
            if(empty($id)) exception('找不到该用户!');
            $data = Input('post.');
            if(empty($data['uid'])) exception('请选择关注用户!');
            if($data['uid'] == $id)
                exception('自己不能关注自己');
            $fans = $this->where(['fans_id'=>$id,'uid'=>$data['uid']])->field('id')->find();
            if($fans) exception('已关注该用户!');
            $follow = $this->where(['fans_id'=>$data['uid'],'uid'=>$id])->field('id,status')->find();
            $status = 0;
            $this::startTrans();
            if(!empty($follow)){
                if($follow['stauts']==0){
                    $status = 1;
                    $this->update(['id'=>$follow['id'],'status'=>1]); #如果被关注方关注了自己则将状态设为互关
                }
            }
            $ret = [
                'uid'=>$data['uid'],
                'fans_id'=>$id,
                'status'=>$status,
                'add_time'=>time(),
            ];
            $this->insert($ret);
            $this::commit();
            return true;
        }catch (\Exception $e){
            $this::rollback();
            if(stristr($e->getMessage(),'SQLSTATE')){
                $this->error = '数据走丢了，请稍后再试！';
            }else $this->error = $e->getMessage();
            return false;
        }
    }

    /*
     * 取消关注
     */
    public function delFollow($id){
        try{
            if(empty($id)) exception('找不到该用户!');
            $data = Input('post.');
            if(empty($data['uid'])) exception('请选择取关用户!');
            $fans = $this->where(['fans_id'=>$id,'uid'=>$data['uid']])->field('id')->find();
            if(!$fans) exception('您未关注该用户!');
            $follow = $this->where(['fans_id'=>$data['uid'],'uid'=>$id])->field('id,status')->find();
            $this::startTrans();
            if($follow){
                $this->update(['id'=>$follow['id'],'status'=>0]); #如果被关注方关注了自己则将状态设为单向关注
            }
            $this->where(['id'=>$fans['id']])->delete();
            $this::commit();
            return true;
        }catch (\Exception $e){
            $this::rollback();
            if(stristr($e->getMessage(),'SQLSTATE')){
                $this->error = '数据走丢了，请稍后再试！';
            }else $this->error = $e->getMessage();
            return false;
        }
    }
}
