<?php
declare (strict_types = 1);

namespace app\user\model;

use think\Model;

/**
 * @mixin \think\Model
 */
class Bank extends Model
{
    public $page = '';//分页数据
    public $count = '';//数据总数
    public $error = '';//报错

    /*
     * 获取银行卡列表
     */
    public function getList(){
        return $this->where('is_delete','=',0)->select();
    }

    /*
     * 获取银行卡列表
     */
    public function getInfo($id){
        try{
            if(empty($id)) exception('找不到该用户!');
            return $this->where(['id'=>$id,'is_delete'=>0])->find();
        }catch (\Exception $e){
            if(stristr($e->getMessage(),'SQLSTATE')){
                $this->error = '数据走丢了，请稍后再试！';
            }else $this->error = $e->getMessage();
            return false;
        }
    }
}
