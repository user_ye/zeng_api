<?php
declare (strict_types = 1);

namespace app\user\model;

use app\video\model\Video;
use think\Model;
use think\Request;

/**
 * @mixin \think\Model
 */
class UserVideoFollow extends Model
{
    public $page = '';//分页数据
    public $count = '';//数据总数
    public $error = '';//报错

    /*
     * 获取我喜欢视频
     */
    public function getMyVideoFollow($id){
        try{
            if(empty($id)) exception('找不到该用户!');
            $data = Input('post.');

            $where[] = ['vf.uid','=',$id];
            $where[] = ['vf.status','=',1];
            $where[] = ['v.status','=',1];
            $where[] = ['v.is_check','=',1];
            $where[] = ['v.is_delete','=',0];
            if(!empty($data['name']))
                $where[] = ['v.title','like',trim($data['name']).'%'];
            $limit = isset($data['limit'])&&!empty($data['limit']) ? $data['limit'] : 15;//每页显示数据
            $query = ['page' => (isset($data['page']) ? $data['page'] : 1)];//分页参数
            $field = 'v.id as vid,v.uid,u.nickname,u.avatarurl,v.title,v.cover,v.video,v.likes,v.add_time';
            $item = $this->alias('vf')->join('video v','v.id=vf.vid','left')->join('user u','u.id=v.uid','left')
                ->where($where)->field($field)->order('vf.add_time desc')->paginate($limit, false, array('query'=>$query));
            $list = empty($item) ? array():$item->toArray();
            if(!empty($list['data'])){
                $year = date('Y',time());
                foreach($list['data'] as $k=>$v){
                    if($v['nickname']==null) $list['data'][$k]['nickname'] = '美妆说';
                    if(empty($v['avatarurl'])) $list['data'][$k]['avatarurl'] = 'https://miaommeis-1301812909.cos.ap-guangzhou.myqcloud.com/icon/%E7%BC%96%E7%BB%84%205%402x.png';
                    $list['data'][$k]['title'] = emojiDecode($v['title']);
                    if($year == date('Y',$v['add_time'])) $list['data'][$k]['add_time'] = date('m-d',$v['add_time']);
                    else $list['data'][$k]['add_time'] = date('Y-m-d',$v['add_time']);

                    $list['data'][$k]['cover'] = getHostDominUrl($list['data'][$k]['cover']);
                }
            }
            /*$field = 'v.id as vid,v.uid,u.nickname,u.avatarurl,v.title,v.cover,v.video,v.likes,v.add_time';
            $list = $this->alias('vf')->join('video v','v.id = vf.vid')->join('user u','v.uid = u.id')->where(['vf.uid'=>$id,'vf.status'=>1,'v.is_check'=>1,'v.is_delete' => 0,'v.status'=>1])->field($field)->select();*/
            return $list;
        }catch (\Exception $e){
            if(stristr($e->getMessage(),'SQLSTATE')){
                $this->error = '数据走丢了，请稍后再试！';
            }else $this->error = $e->getMessage();
            return false;
        }
    }

    /*
     * 获取我喜欢视频数
     */
    public function getMyFollowNumber($id){
        try{
            if(empty($id)) exception('找不到该用户!');
            $field = 'count(vf.id) as video_follow_number';
            $list = $this->alias('vf')->join('video v','v.id=vf.vid','left')->where(['vf.uid'=>$id,'vf.status'=>1,'v.status'=>1])->field($field)->find();
            return !empty($list)? $list['video_follow_number'] : 0;
        }catch (\Exception $e){
            if(stristr($e->getMessage(),'SQLSTATE')){
                $this->error = '数据走丢了，请稍后再试！';
            }else $this->error = $e->getMessage();
            return false;
        }
    }

    /**
     * 点赞喜欢  或者 取消
     */
    public function checkLike($id)
    {
        try{
            $get = Input();
            $vid = isset($get['vid']) ? $get['vid'] : 0;
            if($vid <= 0)
                exception('视频id不得为空!');
            $like = isset($get['like']) ? $get['like'] : -1;
            if($like < 0 || !in_array($like,[0,1]))
                exception('点赞类型不得为空!');
            $userVideoModel =  $this->where(['uid' => $id,'vid'=>$vid])->find();
            if($userVideoModel && $userVideoModel->status == $like)
                exception('当前状态不得相同!');
            // 如果存在 则更新
            if($userVideoModel){
                $userVideoModel->status = $like;
                $userVideoModel->save();
            // 插入
            }else{
                $this->insertGetId(['vid' => $vid, 'status' => $like, 'uid' => $id, 'add_time' => time()]);
            }
            // 喜欢数加1  更新数据喜欢状态
            $videoModel = Video::find($vid);
            if($videoModel){
                Video::where('id',$videoModel->id)->update([
                   'likes' => $videoModel->likes +  ($like == 1 ? 1 : -1)
                ]);
            }
            // 如果等于 喜欢
            if($like == 1)
                (new UserVideoProfit())->updateProfit(['uid'=>$id,'type'=>2]);
            return $like == 1 ? '喜欢成功' : '取消成功';
        }catch (\Exception $e){
            if(stristr($e->getMessage(),'SQLSTATE')){
                $this->error = '数据走丢了，请稍后再试！';
            }else $this->error = $e->getMessage();
            return false;
        }
    }

    /**
     * 模型一对一关联
     */
    public function getOneUser()
    {
        $this::hasOne(\app\user\model\User::class,'id','uid');
    }

    /**
     * 视频关联
     */
    public function getOneVideo()
    {
        $this::hasOne(Video::class,'id','vid');
    }
}
