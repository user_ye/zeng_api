<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\facade\Route;

//用户路由组
Route::group('index', function () {
    Route::post('getInfo', 'getInfo');                             //获取用户详情
    Route::post('checkInviteCode', 'checkInviteCode');          //检查邀请码
    Route::post('bindInviteCode', 'bindInviteCode');            //绑定邀请码
    Route::post('myWallet', 'myWallet');                          //获取用户钱包
    Route::post('getBalances', 'getBalances');                   //获取用户余额流水
    Route::post('updateUserInfo', 'updateUserInfo');            //获取小程序授权信息
    Route::post('withdrawalList', 'withdrawalList');            //获取用户提现记录
    Route::post('getMiao', 'getMiao');                            //获取用户喵呗
    Route::post('getMiaoList', 'getMiaoList');                   //获取用户喵呗兑换记录
    Route::post('sharingRewards', 'sharingRewards');            //获取用户分享奖励
    Route::post('historyList', 'historyList');                   //获取喵呗历史
    Route::post('teamReward', 'teamReward');                     //获取团队奖励(喵呗)
    Route::post('videoReward', 'videoReward');                   //获取视频收益(喵呗)
    Route::post('myCustomer', 'myCustomer');                     //获取我的客户
    Route::post('myCollect', 'myCollect');                       //获取我的收藏
    Route::post('addCollect', 'addCollect');                    //添加收藏
    Route::post('delCollect', 'delCollect');                    //取消收藏
    Route::post('bindingPhone', 'bindingPhone');               //我的活动完成
    Route::post('getLevelPower', 'getLevelPower');             //获取用户特权

})->prefix('Index/')->pattern(['id' => '\d+']);


//用户分支路由组
Route::group('branch', function () {
    Route::post('getBankList', 'getBankList');                    //获取银行卡列表
    Route::post('addCard', 'addCard');                             //添加银行卡
    Route::post('getMyCard', 'getMyCard');                        //获取我的银行卡
    Route::post('checkCard', 'checkCard');                        //校验银行卡信息
    Route::post('addWithdrawal', 'addWithdrawal');              //新增提现申请
    Route::post('orcBank', 'orcBank')->allowCrossDomain();        //新增提现申请
    Route::post('getVolume', 'getVolume');                       //获取购物券信息
    Route::post('rechargeVolume', 'rechargeVolume');           //购买购物券
    Route::post('myActivity', 'myActivity');                    //我的活动
    Route::post('myActivityInfo', 'myActivityInfo');          //我的活动详情
    Route::post('myActivityComplete', 'myActivityComplete'); //我的活动完成
    Route::post('myAddrList', 'myAddrList');                    //我的收货地址列表
    Route::post('addAddress', 'addAddress');                    //添加收货地址
    Route::post('delAddress', 'delAddress');                    //删除收货地址
    Route::post('exchangeMiao', 'exchangeMiao');               //喵呗兑换
    Route::post('tutorWechat', 'tutorWechat');                 //导师微信
    Route::post('lookActivity', 'lookActivity');               //增加浏览活动记录
    Route::post('getActivityList', 'getActivityList');        //获取浏览活动记录

})->prefix('branch/')->pattern(['id' => '\d+']);


//用户视频路由组
Route::group('video', function () {
    Route::post('myFollow', 'myFollow');                          //获取我的关注
    Route::post('myFans', 'myFans');                              //获取我的粉丝
    Route::post('addFollow', 'addFollow');                       //添加关注
    Route::post('delFollow', 'delFollow');                       //取消关注
    Route::post('myVideo', 'myVideo');                           //获取我的视频数据
    Route::post('myVideoList', 'myVideoList');                  //获取我的视频-作品
    Route::post('myLikeVideo', 'myLikeVideo');                  //获取我的视频-喜欢
    Route::post('myVideoComment', 'myVideoComment');           //获取我的视频-评论
    Route::post('videoHome', 'videoHome');                       //个人视频页-评论
    Route::post('delVideo', 'delVideo');                         //删除视频
    Route::post('getType', 'getType');                           //视频分类
    Route::post('addComment', 'addComment');                    //添加视频一级评论
    Route::post('addCommentChild', 'addCommentChild');        //添加视频二级评论
    Route::post('publishVideoLog', 'publishVideoLog');        //发布视频新增记录
    Route::post('publishVideo', 'publishVideo');               //上传视频-视频文件
    Route::post('publishVideoImg', 'publishVideoImg');        //上传视频-图片文件
    Route::post('delVideoFile', 'delVideoFile');               //删除视频文件
    Route::post('getGoods', 'getGoods');                        //获取视频分享商品列表
    Route::post('addWatchRecord', 'addWatchRecord');          //新增观看视频记录

})->prefix('video/')->pattern(['id' => '\d+']);


//用户订单路由组
Route::group('order', function () {
    Route::post('test', 'test');                                  //test

})->prefix('order/')->pattern(['id' => '\d+']);