<?php
declare (strict_types = 1);

namespace app\user\controller;

use app\common\controller\BaseController;

use app\user\model\UserCoupon;
use think\facade\Request;

class Coupon extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        if(empty($this->user)&&!in_array(Request::action(true),[]))
            return retu_json(402, '请先登录！', []);
    }

    // 用户领取优惠券
    public function applyCoupon(){
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new UserCoupon();
        $model->userApplyCoupon($this->user,Request()->param());
    }

    /*
     * 我的优惠券列表
     */
    public function myCouponList()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new UserCoupon();
        $data = $model->getMyCoupon($this->user->id);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }
}
