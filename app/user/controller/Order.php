<?php
declare (strict_types = 1);

namespace app\user\controller;

use app\api\model\WxApi;
use app\common\controller\BaseController;
use app\myself\WeixinRefund;
use app\user\model\MiaoLog;
use app\user\model\User;
use app\user\server\UserOut;
use think\facade\Db;

class Order extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /*
     * test
     */
    public function test()
    {
//        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限', []);
//        $model = new User();
//        $data = $model->test();
//        if($data == false && !empty($model->error))
//            return retu_json(400, $model->error, []);
//        return retu_json(200, '操作成功', $data);
    }

    /*
     * test
     */
    public function test1()
    {
//        $sql = 'insert into m3_user_info (uid,consume,team_consume,team_order_num)  values(%s,%s,%s,%s) on  DUPLICATE key update consume=consume+values(consume),team_consume=team_consume+values(team_consume),team_order_num=team_order_num+values(team_order_num)';
//        $sql = sprintf($sql, 942, 1, 0 , 1);
//        Db::execute($sql);
        die;
        $dir = '/www/wwwroot/api/tp/public/video_list/';//文件地址
        $path = '/www/wwwroot/api/tp/public/copy_video/';//存放地址
        $pathImg = '/copy_video/';//存放地址
        $host = 'https://admin-miaomeimei-1301812909.cos.ap-guangzhou.myqcloud.com/video/';//域名
        //$type = ['lxjk'=>23,'cnzk'=>22,'mrmf'=>21,'jfss'=>20,'mzzc'=>19,'hfys'=>18];//分类名  key为文件目录，value为视频分类表id  'lxjk'=>23,'cnzk'=>22,'mrmf'=>21,'jfss'=>20,'mzzc'=>19,'hfys'=>18
        $type = ['yimei' => 24];
        $uids = ["656","654","648",629,644,606];
        $uidsLen = count($uids) - 1;
        $array_name = array();
        $array_img = array();
        $i = 0;
        foreach($type as $k=>$v){
            if(is_dir($dir.$k)){
                $handle = opendir($dir. $k. "/.");
                while (false !== ($file = readdir($handle))) {
                    if ($file != "." && $file != "..") {
                        $array_name[] = $file;
                        $key = rand(0,$uidsLen);
                        $_name = explode('.',$file);
                        $filename = date('YmdHis').'_'.$v."_$i.".(isset($_name[1])?$_name[1]:'mp4');//生成文件名
                        //rename($dir.$v.'/'.$file,$dir.$v.'/'.$filename);
                        copy($dir.$k.'/'.$file, $path.$k.'/'.$filename);//拷贝
                        //获取视频第一帧画面生成图片并保存到数据库  参数1-文件地址，2-生成图片名，3-存储文件夹，4-第几秒画面，5-宽高
                        $img_name  = date('YmdHis').'_'.$v."_$i.jpeg";
                        $array_img[] = getVideoCover($path.$k.'/'.$filename, $img_name,$pathImg.$k.'_img/', 3, 0);
                        $array_name[] = ['uid'=>(int)$uids[$key],'type'=>0,'type_id'=>$v,'relation_id'=>0,'title'=>$_name[0],'cover'=>$host.$k.'/'.$img_name,'video'=>$host.$k.'/'.$filename,'status'=>0,'is_check'=>0,'likes'=>rand(500,5000),'shares'=>rand(100,599)];
                        $i++;
                    }
                }
                closedir($handle);
            }
        }
        $sql = $this->multArrayInsert('m3_video',['uid','type','type_id','relation_id','title','cover','video','status','is_check','likes','shares'],$array_name);//生成sql
        file_put_contents('./sql',$sql);//保存sql
        file_put_contents('./name.json',json_encode($array_name));//保存json
        var_dump($sql,$array_name,$array_img);
    }


    public function multArrayInsert($table,$arr_key, $arr, $split = '`') {
        $arrValues = array();
        if (empty($table) || !is_array($arr_key) || !is_array($arr)) return false;
        $sql = "INSERT INTO %s( %s ) values %s ";
        foreach ($arr as $k => $v) {
            $arrValues[$k] = "'".implode("','",array_values($v))."'";
        }
        $sql = sprintf($sql, $table, "{$split}" . implode("{$split} ,{$split}", $arr_key) . "{$split}", "(" . implode(") , (", array_values($arrValues)) . ")");
        return $sql;
    }

    /*
     * test
     */
    public function test2()
    {
        $app = app();
        if(!isset($app->user)){
            return retu_json(400, 'err', []);
        }else return retu_json(200, 'ok', $app->user);
        $model = new UserOut();
        return retu_json(200, $model->error, $model->out(1108));


        /*//微信提现到零钱测试  唐总账号
        $model = new WeixinRefund();
        $data = [
            'partner_trade_no'=>'20080719250890948500',
            'openid'=>'o68MO5PvkqSRHyQJ3_svFlN-ghS8',
            'amount'=>100,
        ];
        $ret = $model->transfers($data);
        return retu_json(200, $model->error, $ret);*/
    }

    /*
     * test
     */
    public function test3()
    {
        $sign = input('post.sign');
        if(empty($sign)){
            retu_json(200, 'sign为空', []);
        }
        $timestamp = input('post.timestamp');
        $postParams = input('post.');
        unset($postParams['sign']);
        $params = [];
        ksort($postParams);
        foreach($postParams as $k=>$v) {
            $params[] = sprintf("%s%s", $k,$v);
        }
        $apiSerect = 'kajsbkd123b55kwbdkj128y87';
        $str = sprintf("%s%s%s", $apiSerect, implode('', $params), $apiSerect);
        return  retu_json(200, 'ok', ['str'=>$str,'data'=>$postParams,'md5'=>md5($str)]);

    }
}
