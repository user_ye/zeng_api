<?php
declare (strict_types = 1);

namespace app\user\controller;

use app\common\controller\BaseController;
use app\mall\model\ActivityLog;
use app\mall\model\Goods;
use app\user\model\User;
use app\user\model\UserFollow;
use app\user\model\UserVideoFollow;
use app\video\model\Video as Videos;
use app\video\model\VideoComment;
use app\video\model\VideoCommentChild;
use app\video\model\VideoType;


class Video extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /*
     * 个人视频页
     */
    public function videoHome()
    {
        /*if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);*/
        $mid = empty($this->user->id) ? 0 : $this->user->id;
        $uid = Input('post.uid');
        if(empty($uid)||!is_numeric($uid))
            return retu_json(400, '数据错误，请重试！', []);
        $model = new Videos();
        $mod = new UserFollow();
        $data = $model->videoHome();
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        $ret = [
            'is_follow'=>$mod->checkMyFollow($mid,$uid?:0)?:false,
            'fans_number'=>$mod->getMyFansNum($uid?:0)?:0,
            'likes_number'=>$model->getVideoLikeNum($uid?:0)?:0,
            'user'=>(new User())->getUserInfoById($uid?:0)?:[],
            'list'=>$data,
        ];
        /*if(empty($this->user)) $ret['user'] = (new User())->getUserInfoById($uid?:0)?:[];
        else $ret['user'] = ['id'=>$this->user->id,'nickname'=>$this->user->nickname,'avatarurl'=>$this->user->avatarurl];*/
        return retu_json(200, '操作成功', $ret);
    }

    /*
     * 获取我的视频数据
     */
    public function myVideo()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new Videos();
        $mod = new UserFollow();
        $data = $model->getLikeNum($this->user->id);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        $data['follow_number'] = $mod->getMyFollowNum($this->user->id)?:0;
        $data['fans_number'] = $mod->getMyFansNum($this->user->id)?:0;
        $data['new_comment'] = (new VideoComment())->getNewComment($this->user->id)?:0;
        $data['video_follow_number'] = (new UserVideoFollow())->getMyFollowNumber($this->user->id)?:0;
        return retu_json(200, '操作成功', $data);
    }

    /*
     * 获取我的视频列表
     */
    public function myVideoList()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new Videos();
        $data = $model->getUserVideos($this->user->id);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    /*
     * 获取我喜欢的视频  -
     */
    public function myLikeVideo()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new UserVideoFollow();
        $data = $model->getMyVideoFollow($this->user->id);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    /*
     * 获取我的视频评论
     */
    public function myVideoComment()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new VideoComment();
        $data = $model->getCommentList($this->user->id);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    /*
     * 获取我的粉丝 - 粉丝
     */
    public function myFans()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new UserFollow();
        $data = $model->getMyFans($this->user->id);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        $ret = [
            'follow_number'=>$model->getMyFollowNum($this->user->id),
            'fans_number'=>$model->getMyFansNum($this->user->id),
            'list'=>$data,
        ];
        return retu_json(200, '操作成功', $ret);
    }

    /*
     * 获取我的关注- 用户
     */
    public function myFollow()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new UserFollow();
        $data = $model->getMyFollow($this->user->id);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        $ret = [
            'follow_number'=>$model->getMyFollowNum($this->user->id),
            'fans_number'=>$model->getMyFansNum($this->user->id),
            'list'=>$data,
        ];
        return retu_json(200, '操作成功', $ret);
    }

    /*
     * 添加关注 - 用户
     */
    public function addFollow()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new UserFollow();
        $data = $model->addFollow($this->user->id);
        if($data == false)
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    /*
     * 取消关注
     */
    public function delFollow()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new UserFollow();
        $data = $model->delFollow($this->user->id);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    /*
     * 取消关注
     */
    public function delVideo()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new Videos();
        $data = $model->delVideo($this->user->id);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    /*
     * 获取视频分类
     */
    public function getType()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new VideoType();
        $data = $model->getList();
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    /*
     * 添加视频评论
     */
    public function addComment()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new VideoComment();
        $data = $model->addComment($this->user);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    /*
     * 添加视频评论
     */
    public function addCommentChild()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new VideoCommentChild();
        $data = $model->addCommentChild($this->user->id);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    /**
     * 视频喜欢或者 取消
     */
    public function videoChecklike()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new UserVideoFollow();
        $data = $model->checkLike($this->user->id);
        if($data == false)
            return retu_json(400, $model->error, []);
        return retu_json(200, $data);
    }

    /**
     * 发布视频 上传视频文件
     */
    public function publishVideo()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new Videos();
        $data = $model->publishVideo($this->user->id);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    /**
     * 发布视频 上传图片文件
     */
    public function publishVideoImg()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new Videos();
        $data = $model->publishVideoImg($this->user->id);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    /**
     * 发布视频 删除视频文件
     */
    public function delVideoFile()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new Videos();
        $data = $model->delVideoFile($this->user->id);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    /**
     * 发布视频 获取分销商品列表
     */
    public function getGoods()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new Goods();
        $data = $model->getGoods($this->user);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    /**
     * 发布视频 获取分销商品列表
     */
    public function getActivityLog()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new ActivityLog();
        $data = $model->myActivityOngoing($this->user);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    /**
     * 发布视频新增记录
     */
    public function publishVideoLog()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new Videos();
        $data = $model->publishVideoLog($this->user);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    /**
     * 新增观看视频记录
     */
    public function addWatchRecord()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new Videos();
        $data = $model->addWatchRecord($this->user);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }
}
