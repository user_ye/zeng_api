<?php
declare (strict_types = 1);

namespace app\user\controller;

use app\api\model\WxApi;
use app\common\controller\BaseController;
use app\common\model\Config;
use app\mall\model\ActivityLog;
use app\mall\model\ActivityLookLog;
use app\user\model\Bank;
use app\user\model\User;
use app\user\model\UserAddress;
use app\user\model\UserBank;
use app\user\model\UserCash;
use app\user\model\UserVolume;
use think\facade\Request;

class Branch extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        if(empty($this->user)&&!in_array(Request::action(true),['getVolume','lookActivity']))
            return retu_json(402, '请先登录！', []);
    }

    /*
     * 获取银行卡列表
     */
    public function getBankList()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new Bank();
        $data = $model->getList();
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    /*
     * 校验银行卡信息
     */
    public function checkCard()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new UserBank();
        $data = $model->checkCard();
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }


    /*
     * 获取添加银行卡
     */
    public function addCard()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new UserBank();
        $data = $model->addCard($this->user->id);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    /*
     * 获取我的银行卡
     */
    public function getMyCard()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new UserBank();
        $data = $model->getMyCard($this->user->id);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    /*
     * 新增提现申请
     */
    public function addWithdrawal()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new UserBank();
        $data = $model->addWithdrawal($this->user);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    /*
     * orc校验
     */
    public function orcBank()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new UserBank();
        $data = $model->orcBank($this->user->id);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    /*
     * 获取购物券信息
     */
    public function getVolume()
    {
        $model = new UserVolume();
        $data = $model->getVolumeInfo($this->user);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    /*
     * 充值购物券
     */
    public function rechargeVolume()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new UserVolume();
        $data = $model->rechargeVolume($this->user);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    /*
     * 我的活动
     */
    public function myActivity()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new ActivityLog();
        $data = $model->myActivityList($this->user->id);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    /*
     * 我的活动详情
     */
    public function myActivityInfo()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new ActivityLog();
        $data = $model->myActivityInfo($this->user->id);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    /*
     * 我的收货地址列表
     */
    public function myAddrList()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new UserAddress();
        $data = $model->myAddrList($this->user->id);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    /*
     * 添加收获地址
     */
    public function addAddress()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new UserAddress();
        $data = $model->addAddress($this->user->id);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    /*
     * 添加收获地址
     */
    public function delAddress()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new UserAddress();
        $data = $model->delAddress($this->user->id);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    /*
     * 我的活动完成
     */
    public function check()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new WxApi();
        $data = $model->msgSecCheck($this->user->id);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    /*
     * 喵呗兑换
     */
    public function exchangeMiao()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new UserCash();
        $data = $model->exchangeMiao($this->user->id);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    /*
     * 导师微信
     */
    public function tutorWechat()
    {
        $model = new WxApi();
        $data = $model->tutorWechat();
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    /*
     * 获取浏览活动记录
     */
    public function getActivityList()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new ActivityLookLog();
        $data = $model->getList($this->user->id);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    /*
     * 增加浏览活动记录
     */
    public function lookActivity()
    {
        $model = new ActivityLookLog();
        $data = $model->increaseLog($this->user->id);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }
}
