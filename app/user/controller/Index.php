<?php
declare (strict_types = 1);

namespace app\user\controller;

use app\common\controller\BaseController;
use app\order\model\Order;
use app\user\model\BalanceLog;
use app\user\model\MiaoLog;
use app\user\model\User;
use app\user\model\UserCash;
use app\user\model\UserGoodsCollect;
use app\user\model\UserLevel;
use app\user\model\UserVideoProfit;
use app\user\model\UserWallet;
use think\facade\Request;

class Index extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        if(empty($this->user)&&!in_array(Request::action(true),[]))
            return retu_json(402, '请先登录！', []);
    }

    /*
     * 获取小程序授权信息
     */
    public function updateUserInfo()
    {
        $model = new User();
        $data = $model->updateUserInfo($this->user);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    /*
     * 获取小程序授权手机号绑定
     */
    public function bindingPhone()
    {
        if(!empty($this->user->phone))
            return retu_json(200, '操作成功', []);
        $model = new User();
        $data = $model->bindingPhone($this->user->id);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    /*
     * 检查邀请码
     */
    public function checkInviteCode()
    {
        if(!empty($this->user->invite_id))
            return retu_json(400, '已绑定邀请人', []);
        $model = new User();
        $data = $model->checkInviteCode($this->user->id);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);

        return retu_json(200, '操作成功', ['name'=>$data]);
    }

    /*
     * 绑定邀请码
     */
    public function bindInviteCode()
    {
        if(!empty($this->user->invite_id))
            return retu_json(400, '已绑定邀请人', []);
        $model = new User();
        $data = $model->bindInviteCode($this->user);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);

        return retu_json(200, '操作成功', $data);
    }

    /*
     * 获取用户详情
     */
    public function getInfo()
    {
        $model = new User();
        $mod = new Order();
        $info = $model->getInfo($this->user);
        if($info == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        $oredr = $mod->getOrderNumOnStatus($this->user->id);//用户订单数据

        return retu_json(200, '操作成功', [$info,$oredr?:[]]);
    }

    /*
     * 获取用户钱包
     */
    public function myWallet()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new UserWallet();
        $data = $model->getWallet($this->user->id);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        $ret = [
            'balance' => number_format($data['balance']+$data['give_balance']/100,2,'.',''),
            'miao' => $data['miao']
        ];
        return retu_json(200, '操作成功', $ret);
    }

    /*
     * 获取用户余额流水
     */
    public function getBalances()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new BalanceLog();
        $data = $model->getBalances($this->user->id);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);

        return retu_json(200, '操作成功', $data);
    }

    /*
     * 获取用户提现记录
     */
    public function withdrawalList()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new UserCash();
        $data = $model->withdrawaList($this->user->id);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);

        return retu_json(200, '操作成功', $data);
    }

    /*
     * 获取用户喵呗
     */
    public function getMiao()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new MiaoLog();
        $data = $model->getMiaoList($this->user->id);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    /*
     * 获取用户喵呗兑换记录
     */
    public function getMiaoList()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new UserCash();
        $data = $model->exchangeList($this->user->id);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    /*
     * 获取用户分享奖励
     */
    public function sharingRewards()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new MiaoLog();
        $data = $model->miaoReward($this->user->id);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    /*
     * 获取喵呗历史
     */
    public function historyList()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new MiaoLog();
        $data = $model->historyList($this->user->id);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    /*
     * 获取团队奖励(喵呗)
     */
    public function teamReward()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new MiaoLog();
        $data = $model->teamReward($this->user->id);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    /*
     * 获取视频收益(喵呗)
     */
    public function videoReward()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new MiaoLog();
        $data = $model->videoReward($this->user->id);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    /*
     * 获取我的客户
     */
    public function myCustomer()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new User();
        $data = $model->myCustomer($this->user->id);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    /*
     * 获取我的收藏
     */
    public function myCollect()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new UserGoodsCollect();
        $data = $model->getMyCollect($this->user->id);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    /*
     * 添加收藏
     */
    public function addCollect()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new UserGoodsCollect();
        $data = $model->addCollect($this->user->id);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    /*
     * 取消收藏
     */
    public function delCollect()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new UserGoodsCollect();
        $data = $model->delCollect($this->user->id);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    /*
     * 获取用户等级特权
     */
    public function getLevelPower()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new UserLevel();
        $data = $model->getLevelPower($this->user);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }
}
