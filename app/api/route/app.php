<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\facade\Route;

//用户路由组
Route::group('index', function () {
//    用户登录
    Route::any('login/:code', 'login');
    Route::any('wxaCode', 'wxaCode');
    Route::any('myCardShare', 'myCardShare');
})->prefix('Index/')->pattern(['id' => '\d+']);