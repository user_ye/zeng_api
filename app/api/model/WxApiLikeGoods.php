<?php
declare (strict_types = 1);

namespace app\api\model;

use app\common\model\Config;
use app\common\self\SelfRedis;
use app\mall\model\Activity;
use app\mall\model\Goods;
use app\video\model\Video;
use think\facade\Db;

/**
 * 微信api-直播商品操作api
 */

/**
 * @mixin \think\Model
 */
class WxApiLikeGoods
{
    public $prefix_url = 'https://api.weixin.qq.com/';
    public $wx;
    public $access_token = '';

    public  function __construct()
    {
        $this->wx = new WxApi();
        $this->access_token = $this->wx->getToken();
    }

    /**
     * 获取商品列表
     * @param int $status 状态值  商品状态，0：未审核。1：审核中，2：审核通过，3：审核驳回
     * @param int $offset 开始拿取的数量
     * @param int $limit 分页的数据
     * @return array 返回数组 包含状态码
     */
   public function getGoodsList($status = 2, $offset = 0, $limit = 100)
   {
       $list = [
           'bool' => false,
           'data' => [],
           'msg' => '请求失败'
       ];
        $url = $this->prefix_url.'wxaapi/broadcast/goods/getapproved?access_token='.$this->access_token."&status=$status&offset=$offset&limit=$limit";
        $data = curlGet($url);
        if(!$data) return $list;
        $list['data'] = json_decode($data,true);
        if(!$list['data']) return $list;
        $list['bool'] = true;
       $list['msg'] = '拿取成功';
        return $list;
   }

    /**
     * 商品获取状态
     * @param $ids 商品的id  一个数组
     */
    public function getGoodsStatus($ids)
    {
        $url = $this->prefix_url.'wxa/business/getgoodswarehouse?access_token='.$this->access_token;
        // 查询商品id状态
//        $data = [
//        'goods_ids' => $ids
//        ];
        $data = [
            'goods_ids' => $ids
        ];
        $header[] = 'Content-Type:application/json';
        $header[] = 'Content-Length:'.strlen(json_encode($data));
        $ret = curlPost($url,json_encode($data),$header);
        if(!$ret) return $ret;
        return json_decode($ret,true);
    }

    /**
     * 添加商品入库操作
     * @param $goodsData 需要添加的商品入库 注意  此处需要传递素材图片id,  素材图片id 在 wxapi中有函数
     * @return bool|mixed|string 返回处理结果
     */
    public function addGoods($goodsData)
    {
//        ["goodsId"]=> 4
//        ["auditId"]=> 694416480

        if(!$goodsData || !is_array($goodsData)) return false;
        //$res = $this->wx->uploadImg(['type' => 'image','img' => app()->getRootPath().'public/uploads/ttt.jpg']);
        //$coverImgUrl =  '-YZpT671hIWoiUctQnTROWf-IQxfKyXKoKfkm_DpFR-2HRUyk_iMQKKvGhmMiI4m';
        // 此处需要json专用  curlPost
        // "{"goodsId":"3","auditId":694416457,"errcode":0}"
        $url = $this->prefix_url.'wxaapi/broadcast/goods/add?access_token='.$this->access_token;
//        $data = [
//            'goodsInfo' => [
//                'coverImgUrl' => $coverImgUrl,
//                'name' => '测试商品',
//                'priceType' => 3,
//                'price' => 100,
//                'price2' => 50,
//                'url' => 'pages/productDetails/productDetails?id=116'
//            ]
//        ];
        $data = [
            'goodsInfo' => $goodsData
        ];
        $header[] = 'Content-Type:application/json';
        $header[] = 'Content-Length:'.strlen(json_encode($data));
        $ret = curlPost($url,json_encode($data),$header);
        if(!$ret) return $ret;
        return json_decode($ret,true);
    }

    /**
     * 更新商品
     * @param $goodsData 需要更新的数据
     * @return bool|mixed|string 返回处理结果 false 表示失败
     */
    public function updateGoods($goodsData){
        $url = $this->prefix_url.'wxaapi/broadcast/goods/update?access_token='.$this->access_token;
        // 更新名字 不可以 会失败
        // 像更新哪个字段就更新哪个字段  goodsId  必传
//        $data = [
//            'goodsInfo' => [
//                'name' => '正式商品',
//                'priceType' => 3,
//                'price' => 100,
//                'price2' => 50,
//                'url' => 'pages/productDetails/productDetails?id=116',
//                'goodsId' => 3
//            ]
//        ];
        $data = [
            'goodsInfo' => $goodsData
        ];
        $header[] = 'Content-Type:application/json';
        $header[] = 'Content-Length:'.strlen(json_encode($data));
        $ret = curlPost($url,json_encode($data),$header);
        if(!$ret) return $ret;
        return json_decode($ret,true);
    }

    /**
     * 删除商品id
     * @param $id 操作的商品id
     * @return bool|mixed|string 返回处理结果 为false 表示失败
     */
    public function delGoods($id){
        if(!$id || $id < 0) return false;
        // https://api.weixin.qq.com/wxaapi/broadcast/goods/delete?access_token=
        $url = $this->prefix_url.'wxaapi/broadcast/goods/delete?access_token='.$this->access_token;
        $data = [
            'goodsId' => $id
        ];
        $header[] = 'Content-Type:application/json';
        $header[] = 'Content-Length:'.strlen(json_encode($data));
        $ret = curlPost($url,json_encode($data),$header);
        if(!$ret) return $ret;
        return json_decode($ret,true);
    }

    /**
     * 撤回审核
     * @param $auditId
     * @param $goodsId
     * @return array
     */
    public function returnGoods($auditId, $goodsId){
        if(!$auditId || !$goodsId) return false;
        $url = $this->prefix_url.'wxaapi/broadcast/goods/resetaudit?access_token='.$this->access_token;
        $data = [
            'auditId' => $auditId,
            'goodsId' => $goodsId
        ];
        $header[] = 'Content-Type:application/json';
        $header[] = 'Content-Length:'.strlen(json_encode($data));
        $ret = curlPost($url,json_encode($data),$header);
        if(!$ret) return $ret;
        return json_decode($ret,true);
    }
}
