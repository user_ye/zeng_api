<?php
declare (strict_types = 1);

namespace app\api\model;

use think\Model;

/**
 * @mixin \think\Model
 */
class WechatApiLog extends Model
{
    public $page = '';//分页数据
    public $count = '';//数据总数
    public $error = '';//报错
}
