<?php
declare (strict_types = 1);

namespace app\api\controller;

use app\common\model\Config;
use app\user\model\User;
use app\common\controller\BaseController;
use app\api\model\WxApi;
use think\facade\Log;
use think\facade\Request;

class Index extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        if(empty($this->user)&&!in_array(Request::action(true),['uploadimgwx']))
            return retu_json(402, '请先登录！', []);
    }

    public function index()
    {
        return '您好！这是一个[api]示例应用';
    }

    /*
     * 获取小程序二维码
     */
    public function wxaCode()
    {
        $model = new WxApi();
        $data = $model->wxaCode($this->user);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    /*
     * 我的卡片分享
     */
    public function myCardShare()
    {
        $model = new WxApi();
        $data = $model->myCardShare($this->user);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    //获取小程序配置
    private function getConfig(){
        $model = new Config();
        return $model->toData('wechat');
    }

    /**
     * 公共图片上传
     */
    public function uploadImg()
    {
        $time =time();
        $prefix = Input('prefix','images');
        if(!$prefix)
            $prefix = 'images';
        $name = Input('name','img');
        if(!$name)
            $name = 'img';
        $prefix .= '/'.date('Ym');
        $dir = app()->getRootPath() .'public/uploads/'.$prefix.'/';
        if(!is_dir($dir))  @mkdir($dir,0777); //目录不存在则创建
        $imgInfo = imgUpdate($name,$prefix,"{$prefix}_{$time}.jpg");
        Log::write($imgInfo,'uploadImg');
        if(!$imgInfo || $imgInfo['code'] != 1)
            return retu_json(400,'上传失败');
        $imgInfo['img'] = getApiDominUrl($imgInfo['result']);
        return retu_json(200,'ok',$imgInfo);
    }

    /**
     * 文件上传操作
     */
    public function uploads()
    {
        $time =time();
        $prefix = Input('prefix','linshi');
        if(!$prefix)
            $prefix = 'linshi';
        $name = Input('name','img');
        if(!$name)
            $name = 'img';
        $imgInfo = imgUpdate($name,$prefix,"{$prefix}_{$time}.jpg");
        Log::write($imgInfo,'uploads');
        if(!$imgInfo || $imgInfo['code'] != 1)
            return retu_json(400,'上传失败');
        $imgInfo['img'] = getApiDominUrl($imgInfo['result']);
        return retu_json(200,'ok',$imgInfo);
    }

    /**
     * 此接口专用提供给后台 上传素材用
     * 验证规则如下： ip , 签名由时间戳取100 四舍五入加上key字符串 md5生成
     * 图片上传操作，返回成功后的图片id
     */
    public function uploadImgWx(){
        //if(Request::ip() != '129.204.245.7' || Request::ip() != '116.23.17.175') return retu_json(400,'嘿嘿');
        $img = Request::param('img',0);
        $sign = Request::param('sign',0);
        if(!$img) return retu_json(400,'嘿嘿1');
        if(!$sign)return retu_json(400,'嘿嘿2');
        if($sign != md5(round(time()/100).'lu_a_lu_lzs')) if(!$sign)return retu_json(400,'嘿嘿3');
        $wx = new WxApi();
        $list = $wx->uploadImg([
           'type' => 'image',
           'img' => $img
        ]);
        if(!$list) return  retu_json(400,'失败了'.$list);
        return retu_json(200,'图片上传ok',$list);
    }

    /**
     * 回调
     */
    public function refundLog()
    {
        fileLog(file_get_contents("php://input"),date('Y-m-d').'_wechat_refundlog');
        header('Content-type: text/xml');
        ob_clean();
        echo <<<XML
<xml>
  <return_code><![CDATA[SUCCESS]]></return_code>
  <return_msg><![CDATA[OK]]></return_msg>
</xml>
XML;
        exit;
    }

    /**
     * 支付宝回调
     */
    public function aliCallback()
    {
        fileLog(file_get_contents("php://input"),date('Y-m-d').'_ali_callbacklog');
        header('Content-type: text/xml');
        echo 'ok';
        exit;
    }
}
