<?php
declare (strict_types = 1);
namespace app\mall\model;

use app\common\model\BaseModel;

Class ActivityLookLog extends BaseModel
{

    public $error = '';

    /**
     * 增加记录
     * @param int $id  用户id
     * @return array
     */
    public function getList($id)
    {
        try{
            $data = Input('post.');
            if(empty($id)) exception('找不到该用户!');
            if(empty($data['aid'])||!is_numeric($data['aid'])) exception('活动记录不能为空!');
            $limit = isset($data['limit'])&&!empty($data['limit']) ? $data['limit'] : 20;//每页显示数据
            $query = ['page' => (isset($data['page']) ? $data['page'] : 1)];//分页参数
            $where = ['l.upid' => $id,'l.aid'=>$data['aid']];
            $field = 'u.id as uid,u.nickname,u.avatarurl,l.add_time';
            $item = $this->alias('l')->join('user u','u.id=l.uid','left')->field($field)->where($where)->order('add_time desc')->paginate($limit, false, array('query'=>$query));
            $info = empty($item) ? array():$item->toArray();
            return $info;
        }catch (\Exception $e){
            if(stristr($e->getMessage(),'SQLSTATE')){
                $this->error = '数据走丢了，请稍后再试！';
            }else $this->error = $e->getMessage();
            return false;
        }
    }

    /**
     * 增加记录
     * @param int $id  用户id
     * @return array
     */
    public function increaseLog($id)
    {
        try{
            $data = Input('post.');
            if(empty($id)) exception('找不到该用户!');
            if(empty($data['aid'])||!is_numeric($data['aid'])) exception('活动记录不能为空!');
            if(empty($data['uid'])||!is_numeric($data['uid'])) exception('分享用户不能为空!');
            if($data['uid'] == $id) exception('分享用户和浏览用户是同一人!');
            $info = $this->where(['aid'=>$data['aid'],'upid'=>$data['uid'],'uid'=>$id])->find();
            if(!empty($info)) exception('已存在记录!');
            $ret = [
                'aid'=>$data['aid'],
                'upid'=>$data['uid'],
                'uid'=>$id,
                'add_time'=>time(),
            ];
            $log = $this->insertGetId($ret);
            return true;
        }catch (\Exception $e){
            if(stristr($e->getMessage(),'SQLSTATE')){
                $this->error = '数据走丢了，请稍后再试！';
            }else $this->error = $e->getMessage();
            return false;
        }
    }
}