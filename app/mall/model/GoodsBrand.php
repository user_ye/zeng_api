<?php
declare (strict_types = 1);
namespace app\mall\model;
use think\Model;

Class GoodsBrand extends Model
{
    public $field = 'id,name,cover,desc';
    //    预处理数据处理
    // public function getCoverAttr($value)
    // {
    //     //return env('app.host_domin').'://'.$_SERVER['HTTP_HOST'].$value;
    //     return env('app.host_domin').'://'.env('app.static_host').$value;
    // }
}