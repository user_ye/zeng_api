<?php
declare (strict_types = 1);
namespace app\mall\model;
use think\Model;

Class GoodsType extends Model
{
    public $field = 'id,name,cover';
    //    预处理数据处理
    public function getCoverAttr($value)
    {
        //return env('app.host_domin').'://'.$_SERVER['HTTP_HOST'].$value;
        return env('app.host_domin').'://'.env('app.static_host').$value;
    }


    // 获取具体某类信息
    public function getList($pid = 0)
    {
        /*
        $oldlist = $this->field('id,pid,sort,name')->order('sort desc')->select()->toArray();
        $allData = [];
        foreach($oldlist as $item){
            $item['son'] = [];
            $allData[$item['id']] = $item;
        }
        $list = genTreeData($allData);
        return $list;
        */
        return $this->where(['pid'=>$pid,'is_delete'=>0])->order('sort desc')->field($this->field)->select()->toArray();
    }
}