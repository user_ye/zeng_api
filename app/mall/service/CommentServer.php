<?php


namespace app\mall\service;


use app\mall\model\GoodsComment;

class CommentServer
{
    // 删除重复的评论
    public function deleteComment($id){
        $return_list = ['total' =>0,'delete_limit'=>0,'str'=>'','delete_ids'=>[]];
        if(!$id) return $return_list;
        $list =  GoodsComment::where(['goods_id'=>$id,'cover'=>null])->select();
        $return_list['total'] = count($list);
        if($return_list['total'] <= 0) return $return_list;
        $objKeys = [];
        for($i = 0;$i < count($list);$i++) $objKeys[$list[$i]['discuss']] = $list[$i]['id'];
        $delete_objKeys = [];
        foreach($list as $item){
            if(isset($objKeys[$item['discuss']])){
                $delete_objKeys[$item['discuss']] = (isset($delete_objKeys[$item['discuss']]) ? $delete_objKeys[$item['discuss']] : 0) + 1;
                if($delete_objKeys[$item['discuss']] == 2){
                    // 删除操作
                    $return_list['str'] .= '删除：'.$item['id']."\t";
                    GoodsComment::where('id',$item['id'])->delete() ? $return_list['delete_limit'] += 1 :$return_list['delete_ids'][] = $item['id'];
                }
            }
        }
        return $return_list;
    }
}