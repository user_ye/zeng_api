<?php
declare (strict_types = 1);

namespace app\mall\controller;


use app\common\controller\BaseController;
use app\mall\model\Activity as ActivityModel;


class Activity extends BaseController
{
    public function __construct()
    {
        parent::__construct(false);
    }


    /**
     *  活动列表
     */
    public function list()
    {
        $model = new ActivityModel();
        $model->getList(Request()->param());
        return retu_json(200,'拼团活动列表ok',$model->list);
    }

    /**
     * 拿取单一数据 ， 拼团活动
     */
    public function get()
    {
        $model = new ActivityModel();
        $list = $model->getOne(Request()->param('id',-1));
        return retu_json(200,'拼团详情ok',$list);
    }

    public function getRule(){
        $model = new ActivityModel();
        $list = $model->getOneRule(Request()->param('id',-1));
        return retu_json(200,'规则ok',$list);
    }
}
