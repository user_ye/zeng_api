<?php
declare (strict_types = 1);

namespace app\mall\controller;


use app\common\controller\BaseController;
use app\mall\model\Goods;
use app\mall\model\GoodsType;
use app\myself\WeixinPay;
use app\myself\WeixinNotify;
use app\order\model\Order;
use app\user\model\UserLevel;
use think\facade\Log;
use think\Request;


/**
 * Class Pay
 * @package app\mall\controller
 */
class Pay extends BaseController
{
    public function __construct()
    {
        parent::__construct(false);
    }

    /**
     * 调用支付统一下单
     */
    public function index()
    {
        echo 3333333;
        echo '<xml><return_code><![CDATA[FAIL]]></return_code><return_msg><![CDATA[签名失败]]></return_msg></xml>';;
        echo 6666;
        exit;
        return $str = '<xml><return_code><![CDATA[FAIL]]></return_code><return_msg><![CDATA[签名失败]]></return_msg></xml>';
//        $get = Request()->get();
//        if(!isset($get['amount']) || $get['amount'] <= 0)
//            return retu_json(400,'支付金额错误');
//        $rs = [
//            'openid' => 'o68MO5KNfelTzumDjLR-OAaCfi5I',
//            'order_number' => time(),
//            'amount' => $get['amount']
//        ];
//        $a = new WeixinPay($rs['openid'],$rs['order_number'],'订单支付',$rs['amount']);
//        $pay = $a->pay(); //下单获取返回值
//        return retu_json(200,'统一下单ok',$pay);
    }

    /**
     * @return mixed 回调成功的函数
     */
    public function notify()
    {
		fileLog('回调处理开始','微信回调开始.log');
        $wx = new WeixinNotify();
        $data = $wx->wx_notify();
		fileLog('参数如下：'.date('y-m-d H:i:s')."\r\n".json_encode($data),'微信回调开始.log');
        if(!$data)
            return retu_json(400,'.....');
        $data['ip'] = $_SERVER['REMOTE_ADDR'];
//        Log::write($data,'info');
        // 如果回调ok 输出字符串  微信不会发起回调
//        if($data && $data['wechat_str'])
//            echo  $data['wechat_str'];die;

        // 微信回调成功 且 支付成功ok  进行微信回调订单逻辑处理
        if($data && isset($data['bool']) && $data['bool']){
            // 验证 订单逻辑开始
            $model = new Order();
            $list = $model->payModelNotify($data);
            $list['pay_date'] = date("Y-m-d H:i:s");
			fileLog('处理完毕----'.json_encode($list),'微信回调开始.log');
            Log::write($list,'wecaht_pay');
        }

        // 如果回调ok 输出字符串  微信不会发起回调
        if($data && $data['wechat_str']){
header('Content-type: text/xml');
ob_clean();
echo <<<XML
<xml>
  <return_code><![CDATA[SUCCESS]]></return_code>
  <return_msg><![CDATA[OK]]></return_msg>
</xml>
XML
;
exit;
        }

    }

    public function requestPay()
    {
        if(!Request::instance()->isPost()){
            return $this -> re(0,'请用POST获取数据');
        }
        $id = input('id');
        $rs = Db::name('表名') -> field('id,openid,order_amount,order_payment,order_number') -> where(['id' => $id,'order_payment' => 1]) -> find();
        if(empty($rs)){
            return $this -> re(0,'未找到此订单');
        }
        // halt($rs);
        $a = new WeixinPay($rs['openid'],$rs['order_number'],'订单支付',$rs['order_amount']);
        $pay = $a -> pay(); //下单获取返回值
        // halt($pay);
        return $this -> re(1,'下单成功',$pay);
    }
}
