<?php
declare (strict_types = 1);

namespace app\mall\controller;


use app\common\controller\BaseController;
use app\common\model\Config;
use app\live\model\LiveBroadcastGoods;
use app\mall\model\ActivityLog;
use app\mall\model\Goods;
use app\mall\model\GoodsAttr;
use app\mall\model\GoodsBrand;
use app\mall\model\GoodsType;
use app\myself\Bc;
use app\user\model\UserLevel;
use app\user\model\UserWallet;
use app\video\model\Video;
use think\Request;

use app\order\model\Order;
use app\order\model\OrderInfo;
use app\user\model\User;

class Index extends BaseController
{
    public $no_login_actios = ['malltype','malls','getmalls','getactivitylogid','getbrand'];
    public function __construct()
    {
        parent::__construct(false);
        $this->checkLogin($this->no_login_actios);
    }

    public function index()
    {

        // 指令输出
        $oid = 300;

        $orderModel = new Order();
        $userModel = new User();
        $orderInfoModel = new OrderInfo();

        // 拿取模型数据
        $orderOrnModel = $orderModel->find($oid);
        $orderInfoIdModel = $orderInfoModel->where('oid',$orderOrnModel->id)->find();
        $userModel = $userModel->find($orderOrnModel->uid);

        

        // $output->writeln('---模型调用开始-------------------');

        // 调用统一处理逻辑  需要走下面的处理
        $list = $orderModel->completeOrder([
            'orderOrnModel' => $orderOrnModel,
            'orderInfoModel' => $orderInfoIdModel,
            'userModel' => $userModel
        ]);
        var_dump($list);

        exit;
        return '您好！返回'.bcdiv((string)400,(string)500,2);
    }

    // 商品分类
    public function mallType(Request $request)
    {
        $model = new GoodsType();
        $type = $request->param('type',0);
        return retu_json(200,'111商城分类ok',$model->getList($type));
    }

//  商品归类列表信息 商品搜索走此处
    public function malls(Request $request){
        $model = new Goods();
        $model->getList(Request()->param());
        // 验证用户是否有登录
        if($this->getToken()){
            // 调用 用户模型中的操作获取到单条记录
            $data = $this->user->getShareLevelData($model->list['data']);
            if($data)
                $model->list['data'] = $data;
        }
        return retu_json(200,'商品ok',$model->list);
    }

    // 商品详情 某一条记录，此处需要判断当前商品是否为拼团 活动等
    public function getMalls(Request $request)
    {
        $model = new Goods();
        $this->getToken();
        $model->user = $this->user;
        $list = $model->getOne(Request());
        // 如果有登录 且 不是拼团活动商品 不是 视频点击的
        if($this->getToken() && $list['bool']){
            // 声明bc函数
            $bc = new Bc();
            // 如果是层级为1  且 购买了 购物券的
            if($this->user && $this->user->level == 1 && $this->user->is_proceeds == 1) {
                $volumeConfig = (new Config())->getSrTypes('volumeVip');
                // 需要大于0
                if($volumeConfig->share && $volumeConfig->share > 0){
                    // 该用户的 比例
                    $list['commodity']['user_share'] = $volumeConfig->share;
                    // 默认折扣价
                    $list['commodity']['default_discount_price_share'] = $bc->calcDiv($volumeConfig->share /10  * $list['commodity']['old_default_discount_price'],100,2);
                }
            // 如果有身份  且 该商品有 开启分销  分享和团购省   
            } else if($this->user && $this->user->getLevelModel && $this->user->getLevelModel->share > 0){
                // 该用户的 比例
                $list['commodity']['user_share'] = $this->user->getLevelModel->share;
                // 默认折扣价
                $list['commodity']['default_discount_price_share'] = $bc->calcDiv($this->user->getLevelModel->share * $list['commodity']['old_default_discount_price'],100,2);
            }
        }
        return retu_json(200,'商品详情ok',$list);
    }

    /**
     * 支付详情-获取到用户购物券  用户等级
     */
    public function payData(Request $request)
    {
        $model = new Goods();
        $list = $model->getPayData($this->user);
        return retu_json(200,$list['msg'],$list['data']);
    }

    /**
     * 发起支付
     */
    public function pay(){
        $model = new Goods();
        $list = $model->infoPay($this->user);
        return retu_json(200,$list['msg'],$list['data']);
    }

    /**
     * 获取到 拼团列表中的log - id
     */
    public function getActivityLogId()
    {
        $request = Request();
        $activity_id = $request->param('activity_id',-1);
        // 没有活动id
        if(!$activity_id || $activity_id < 0)
            return retu_json(400,'活动id不存在');
        $video_id = $request->param('video_id',-1);
        // 视频id不存在
        if(!$video_id || $video_id < 0)
            return retu_json(400,'视频id不存在');
        if(!$videoModel = Video::find($video_id))
            return retu_json(400,'视频没有该记录');
        if(!$ActivityLogModel =  ActivityLog::where(['aid' => $activity_id,'uid' => $videoModel->uid])->find())
            return retu_json(400,'没有该拼团记录');
        if($ActivityLogModel->end_time - time() <= 0)
            return retu_json(400,'该拼团活动时间已结束'.($ActivityLogModel->end_time - time()));
        if($ActivityLogModel->astatus != 0)
            return retu_json(400,'该拼团已结束');
        return retu_json(200,'拿取视频关联的活动-拼团列表id-ok',['activity_log_id'=>$ActivityLogModel->id]);
    }

    /**
     * 获取到品牌列表
     */
    public function getBrand()
    {
        $this->ruleValidate(['id|id'=>'require|>:0']);
        $id = $this->getSetDefaultParams('id');
        $model =  new GoodsBrand();
        $list = $model->where('is_delete',0)->field('id,name,cover,desc,sort')->find($id);
        return retu_json(200,'品牌描述ok',$list);
    }

    /**
     * 计算价格
     */
    public function sumTotal()
    {
        if(!$this->user) return retu_json(402,'麻烦重新登录');
        $get = Request::param();
        // 数量不得为空
        if(!isset($get['num']) || $get['num'] <= 0) return retu_json(400, '数量不得为空');
        // 商品序号
        if(!isset($get['id']) || $get['id'] <= 0) return retu_json(400, '商品序号不得为空');
        if(!$one = Goods::find($get['id']))return retu_json(400, '没有该商品记录');
        // 商品规格序号
        if(!isset($get['attr_id']) || $get['attr_id'] <= 0) return retu_json(400, '商品规格序号不得为空');
        if(!$oneAttr = GoodsAttr::find($get['attr_id']))return retu_json(400, '没有该商品规格记录');

        $bc = new Bc();

        // 商品价格
        $price = $oneAttr->price;

        //是否在直播中跳转过来的
        $liveBool = isset($get['lives']) ? $get['lives'] == 'lives' : false;
        if($liveBool){
            $liveList = (new LiveBroadcastGoods())->getReGoods($get['id']);
            // 如果为true 赋值直播价
            if($liveList) $price = $oneAttr->live_price;
        }

        // 购物券可以抵扣金额
        $wallet = 0;
        // 购物券余额
        $wallet_money = 0;
        // 是否使用购物券 且 该商品符合购物券
        if(isset($get['wallet']) && $get['wallet'] == 1 && $one->is_use == 1 && $one->ticket > 0){
            if($walletModel = UserWallet::where('uid',$this->user->id)->find()){
                if($walletModel->volume_balance > 0){
                    $wallet_money = $bc->calcDiv($walletModel->volume_balance,100,2);
                    // 购物券可以使用金额
                    $wallet = $bc->calcMul($bc->calcMul($price, $bc->calcDiv($one->ticket,100,2)),$get['num']);
                    // 如果大于 购物券余额
                    if($wallet > $wallet_money) $wallet = $wallet_money;
                }
            }
        }

        // 自省购比例
        
    }

}
