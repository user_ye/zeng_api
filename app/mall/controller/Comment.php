<?php
declare (strict_types = 1);

namespace app\mall\controller;


use app\common\controller\BaseController;
use app\mall\model\Goods;
use app\mall\model\GoodsComment;
use think\Request;

class Comment extends BaseController
{
    public $no_login_actios = ['list'];
    public function __construct()
    {
        parent::__construct(false);
        $this->checkLogin($this->no_login_actios);
    }

    public function list()
    {
        $model = new GoodsComment();
        $model->getList(Request()->param());
        return retu_json(200,'商品评论列表ok',$model->list);
    }

    /**
     * 添加商品评论
     */
    public function add()
    {
        $this->getToken(true);
        $model = new GoodsComment();
        $newId = $model->add(Request(),$this->user);
        return retu_json(200,'商品评论ok',['id'=>$newId]);
    }
}
