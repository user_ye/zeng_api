<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\facade\Route;

// 商城-分类
Route::rule('mallType','mallType');
// 商城-某一类
Route::rule('malls','malls');

// 商品获取详情操作
Route::rule('getMalls','getMalls');


// 轮播图等
//Route::get('banner','banner');

// 视频分类 一级
//Route::get('videoType','videoType');

// 视频拿取同一类
//Route::rule('videos','videos');
#Route::get('videos/:type','videos');

//Route::group('index', function () {
////    用户登录
//    Route::any('login/:code', 'login');
//})->prefix('Index/')->pattern(['id' => '\d+']);