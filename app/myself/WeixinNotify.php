<?php
namespace app\myself;

/*
 * 小程序微信支付
 */


use app\order\model\PayLog;

class WeixinNotify {


    /*protected $appid;
    protected $mch_id;
    protected $key;
    protected $openid;
    protected $out_trade_no;
    protected $body;
    protected $total_fee; */
    function __construct() {
        // 支付秘钥
        $this->key = config('wechat.key'); //这个是你商户号的api秘钥，在产品中心里边找，
    }

    // 微信支付回调 函数
    public function wx_notify(){
        $xml = file_get_contents("php://input");
        if(!$xml)
            return false;
        //xml数据转数组
        $data = json_decode(json_encode(simplexml_load_string($xml,'SimpleXMLElement',LIBXML_NOCDATA)),true);
        if(!$data)
            return false;
        if(!isset($data['sign']))
            return false;
        //保存微信服务器返回的签名sign
        $data_sign = $data['sign'];

        //sign不参与签名算法
        unset($data['sign']);
        $sign = $this->makeSign($data);



        //判断签名是否正确,判断支付状态
        if (($sign===$data_sign) && ($data['return_code'] == 'SUCCESS') && ($data['result_code'] == 'SUCCESS')) {
            $results = true;
            //获取服务器返回的数据
//            $order_sn = $data['out_trade_no'];	//订单号
//            $order_id = $data['attach'];		//附加参数,选择传递订单ID
//            $openid = $data['openid'];			//付款人openID
//            $total_fee = $data['total_fee'];	//付款金额
            //更新状态
//            $this->updatePsDB($order_sn,$order_id,$openid,$total_fee);
        } else {
            $results = false;
        }
        //返回状态给微信服务器
        if ($results) {
            $str = '<xml><return_code><![CDATA[SUCCESS]]></return_code><return_msg><![CDATA[OK]]></return_msg></xml>';
        } else {
            $str = '<xml><return_code><![CDATA[FAIL]]></return_code><return_msg><![CDATA[签名失败]]></return_msg></xml>';
        }

//        echo $str;
        return [
            'bool' => $results,
            'data' => $data,
            'wechat_str' => $str
        ];
    }


    /*生成签名*/
    protected function makeSign($data){
        //去空
        $data = array_filter($data);
        //字典排序
        ksort($data);
        $string_a = http_build_query($data);
        $string_a = urldecode($string_a);
        $string_sign_temp = $string_a."&key=".$this->key;
        $sign = md5($string_sign_temp);
        $result = strtoupper($sign);
        return $result;
    }
}