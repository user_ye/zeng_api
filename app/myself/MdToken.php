<?php
/**
 * Created by PhpStorm.
 * User: snuz
 * Date: 2020/7/7
 * Time: 9:35
 */

namespace app\myself;


class MdToken
{
    // 加密传输信息
    public $config = [
        'name' => 'miaomeimei',
        'title' => 'xiaochengxu',
        'key' => 'jdksajk12345#12377++',
    ];

    /**
     * 生成key
     */
    public function getKey()
    {
        $strKey = 'name='.$this->config['name'].'&title='.$this->config['title'];
        $time = floor(time()/100);
        $timeKey = $this->config['key'].$time;
        $key = md5(md5($strKey).($timeKey));
        return $key;
    }

    /**
     * 验证token
     */
    public function verifyToken($token){
        $sign = $this->getKey();
        return $sign == $token;
    }
}