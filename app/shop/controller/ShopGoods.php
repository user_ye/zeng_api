<?php
declare (strict_types = 1);

namespace app\shop\controller;
use app\common\controller\BaseController;
use app\live\model\LiveBroadcastGoods;
use app\mall\model\Goods;
use app\shop\model\ShopCoupon;
use app\shop\model\ShopUsers;
use app\shop\model\UserCoupon;
use think\Request;
class ShopGoods extends BaseController
{
    public function __construct()
    {
        parent::__construct(false);
    }

    public function index()
    {
        return '您好！这是一个[video]示例应用';
    }

    /**
     * 店铺商品 - 列表
     */
    public function goodsList(){
        // 拿取店铺信息
        $shopModel = new  ShopUsers();
        $shopList = $shopModel->regShop($this->user->id);
        if(!$shopList['flag']) return retu_json(400,$shopList['msg']);
        $shopUser = $shopList['data'];
        $model = new Goods();
        $param = Request()->param();
        $type = isset($param['type']) ? $param['type'] : 0;
        $status = isset($param['status']) ? $param['status'] : 1;
        $limit = isset($param['limit']) ? $param['limit'] : 10;
        $page = isset($param['page']) ? $param['page'] : 1;
        if($page < 0 ) $page  = 0;
        if($limit > 20)$limit = 20;
        $where = [];
        $where[] = ['g.shop_id','=',$shopUser->id];
        $where[] = ['g.is_delete','=',0];
        // 分类
        if($type && $type >= 1)
            $where[] = ['g.cid','=',$type - 0];
        // 是否上架
        if($status && is_numeric($status))$where[] = ['g.status','=',$status - 0];
        // 搜索 name
        if(isset($param['name']) && $param['name'])
            $where[] = ['g.name','like','%'.trim($param['name']).'%'];
        $oredr = 'g.sort desc,g.add_time desc';
        $field = 'g.id,g.name,g.share,g.status,g.stock,g.sale,g.default_discount_price,g.min_money,g.default_live_price';
        $list = $model->alias('g')
                ->where($where)->field($field)
                ->order($oredr)
                ->paginate($limit,false,array('query'=>$param));
        $list = $list->toArray();
        foreach($list['data'] as $k => $v){
            $list['data'][$k]['live_status'] = 2;  // 默认为2 表示 没有该直播商品 则不能进行 申请直播和取消直播
            // 商品状态为 上架
            if($list['data'][$k]['status'] == 1 && $liveGoods = LiveBroadcastGoods::where([
                'goods_id'=>$v['id'],
                'is_delete' => 0
            ])->field('live_status')->find())$list['data'][$k]['live_status'] = $liveGoods->live_status;
            if($list['data'][$k]['status'] == 2) $list['data'][$k]['live_status'] = 2;
            if($list['data'][$k]['min_money'] > 0 && $list['data'][$k]['default_discount_price'] > $list['data'][$k]['min_money']){
                $list['data'][$k]['default_discount_price'] = $list['data'][$k]['min_money'];
            }
        }
        return retu_json(200,'店铺商品列表',$list);
    }

    /**
     * 商品 - 上架下架
     */
    public function editStatus(){
        $param = Request()->param();
        $id =  isset($param['id']) ? $param['id'] : 0;
        $status =  isset($param['status']) ? $param['status'] : 0;
        if($status == 0)return  retu_json(400,'状态为空');
        if(!in_array($status,[1,2]))return  retu_json(400,'状态错误');
        if(!$id || $id <= 0)return retu_json(400,'商品序号不得为空');
       // 拿取店铺信息
       $shopModel = new  ShopUsers();
       $shopList = $shopModel->regShop($this->user->id);
       if(!$shopList['flag']) return retu_json(400,$shopList['msg']);
       $shopUser = $shopList['data'];
       // 商品列表
       $model = new Goods();
       $where = [
        'id' => $param['id'],
        'shop_id' => $shopUser->id
       ];
       $status_str = [
           1 => '上架',
           2 => '下架'
       ];
       if(!$one = $model->where($where)->field('status')->find())return retu_json(400,'没有该商品记录');
       if($one->status == $status)return retu_json(400,'当前商品已经是'.$status_str[$status]);
       $flag = $model->where($where)->update(['status' => $status]);
       if(!$flag) return retu_json(400,'网络异常，麻烦重新尝试');
       return retu_json(200,'商品'.$status_str[$status].'成功');
    }

    /**
     * 加直播  取消直播
     */
    public function editLives(){
        $param = Request()->param();
        $id =  isset($param['id']) ? $param['id'] : 0;
        $live_status =  isset($param['live_status']) ? $param['live_status'] : -1;
        if(!in_array($live_status,[0,1]))return  retu_json(400,'状态错误');
        if(!$id || $id <= 0)return retu_json(400,'商品序号不得为空');
       // 拿取店铺信息
       $shopModel = new  ShopUsers();
       $shopList = $shopModel->regShop($this->user->id);
       if(!$shopList['flag']) return retu_json(400,$shopList['msg']);
       $shopUser = $shopList['data'];
       // 直播商品状态拿取
       $one = LiveBroadcastGoods::where([
           'goods_id' => $id,
           'wx_status' => 2,
           'is_delete' => 0
       ])->find();
       if(!$one) return retu_json(400, '直播商品记录为空');
       $live_status_str = [0 => '取消直播',1 => '加直播'];
       // live_status 0 取消直播，  1  店铺商品允许其他主播播放
       if($one->live_status == $live_status) return retu_json(400,'当前商品状态已经是：'.$live_status_str[$live_status]);
       LiveBroadcastGoods::where('id',$one->id)->update([
           'live_status' => $live_status
       ]);
       return retu_json(200,'状态更改为：'.$live_status_str[$live_status]);
    }

    /**
     * 商品删除
     */
    public function goodsDel(){
        $param = Request()->param();
        $id =  isset($param['id']) ? $param['id'] : 0;
        if(!$id || $id <= 0)return retu_json(400,'商品序号不得为空');
       // 拿取店铺信息
       $shopModel = new  ShopUsers();
       $shopList = $shopModel->regShop($this->user->id);
       if(!$shopList['flag']) return retu_json(400,$shopList['msg']);
       $shopUser = $shopList['data'];
      // 商品列表
      $model = new Goods();
      $where = [
       'id' => $param['id'],
       'shop_id' => $shopUser->id
      ];
      $model::where($where)->update([
          'is_delete' => 1
      ]);
      return retu_json(200,'商品删除成功');
    }
}
