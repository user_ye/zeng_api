<?php
declare (strict_types = 1);

namespace app\shop\controller;
use app\common\controller\BaseController;
use app\service\model\Service as ModelService;

class Service extends BaseController{
    // 售后列表
    public function list(){
        // 判断用户已经审核
        if($this->checkeUser() == false)return retu_json(401, '游客无此权限，请先登录授权！', []);
        // 店铺售后列表
        $model = new ModelService();
        $list = $model->shopList($this->user,Request()->param());
        return retu_json(200,'售后列表',$list);
    }

    // 售后详情
    public function getInfo(){
        // 判断用户已经审核
        if($this->checkeUser() == false)return retu_json(401, '游客无此权限，请先登录授权！', []);
        // 店铺售后列表-详情
        $model = new ModelService();
        $list = $model->getShopOne($this->user,Request()->param());
        return retu_json(200,'售后详情',$list);
    }

    // 同意审核
    public function yesReview(){
        // 判断用户已经审核
        if($this->checkeUser() == false)return retu_json(401, '游客无此权限，请先登录授权！', []);
        // 店铺售后列表-详情
        $model = new ModelService();
        $list = $model->toYesReview($this->user,Request()->param());
    }

    // 拒绝审核
    public function noReview(){
        // 判断用户已经审核
        if($this->checkeUser() == false)return retu_json(401, '游客无此权限，请先登录授权！', []);
        // 店铺售后列表-详情
        $model = new ModelService();
        $list = $model->toNoReview($this->user,Request()->param());
    }

}
?>