<?php
declare (strict_types = 1);

namespace app\shop\controller;
use app\common\controller\BaseController;
use app\order\model\Order as Orders;
use think\Request;
class Order extends BaseController
{
    public function __construct()
    {
        parent::__construct(false);
    }

    public function index()
    {
        return '您好！这是一个[video]示例应用';
    }

    /*
     * 我的店铺订单列表
     */
    public function myOrderList()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new Orders();
        $data = $model->getShopOrders($this->user->id);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    /*
     * 获取订单详情
     */
    public function getOrderInfo()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new Orders();
        $data = $model->getShopOrdersInfo($this->user->id);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    /*
     * 订单发货
     */
    public function deliverGoods()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $this->ruleValidate([
            'id|订单id'=>'require|integer|>:0',
            'company_id|物流公司'=>'require|integer|>:0',
            'express_sn|快递单号'=>'require|min:6|max:40|alphaDash'
        ]);
        $model = new Orders();
        $data = $model->deliverGoods($this->user->id);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }
}
