<?php
declare (strict_types = 1);

namespace app\shop\controller;
use app\common\controller\BaseController;
use app\shop\model\ShopUsers;
use app\view\model\Ad;
use think\Request;
class Index extends BaseController
{
    public function __construct()
    {
        parent::__construct(false);
    }

    public function index()
    {
        return '您好！这是一个[video]示例应用';
    }

    /**
     * 申请店铺
     */
    public function createShop()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new ShopUsers();
        $data = $model->createShop($this->user);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    /**
     * 获取到店铺中心数据
     */
    public function info(){
        $model = new ShopUsers();
        $data = $model->infoShop($this->user);
        return retu_json(200,'拿取店铺中心数据Ok',$data);
    }

    /*
     * 判断店铺状态
     */
    public function checkShop()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new ShopUsers();
        $data = $model->checkShop($this->user->id);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    /*
     * 店铺经营类目列表
     */
    public function getShopNavList()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new Ad();
        $data = $model->getListByWhere();
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    /*
     * 判断店铺状态
     */
    public function getReapplyInfo()
    {
        if($this->checkeUser() == false)
            return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new ShopUsers();
        $data = $model->getReapplyInfo($this->user);
        if($data == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $data);
    }

    /**
     * 店铺营收数据
     */
    public function getRevenue(){
        if($this->checkeUser() == false)return retu_json(401, '游客无此权限，请先登录授权！', []);
        $model = new ShopUsers();
        $data = $model->getRevenueData($this->user,Request()->param());
        return retu_json(200,'拿取营收数据成功',$data);
    }
}
