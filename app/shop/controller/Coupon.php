<?php
declare (strict_types = 1);

namespace app\shop\controller;
use app\common\controller\BaseController;
use app\shop\model\ShopCoupon;
use app\shop\model\ShopUsers;
use app\shop\model\UserCoupon;
use think\Request;
class Coupon extends BaseController
{
    public function __construct()
    {
        parent::__construct(false);
    }

    public function index()
    {
        return '您好！这是一个[video]示例应用';
    }

    /**
     * 创建优惠券
     */
    public function createCoupon()
    {
        $this->ruleValidate([
            //'related_id|类型' => 'require|gt:1|number',
            'status|类型' => 'require|between:1,3|number',
            'amount|金额' => 'require|gt:0|number',
            'total|发放数量' => 'require|gt:0|number',
            'start_time|优惠券开始时间' => 'require',
            'end_time|优惠券结束时间' => 'require'
        ]);
        $model = new ShopCoupon();
        $model->createCoupon($this->user,Request()->param(),$this);
        return retu_json(200,'创建优惠券-ok',$model->list);
    }

    /**
     * 优惠券列表 -  发放记录
     */
    public function couponList(){
        $get = Request()->param();
        $model = new ShopCoupon();
        $model->getList($this->user,$get);
        return retu_json(200,'优惠券发放记录列表ok',$model->list);
    }
}
