<?php
declare (strict_types = 1);
namespace app\shop\model;
use app\api\model\IdentityCard;
use app\common\model\BaseModel;
use think\Model;

use think\Db;

Class ShopUserInfo extends BaseModel
{
    public $error;


    // 视频-获取列表操作
    public function getList($get)
    {
        $type = isset($get['type']) ? $get['type'] : 0;
        $oredr = 'sort desc';
        $where = [
            ['status','=',1], // 状态1
            ['is_check','=',1], // 审核通过
            ['is_delete','=',0] // 存在的
        ];
        $whereOr = [];
        $is_hot = isset($get['is_hot']) ? $get['is_hot'] : -1;
        // 是否为首页视频 推荐视频
        if($is_hot >= 0 && !isset($get['title']) && $is_hot == 1)
            $where[] = ['is_hot','=',$is_hot];

        // 分类
        if($type && $type >= 1)
            $where[] = ['type_id','=',$type];
        // 搜索 title ,视频名称 和 用户昵称
        if(isset($get['title']) && $get['title'] && trim($get['title'])){
            $whereOr = [
                [['title','like',trim($get['title']).'%']],
                [['user_nick_name','like',trim($get['title']).'%']]
            ];
        }

        $field= 'id,title,cover,video,likes,shares,comments,add_time,uid,is_hot,position';
        $this->queryList($get,$field,$where,$oredr,$whereOr);
        foreach($this->modelList as $k => $v){
            $this->list['data'][$k]['user_nickname'] = isset($v->oneUser->nickname) ? $v->oneUser->nickname : '风再起时';
            $this->list['data'][$k]['user_avatarurl'] = isset($v->oneUser->avatarurl) ? getHostDominUrl($v->oneUser->avatarurl) : 'https://miaommeis-1301812909.cos.ap-guangzhou.myqcloud.com/icon/%E7%BC%96%E7%BB%84%205%402x.png';
//            if($this->list['data'][$k]['is_hot'] != $is_hot)
//                unset($this->list['data'][$k]);
        }
        $isRefresh = isset($get['isRefresh']) ? $get['isRefresh'] : 0;
        if($isRefresh && $isRefresh == 1) shuffle($this->list['data']);
    }


    /*
     * 创建店铺
     */
    public function createShop($user){
        try{
            if(empty($user)) exception('找不到该用户!');
            $data = Input('post.');
            if(empty($data['name'])) exception('店铺名不能为空!');
            if(empty($data['real_name'])) exception('真实姓名不能为空!');
            if(empty($data['phone'])) exception('手机号不能为空!');
            elseif(!preg_match("/^1[3456789]\d{9}$/", $data['phone'])) exception('请填写正确的手机号!');
            if(empty($data['identity'])) exception('身份证不能为空!');
            elseif(!IdentityCard::isValid($data['identity'])) exception('请填写正确的身份证!');
            if(empty($data['logo'])) exception('店铺logo不能为空!');
            if(empty($data['card_img'])) exception('身份证不能为空!');
            if(empty($data['desc'])) exception('店铺描述不能为空!');
            $main = [
                'uid'=>$user->id,
                'name'=>emojiEncode($data['name']),
                'real_name'=>$data['real_name'],
                'phone'=>$data['phone'],
                'logo'=>$data['logo'],
                'add_time'=>time(),
            ];
            $info = [
                'identity'=>$data['identity'],
                'card_img'=>$data['card_img'],
                'desc'=>$data['desc'],
            ];
            if($last_id = $this->insertGetId($main)){
                $info['shop_id'] = $last_id;
            }
            return true;
        }catch (\Exception $e){
            if(stristr($e->getMessage(),'SQLSTATE')){
                $this->error = '数据走丢了，请稍后再试！';
            }else $this->error = $e->getMessage();
            return false;
        }
    }
}