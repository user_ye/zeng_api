<?php
declare (strict_types = 1);
namespace app\shop\model;
use app\api\model\IdentityCard;
use app\common\model\BaseModel;
use app\myself\Bc;
use think\Model;

use think\Db;

Class ShopOrderLog extends BaseModel
{

    /**
     * 订单回调完成支付推送到店铺数据记录
     */
    public function pushShopOrderData($orderOrnModel,$orderGoods=null){
        $bc = new Bc();
        // 计算规则如下： 购物券加上实际支付的金额
        $shop_money = $bc->calcAdd($orderOrnModel->total,$orderOrnModel->volume,2);
        $shopOrderDataAll = [];
        if($orderGoods && count($orderGoods) > 0){
            $shopOrderData = [
                'shop_id' => $orderOrnModel->shop_id,
                'oid' => $orderOrnModel->id,
                'uid' => $orderOrnModel->uid - 0,
                'order_goods_id' => 0,
                //'money' => $shop_money,
                'add_time' => time()
            ];
            // 迭代遍历
            foreach($orderGoods as $item){
                $shopOrderData['order_goods_id'] = $item['id'];
                // 计算规则如下： 购物券加上实际支付的金额
                $shopOrderData['money'] = $bc->calcAdd($item->total,$item->volume * 100,2) / 100;
                $shopOrderDataAll[] = $shopOrderData;  
            }
        }
        
        // 进行操作钱包表 , 预估写入
        $shopWalletModel =  ShopWallet::where('shop_id',$orderOrnModel->shop_id)->find();
        // 如果没有则创建新的数据
        if(!$shopWalletModel){
            ShopWallet::insert([
                'shop_id'=>$orderOrnModel->shop_id,
                'pre_money' => $shop_money - 0
            ]);
        }else{
            ShopWallet::where('shop_id',$orderOrnModel->shop_id)->inc('pre_money',$shop_money - 0)->update();
        }
        // 进行钱包操作表写入
        $flag = ShopWalletLog::insert([
            'shop_id' => $orderOrnModel->shop_id,
            'money' => $shop_money,
            'type' => 1,
            'uid' => $orderOrnModel->uid - 0,
            'status' => 1,
            'add_time' => time(),
            'operating_money' => $shopWalletModel ? $shopWalletModel->pre_money : 0,
            'oid' => $orderOrnModel->id
        ]);
        if($shopOrderDataAll && count($shopOrderDataAll) > 0)$flagLog = $this::insertAll($shopOrderDataAll);
        return true;
    }

     /**
     * 订单完结 处理逻辑
     */
    function orderFinish($order){
        # 店铺钱包模型
        if(!$walletOne = ShopWallet::where('shop_id',$order->shop_id)->find()) return false;
        # 店铺订单记录模型
        if(!$shopOrderOne = $this::where(['shop_id'=>$order->shop_id,'oid'=>$order->id,'status'=>0])->find())return false;
        # 改变状态店铺订单 记录 状态
        $this::where('id',$shopOrderOne->id)->update(['status' => 1]);
        # 更新 店铺钱包表
        ShopWallet::where('shop_id',$order->shop_id)->dec('pre_money',$shopOrderOne->money)->inc('money',$shopOrderOne->money)->update();
        $dataAll = [];
        $time = time();
        # 预估金额
        $dataAll[] = [
            'shop_id' => $order->shop_id,
            'money' => $shopOrderOne->money,
            'type' => 1,
            'status' => 0,
            'operating_money' => $walletOne->pre_money,
            'oid' => $order->id,
            'uid' => $order->uid,
            'action_type' => 1,
            'add_time' => $time
        ];
        # 余额
        $dataAll[] = [
            'shop_id' => $order->shop_id,
            'money' => $shopOrderOne->money,
            'type' => 2,
            'status' => 1,
            'operating_money' => $walletOne->money,
            'oid' => $order->id,
            'uid' => $order->uid,
            'action_type' => 1,
            'add_time' => $time
        ];
        return ShopWalletLog::insertAll($dataAll);
    }

}