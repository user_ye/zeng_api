<?php
declare (strict_types = 1);

namespace app\video\controller;
use app\video\model\Video;
use app\video\model\VideoComment;
use app\video\model\VideoCommentChild;
use think\Request;
class Comment
{
    public function index()
    {
        return '您好！这是一个[video]示例应用';
    }

    /**
     * 获取到一级评论
     */
    public function list(Request $request)
    {
        $get = $request->param();
        $model = new VideoComment();
        $model->getList($get);
        return retu_json(200,'一级评论ok',$model->list);
    }

    /**
     * 获取到二级评论列表
     */
    public function getChildList(Request $request)
    {
        $get = $request->param();
        $model = new VideoCommentChild();
        $model->getList($get);
        return retu_json(200,'二级评论ok',$model->list);
    }

    /**
     * 视频详情操作
     */
    // 商品详情 某一条记录，此处需要判断当前商品是否为拼团 活动等
    public function get(Request $request)
    {
//        $model = new Video();
//        $list = $model->getOne(Request()->get('id',-1));
//        return retu_json(200,'视频详情ok',$list);
    }
}
