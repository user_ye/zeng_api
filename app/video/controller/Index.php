<?php
declare (strict_types = 1);

namespace app\video\controller;
use app\common\controller\BaseController;
use app\common\model\BaseModel;
use app\user\model\UserFollow;
use app\user\model\UserVideoProfit;
use app\video\model\Video;
use think\Request;
class Index extends BaseController
{
    public function __construct()
    {
        parent::__construct(false);
    }

    public function index()
    {
        return '您好！这是一个[video]示例应用';
    }

    /**
     * 获取到视频列表
     */
    public function list(Request $request)
    {
        $get = $request->param();
        $model = new Video();
        $model->getList($get);
        return retu_json(200,'视频分类ok11',$model->list);
    }

    /**
     * 视频详情操作
     */
    // 视频详情  一条记录，此处需要判断当前商品是否为拼团 活动等
    public function get(Request $request)
    {
        $model = new Video();
        $this->getToken();
        $model->user = $this->user;
        // $list = $model->getOne($request);
        $list = $model->oneLimit($request);
        return retu_json(200,'单条记录',$list);
    }

    /**
     * 视频添加关注
     * @param Request $request
     */
//    public function

    public function newGet(Request $request)
    {
        $model = new Video();
        $list = $model->newGetOne($request);
        return retu_json(200,'视频详情ok',$list);
    }

    /**
     * @param Request $request 新的 视频详情 切换数据
     */
    public function newList(Request $request)
    {
        $model = new Video();
        $this->getToken();
        $model->user = $this->user;
        $list = $model->getNewList($request);
        return retu_json(200,'视频详情ok',$list);
    }


    /**
     * 视频详情 上一条
     */
    public function getSlide(Request $request)
    {
        $model = new Video();
        $this->getToken();
        $model->user = $this->user;
        $list = $model->getSlideOne($request);
        return retu_json(200,'视频评论',$list);
    }

    /**
     * 视频中  - 用户关注
     */
    public function addFollow()
    {
        $this->getToken(true);
        $fans_id = request()->param('fans_id',false);
        if(!$fans_id || $fans_id <= 0)
            return retu_json(400,'关注用户不存在');
        $model = new UserFollow();
        $list = $model->addFollowNew($this->user,$fans_id);
        return retu_json(200,'关注用户Ok',$list);
    }

    /**
     * 转发分享次数加1  并且 调用 个人的转发分享视频
     */
    public function shareVideo()
    {
        $this->getToken(true);
        $id = Request()->param('id',-1);
        if(!$id || $id < 0)
            return retu_json(400,'视频id不得为空');
        $videoModel = Video::find($id);
        if(!$videoModel)
            return retu_json(400,'视频没有改记录');
        // 视频更新
        Video::where('id',$videoModel->id)->update([
            'shares' => $videoModel->shares + 1
        ]);
        (new UserVideoProfit())->updateProfit([
           'uid' => $this->user->id,
            'type' => 3
        ]);
        return retu_json(200,'分享ok');
    }
}
