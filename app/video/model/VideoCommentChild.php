<?php
declare (strict_types = 1);
namespace app\video\model;
use app\common\model\BaseModel;
use app\mall\model\Activity;
use app\mall\model\Goods;
use app\user\model\UserVideoProfit;
use think\Model;

use app\user\model\User;

Class VideoCommentChild extends BaseModel
{


    // 视频-获取列表操作
    public function getList($get)
    {
        $fid =  isset($get['fid']) ? $get['fid'] : -1;
        $qc = isset($get['qc']) ? $get['qc'] : false;
        // 默认数据，需要测试
        $get['page'] = isset($get['page']) ? $get['page'] : 1;
        $get['limit'] = isset($get['limit']) ? $get['limit'] : 3;
        if($get['limit'] > 15)
            $get['limit'] = 15;

        if($fid < 0)
            retu_json(400,'没有一级评论');
        $where = [
            ['fid','=',$fid]
        ];
        $oredr = 'add_time desc';

        // 如果有去重
        if($qc && strlen($qc) >= 1){
            $qcArrs = explode(',',$qc);
            if($qcArrs > count($qcArrs))
                $where[] = ['id','not in',$qcArrs];
        }
        $this->queryList($get,$this->field,$where,$oredr);
        foreach($this->modelList as $k => $i)
            $allList[] = $this->getUserData($i,$this->list['data'][$k]);
        $this->list['data'] = $allList??[];
    }

    /**
     * 处理出uid,puid对应的昵称和头像
     */
    public function getUserData($one,$list)
    {

        // 评论用户的头像和昵称
        if($one->oneUser){
            $list['user_nickname'] = $one->oneUser->nickname;
            $list['user_avatarurl'] = $one->oneUser->avatarurl;
        }
        $list['content'] =  emojiDecode($one->content);
        // @ 人的用户和头像信息
        if($one->poneUser){
            $list['p_user_nickname'] = $one->poneUser->nickname;
            $list['p_user_avatarurl'] = $one->poneUser->avatarurl;
        }
        return $list;
    }

    /**
     * 一对一关联 回复的用户id
     */
    public function oneUser()
    {
//        $list = $this->find(1);
//        dump($list->user->nickname);
//        exit;
        // 父user  id  ，  当前字段的id
        return $this->hasOne(User::class,'id','uid');
    }

    /**
     * 一对一关联 user表 @人
     */
    public function poneUser()
    {
//        $list = $this->find(1);
//        dump($list->user->nickname);
//        exit;
        // 父user  id  ，  当前字段的id
        return $this->hasOne(User::class,'id','upid');
    }

    /**
     * 一对一关联 此处获取到商品id
     */
    public function getModelGoods()
    {
        // 关联商品表goods  商品表goods的id  ，  当前关联字段名称
        return $this->hasOne(Goods::class,'id','relation_id');
    }

    /**
     * 一对一关联 此处获取到活动id
     */
    public function getModelActivity()
    {
        // 关联活动表  活动表的id  ，  当前关联字段名称
        return $this->hasOne(Activity::class,'id','relation_id');
    }

    /*
     一对多关联
     $list->comments // 查询单条记录
    // 按照条件查询
    dump($list->comments()->where('id',1)->select()->toArray());
    //  一对多关联
    public function comments()
    {
        return $this->hasMany(User::class,'id','uid');
    }
    */

    /*
     * 添加二级评论 视频
     */
    public function addCommentChild($user){
        try{
            if(empty($user->id)) exception('找不到该用户!');
            $data = Input('post.');
            if(empty($data['id']))
                exception('请选择评论的视频');
            if(empty($data['uid']))
                exception('评论用户不能为空');
            if(empty($data['content']))
                exception('请输入回复内容');

            $ins = [
                'uid'=>$user->id,
                'upid'=>$data['uid'],
                'fid'=>$data['id'],
                'content'=>emojiEncode($data['content']),//转义表情
                'add_time'=>time()
            ];
            $model = new VideoComment();
            $video = $model->where(['id'=>$data['id']])->field('relation_id')->find();
            $this::startTrans();
            $vid = $this->insertGetId($ins);
            $fdata = $model->where(['id'=>$data['id']])->inc('child_count',1)->update(); #child_count一级评论表评论数+1
            if(!empty($video['relation_id']))
                $vdata = (new Video())->where(['id'=>$video['relation_id']])->inc('comments',1)->update(); #comments视频表评论数+1
            if($user->level > 1) $pdata = (new UserVideoProfit())->updateProfit(['uid'=>$user->id,'type'=>4]);#视频任务记录
            $this::commit();
            return ['fid'=>$vid,'uid'=>$user->id,'id'=>$vid,'add_time'=>$ins['add_time']];
        }catch (\Exception $e){
            $this::rollback();
            if(stristr($e->getMessage(),'SQLSTATE')){
                $this->error = '数据走丢了，请稍后再试！';
            }else $this->error = $e->getMessage();
            return false;
        }
    }
}