<?php
declare (strict_types = 1);
namespace app\video\model;
use think\Model;

Class VideoType extends Model
{
    // 获取一级分类视频
    public function getPid()
    {
        $list = $this->where(['pid'=>0,'is_delete'=>0])->field('id,name')->order('sort desc')->select()->toArray();
        array_unshift($list,[
            'id'=> 0,
            'name' => '热门精选'
        ]);
        return $list;
    }

    /*
     * 全部分类
     */
    public function getList()
    {
        $list = $this->where(['status'=>1,'is_delete'=>0])->field('id,pid,name')->order('pid asc')->select()->toArray();
        $data = [];
        if(!empty($list)){
            foreach($list as $k=>$v){
                if($v['pid'] == 0) $data[$k] = $v;
                else $data[$v['pid']]['list'][] = $v;
                if(isset($data[$k])&&!isset($data[$k]['list'])) $data[$k]['list'] = [];
            }
        }
        return $data;
    }
}