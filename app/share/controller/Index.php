<?php
declare (strict_types = 1);

namespace app\share\controller;

use app\common\controller\BaseController;
use app\share\model\UserShareOrder;
use think\Request;

class Index extends BaseController
{
    public function index()
    {
        return '您好！这是一个[share]示例应用';
    }

    public function list()
    {
        $model = new UserShareOrder();
        $model->getList(Request()->param(),$this);
        return retu_json(200,'分享订单ok',$model->list);
    }
}
