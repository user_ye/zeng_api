<?php
declare (strict_types = 1);

namespace app\share\model;

use app\common\model\BaseModel;
use app\order\model\Order;
use app\Request;
use app\user\model\User;
use think\Model;

/**
 * @mixin \think\Model
 */
class UserShareOrder extends BaseModel
{
    
    // 分享订单列表
    public function getList($get,$base)
    {
        $where  = [['puid','=',$base->user->id]];
        $order = 'add_time desc';
        $status = isset($get['status']) ? $get['status'] : 0;
        // list($status) = $base->getSetDefaultParams('status|0',$get);
        if($status > 0){
            // 不等于待评价
            if($status == 4 || $status == 5){
                $where[] = ['status','in',[4,5]];
            }else{
                $where[] = ['status','=',$status];
            }
        }    
        $this->queryList($get,'id,uid,add_time,status,miao',$where,$order);
        foreach($this->modelList as $k =>$item){
            $this->list['data'][$k]['miao'] = $this->list['data'][$k]['miao'] / 100;
            if($item->oneUser){
                $this->list['data'][$k]['user_nickname'] = $item->oneUser->nickname;
                $this->list['data'][$k]['user_avatarurl'] = $item->oneUser->avatarurl;
            }
        }    
    }

    /**
     * 模型一对一关联  购买人的id
     */
    public function oneUser()
    {
        return  $this::hasOne(User::class,'id','uid');
    }

    /**
     * 模型一对一关联  订单id
     */
    public function oneOrder()
    {
        return  $this::hasOne(Order::class,'id','oid');
    }
}
