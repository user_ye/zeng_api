<?php
declare (strict_types = 1);

namespace app\live\controller;

use app\user\model\UserInvite;
use think\facade\Db;
use think\facade\Request;
use app\api\model\WxApi;
use app\common\controller\BaseController;
use app\live\model\LiveBroadcast;
use app\live\model\LiveBroadcastGoods;
use app\live\model\LiveBroadcastType;

class Index extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        if(empty($this->user)&&!in_array(Request::action(true),['getlist','getnoticelist','index']))
            return retu_json(402, '请先登录！', []);
    }

    public function index()
    {
        $model = new UserInvite();
        $arr = Db::name('user')->field('id,invite_id')->where(['is_delete'=>0])->select()->toArray();
        $num = [];  //IF(LOCATE('.$id.',level)>0,CONCAT(\''.$level.',\','.Db::raw('level').'),\''.$level.'\')
        if(!empty($arr)){
            foreach($arr as $v){
//                $is_ok = $model->testUpdate($v['id'],$v['invite_id']);
                $level = $model->testGetLevel($v['id']);
                $num[] = $model->testUpdateLevel($v['id'],array_reverse($level),$v['invite_id']);
            }
        }
        return  retu_json(200, 'test', [count($arr)]);
//        $model = new WxApi();
//        $total = 0;
//        $room_info = [];
//        $data = $model->getLiveList(['start'=>0,'limit'=>100]);
        /*  if(!empty($data)){
              $total_wx = $data['total'];
              if(isset($data['room_info'])) {
                  while ($total < $total_wx) {
                      $total += count($data['room_info']);
                      $room_info = array_merge($room_info, $data['room_info']);
                      if ($total_wx - $total > 10) $num = 10;
                      else $num = $total_wx - $total;
                      $data = $model->getLiveList(['start' => $total, 'limit' => $num]);
                      if (empty($data['room_info'])) break;
                  }
                  return retu_json(200, 'test', [$data,$room_info,$total,$total_wx]);
              }
          }*/

        $userInviteModel = new UserInvite();
        $userInvite = $userInviteModel->loopTop(5278);
        return retu_json(200, 'test', [$userInvite]);
        return retu_json(200, 'test', [$data,$room_info,$total]);


        $model = new WxApi();
        $info = $model->getLiveList(['start'=>0,'limit'=>10,'uid'=>1080]);
        return retu_json(200, $model->error, $info);

    }

    /**
     * 直播列表
     */
    public function getList()
    {
        $model = new LiveBroadcast();
        $info = $model->getList();
        if($info == false && !empty($model->error))
            return retu_json(0, $model->error, []);
        return retu_json(200, '操作成功', $info);
    }

    /**
     * 直播列表
     */
    public function getNoticeList()
    {
        $model = new LiveBroadcast();
        $info = $model->getNoticeList();
        if($info == false && !empty($model->error))
            return retu_json(0, $model->error, []);
        return retu_json(200, '操作成功', $info);
    }

    /**
     * 直播列表
     */
    public function getMyList()
    {
        $model = new LiveBroadcast();
        $info = $model->getMyList($this->user->id);
        if($info == false && !empty($model->error))
            return retu_json(0, $model->error, []);
        return retu_json(200, '操作成功', $info);
    }

    /**
     * 上传直播间图片
     */
    public function uploadImg()
    {
        $model = new LiveBroadcast();
        $info = $model->uploadImg($this->user->id);
        if($info == false && !empty($model->error))
            return retu_json(400, $model->error, []);
        return retu_json(200, '操作成功', $info);
    }

    /**
     * 创建直播
     */
    public function createLive()
    {
        $model = new LiveBroadcast();
        $info = $model->createliveBroadcast($this->user);
        if($info == false && !empty($model->error)){
            if($model->error == 300036)
                return retu_json(403, '主播未验证，扫描二维码进行身份验证！', []);
            return retu_json(0, $model->error, []);
        }
        return retu_json(200, '操作成功', $info);
    }

    /**
     * 删除直播
     */
    public function delLive()
    {
        $model = new LiveBroadcast();
        $info = $model->delLive($this->user->id);
        if($info == false && !empty($model->error)){
            return retu_json(0, $model->error, []);
        }
        return retu_json(200, '操作成功', $info);
    }

    /**
     * 直播商品列表
     */
    public function getGoodsList()
    {
        $model = new LiveBroadcastGoods();
        $info = $model->getList($this->user->id??0);
        if($info == false && !empty($model->error))
            return retu_json(0, $model->error, []);
        return retu_json(200, '操作成功', $info);
    }

    /**
     * 选择商品
     */
    public function selectGoods()
    {
        $model = new LiveBroadcastGoods();
        $info = $model->selectGoodsById($this->user->id);
        if($info == false && !empty($model->error))
            return retu_json(0, $model->error, []);
        return retu_json(200, '操作成功', $info);
    }

    /**
     * 删除商品
     */
    public function delGoods()
    {
        $model = new LiveBroadcastGoods();
        $info = $model->delGoodsById($this->user->id);
        if($info == false && !empty($model->error))
            return retu_json(0, $model->error, []);
        return retu_json(200, '操作成功', $info);
    }

    /**
     * 直播分类列表
     */
    public function getTypeList()
    {
        $model = new LiveBroadcastType();
        $info = $model->getList();
        if($info == false && !empty($model->error))
            return retu_json(0, $model->error, []);
        return retu_json(200, '操作成功', $info);
    }

}
