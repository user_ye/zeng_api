<?php
declare (strict_types = 1);

namespace app\live\controller;

use app\live\model\LiveBroadcastInfo;
use think\facade\Request;
use app\api\model\WxApi;
use app\common\controller\BaseController;
use app\live\model\LiveBroadcast;
use app\live\model\LiveBroadcastGoods;
use app\live\model\LiveBroadcastType;

class Money extends BaseController
{
    public function __construct()
    {
        parent::__construct();
//        if(empty($this->user)&&!in_array(Request::action(true),['getlist','index']))
//            return retu_json(402, '请先登录！', []);
    }

    // 拿取预估，待结算，已结算喵币-佣金总数据
    public function getliveMiao(){
//        $this->user;
        if(!$this->user) return retu_json(402,'请先登录');
        $model  = new LiveBroadcastInfo();
        $list = $model->getMiaoData($this->user->id);
        return retu_json(200,'拿取佣金数据ok',$list);
    }

    // 直播列表-并且佣金返回
    public function getLiveListMiao()
    {
        if(!$this->user) return retu_json(402,'请先登录');
        $model = new LiveBroadcastInfo();
        $list = $model->getList(Request::param(),$this->user->id);
        return retu_json(200,'直播数据列表返回ok',$list);
    }
}
