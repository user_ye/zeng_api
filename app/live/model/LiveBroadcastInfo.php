<?php
declare (strict_types = 1);

namespace app\live\model;

use app\api\model\WxApi;
use think\Model;

/**
 * @mixin \think\Model
 */
class LiveBroadcastInfo extends Model
{
    // 拿取收益数据
    public function getMiaoData($id){
        $where = ['uid'=>$id];
        // 总数据 成交订单， 佣金总额， 总订单金额
        $total = [
            'order' => $this->where($where)->sum('order_num'),
            'miao' => $this->where($where)->sum('miao'),
            'money' => $this->where($where)->sum('money')
        ];

        // 今天数据
        $where['time_day'] = date('Ymd');
        $today_total = [
            'order' => $this->where($where)->sum('order_num'),
            'miao' => $this->where($where)->sum('miao'),
            'money' => $this->where($where)->sum('money')
        ];

        return [
            'total' => $total,
            'today' => $today_total,
            'pre_miao' => LiveMiaoLog::where(['uid' => $id,'status' => 1])->sum('reward'), // 预估
            'await_miao' => LiveMiaoLog::where(['uid' => $id,'status' => 2])->sum('reward'), // 待结算
            'credit_miao' => LiveMiaoLog::where(['uid' => $id,'status' => 3])->sum('reward'), // 累计喵币
            'fail_miao' => LiveMiaoLog::where(['uid' => $id,'status' => 4])->sum('reward') // 失效
        ];
    }

    // 拿取分页数据-并且联表查询
    public function getList($get,$uid = 0)
    {
        if(!isset($get['limit'])) $get['limit'] = 15;
        if($get['limit'] > 20) $get['limit'] = 20;
        $where = ['li.uid' => $uid];
        $field = 'li.id,li.order_num,li.miao,li.money,l.title,l.share,l.start_time';
        $list = $this->alias('li')
                    ->join('liveBroadcast l','l.id=li.live_id','left')
                    ->where($where)->order('l.start_time desc')->field($field)
                    ->paginate($get['limit'],false,array('query'=>$get));
        $list = $list->toArray();
        if(isset($list['data'])){
            foreach($list['data'] as $k=>$v){
                $list['data'][$k]['title'] = emojiDecode($v['title']);
                $list['data'][$k]['share'] = getApiDominUrl($v['share']);
                $list['data'][$k]['add_time'] = date('Y-m-d H:i:s', $v['start_time']);
            }
        }
        return $list;
    }

}
