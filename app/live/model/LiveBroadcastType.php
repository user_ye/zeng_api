<?php
declare (strict_types = 1);

namespace app\live\model;

use think\Model;

/**
 * @mixin \think\Model
 */
class LiveBroadcastType extends Model
{
    public $page = '';//分页数据
    public $count = '';//数据总数
    public $error = '';//报错


    /*
     * 获取直播分类列表
     */
    public function getList()
    {
        try{
            //查询条件
            $where = ['is_delete'=> 0, 'status'=> 1];
            $list = $this->where($where)->field('id,name')->order('sort desc')->select()->toArray();
            return $list;
        }catch (\Exception $e){
            if(stristr($e->getMessage(),'SQLSTATE')){
                $this->error = '数据走丢了，请稍后再试！';
            }else $this->error = $e->getMessage();
            return false;
        }
    }


    /*
     * 判断分类id是否存在
     */
    public function checkType($id)
    {
        try{
            //查询条件
            $list = $this->where(['id'=>$id,'is_delete'=> 0, 'status'=> 1])->field('id')->count();
            return !empty($list) ? true : false;
        }catch (\Exception $e){
            if(stristr($e->getMessage(),'SQLSTATE')){
                $this->error = '数据走丢了，请稍后再试！';
            }else $this->error = $e->getMessage();
            return false;
        }
    }
}
