<?php
declare (strict_types = 1);

namespace app\common\validate;

use think\Validate;

// 验证器基类  动态匹配验证器规则
class BaseValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */
    public  $rule =   [];
}