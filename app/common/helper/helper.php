<?php
/**
 * Created by.
 * @author zengye
 * @since 20221015 18:58
 */

namespace app\common\helper;

/**
 * 全局的 公共函数
 * Class index
 * @package app\common\helper
 * @author zengye
 * @since 20221015 18:58
 */
class helper
{
    /**
     * 根据传递的 url  返回到真实的路径
     * @param string $url 图片的资源 路径
     * @return string 真实可用的图片资源路径
     * @author zengye
     * @since 20221015 18:59
     */
    public static function getImgUrls(string $url = '') {
        if(strpos( $url, 'http') !== false) {
            return $url;
        }

        $cosConfig = config('cos');
        $staticConfig = config('static');

        // 如果没有cos
        if (!$cosConfig['open']) {
            return $staticConfig['static_host'] . $url;
        }
        return $url;
    }
}