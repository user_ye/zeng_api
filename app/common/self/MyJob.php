<?php
/**
 * Created by PhpStorm.
 * User: snuz
 * Date: 2020/7/24
 * Time: 11:34
 */
namespace app\common\self;

use think\facade\Queue;
class MyJob{

    /**
     * @param string $queueName 队列名称
     * @param array $data  队列数据
     * @param int $time 延迟执行时间
     * @return bool|mixed|\think\cache\driver\void|void 返回 布尔类型 为true 表示ok
     */
    public static function pushQueue($queueName = '',$data = [],$time = 0)
    {
        if(!$queueName) return false;
        $queueObj = config('queueData');
        if(!isset($queueObj[$queueName])) return false;
        return $time > 0 ? Queue::later($time,$queueObj[$queueName]['class'],$data,$queueObj[$queueName]['name']) : Queue::push($queueObj[$queueName]['class'],$data,$queueObj[$queueName]['name']);
    }
}