<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/3/003
 * Time: 13:26
 */
namespace app\common\job;

use app\video\model\Video;
use Exception;
use think\Db;
use think\queue\Job;
class LiveBroadcastGoodsJob
{
    public $desc = '删除直播商品缓存';

    /**
     * fire方法是消息队列默认调用的方法
     * @param Job            $job      当前的任务对象
     * @param array|mixed    $data     发布任务时自定义的数据
     */
    public function fire(Job $job,$data)
    {
        ini_set('memory_limit','512M');
        //send_telegram($msg);
        $this->_doJob($data);
        // 一定要删除  不然会重复跑
        $job->delete();
        return;
    }

    /**
     * 业务处理
     *  延时处理选择完未成功创建直播间的商品
     * @param $data
     * @return bool|int
     */
    private function _doJob($data){
        try{
            // 判断是否有视频id ['id'=> $data['goods_id'],'uid'=>$id,'key'=>$timeKey]
            if(!isset($data['id']) || $data['id'] < 0) return false;
            if(!isset($data['uid']) || $data['uid'] < 0) return false;
            if(!isset($data['key']) || empty($data['key'])) return false;
            $key = 'livebroadcastgoods_';
            $redis = getRedis();
            if(date('H') == 00) $day = date("Ymd", strtotime("1 days ago"));
            else $day = date('Ymd');
            $redis->hDel($key.$data['uid'],$data['key']);
            $redis->hDel($key.$day.'_'.$data['id'],$data['key']);
            return true;
        }catch (Exception $e){
            return false;
        }
    }

    // 处理失败
    public function failed($data){

    }
}