<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/3/003
 * Time: 13:26
 */
namespace app\common\job;

use app\video\model\Video;
use Exception;
use think\Db;
use think\facade\Log;
use think\queue\Job;
class VideoDelayJob
{
    public $desc = '删除未提交视频记录';

    /**
     * fire方法是消息队列默认调用的方法
     * @param Job            $job      当前的任务对象
     * @param array|mixed    $data     发布任务时自定义的数据
     */
    public function fire(Job $job,$data)
    {
        ini_set('memory_limit','512M');
        //send_telegram($msg);
        $this->_doJob($data);
        // 一定要删除  不然会重复跑
        $job->delete();
        return;
    }

    /**
     * 业务处理
     *
     * @param $data
     * @return bool|int
     */
    private function _doJob($data){
        try{
            // 判断是否有视频id
            if(!isset($data['id']) || $data['id'] < 0) return false;
            $video = Video::find($data['id']);
            $info = !empty($video) ? $video->getData() : [];
            if(empty($info)) return false; #视频记录已清除
            #如果视频记录存在，并且标题，类型，添加时间都为空则删除记录和文件
            if(empty($info['title'])&&empty($info['type_id'])&&empty($info['add_time'])&&$info['status']==0&&$info['is_check']==0){
                $return = [];
                if(!empty($info['video'])&&file_exists(app()->getRootPath() .'public'.$info['video'])){
                    $return['video'] = unlink(app()->getRootPath() .'public'.$info['video']);//删除文件
                }
                if(!empty($info['cover'])&&file_exists(app()->getRootPath() .'public'.$info['cover'])){
                    $return['cover'] = unlink(app()->getRootPath() .'public'.$info['cover']);//删除文件
                }
                $return['log'] = (new Video())->where('id',$info['id'])->delete();
                fileLog(date('Y-m-d H:i:s').'---'.$this->desc.'---视频ID：'.$data['id'].'---队列处理结果：'.json_encode(['ret'=>$return,'info'=>$info]),'VideoDelay.log');
            }
            return true;
        }catch (Exception $e){
            fileLog(date('Y-m-d H:i:s').'---'.$this->desc.'---视频ID：'.$data['id'].'---队列处理失败：'.$e->getMessage(),'VideoDelay.log');
            return false;
        }
    }

    // 处理失败
    public function failed($data){

    }
}