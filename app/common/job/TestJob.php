<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/3/003
 * Time: 13:26
 */
namespace app\common\job;
use think\Db;
use think\facade\Log;
use think\queue\Job;
class TestJob
{
    public $desc = '测试队列';

    /**
     * fire方法是消息队列默认调用的方法
     * @param Job            $job      当前的任务对象
     * @param array|mixed    $data     发布任务时自定义的数据
     */
    public function fire(Job $job,$data)
    {
        ini_set('memory_limit','512M');
        //send_telegram($msg);
        $this->_doJob($data);
        fileLog(date('Y-m-d H:i:s').'---测试队列开始中---','测试.log');
        // 一定要删除  不然会重复跑
        $job->delete();
        return;
    }

    /**
     * 业务处理
     *
     * @param $data
     * @return bool|int
     */
    private function _doJob($data){
        fileLog(date('Y-m-d H:i:s').'---数据如下---','测试.log');
        Log::write($data,'----开始'.date('Y-m-d H;i:s'));
    }

    // 处理失败
    public function failed($data){

    }
}