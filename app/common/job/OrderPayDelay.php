<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/3/003
 * Time: 13:26
 */
namespace app\common\job;

use app\mall\controller\Activity;
use app\mall\model\ActivityLog;
use app\mall\model\Goods;
use app\mall\model\GoodsAttr;
use app\order\model\Order;
use app\order\model\OrderGoods;
use Exception;
use think\Db;
use think\facade\Log;
use think\queue\Job;
class OrderPayDelay
{
    public $desc = '待付款订单支付中延时处理';

    /**
     * fire方法是消息队列默认调用的方法
     * @param Job            $job      当前的任务对象
     * @param array|mixed    $data     发布任务时自定义的数据
     */
    public function fire(Job $job,$data)
    {
        ini_set('memory_limit','512M');
        //send_telegram($msg);
        fileLog(date('Y-m-d H:i:s').'---'.$this->desc.'开始中---','OrderPayDelay.log');
        $this->_doJob($data);
        fileLog(date('Y-m-d H:i:s').'---'.$this->desc.'---结束了---','OrderPayDelay.log');
        // 一定要删除  不然会重复跑
        $job->delete();


        return;
    }

    /**
     * 业务处理
     *
     * @param $data
     * @return bool|int
     */
    private function _doJob($data){
        // 判断是否有订单id
        if(!isset($data['id']) || $data['id'] < 0) return false;

        $one = Order::find($data['id']);
        if(!$one) return false;
        // 如果状态还是等于 1  待付款 那么 则取消
        if($one->status == 1){
            fileLog(date('Y-m-d H:i:s').'---'.$this->desc.'---订单为：---'.$one->id.'队列开始取消状态','OrderPayDelay.log');
            // 开启事务
            $one::startTrans();
            try{
                Log::write('订单取消开始----',__FUNCTION__);
                // 判断类型是否为活动且 当前活动拼团有该id
                $whereDelete = [['oid','=',$one->id],['uid','=',$one->uid],['pay_status','=',0]];
                if($one['type'] == 2 && $activityLogModel = ActivityLog::where($whereDelete)->find()){
                    ActivityLog::where('id',$activityLogModel->id)->delete();
                    Log::write($activityLogModel,__FUNCTION__.'订单取消开始----删除记录');
                }
                # 拿取到当前订单的  订单商品
                $OrderGoodsModel = OrderGoods::where('order_id',$one->id)->find();
                // 获取到 订单对应的商品
                $goodsModel = Goods::find($OrderGoodsModel->goods_id);
                // 如果是拍下减库存 ，需要加回去
                if($goodsModel->is_stock == 1){
                    // 找到商品规格
                    $goodsAttrModel = GoodsAttr::find($OrderGoodsModel->attr_id);
                    if($goodsAttrModel){
                        GoodsAttr::where('id',$goodsAttrModel->id)->update([
                        'stock' =>  $goodsAttrModel->stock + $OrderGoodsModel->num
                        ]);
                    }
                    Goods::where('id',$goodsModel->id)->update([
                        'stock' =>  $goodsModel->stock + $OrderGoodsModel->num
                    ]);
                }
                $ok = Order::where('id',$one->id)->update([
                    'status' => 0
                ]);
                if(!$ok)
                    throw new Exception('数据错误');
                // 提交事务
                Log::write('订单取消开始----处理完毕',__FUNCTION__);
                $one::commit();
                fileLog(date('Y-m-d H:i:s').'---'.$this->desc.'---订单为：---'.$one->id.'队列开始处理ok状态','OrderPayDelay.log');
                return true;
            }catch (Exception $e){
                $one::rollback();
                fileLog(date('Y-m-d H:i:s').'---'.$this->desc.'---订单为：---'.$one->id.'队列开始处理失败'.$e->getMessage(),'OrderPayDelay.log');
                return false;
            }
        }
    }

    // 处理失败
    public function failed($data){

    }
}