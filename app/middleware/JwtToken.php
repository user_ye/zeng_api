<?php
declare (strict_types = 1);

namespace app\middleware;

use app\myself\Jwt;
use app\myself\MdToken;

class JwtToken
{
    /**
     * 处理请求
     *
     * @param \think\Request $request
     * @param \Closure       $next
     * @return Response
     */
    public function handle($request, \Closure $next)
    {
        // 如果全局配置没有开启
        if(!env('app.jwt')){
            return $next($request);
        }

        $token = $request->get('token','');
        if(!$token ){
            return retu_json(400,'请输入key');
        }
        $MdToken = new MdToken();
        if(!$MdToken->verifyToken($token)){
            $jwtstr = env('app.jwtstr');
            if(isset($jwtstr) && $jwtstr != $token){
                return retu_json(400,'请输入key_1');
            }
        }
        return $next($request);
    }
}
